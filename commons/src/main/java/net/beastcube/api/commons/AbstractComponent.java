package net.beastcube.api.commons;

import net.beastcube.api.commons.configuration.Configuration;
import net.beastcube.api.commons.configuration.ConfigurationSection;

import java.util.logging.Logger;

/**
 * Interface that represents component that can be enabled, disabled and have own {@link Logger}.
 */
public abstract class AbstractComponent implements Component {
    /**
     * Logger object of this componenet.
     */
    protected Logger logger = Providers.LOGGER;
    /**
     * Configuration section of this component.
     */
    protected ConfigurationSection config;

    /**
     * Called when internal logic of component (not buisness logic) should be initialized.
     *
     * @param parentConfiguration configuration of parent, who contains this component.
     */
    protected void _initConfig(final Configuration parentConfiguration) {
        this.config = parentConfiguration.getSection(this.getClass().getCanonicalName());
        if (this.config == null) {
            this.config = parentConfiguration.createSection(this.getClass().getCanonicalName());
        }
        logger.info(config.toString());
    }

    /**
     * Returns {@link ConfigurationSection} of this component.
     *
     * @return configuration section of this component.
     */
    public ConfigurationSection getConfiguration() {
        return this.config;
    }

    @Override
    public void onDisable() {
    }

    @Override
    public void onEnable() {
    }
}
