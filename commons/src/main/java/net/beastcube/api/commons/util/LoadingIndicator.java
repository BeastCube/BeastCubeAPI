package net.beastcube.api.commons.util;

/**
 * @author BeastCube
 */
public class LoadingIndicator {

    String[] list = new String[]{"Ooo", "oOo", "ooO", "oOo"};
    int current = 0;

    public String next() {
        current++;
        if (current >= list.length)
            current = 0;
        return list[current];
    }

}
