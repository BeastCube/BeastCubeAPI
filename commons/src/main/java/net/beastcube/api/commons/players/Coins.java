package net.beastcube.api.commons.players;

import net.beastcube.api.commons.database.PlayersDatabaseHandler;

import java.util.UUID;

/**
 * @author BeastCube
 */
public class Coins {

    public static int getCoins(UUID p) {
        return PlayersDatabaseHandler.getCoins(p);
    }

    public static void setCoins(UUID p, int coins) {
        PlayersDatabaseHandler.setCoins(p, coins);
    }

    public static void addCoins(UUID p, int coins) {
        PlayersDatabaseHandler.addCoins(p, coins);
    }

    public static void removeCoins(UUID p, int coins) {
        PlayersDatabaseHandler.removeCoins(p, coins);
    }

}
