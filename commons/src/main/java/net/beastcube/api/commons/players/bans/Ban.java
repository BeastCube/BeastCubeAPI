package net.beastcube.api.commons.players.bans;

import lombok.Data;
import net.beastcube.api.commons.text.ChatColor;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @author BeastCube
 */
@Data
public class Ban {

    private int id = -1;

    private UUID player;
    private UUID author;
    private String reason;
    private Timestamp createdAt;
    private Timestamp expiresAt;
    private boolean permanent;

    public Ban(UUID player, UUID author, String reason, Timestamp createdAt, Timestamp expiresAt, boolean permanent) {
        this.player = player;
        this.author = author;
        this.reason = reason;
        this.createdAt = createdAt;
        this.expiresAt = expiresAt;
        this.permanent = permanent;
    }

    public boolean isActive() {
        Timestamp currentTimestamp = new Timestamp(new Date().getTime());
        return isPermanent() || currentTimestamp.after(createdAt) && currentTimestamp.before(expiresAt);
    }

    public String getMessage() {
        return String.format(ChatColor.RED + "Du wurdest %s vom BeastCube Network gebannt!\n" + ChatColor.DARK_AQUA + "Grund:" + ChatColor.YELLOW + " %s",

                (permanent ? ChatColor.AQUA + "PERMANENT" + ChatColor.RED : "bis zum" + ChatColor.AQUA + new SimpleDateFormat("dd.MM.yyyy HH:mm").format(expiresAt) + ChatColor.RED),
                reason);
    }
}
