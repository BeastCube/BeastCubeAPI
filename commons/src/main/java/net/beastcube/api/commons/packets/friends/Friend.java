package net.beastcube.api.commons.packets.friends;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

/**
 * @author BeastCube
 */
@Data
@AllArgsConstructor
public class Friend implements Serializable {

    private String name;
    private UUID uniqueId;
    private String currentServer;
    private boolean online;

}
