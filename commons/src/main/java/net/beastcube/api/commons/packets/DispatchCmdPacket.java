package net.beastcube.api.commons.packets;

import com.ikeirnez.pluginmessageframework.packet.StandardPacket;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author BeastCube
 */
@AllArgsConstructor
public class DispatchCmdPacket extends StandardPacket {
    @Getter
    private String command;

}