package net.beastcube.api.commons;

import java.util.HashMap;
import java.util.Map;

/**
 * Enum that specifies transfer purpose.
 */
public enum TransferPurpose {
    /**
     * Installing on slave purpose. File will be installed on slave server.
     */
    INSTALLING_ON_SLAVE(0);

    private byte b;

    private TransferPurpose(final int i) {
        this.b = (byte) i;
    }

    public byte getByte() {
        return this.b;
    }

    public static TransferPurpose fromByte(final byte b) {
        return mapping.get(b);
    }

    private static Map<Byte, TransferPurpose> mapping = new HashMap<Byte, TransferPurpose>();

    static {
        for (TransferPurpose tp : TransferPurpose.values()) {
            TransferPurpose.mapping.put(tp.b, tp);
        }
    }
}
