package net.beastcube.api.commons;

/**
 * Class that provides random names.
 */
public class RandomNameProvider {
    private NameGenerator gen = new NameGenerator();

    public String next() {
        return this.build(2, true);
    }

    private String build(final int i, final boolean b) {
        return gen.getName();
    }
}
