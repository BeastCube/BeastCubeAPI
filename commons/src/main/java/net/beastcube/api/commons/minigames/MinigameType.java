package net.beastcube.api.commons.minigames;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.beastcube.api.commons.stats.Stat;
import net.beastcube.api.commons.stats.Stats;
import net.beastcube.api.commons.stats.minigames.BeastRushStats;
import net.beastcube.api.commons.stats.minigames.BedwarsStats;
import net.beastcube.api.commons.stats.minigames.FreeForAllStats;
import net.beastcube.api.commons.stats.minigames.SurvivalGamesStats;

/**
 * @author BeastCube
 */
@RequiredArgsConstructor
public enum MinigameType {
    BEDWARS("bw", "Bedwars", new BedwarsStats()),
    //RUNNER("rn", "Runner"),
    //SPLEGG("sp", "Splegg"),
    //FORMELA("fa", "FormelA"),
    //BOWBASH("bb", "BowBash"),
    //COPSANDROBBERS("cnr", "Cops and Robbers"),
    FFA("ffa", "Free for all", new FreeForAllStats()),
    SURVIVALGAMES("sg", "Survival Games", new SurvivalGamesStats()),
    BEASTRUSH("br", "Beast Rush", new BeastRushStats());

    @Getter
    private final String id;
    @Getter
    private final String displayName;
    @Getter
    private final Stats<? extends Stat> stats;

}
