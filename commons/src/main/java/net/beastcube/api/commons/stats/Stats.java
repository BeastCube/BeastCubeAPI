package net.beastcube.api.commons.stats;

import com.google.common.base.Joiner;
import net.beastcube.api.commons.database.PlayersDatabaseHandler;
import net.beastcube.api.commons.minigames.MinigameType;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author BeastCube
 */
public abstract class Stats<T extends Enum<T> & Stat> {

    public abstract Class<T> getStatType();

    public String getColumnDefinitions() {
        List<String> list = Arrays.asList(getStatType().getEnumConstants()).stream().map(p -> p.getId() + " " + p.getSqlDefinition()).collect(Collectors.toList());
        return Joiner.on(", ").join(list);
    }

    public Map<String, Long> getTop10() {
        return PlayersDatabaseHandler.getTop10(this);
    }

    public Optional<Map<Stat, Long>> getStats(UUID player) {
        return PlayersDatabaseHandler.getStats(player, this);
    }

    public Optional<Long> getStat(UUID player, Stat stat) {
        return PlayersDatabaseHandler.getStat(player, this, stat);
    }

    public void setStat(UUID player, Stat stat, long value) {
        PlayersDatabaseHandler.setStat(player, this, stat, value);
    }

    public void increaseStat(UUID player, Stat stat, long value) {
        PlayersDatabaseHandler.increaseStat(player, this, stat, value);
    }

    public void decreaseStat(UUID player, Stat stat, long value) {
        PlayersDatabaseHandler.decreaseStat(player, this, stat, value);
    }

    public abstract MinigameType getMinigameType();

    public String getTableName() {
        return "stats_" + getMinigameType().getId();
    }

    public abstract List<String> format(Map<Stat, Long> map); //TODO with kd an such things for all minigames

    public static String format(StatsDisplayFormat format, Long value) {
        switch (format) {
            case MINUTES:
                return String.format("%02dh %02dmin",
                        TimeUnit.MINUTES.toHours(value),
                        value - TimeUnit.HOURS.toMinutes(TimeUnit.MINUTES.toHours(value))
                );
            case TIMESTAMP:
                return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(new Date(value));
            default:
                return String.valueOf(value);
        }
    }
}
