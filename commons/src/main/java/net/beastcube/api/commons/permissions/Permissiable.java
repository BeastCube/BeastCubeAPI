package net.beastcube.api.commons.permissions;

import java.util.Set;

/**
 * Interface that represents type that can be permissiable.
 */
public interface Permissiable {
    /**
     * Adds Permission to this object.
     *
     * @param permission permission to add
     */
    public void addPermission(String permission);

    /**
     * Removes Permission from this object.
     *
     * @param permission permission to remove
     */
    public void removePermission(String permission);

    /**
     * Returns whether this roles has permission to access specified permission.
     *
     * @param permission permission to check
     * @return whether this role has this permission
     */
    public boolean hasPermission(String permission);

    /**
     * Returns collection of permissions that can this role access.
     *
     * @return collection of this role permissions
     */
    public Set<String> getPermissions();
}
