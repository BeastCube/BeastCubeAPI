package net.beastcube.api.commons.minigames;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.beastcube.api.network.Message;

/**
 * @author BeastCube
 */
@RequiredArgsConstructor
public class MinigameUpdateMessage extends Message {

    @Getter
    private final String serverName;
    @Getter
    private final MinigameType type;
    @Getter
    private final int players;
    @Getter
    private final int maxPlayers;
    @Getter
    private final String arenaName;
    @Getter
    private final MinigameState state;
    @Getter
    private final boolean joinIngame;
    @Getter
    private final boolean remove;

}
