package net.beastcube.api.commons.database.mysql;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author BeastCube
 */
public class BungeeDatabase extends Database<Plugin> {

    /*
         * Construct a MySQL database instance.
         *
         * @param host     Host/IP of the MySQL database.
         * @param port     Port of the MySQL database.
         * @param database Database wanted to be access.
         * @param username Username to be authenticated with.
         * @param password Password for the user authentication.
         * @param plugin   Plugin for the schedulers to be assigned to.
         */
    @SuppressWarnings("deprecation")
    public BungeeDatabase(String host, int port, String database, String username, String password, Plugin plugin) {
        super("com.mysql.jdbc.Driver", "jdbc:mysql://" + host + ":" + port + "/" + database, username, password, plugin, ((ThreadPoolExecutor) plugin.getExecutorService()).getThreadFactory());
    }

    @Override
    void execute(Runnable runnable) {
        ProxyServer.getInstance().getScheduler().runAsync(plugin, runnable);
    }
}
