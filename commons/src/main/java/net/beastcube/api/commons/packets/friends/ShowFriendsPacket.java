package net.beastcube.api.commons.packets.friends;

import com.ikeirnez.pluginmessageframework.packet.StandardPacket;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;

/**
 * @author BeastCube
 */
@AllArgsConstructor
public class ShowFriendsPacket extends StandardPacket {

    @Getter
    private ArrayList<Friend> friends;

}