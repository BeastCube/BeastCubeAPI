package net.beastcube.api.commons.packets.friends;

import com.ikeirnez.pluginmessageframework.packet.StandardPacket;

import java.util.UUID;

/**
 * @author BeastCube
 */
public class ShowPlayerRequestPacket extends StandardPacket {
    private UUID uniqueId;

    public ShowPlayerRequestPacket(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }
}