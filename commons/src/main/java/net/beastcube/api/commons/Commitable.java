package net.beastcube.api.commons;

/**
 * Interaface that specifies commitable type.
 */
public interface Commitable {
    /**
     * Updates values in data storage (db).
     */
    public void commit();
}
