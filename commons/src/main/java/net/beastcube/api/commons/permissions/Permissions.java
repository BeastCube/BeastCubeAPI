package net.beastcube.api.commons.permissions;

/**
 * All default permissions in beastcube.
 */
public class Permissions {

    private Permissions() {

    }

    /**
     * Permission to write colored signs.
     */
    public static final String COLORED_SIGNS_PERMISSION = "beastcube.coloredsigns";
    /**
     * Permission to give temporary ban to name.
     */
    public static final String BAN_TEMPORARY = "beastcube.ban.temporary";
    /**
     * Permission to hive permanent ban to name.
     */
    public static final String BAN_PERMANENT = "beastcube.ban.permanent";
    /**
     * Permission to unban any kind of ban.
     */
    public static final String BAN_PARDON = "beastcube.ban.pardon";
    /**
     * Permission to kick player from server.
     */
    public static final String KICK = "beastcube.ban.kick";
    /**
     * Permission to chat.
     */
    public static final String CHAT = "beastcube.chat";
    /**
     * Permission to broadcast global message.
     */
    public static final String BROADCAST = "beastcube.broadcast";
    /**
     * Permission to edit player roles.
     */
    public static final String ROLE = "beastcube.role";
    /**
     * Permission to change maintenance mode.
     */
    public static final String MAINTENANCE = "beastcube.maintenance.admin";
    /**
     * Permission to bypass aminteance.
     */
    public static final String MAINTENANCE_BYPASS = "beastcube.maintenance.bypass";
    /**
     * Permission to use the teamchat.
     */
    public static final String TEAMCHAT = "beastcube.teamchat";
    /**
     * Permission to see the modlist of a player.
     */
    public static final String MODLIST = "beastcube.modlist";
    /**
     * Permission to change the MOTD.
     */
    public static final String MOTD = "beastcube.motd";
    /**
     * Permission to send a private message to everybody.
     */
    public static final String PM_EVERYBODY = "beastcube.pm_everybody";
    /**
     * Permission to change hub settings.
     */
    public static final String HUB_ADMIN = "beastcube.hub.admin";

    /**
     * Permission to change hub settings.
     */
    public static final String HUBMAGIC_ADMIN = "hubmagic.admin";

    /**
     * Special permission that gives all permissions.
     */
    public static final String SPECIAL_ALL = "beastcube.all";

    //----------------------------------------------------\\
    //--------------------- BUNGEECORD -------------------\\
    //----------------------------------------------------\\

    /**
     * Special permission to execute BungeeCord's server command
     */
    public static final String BUNGEECORD_SERVER = "bungeecord.command.server";

    /**
     * Special permission to execute BungeeCord's list command
     */
    public static final String BUNGEECORD_LIST = "bungeecord.command.list";

    /**
     * Special permission to execute BungeeCord's alert command
     */
    public static final String BUNGEECORD_ALERT = "bungeecord.command.alert";

    /**
     * Special permission to execute BungeeCord's end command
     */
    public static final String BUNGEECORD_END = "bungeecord.command.end";

    /**
     * Special permission to execute BungeeCord's ip command
     */
    public static final String BUNGEECORD_IP = "bungeecord.command.ip";

    /**
     * Special permission to execute BungeeCord's reload command
     */
    public static final String BUNGEECORD_RELOAD = "bungeecord.command.reload";

    /**
     * Special permission to execute BungeeCord's send command
     */
    public static final String BUNGEECORD_SEND = "bungeecord.command.send";

    /**
     * Special permission to execute BungeeCord's find command
     */
    public static final String BUNGEECORD_FIND = "bungeecord.command.find";

    //----------------------------------------------------\\
    //---------------------- Builder ---------------------\\
    //----------------------------------------------------\\

    /**
     * Builder Permissions
     */
    public static final String BUILDER = "beastcube.builder.basic";

    /**
     * Special Builder Permissions
     */
    public static final String BUILDER_SPECIAL = "beastcube.builder.special";

    //----------------------------------------------------\\
    //-------------------- MinigameAPI -------------------\\
    //----------------------------------------------------\\

    /**
     * MinigameAPI Admin Permissions
     */
    public static final String MINIGAMEAPI_ADMIN = "beastcube.minigameapi.admin";

    /**
     * Permission to start the minigame
     */
    public static final String MINIGAMEAPI_GAME_START = "beastcube.minigameapi.game.start";

}
