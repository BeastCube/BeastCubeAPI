package net.beastcube.api.commons.chat;

/**
 * Interface for chat channel subscriber.
 */
public interface ChannelSubscriber {
    /**
     * Sends message to this subscriber.
     *
     * @param message message to be send
     */
    void sendMessage(String message);

    /**
     * Returns this subscriber's mode.
     *
     * @return subscribe mode
     */
    SubscribeMode getMode();

    /**
     * Returns if is this subscriber online/active.
     *
     * @return whether the player is online or not
     */
    boolean isOnline();

    /**
     * Returns name of channel subscriber.
     *
     * @return name of subscriber
     */
    String getName();
}
