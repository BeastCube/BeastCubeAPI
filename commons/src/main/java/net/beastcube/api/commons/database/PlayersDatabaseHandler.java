package net.beastcube.api.commons.database;

import net.beastcube.api.commons.database.mysql.DatabaseHelper;
import net.beastcube.api.commons.permissions.Role;
import net.beastcube.api.commons.permissions.Roles;
import net.beastcube.api.commons.stats.Stat;
import net.beastcube.api.commons.stats.Stats;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

/**
 * @author BeastCube
 */
public class PlayersDatabaseHandler {

    public static int getCoins(UUID player) {
        try {
            ResultSet result = DatabaseHelper.database.query("SELECT coins FROM users WHERE player=?", player.toString());
            if (result.next()) {
                return result.getInt("coins");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static Map<String, Long> getTop10(Stats stats) {
        Map<String, Long> map = new LinkedHashMap<>();
        try {
            String query = "SELECT stats.points, users.lastUsername\n" +
                    "FROM (SELECT points, player FROM " + stats.getTableName() + " ORDER BY points DESC) stats\n" +
                    "JOIN users ON (stats.player = users.player)\n" +
                    "ORDER BY points DESC\n" +
                    "LIMIT 10";
            ResultSet result = DatabaseHelper.database.query(query);
            while (result.next()) {
                map.put(result.getString("lastUsername"), result.getLong("points"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return map;
    }


    public static Optional<Long> getStat(UUID player, Stats stats, Stat stat) {
        try {
            ResultSet result = DatabaseHelper.database.query("SELECT " + stat.getId() + " FROM " + stats.getTableName() + " WHERE player=?", player.toString());
            if (result.next()) {
                return Optional.ofNullable(result.getLong(stat.getId()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public static Optional<Map<Stat, Long>> getStats(UUID player, Stats<? extends Stat> stats) {
        try {
            Map<Stat, Long> map = new HashMap<>();
            ResultSet result = DatabaseHelper.database.query("SELECT * FROM " + stats.getTableName() + " WHERE player=?", player.toString());
            if (result.next()) {
                for (Stat stat : stats.getStatType().getEnumConstants()) {
                    if (stat.getType() == Timestamp.class) { //TODO
                        map.put(stat, result.getTimestamp(stat.getId()).getTime());
                    } else {
                        map.put(stat, result.getLong(stat.getId()));
                    }
                }
                return Optional.ofNullable(map);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private static void executeStatsQuery(UUID player, Stats stats, Stat stat, long value, String suffix) throws SQLException {
        //If the type is TimeStamp, don't increase or decrease, but set it
        if (stat.getType() == Timestamp.class) {
            suffix = "";
        }
        PreparedStatement preparedStatement = DatabaseHelper.database.getConnection().prepareStatement(
                "INSERT INTO " + stats.getTableName() + " (player, " + stat.getId() + ") VALUES (?, ?)" +
                        " ON DUPLICATE KEY UPDATE " + stat.getId() + "=" + suffix + "?");
        preparedStatement.setString(1, player.toString());
        for (int i = 2; i <= 3; i++) {
            if (stat.getType() == Timestamp.class) { //TODO
                preparedStatement.setTimestamp(i, new Timestamp(value));
            } else {
                preparedStatement.setLong(i, value);
            }
        }
        DatabaseHelper.database.execute(preparedStatement);
    }

    public static void increaseStat(UUID player, Stats stats, Stat stat, long value) {
        try {
            executeStatsQuery(player, stats, stat, value, stat.getId() + "+");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void decreaseStat(UUID player, Stats stats, Stat stat, long value) {
        try {
            executeStatsQuery(player, stats, stat, value, stat.getId() + "-");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setStat(UUID player, Stats stats, Stat stat, long value) {
        try {
            executeStatsQuery(player, stats, stat, value, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Optional<UUID> getPlayer(String username) {
        try {
            ResultSet result = DatabaseHelper.database.query("SELECT player FROM users WHERE lastUsername=?", username);
            if (result.next()) {
                return Optional.ofNullable(UUID.fromString(result.getString("player")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public static Optional<String> getPlayer(UUID player) {
        try {
            ResultSet result = DatabaseHelper.database.query("SELECT lastUsername FROM users WHERE player=?", player.toString());
            if (result.next()) {
                return Optional.ofNullable(result.getString("lastUsername"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public static void setCoins(UUID player, int coins) {
        DatabaseHelper.database.execute("UPDATE users SET coins=? WHERE player=?", String.valueOf(coins), player.toString());
    }

    public static void addCoins(UUID player, int coins) {
        DatabaseHelper.database.execute("UPDATE users SET coins=coins+? WHERE player=?", String.valueOf(coins), player.toString());
    }

    public static void removeCoins(UUID player, int coins) {
        if (coins < 0) coins = 0;
        DatabaseHelper.database.execute("UPDATE users SET coins=coins-? WHERE player=?", String.valueOf(coins), player.toString());
    }

    public static Role getRole(UUID player) {
        try {
            ResultSet result = DatabaseHelper.database.query("SELECT role FROM users WHERE player=?", player.toString());
            if (result.next()) {
                return Roles.byId(result.getString("role")).orElse(Roles.DEFAULT);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Roles.DEFAULT;
    }

    public static List<UUID> getPlayersWithRole() {
        List<UUID> list = new ArrayList<>();
        try {
            ResultSet result = DatabaseHelper.database.query("SELECT player FROM users WHERE role IS NOT NULL AND role <> ?", Roles.DEFAULT.getId());
            while (result.next()) {
                list.add(UUID.fromString(result.getString("player")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void setRole(UUID player, Role role) {
        DatabaseHelper.database.execute("UPDATE users SET role=? WHERE player=?", role.getId(), player.toString());
    }

    public static void setPlayerInfos(UUID player, String servername) {
        DatabaseHelper.database.execute("INSERT INTO users (player, server, role, coins, firstLogin, lastLogin) VALUES (?, ?, ?, 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP) ON DUPLICATE KEY UPDATE server=?", player.toString(), servername, Roles.DEFAULT.getId(), servername);
    }

    public static void updateLastLogin(UUID player, String lastusername) {
        DatabaseHelper.database.execute("INSERT INTO users (player, lastUsername, role, coins, firstLogin, lastLogin) VALUES (?, ?, ?, 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP) ON DUPLICATE KEY UPDATE lastUsername=?, lastLogin=CURRENT_TIMESTAMP", player.toString(), lastusername, Roles.DEFAULT.getId(), lastusername);
    }

}
