package net.beastcube.api.commons.configuration;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

/**
 * Section of configuration.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ConfigurationSection extends Unmarshaller.Listener {
    @XmlAttribute
    protected String key;

    protected List<ConfigurationValue> entry;
    // Internal map.
    protected transient Map<String, ConfigurationValue> map;

    public ConfigurationSection() {

    }

    public ConfigurationSection(final String key) {
        this.key = key;
        this.entry = new ArrayList<>();
        this.map = new HashMap<>();
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    /**
     * Returns value by specified key or null if not found.
     *
     * @param key key
     * @return value
     */
    public ConfigurationValue get(final String key) {
        if (this.map == null) {
            this.afterUnmarshal(null, null);
        }
        return this.map.get(key);
    }

    /**
     * Returns value by key or returns default value. This also adds default value to config.
     *
     * @param key          key
     * @param defaultValue default values that will be used, when no value is found
     * @return value
     */
    public ConfigurationValue get(final String key, final Object defaultValue) {
        if (this.map == null) {
            this.afterUnmarshal(null, null);
        }
        if (this.map.containsKey(key)) {
            return this.get(key);
        }
        return this.add(new ConfigurationValue(key, defaultValue));
    }

    /**
     * Returns sub section or null if not found.
     *
     * @param key section key
     * @return configuration section
     */
    public ConfigurationSection getSection(final String key) {
        return this.get(key).asSection();
    }

    public ConfigurationValue add(final ConfigurationValue entry) {
        this.entry.add(entry);
        this.map.put(entry.key, entry);
        return entry;
    }

    public void remove(final String key) {
        for (Iterator<ConfigurationValue> iterator = this.entry.iterator(); iterator.hasNext(); ) {
            ConfigurationValue ce = iterator.next();
            if (ce.key.equals(key)) {
                iterator.remove();
                this.map.remove(key);
            }
        }
    }

    @Override
    public void afterUnmarshal(final Object target, final Object parent) {
        // Create map.
        this.map = new HashMap<>(this.entry.size());
        for (ConfigurationValue entry : this.entry) {
            this.map.put(entry.key, entry);
        }
    }
}
