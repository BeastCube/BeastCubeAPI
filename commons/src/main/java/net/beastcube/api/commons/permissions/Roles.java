package net.beastcube.api.commons.permissions;

import net.beastcube.api.commons.database.PlayersDatabaseHandler;
import net.beastcube.api.commons.text.ChatColor;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * All basic roles on BeastCube.
 */
public class Roles {

    /**
     * Default player role that have permission to chat.
     */
    public static final Role DEFAULT = new Role("default", "Mitglied", ChatColor.GRAY,
            Permissions.CHAT);

    /**
     * Default architect role that has permission to build.
     */
    public static final Role ARCHITECT = new Role("architect", "Architekt", ChatColor.GOLD,
            Roles.DEFAULT,
            Permissions.BUILDER,
            Permissions.BUNGEECORD_SERVER);

    /**
     * Default mod role that have permissions to kick, ban, broadcast and basically moderate server.
     */
    public static final Role MOD = new Role("moderator", "Moderator", ChatColor.DARK_AQUA,
            Roles.ARCHITECT,
            Permissions.KICK,
            Permissions.BAN_PARDON,
            Permissions.BAN_PERMANENT,
            Permissions.BAN_TEMPORARY,
            Permissions.TEAMCHAT,
            Permissions.MODLIST,
            Permissions.MINIGAMEAPI_GAME_START);

    /**
     * Default admin role that has permissions to administrate server.
     */
    public static final Role ADMIN = new Role("admin", "Admin", ChatColor.DARK_RED,
            Roles.MOD,
            Permissions.BROADCAST,
            Permissions.TEAMCHAT,
            Permissions.MOTD,
            Permissions.MAINTENANCE,

            Permissions.HUB_ADMIN,
            Permissions.HUBMAGIC_ADMIN,
            Permissions.BUILDER_SPECIAL,
            Permissions.MINIGAMEAPI_ADMIN,

            Permissions.BUNGEECORD_LIST,
            Permissions.BUNGEECORD_ALERT,
            Permissions.BUNGEECORD_END,
            Permissions.BUNGEECORD_IP,
            Permissions.BUNGEECORD_RELOAD,
            Permissions.BUNGEECORD_SEND,
            Permissions.BUNGEECORD_FIND);

    /**
     * Default owner role that has all permissions.
     */
    public static final Role OWNER = new Role("owner", "Besitzer", ChatColor.DARK_RED,
            Roles.ADMIN,
            Permissions.ROLE,
            Permissions.SPECIAL_ALL);

    public static List<Role> roles = Arrays.asList(
            Roles.DEFAULT,
            Roles.ARCHITECT,
            Roles.MOD,
            Roles.ADMIN,
            Roles.OWNER
    );

    /**
     * Returns {@link Role} by its name.
     *
     * @param roleName name of role.
     * @return role object
     */
    public static Optional<Role> byName(final String roleName) {
        return Roles.roles.stream().filter(p -> p.getName().equalsIgnoreCase(roleName)).findFirst();
    }

    /**
     * Returns {@link Role} by its id.
     *
     * @param roleId id of role.
     * @return role object
     */
    public static Optional<Role> byId(final String roleId) {
        return Roles.roles.stream().filter(p -> p.getId().equalsIgnoreCase(roleId)).findFirst();
    }

    public static Role getRole(UUID p) {
        return PlayersDatabaseHandler.getRole(p);
    }

    public static void setRole(UUID p, Role role) {
        PlayersDatabaseHandler.setRole(p, role);
    }

    public static void removeRole(UUID p) {
        PlayersDatabaseHandler.setRole(p, DEFAULT);
    }

    public static List<UUID> getPlayersWithRole() {
        return PlayersDatabaseHandler.getPlayersWithRole();
    }

}
