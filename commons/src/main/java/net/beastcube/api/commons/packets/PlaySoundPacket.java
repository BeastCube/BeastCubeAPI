package net.beastcube.api.commons.packets;

import com.ikeirnez.pluginmessageframework.packet.StandardPacket;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author BeastCube
 */
@AllArgsConstructor
public class PlaySoundPacket extends StandardPacket {
    @Getter
    private String sound;
    @Getter
    private float volume;
    @Getter
    private float pitch;

    public PlaySoundPacket(String sound) {
        this(sound, 1);
    }

    public PlaySoundPacket(String sound, float volume) {
        this(sound, volume, 1);
    }
}