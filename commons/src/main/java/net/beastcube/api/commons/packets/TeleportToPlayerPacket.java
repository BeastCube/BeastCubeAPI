package net.beastcube.api.commons.packets;

import com.ikeirnez.pluginmessageframework.packet.StandardPacket;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

/**
 * @author BeastCube
 */
@AllArgsConstructor
public class TeleportToPlayerPacket extends StandardPacket {
    @Getter
    private UUID player;
    @Getter
    private UUID target;
}