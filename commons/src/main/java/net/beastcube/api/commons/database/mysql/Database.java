package net.beastcube.api.commons.database.mysql;

import com.sun.rowset.CachedRowSetImpl;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.rowset.CachedRowSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.*;

/*
 * @author BeastCube
 * Credits to: ByteUtils by VirtualByte (Lewes D. B.)
 */
public abstract class Database<T> {

    private static final Integer IDLE_TIMEOUT_DEFAULT = 240000;
    private static final Boolean PREP_STMT_CACHE_DEFAULT = true;
    private static final Integer PREP_STMT_CACHE_SIZE_DEFAULT = 250;
    private static final Integer PREP_STMT_CACHE_SQL_LIMIT_DEFAULT = 2048;
    private static final Integer MAX_POOL_SIZE = 10;
    private static final Integer MIN_IDLE_POOL = 5;
    private static final Integer LEAK_DETECTION_THRESHOLD = 30000;

    private HikariDataSource dataSource;
    public T plugin;
    public ThreadFactory threadFactory;

    /*
     * Construct a database instance.
     *
     * @param className     The class name used to get the driver.
     * @param jdbcURL       A JDBC url to use for connecting.
     * @param username      Username to connect with.
     * @param password      Password to authenticate username.
     * @param plugin        A plugin instance for the schedulers to be assigned to.
     * @param threadFactory The threadFactory to use for the datasource. null by default.
     */
    public Database(String className, String jdbcURL, String username, String password, T plugin, ThreadFactory threadFactory) {
        this.plugin = plugin;
        this.threadFactory = threadFactory;

        try {
            Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }

        HikariConfig config = new HikariConfig();

        config.setJdbcUrl(jdbcURL);

        config.setUsername(username);
        config.setPassword(password);

        config.addDataSourceProperty("cachePrepStmts", PREP_STMT_CACHE_DEFAULT);
        config.addDataSourceProperty("prepStmtCacheSize", PREP_STMT_CACHE_SIZE_DEFAULT);
        config.addDataSourceProperty("prepStmtCacheSqlLimit", PREP_STMT_CACHE_SQL_LIMIT_DEFAULT);
        config.setMinimumIdle(MIN_IDLE_POOL);
        config.setLeakDetectionThreshold(LEAK_DETECTION_THRESHOLD);
        config.setMaximumPoolSize(MAX_POOL_SIZE);
        config.setMaxLifetime(IDLE_TIMEOUT_DEFAULT);

        if (threadFactory != null) {
            config.setThreadFactory(threadFactory);
        }

        try {
            dataSource = new HikariDataSource(config);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * Connects the data pool to the database.
     */
    public void connect() {
        isConnected();
    }

    /*
     * Disconnects (shutdown) the data pool and all connections.
     */
    public void disconnect() {
        dataSource.shutdown();
    }

    /*
     * Query the database and return a cached result.
     *
     * @param query The statement to be queried.
     * @return      Cached rowset returned from query.
     */
    public CachedRowSet query(final PreparedStatement preparedStatement) {
        CachedRowSet rowSet = null;

        if (isConnected()) {
            try {
                ExecutorService exe;
                if (threadFactory != null) {
                    exe = Executors.newCachedThreadPool(threadFactory);
                } else {
                    exe = Executors.newCachedThreadPool();
                }

                Future<CachedRowSet> future = exe.submit(() -> {
                    ResultSet resultSet = preparedStatement.executeQuery();

                    CachedRowSet cachedRowSet = new CachedRowSetImpl();
                    cachedRowSet.populate(resultSet);
                    resultSet.close();

                    return cachedRowSet;
                });

                if (future.get() != null) {
                    rowSet = future.get();
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                    preparedStatement.getConnection().close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return rowSet;
    }

    /*
     * Query the database and return a cached result.
     *
     * @param query Query to be prepared.
     * @param vars  Variables to be replaced from ?.
     */
    public CachedRowSet query(String query, String... vars) {
        return query(prepareStatement(query, vars));
    }

    /*
     * Execute a query
     *
     * @param preparedStatement query to be executed.
     */
    public void execute(final PreparedStatement preparedStatement) {
        if (isConnected()) {
            //Runnable r = () -> { //TODO Was delayed 1 tick and caused bugs...
            //{
            try {
                preparedStatement.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                    preparedStatement.getConnection().close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            //}
            //};
            //execute(r);
        }
    }

    abstract void execute(Runnable runnable);

    /*
     * Execute a query
     *
     * @param query Query to be prepared.
     * @param vars  Variables to be replaced from ?.
     */
    public void execute(String query, String... vars) {
        execute(prepareStatement(query, vars));
    }

    /*
     * Prepare a statement
     *
     * @param query Query to be prepared.
     * @param vars  Variables to be replaced from ?.
     * @return      a prepared statement.
     */
    public PreparedStatement prepareStatement(String query, String... vars) {
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement(query);

            int x = 1;

            if (query.contains("?") && vars.length != 0) {
                for (String var : vars) {
                    preparedStatement.setString(x, var);
                    x++;
                }
            }

            return preparedStatement;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /*
     * Get a connection from the data pool
     *
     * @return a connection.
     */
    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /*
     * Check if the data pool is connected.
     *
     * @return connected Whether the data pool is connected or not.
     */
    public boolean isConnected() {
        try {
            Connection connection = dataSource.getConnection();
            connection.close();
        } catch (SQLException e) {
            return false;
        }

        return true;
    }

    public HikariDataSource getDataSource() {
        return dataSource;
    }
}