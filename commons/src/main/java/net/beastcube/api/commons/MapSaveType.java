package net.beastcube.api.commons;

/**
 * Enum that specifies all supported savegame types.
 */
public enum MapSaveType {
    /**
     * Minecraft world save.
     */
    MINECRAFT_WORLD,
    /**
     * Schematic file.
     */
    SCHEMATIC;
}
