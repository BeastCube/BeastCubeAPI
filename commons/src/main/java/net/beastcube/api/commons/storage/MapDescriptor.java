package net.beastcube.api.commons.storage;

import net.beastcube.api.commons.MapSaveType;
import net.beastcube.api.network.annotations.JsonType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Description of map.
 */
@JsonType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MapDescriptor extends Description {
    private MapSaveType saveType;
    private String minigame;

    /**
     * Only for JAXB.
     */
    public MapDescriptor() {
    }

    public MapSaveType getSaveType() {
        return this.saveType;
    }

    public void setSaveType(final MapSaveType saveType) {
        this.saveType = saveType;
    }

    public String getMinigame() {
        return this.minigame;
    }

    public void setMinigame(final String minigame) {
        this.minigame = minigame;
    }
}
