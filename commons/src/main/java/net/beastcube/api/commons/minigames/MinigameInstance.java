package net.beastcube.api.commons.minigames;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @author BeastCube
 */
@Data
@AllArgsConstructor
public class MinigameInstance implements Serializable {
    private String serverName;
    private String arenaName;
    private MinigameType type;
    private int players;
    private int maxPlayers;
    private MinigameState state;
    private boolean joinIngame;

    public boolean isJoinable() {
        return joinIngame || state.isLobby();
    }

    public String getJoinMessage() {
        return isJoinable() ? "&a&nKlicke um beizutreten" : "&e&nKlicke um zuzusehen";
    }

    public int getDyeColorData() {
        return isJoinable() ? 5 : state.getDyeColorData();
    }

}
