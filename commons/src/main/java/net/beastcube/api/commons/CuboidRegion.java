package net.beastcube.api.commons;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Class that represents cuboid region.
 *
 * @author BeastCube
 */
public class CuboidRegion {
    // Random for getRandomLocation calls.
    private static Random random = new Random();

    /**
     * First vector.
     */
    protected final Vector v1;
    /**
     * Second vector.
     */
    protected final Vector v2;

    protected final World world;

    /**
     * Creates a new region from two vectors and one world.
     *
     * @param v1    minimum point
     * @param v2    maximum point
     * @param world world
     */
    public CuboidRegion(final Vector v1, final Vector v2, final World world) {
        this.v1 = v1;
        this.v2 = v2;
        this.world = world;
    }

    /**
     * Creates a new region with center and size.
     *
     * @param center center
     * @param size   size
     * @param world  world
     */
    public CuboidRegion(final Vector center, final int size, final World world) {
        this.v1 = center.add(new Vector(size, size, size));
        this.v2 = center.add(new Vector(-size, -size, -size));
        this.world = world;
    }

    /**
     * Returns whether the location intersect the region.
     *
     * @param loc location to check.
     * @return true if the location intersect the region
     */
    public boolean intersects(final Location loc) {
        return this.world.equals(loc.getWorld()) && this.intersects(loc.toVector());
    }

    /**
     * Returns whether the location intersects X and Z coordinate of this region.
     *
     * @param loc location to check
     * @return true if the location intersects X and Z coordinate of this region
     */
    public boolean intersectsXZ(final Location loc) {
        return this.world.equals(loc.getWorld()) && this.intersectsXZ(loc.toVector());
    }

    /**
     * Returns whatever the vector intersects the region.
     *
     * @param v vector to check
     * @return true if the vector intersects the region.
     */
    public boolean intersects(final Vector v) {
        return CuboidRegion.range(this.v1.getX(), this.v2.getX(), v.getX())
                && CuboidRegion.range(this.v1.getY(), this.v2.getY(), v.getY())
                && CuboidRegion.range(this.v1.getZ(), this.v2.getZ(), v.getZ());
    }

    public boolean intersectsXZ(final Vector v) {
        return CuboidRegion.range(this.v1.getX(), this.v2.getX(), v.getX())
                && CuboidRegion.range(this.v1.getZ(), this.v2.getZ(), v.getZ());
    }

    /**
     * Returns players in region.
     *
     * @param players list of players to search for
     * @return list of players
     */
    public List<Player> getPlayersXYZ(final List<Player> players) {
        return players.stream().filter(player -> this.intersects(player.getLocation())).collect(Collectors.toList());
    }

    private static boolean range(final double min, final double max,
                                 final double value) {
        if (max > min)
            return (value <= max && value >= min);
        else
            return (value <= min && value >= max);
    }

    /**
     * Retruns first vector.
     *
     * @return vector
     */
    public Location getV1Location() {
        return new Location(this.world, this.v1.getX(), this.v1.getY(), this.v1.getZ());
    }

    /**
     * Returns second vector.
     *
     * @return vector
     */
    public Location getV2Location() {
        return new Location(this.world, this.v2.getX(), this.v2.getY(), this.v2.getZ());
    }

    @Override
    public String toString() {
        return "CuboidRegion [v1=" + this.v1 + ", v2=" + this.v2 + ", world=" + this.world + "]";
    }

    /**
     * Returns players in XZ region.
     *
     * @param players list of players to search for
     * @return list of players.
     */
    public List<Player> getPlayersXZ(final List<Player> players) {
        return players.stream().filter(player -> this.intersectsXZ(player.getLocation())).collect(Collectors.toList());
    }

    /**
     * Returns maximum X co-ordinate value of this region.
     *
     * @return max x value
     */
    public double getMaxX() {
        if (this.v1.getX() > this.v2.getX())
            return this.v1.getX();
        else
            return this.v2.getX();
    }

    /**
     * Returns maximum Y co-ordinate value of this region.
     *
     * @return max y value
     */
    public double getMaxY() {
        if (this.v1.getY() > this.v2.getY())
            return this.v1.getY();
        else
            return this.v2.getY();
    }

    /**
     * Returns maximum Z co-ordinate value of this region.
     *
     * @return max z value
     */
    public double getMaxZ() {
        if (this.v1.getZ() > this.v2.getZ())
            return this.v1.getZ();
        else
            return this.v2.getZ();
    }

    /**
     * Returns minumum X co-ordinate value of this region.
     *
     * @return min x value
     */
    public double getMinX() {
        if (this.v1.getX() > this.v2.getX())
            return this.v2.getX();
        else
            return this.v1.getX();
    }

    /**
     * Returns minumum Y co-ordinate value of this region.
     *
     * @return min y value
     */
    public double getMinY() {
        if (this.v1.getY() > this.v2.getY())
            return this.v2.getY();
        else
            return this.v1.getY();
    }

    /**
     * Returns minumum Z co-ordinate value of this region.
     *
     * @return min z value
     */
    public double getMinZ() {
        if (this.v1.getZ() > this.v2.getZ())
            return this.v2.getZ();
        else
            return this.v1.getZ();
    }

    public World getWorld() {
        return this.world;
    }

    /**
     * Returns random location from this region.
     *
     * @return random location in bounds of this region
     */
    public Location getRandomLocation() {
        int a = this.rnd((int) (this.getMaxX() - this.getMinX()));
        int b = this.rnd((int) (this.getMaxY() - this.getMinY()));
        int c = this.rnd((int) (this.getMaxZ() - this.getMinZ()));
        return new Location(this.world, this.getMinX() + a, this.getMinY() + b, this.getMinZ() + c);
    }

    private int rnd(final int max) {
        if (max == 0) {
            return 0;
        } else {
            return CuboidRegion.random.nextInt(max);
        }
    }

    /**
     * Creates region around specified location with specified size.
     *
     * @param center center
     * @param size   size
     * @return region
     */
    public static CuboidRegion createAround(final Location center, final int size) {
        return new CuboidRegion(center.toVector().add(new Vector(size, size, size)),
                center.toVector().subtract(new Vector(size, size, size)),
                center.getWorld());
    }

    /**
     * Returns width of this region.
     *
     * @return width
     */
    public double getWidth() {
        return this.getMaxX() - this.getMinX();
    }

    /**
     * Returns height of this region.
     *
     * @return height
     */
    public double getHeight() {
        return this.getMaxY() - this.getMinY();
    }

    /**
     * Returns length of this region.
     *
     * @return length
     */
    public double getLength() {
        return this.getMaxZ() - this.getMinZ();
    }
}