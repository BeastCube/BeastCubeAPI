package net.beastcube.api.commons;

/**
 *
 */
public enum GameMode {
    CREATIVE,
    SURVIVAL,
    ADVENTURE;
}
