package net.beastcube.api.commons.stats.minigames;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.beastcube.api.commons.minigames.MinigameType;
import net.beastcube.api.commons.stats.Stat;
import net.beastcube.api.commons.stats.Stats;
import net.beastcube.api.commons.stats.StatsDisplayFormat;

import java.security.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * @author BeastCube
 */
@RequiredArgsConstructor
public class FreeForAllStats extends Stats<FreeForAllStats.FreeForAllStat> {

    @Override
    public MinigameType getMinigameType() {
        return MinigameType.FFA;
    }

    @Override
    public List<String> format(Map<Stat, Long> map) {
        return null;
    }

    @Override
    public Class<FreeForAllStat> getStatType() {
        return FreeForAllStat.class;
    }

    @RequiredArgsConstructor
    public enum FreeForAllStat implements Stat {
        KILLS("kills", "Kills", "INT DEFAULT 0", Integer.class, StatsDisplayFormat.NUMBER),
        DEATHS("deaths", "Tode", "INT DEFAULT 0", Integer.class, StatsDisplayFormat.NUMBER),
        HIGHEST_KILLSTREAK("highest_killstreak", "Höchste Killstreak", "INT DEFAULT 0", Integer.class, StatsDisplayFormat.NUMBER),
        POINTS("points", "Punkte", "INT DEFAULT 0", Integer.class, StatsDisplayFormat.NUMBER),
        PLAYING_TIME("playing_time", "Spielzeit", "BIGINT DEFAULT 0", Long.class, StatsDisplayFormat.MINUTES),
        LAST_PLAYED("last_played", "Zuletzt Gespielt", "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP", Timestamp.class, StatsDisplayFormat.TIMESTAMP);

        @Getter
        final String id;
        @Getter
        final String displayName;
        @Getter
        final MinigameType minigameType = MinigameType.FFA;
        @Getter
        final String sqlDefinition;
        @Getter
        final Class type;
        @Getter
        final StatsDisplayFormat statsDisplayFormat;

        @Override
        public String format(Long value) {
            return Stats.format(statsDisplayFormat, value);
        }
    }

}
