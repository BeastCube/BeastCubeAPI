package net.beastcube.api.commons.stats;

/**
 * @author BeastCube
 */
public enum StatsDisplayFormat {
    NUMBER,
    MINUTES,
    TIMESTAMP
}
