package net.beastcube.api.commons;

import net.beastcube.api.network.annotations.JsonType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Class that represents revision.
 */
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@JsonType
public class Revision implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * Higher revision will be evaluated as newer version.
     */
    protected int revision;
    /**
     * String representation of this revision.
     */
    protected String name;

    public Revision() {
    }

    /**
     * Creates new Revision with specified numeric revision and name.
     *
     * @param revision numeric representaton of this revision
     * @param name     human compactibile name of this revision
     */
    public Revision(final int revision, final String name) {
        this.revision = revision;
        this.name = name;
    }

    /**
     * Returns numeric representation of this revision.
     *
     * @return numeric representation
     */
    public int getNumeric() {
        return this.revision;
    }

    /**
     * Set's numeric representation of this revision.
     *
     * @param revision numeric representation
     */
    public void setNumeric(final int revision) {
        this.revision = revision;
    }

    /**
     * Returns human readable name of this revision / version.
     *
     * @return human readable string
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set's human readable name of this revision / version.
     *
     * @param name new human readable name
     */
    public void setName(final String name) {
        this.name = name;
    }

}
