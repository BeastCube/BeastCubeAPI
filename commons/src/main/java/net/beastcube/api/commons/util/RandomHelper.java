package net.beastcube.api.commons.util;

import java.util.Random;

/**
 * @author BeastCube
 */
public class RandomHelper {

    public static Random random = new Random();

    public static int randomInt(int min, int max) {
        return random.nextInt((max - min) + 1) + min;
    }

    public static int randomInt(int range) {
        return random.nextInt(range);
    }

}
