package net.beastcube.api.commons;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Random;
import java.util.logging.Logger;

/**
 * Class that conitains JSON provider.
 */
public abstract class Providers {
    /**
     * Global network JSON provider.
     */
    public static final Gson JSON = new GsonBuilder().setPrettyPrinting()
            .create();
    /**
     * Global random provider.
     */
    public static final Random RANDOM = new Random();

    /**
     * Global random name provider.
     */
    public static final RandomNameProvider RANDOM_NAME = new RandomNameProvider();

    /**
     * Global logger provider.
     */
    public static Logger LOGGER;
}
