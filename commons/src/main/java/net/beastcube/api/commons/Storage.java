package net.beastcube.api.commons;

import net.beastcube.api.commons.storage.MapDescriptor;
import net.beastcube.api.commons.storage.MinigameDescriptor;

import java.util.Collection;

/**
 * Interface that specifies methods for {@link StorageImpl}.
 */
public interface Storage {

    Collection<MinigameDescriptor> getAvaiablePlugins();

    Collection<MapDescriptor> getAvaiableMaps();

    boolean hasPlugin(String PluginName);

}
