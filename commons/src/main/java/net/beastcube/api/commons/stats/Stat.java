package net.beastcube.api.commons.stats;

import net.beastcube.api.commons.minigames.MinigameType;

/**
 * @author BeastCube
 */
public interface Stat {

    MinigameType getMinigameType();

    String getId();

    String getDisplayName();

    String getSqlDefinition();

    Class getType();

    StatsDisplayFormat getStatsDisplayFormat();

    String format(Long value);

}
