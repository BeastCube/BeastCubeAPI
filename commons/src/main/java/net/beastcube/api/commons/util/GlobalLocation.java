package net.beastcube.api.commons.util;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.Serializable;

/**
 * @author BeastCube
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class GlobalLocation implements Serializable {
    private double x = 0.0d, y = 0.0d, z = 0.0d;
    private float yaw = 0.0f, pitch = 0.0f;
    private String world = "", server = "";

    public GlobalLocation(double x, double y, double z, String world, String server) {
        setX(x);
        setY(y);
        setZ(z);
        setWorld(world);
        setServer(server);
    }

    public GlobalLocation(Player player) {
        Location pLoc = player.getLocation();

        setX(pLoc.getX());
        setY(pLoc.getY());
        setZ(pLoc.getZ());

        setPitch(pLoc.getPitch());
        setYaw(pLoc.getYaw());

        setWorld(pLoc.getWorld().getName());
        setServer("this");
    }

    public boolean bungeeServerExists() {
        return getBungeeServerInfo() != null;
    }

    public ServerInfo getBungeeServerInfo() {
        return ProxyServer.getInstance().getServerInfo(server);
    }

    public Location getBukkitLocation() {
        return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
    }

}