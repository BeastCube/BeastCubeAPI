package net.beastcube.api.commons.players.bans;

import net.beastcube.api.commons.database.BansDatabaseHandler;
import net.beastcube.api.commons.text.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author BeastCube
 */
public class Bans {

    @SuppressWarnings("deprecation")
    public static void showHelp(ProxiedPlayer player) {
        player.sendMessage(ChatColor.translateAlternateColorCodes('$', "$6Bans Help:"));
        player.sendMessage(ChatColor.translateAlternateColorCodes('$', "$c~~~~~~~~~"));
        player.sendMessage(ChatColor.translateAlternateColorCodes('$', "$a/ban $3<name> <reason> ~ $bSpieler permanent bannen"));
        player.sendMessage(ChatColor.translateAlternateColorCodes('$', "$a/tempban $3<name> <time> <timeUnit> <reason> ~ $bSpieler temporär bannen"));
        player.sendMessage(ChatColor.translateAlternateColorCodes('$', "$a/unban $3<name> ~ $bSpieler entbannen")); //TODO
        player.sendMessage(ChatColor.translateAlternateColorCodes('$', "$a/kick $3<name>  <reason> ~ $bSpieler kicken"));
        player.sendMessage(ChatColor.translateAlternateColorCodes('$', "$c~~~~~~~~~"));
    }

    public static boolean isBanned(UUID uniqueId) {
        return BansDatabaseHandler.isBanned(uniqueId);
    }

    public static void addBan(Ban ban) {
        BansDatabaseHandler.addBan(ban);
        ProxiedPlayer p = ProxyServer.getInstance().getPlayer(ban.getPlayer());
        if (p != null) {
            p.disconnect(new TextComponent(ban.getMessage()));
        }
    }

    public static void editBan(Ban ban) {
        BansDatabaseHandler.editBan(ban);
    }

    public static Optional<List<Ban>> getBans(UUID uniqueId) {
        return BansDatabaseHandler.getBans(uniqueId);
    }

}
