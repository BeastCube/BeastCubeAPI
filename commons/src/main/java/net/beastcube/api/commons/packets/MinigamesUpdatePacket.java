package net.beastcube.api.commons.packets;

import com.ikeirnez.pluginmessageframework.packet.StandardPacket;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.beastcube.api.commons.minigames.MinigameInstance;

import java.util.List;

/**
 * @author BeastCube
 */
@RequiredArgsConstructor
public class MinigamesUpdatePacket extends StandardPacket {

    @Getter
    private final List<MinigameInstance> minigameInstances;

}