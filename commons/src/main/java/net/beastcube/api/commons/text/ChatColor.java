package net.beastcube.api.commons.text;

/**
 * Class that represents chat color code.
 */
public class ChatColor {
    public static final ChatColor BLACK = new ChatColor('0');
    public static final ChatColor DARK_BLUE = new ChatColor('1');
    public static final ChatColor DARK_GREEN = new ChatColor('2');
    public static final ChatColor DARK_AQUA = new ChatColor('3');
    public static final ChatColor DARK_RED = new ChatColor('4');
    public static final ChatColor DARK_PURPLE = new ChatColor('5');
    public static final ChatColor GOLD = new ChatColor('6');
    public static final ChatColor GRAY = new ChatColor('7');
    public static final ChatColor DARK_GRAY = new ChatColor('8');
    public static final ChatColor BLUE = new ChatColor('9');
    public static final ChatColor GREEN = new ChatColor('a');
    public static final ChatColor AQUA = new ChatColor('b');
    public static final ChatColor RED = new ChatColor('c');
    public static final ChatColor LIGHT_PURPLE = new ChatColor('d');
    public static final ChatColor YELLOW = new ChatColor('e');
    public static final ChatColor WHITE = new ChatColor('f');

    public static final ChatColor RESET = new ChatColor('r');

    public static final ChatColor OBFUSCATED = new ChatColor('k');
    public static final ChatColor BOLD = new ChatColor('l');
    public static final ChatColor STRIKETROUGH = new ChatColor('m');
    public static final ChatColor UNDERLINE = new ChatColor('n');
    public static final ChatColor ITALIC = new ChatColor('o');

    private final char code;

    private ChatColor(final char code) {
        this.code = code;
    }

    /**
     * Returns character used by this chat color code.
     *
     * @return character of this color code
     */
    public char getChar() {
        return this.code;
    }

    @Override
    public String toString() {
        return new StringBuilder().append((char) 167).append(this.code).toString();
    }

    public static String translateAlternateColorCodes(final char altColorChar,
                                                      final String textToTranslate) {
        char[] b = textToTranslate.toCharArray();
        for (int i = 0; i < b.length - 1; ++i) {
            if ((b[i] == altColorChar)
                    && ("0123456789AaBbCcDdEeFfKkLlMmNnOoRr".indexOf(b[(i + 1)]) > -1)) {
                b[i] = 167;
                b[(i + 1)] = Character.toLowerCase(b[(i + 1)]);
            }
        }
        return new String(b);
    }
}
