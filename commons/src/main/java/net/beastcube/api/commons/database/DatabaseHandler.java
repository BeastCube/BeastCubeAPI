package net.beastcube.api.commons.database;


import net.beastcube.api.commons.database.mysql.DatabaseHelper;
import net.beastcube.api.commons.minigames.MinigameType;

/**
 * @author BeastCube
 */
public class DatabaseHandler {

    public static boolean createTables() {
        DatabaseHelper.database.execute("CREATE TABLE IF NOT EXISTS users (player VARCHAR(36) UNIQUE, server VARCHAR(36), lastUsername VARCHAR(36), coins INT, role VARCHAR(36), firstLogin TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, lastLogin TIMESTAMP NOT NULL)");
        DatabaseHelper.database.execute("CREATE TABLE IF NOT EXISTS friends (player VARCHAR(36), friends VARCHAR(36))");
        DatabaseHelper.database.execute("CREATE TABLE IF NOT EXISTS requests (sender VARCHAR(36), requested VARCHAR(36))"); //TODO rename
        DatabaseHelper.database.execute("CREATE TABLE IF NOT EXISTS cosmetics (id INT NOT NULL AUTO_INCREMENT, player VARCHAR(36), type VARCHAR(36), item VARCHAR(16), PRIMARY KEY (id))");
        DatabaseHelper.database.execute("CREATE TABLE IF NOT EXISTS bans (id INT NOT NULL AUTO_INCREMENT, player VARCHAR(36), author VARCHAR(36), reason VARCHAR(50), createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, expiresAt TIMESTAMP NOT NULL, permanent BOOLEAN, PRIMARY KEY (id))");

        for (MinigameType minigameType : MinigameType.values()) {
            DatabaseHelper.database.execute("CREATE TABLE IF NOT EXISTS " + minigameType.getStats().getTableName() + " (player VARCHAR(36) UNIQUE, " + minigameType.getStats().getColumnDefinitions() + ")");
        }

        return true;
    }

}
