package net.beastcube.api.commons;

import java.util.LinkedList;

/**
 * Component list is a linked list that maintains ordering system first, user second. So the system items are on top of
 * the list and user are ordered after system items part.
 */
public class ComponentList<T> extends LinkedList<T> {
    private static final long serialVersionUID = 1L;
    private int systemCount = 0;
    private int userCount = 0;

    /**
     * Adds item to list to system (first) part.
     *
     * @param item item to add
     */
    public void addSystem(final T item) {
        this.add(this.systemCount, item);
        this.systemCount++;
    }

    /**
     * Adds item to list to user (second) part.
     *
     * @param item item to add
     */
    public void addUser(final T item) {
        this.add(this.systemCount + this.userCount, item);
        this.userCount++;
    }
}
