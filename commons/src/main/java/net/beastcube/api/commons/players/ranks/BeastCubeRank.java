package net.beastcube.api.commons.players.ranks;

/**
 * @author BeastCube
 */
public enum BeastCubeRank {
    GREENHORN,
    NEULING,
    GEFREITER,
    SÖLDNER,
    OFFIZIER,
    GENERAL,
    VETERAN
}
