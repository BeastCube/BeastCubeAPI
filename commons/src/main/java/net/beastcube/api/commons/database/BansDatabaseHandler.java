package net.beastcube.api.commons.database;

import net.beastcube.api.commons.database.mysql.DatabaseHelper;
import net.beastcube.api.commons.players.bans.Ban;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author BeastCube
 */
public class BansDatabaseHandler {

    public static boolean isBanned(UUID p) {
        try {
            ResultSet result = DatabaseHelper.database.query("SELECT id FROM bans WHERE player=? AND permanent OR (UNIX_TIMESTAMP(createdAt) < UNIX_TIMESTAMP() AND UNIX_TIMESTAMP(expiresAt) > UNIX_TIMESTAMP())", p.toString()); //TODO
            if (result.next()) {
                return result.next();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Optional<List<Ban>> getBans(UUID p) {
        try {
            ResultSet result = DatabaseHelper.database.query("SELECT * FROM bans WHERE player=?", p.toString());
            List<Ban> list = new ArrayList<>();
            while (result.next()) {
                UUID player = UUID.fromString(result.getString("player"));
                UUID author = UUID.fromString(result.getString("author"));
                String reason = result.getString("reason");
                Timestamp createdAt = result.getTimestamp("createdAt");
                Timestamp expiresAt = result.getTimestamp("expiresAt");
                boolean permanent = result.getBoolean("permanent");
                list.add(new Ban(player, author, reason, createdAt, expiresAt, permanent));
            }
            if (list.size() > 0) {
                return Optional.of(list);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public static void addBan(Ban ban) {
        try {
            PreparedStatement preparedStatement = DatabaseHelper.database.getConnection().prepareStatement(
                    "INSERT INTO bans (player, author, reason, createdAt, expiresAt, permanent) VALUES (?, ?, ?, CURRENT_TIMESTAMP, ?, ?)");
            preparedStatement.setString(1, ban.getPlayer().toString());
            preparedStatement.setString(2, ban.getAuthor().toString());
            preparedStatement.setString(3, ban.getReason());
            preparedStatement.setTimestamp(4, ban.getExpiresAt());
            preparedStatement.setBoolean(5, ban.isPermanent());
            DatabaseHelper.database.execute(preparedStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void editBan(Ban ban) {
        try {
            PreparedStatement preparedStatement = DatabaseHelper.database.getConnection().prepareStatement(
                    "UPDATE bans SET player=?, author=?, reason=?, createdAt=?, expiresAt=?, permanent=? WHERE id=?");
            preparedStatement.setString(1, ban.getPlayer().toString());
            preparedStatement.setString(2, ban.getAuthor().toString());
            preparedStatement.setString(3, ban.getReason());
            preparedStatement.setTimestamp(4, ban.getCreatedAt());
            preparedStatement.setTimestamp(5, ban.getExpiresAt());
            preparedStatement.setBoolean(6, ban.isPermanent());
            preparedStatement.setInt(7, ban.getId());
            DatabaseHelper.database.execute(preparedStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
