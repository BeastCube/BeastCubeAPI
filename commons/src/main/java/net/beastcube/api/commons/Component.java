package net.beastcube.api.commons;

/**
 * Interface that represents component that can be enabled and disabled.
 */
public interface Component {
    /**
     * Called when componenet is enabled.
     */
    public void onEnable();

    /**
     * Called when componenet is disabled.
     */
    public void onDisable();
}
