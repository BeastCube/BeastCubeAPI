package net.beastcube.api.commons.database;

import net.beastcube.api.commons.database.mysql.DatabaseHelper;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class FriendsDatabaseHandler {

    public static boolean isRequested(UUID sender, UUID requested) {
        try {
            ResultSet result = DatabaseHelper.database.query("SELECT * FROM requests WHERE requested=? AND sender=?", requested.toString(), sender.toString());
            if (result.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public static void sendRequest(UUID sender, UUID requested) {
        DatabaseHelper.database.execute("INSERT INTO requests (requested, sender) VALUES (?, ?)", requested.toString(), sender.toString());
    }

    public static List<UUID> getRequests(UUID player) {
        List<UUID> list = new ArrayList<>();
        try {
            ResultSet result = DatabaseHelper.database.query("SELECT * FROM requests WHERE sender=?", player.toString());
            while (result.next()) {
                list.add(UUID.fromString(result.getString("requested")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    public static List<UUID> getInvitations(UUID player) {
        List<UUID> list = new ArrayList<>();
        try {
            ResultSet result = DatabaseHelper.database.query("SELECT * FROM requests WHERE requested=?", player.toString());
            while (result.next()) {
                list.add(UUID.fromString(result.getString("sender")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void removeRequest(UUID sender, UUID requested) {
        DatabaseHelper.database.execute("DELETE FROM requests WHERE requested=? AND sender=?", requested.toString(), sender.toString());
    }

    public static void addFriend(UUID player1, UUID player2) {
        DatabaseHelper.database.execute("INSERT INTO friends (player, friends) VALUES (?,?)", player1.toString(), player2.toString());
        DatabaseHelper.database.execute("INSERT INTO friends (player, friends) VALUES (?,?)", player2.toString(), player1.toString());
    }

    public static void removeFriend(UUID player1, UUID player2) {
        DatabaseHelper.database.execute("DELETE FROM friends WHERE player=? AND friends=?", player1.toString(), player2.toString());
        DatabaseHelper.database.execute("DELETE FROM friends WHERE player=? AND friends=?", player2.toString(), player1.toString());
    }

    public static List<UUID> getFriendList(UUID player) {
        List<UUID> list = new ArrayList<>();
        try {
            ResultSet result = DatabaseHelper.database.query("SELECT friends FROM friends WHERE player=?", player.toString());
            while (result.next()) {
                String pl = result.getString("friends");
                list.add(UUID.fromString(pl));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<ProxiedPlayer> getOnlineFriendList(UUID player) {
        return FriendsDatabaseHandler.getFriendList(player).stream().map(p -> ProxyServer.getInstance().getPlayer(p)).filter(p -> p != null).collect(Collectors.toList());
    }

    public static boolean hasFriend(UUID player, UUID friend) {
        try {
            ResultSet result = DatabaseHelper.database.query("SELECT * FROM friends WHERE player=? AND friends=?", player.toString(), friend.toString());
            if (result.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}

