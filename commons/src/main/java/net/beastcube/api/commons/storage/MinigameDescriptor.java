package net.beastcube.api.commons.storage;

import net.beastcube.api.network.annotations.JsonType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.io.InputStream;

/**
 * Description of minigame.
 */
@JsonType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MinigameDescriptor extends Description implements Descriptor {
    private String sourceUrl;
    private transient File file;

    /**
     * Only for JAXB.
     */
    public MinigameDescriptor() {
    }

    public String getSourceUrl() {
        return this.sourceUrl;
    }

    public void setSourceUrl(final String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public static MinigameDescriptor load(final File f) {
        try {
            JAXBContext context = JAXBContext.newInstance(MinigameDescriptor.class);
            MinigameDescriptor md = (MinigameDescriptor) context.createUnmarshaller()
                    .unmarshal(f);
            md.file = f;
            return md;
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public byte[] getData() {
        // TODO Auto-generated method stub
        return null;
    }

    public static MinigameDescriptor load(final InputStream remoteDescriptorFile) {
        try {
            JAXBContext context = JAXBContext.newInstance(MinigameDescriptor.class);
            return (MinigameDescriptor) context.createUnmarshaller().unmarshal(
                    remoteDescriptorFile);
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }

    public File getJar() {
        return new File(this.file.getAbsolutePath() + "/" + this.getName() + ".jar");
    }
}
