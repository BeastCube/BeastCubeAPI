package net.beastcube.api.commons.storage;

/**
 * Interface for descriptor.
 */
public interface Descriptor {
    /**
     * Returns byte array containing all data used for reconstruction of part.
     *
     * @return byte array containing all data of this object
     */
    public byte[] getData();
}
