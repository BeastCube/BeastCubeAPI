package net.beastcube.api.commons.packets.friends;

import com.ikeirnez.pluginmessageframework.packet.StandardPacket;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

/**
 * @author BeastCube
 */
@AllArgsConstructor
public class ShowPlayerPacket extends StandardPacket {

    @Getter
    private UUID uniqueId;
    @Getter
    private String name;
    @Getter
    private boolean isFriend;
    @Getter
    private boolean isOnline;
    @Getter
    private boolean isRequested;
    private boolean hasRequested;

    public boolean hasRequested() {
        return hasRequested;
    }
}