package net.beastcube.api.commons.permissions;

import lombok.Getter;
import net.beastcube.api.commons.text.ChatColor;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * Represents user role on BeastCube.
 *
 * @see Roles
 */
public class Role implements Permissiable {
    @Getter
    private final String id;
    @Getter
    private final String name;
    private final Set<String> permissions;
    @Getter
    private final ChatColor color;

    public Role(final String id, final String name, final ChatColor color) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.permissions = new HashSet<>();
    }

    public Role(final String id, final String name, final ChatColor color, final String... permission) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.permissions = new HashSet<>(Arrays.asList(permission));
    }

    public Role(final String id, final String name, final ChatColor color, final Role parent, final String... permission) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.permissions = new HashSet<>(Arrays.asList(permission));
        this.permissions.addAll(parent.getPermissions());
    }

    /**
     * Returns collection of all players that have this role.
     *
     * @return collection of this role players
     */
    public Collection<Player> getAllPlayers() {
        return null;
    } //TODO getAllPlayers from this Role

    @Override
    public boolean hasPermission(final String permission) {
        return this.permissions.contains(permission);
    }

    @Override
    public Set<String> getPermissions() {
        return Collections.unmodifiableSet(this.permissions);
    }

    @Override
    public void addPermission(final String permission) {
        this.permissions.add(permission);
    }

    @Override
    public void removePermission(final String permission) {
        this.permissions.remove(permission);
    }
}
