package net.beastcube.api.commons.packets;

import com.ikeirnez.pluginmessageframework.packet.StandardPacket;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

/**
 * @author BeastCube
 */
@RequiredArgsConstructor
public class RoleChangedPacket extends StandardPacket {

    @Getter
    private final UUID uniqueId;

}
