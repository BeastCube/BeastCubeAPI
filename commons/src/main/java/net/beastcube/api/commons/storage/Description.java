package net.beastcube.api.commons.storage;

import net.beastcube.api.commons.Revision;
import net.beastcube.api.network.annotations.JsonType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;

/**
 * Class that specifies default fields for every description.
 */
@JsonType
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Description {
    private String author;
    private String name;
    private Revision revision;
    private String[] tags;

    /**
     * Only for JAXB.
     */
    public Description() {
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Revision getRevision() {
        return this.revision;
    }

    public void setRevision(final Revision revision) {
        this.revision = revision;
    }

    public String[] getTags() {
        return this.tags;
    }

    public void setTags(final String[] tags) {
        this.tags = tags;
    }

    public void save(final File f) {
        this.save(f, false);
    }

    public void save(final File f, final boolean prettyPrint) {
        try {
            JAXBContext context = JAXBContext.newInstance(this.getClass());
            Marshaller marshaller = context.createMarshaller();
            if (prettyPrint) {
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            }
            marshaller.marshal(this, f);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public static Description load(final File f) {
        try {
            JAXBContext context = JAXBContext.newInstance(Description.class);
            return (Description) context.createUnmarshaller().unmarshal(f);
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }
}
