package net.beastcube.api.commons.packets;

import com.ikeirnez.pluginmessageframework.packet.StandardPacket;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.beastcube.api.commons.util.GlobalLocation;

import java.util.UUID;

/**
 * @author BeastCube
 */
@AllArgsConstructor
public class TeleportToLocationPacket extends StandardPacket {
    @Getter
    private UUID player;
    @Getter
    private GlobalLocation location;
}