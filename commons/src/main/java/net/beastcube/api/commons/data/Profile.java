package net.beastcube.api.commons.data;

import net.beastcube.api.commons.permissions.Permissiable;
import net.beastcube.api.commons.permissions.Role;

import java.util.Locale;
import java.util.UUID;

/**
 * Interface that specifies player's profile.
 */
public interface Profile extends Permissiable {
    /**
     * Returns id of profile in db.
     */
    public long getId();

    /**
     * Returns minecraft UUID of profile.
     */
    public UUID getUniqueId();

    /**
     * Returns last known name of this player.
     */
    public String getLastKnownName();

    /**
     * Returns amount of XP (expierence) on the network.
     */
    public int getXp();

    /**
     * Returns amount of coins on the network.
     */
    public int getCoins();

    /**
     * Returns amount of premium (paid) coins on the network.
     */
    public int getPremiumCoins();

    /**
     * Returns player's {@link Role}.
     *
     * @return player's role
     */
    public Role getRole();

    /**
     * Returns player specified {@link Locale}.
     *
     * @return locale for this player
     */
    public Locale getLocale();
}
