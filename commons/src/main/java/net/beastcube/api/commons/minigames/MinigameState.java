package net.beastcube.api.commons.minigames;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author BeastCube
 */
@RequiredArgsConstructor
public enum MinigameState {
    WAITING_FOR_PLAYERS("waiting_for_players", "Warte auf Spieler", 5, true, false),
    LOBBY_COUNTDOWN_1("voting", "Votephase", 5, true, false),
    LOBBY_COUNTDOWN_2("lobby", "Lobbyphase", 5, true, false),
    ARENA_COUNTDOWN("starting", "Startet", 4, false, true),
    INGAME("ingame", "Im Spiel", 4, false, true),
    RESTARTING("restarting", "Startet neu", 14, false, true),
    DEVELOPMENT("development", "Entwicklungsmodus", 7, false, false);

    @Getter
    private final String id;
    @Getter
    private final String stateMessage;
    @Getter
    private final int dyeColorData;
    @Getter
    private final boolean lobby;
    @Getter
    private final boolean arena;

}