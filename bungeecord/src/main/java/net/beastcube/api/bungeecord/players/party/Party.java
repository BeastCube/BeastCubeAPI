package net.beastcube.api.bungeecord.players.party;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author BeastCube
 */
@SuppressWarnings("deprecation")
public class Party {
    public static ArrayList<String> partyführer = new ArrayList<>();
    public static HashMap<String, String> inparty = new HashMap<>();
    public static HashMap<String, Long> invitetime = new HashMap<>();
    public static HashMap<String, String> invite = new HashMap<>();
    static String pr = PartyCommands.prefix;
    private static Integer maxparty = 20;

    public static void neueParty(ProxiedPlayer p) {
        if (inparty.containsKey(p.getName())) {
            p.sendMessage(pr + "§4Du bist bereits in einer Party!");
            return;
        }
        if (partyführer.contains(p.getName())) {
            p.sendMessage(pr + "§4Du hast schon eine Party!");
            return;
        }
        partyführer.add(p.getName());
        p.sendMessage(pr + "§bParty Erstellt!");
    }

    public static void List(ProxiedPlayer p) {
        if (partyführer.contains(p.getName())) {
            p.sendMessage(pr + "§bPartyLeiter: §c" + p.getName() + " §e" + p.getServer().getInfo().getName());
            p.sendMessage(pr + "§1Spieler:");
            for (ProxiedPlayer y : ProxyServer.getInstance().getPlayers()) {
                if ((inparty.containsKey(y.getName())) && (inparty.get(y.getName()).equals(p.getName()))) {
                    p.sendMessage(pr + "§b" + y.getName() + " §e" + y.getServer().getInfo().getName());
                }
            }
        }
        if (inparty.containsKey(p.getName())) {
            try {
                p.sendMessage(pr + "§bPartyLeiter: §c" + inparty.get(p.getName()) + " §e" + ProxyServer.getInstance().getPlayer(inparty.get(p.getName())).getServer().getInfo().getName());
            } catch (NullPointerException ignored) {
            }
            for (ProxiedPlayer y : ProxyServer.getInstance().getPlayers()) {
                if ((inparty.containsKey(y.getName())) && (inparty.get(y.getName()).equals(inparty.get(p.getName())))) {
                    p.sendMessage(pr + "§b" + y.getName() + " §e" + y.getServer().getInfo().getName());
                }
            }
        }
        if ((!inparty.containsKey(p.getName())) && (!partyführer.contains(p.getName()))) {
            p.sendMessage(pr + "§4Du bist in keiner party!");
        }
    }

    public static void leave(ProxiedPlayer p) {
        if (inparty.containsKey(p.getName())) {
            p.sendMessage(pr + "§bParty verlassen!");
            for (ProxiedPlayer x : ProxyServer.getInstance().getPlayers()) {
                if (inparty.get(x.getName()).equals(inparty.get(p.getName()))) {
                    x.sendMessage(pr + "§eSpieler §b" + p.getName() + " §ehat die Party verlassen!");
                }
            }
            inparty.remove(p.getName());
            return;
        }
        if (partyführer.contains(p.getName())) {
            p.sendMessage(pr + "§bParty gelöscht!");
            partyführer.remove(p.getName());
            for (ProxiedPlayer x : ProxyServer.getInstance().getPlayers()) {
                if ((inparty.containsKey(x.getName())) &&
                        (inparty.get(x.getName()).equals(p.getName()))) {
                    x.sendMessage(pr + "§bDer PartyLeiter hat die Party aufgelöst!");
                    inparty.remove(x.getName());
                }
            }
            return;
        }
        if ((!inparty.containsKey(p.getName())) && (!partyführer.contains(p.getName()))) {
            p.sendMessage(pr + "§4Du bist in keiner Party!");
        }
    }

    public static void chat(ProxiedPlayer p, String text) {
        text = ChatColor.translateAlternateColorCodes('&', text);
        if (partyführer.contains(p.getName())) {
            p.sendMessage(pr + "§b" + p.getName() + " §f:§7 " + text);
            for (ProxiedPlayer x : ProxyServer.getInstance().getPlayers()) {
                if ((inparty.containsKey(x.getName())) &&
                        (inparty.get(x.getName()).equals(p.getName()))) {
                    x.sendMessage(pr + "§b" + p.getName() + " §f:§7 " + text);
                }
            }
        }
        if (inparty.containsKey(p.getName())) {
            ProxyServer.getInstance().getPlayer(inparty.get(p.getName())).sendMessage(pr + "§b" + p.getName() + " §f:§7 " + text);
            for (ProxiedPlayer x : ProxyServer.getInstance().getPlayers()) {
                if ((inparty.containsKey(x.getName())) &&
                        (inparty.get(x.getName()).equals(inparty.get(p.getName())))) {
                    x.sendMessage(pr + "§b" + p.getName() + " §f:§7 " + text);
                }
            }
        }
        if ((!inparty.containsKey(p.getName())) && (!partyführer.contains(p.getName()))) {
            p.sendMessage(pr + "§4Du bist in keiner Party!");
        }
    }

    public static void invite(ProxiedPlayer p, ProxiedPlayer z) {
        long aktuell = System.currentTimeMillis();
        if (partyführer.contains(p.getName())) {
            Integer iparty = 0;
            for (ProxiedPlayer i : ProxyServer.getInstance().getPlayers()) {
                if ((inparty.containsKey(i.getName())) &&
                        (inparty.get(i.getName()).equals(p.getName()))) {
                    iparty = iparty + 1;
                }
            }
            if (iparty >= maxparty) {
                p.sendMessage(pr + " §cDeine Party ist voll!");
                return;
            }
            if (inparty.containsKey(z.getName())) {
                p.sendMessage(pr + "§4Dieser Spieler ist schon in einer Party!");
                return;
            }
            if (partyführer.contains(z.getName())) {
                p.sendMessage(pr + "§4Dieser Spieler hat schon eine Party!");
                return;
            }
            if ((!inparty.containsKey(z.getName())) && (!partyführer.contains(z.getName()))) {
                invite.put(z.getName(), p.getName());
                invitetime.put(z.getName(), aktuell);
                p.sendMessage(pr + "§aDu hast den Spieler §b" + z.getName() + " §aeingeladen!");
                z.sendMessage(pr + "§d" + p.getName() + " §bhat sie in seine Party eingeladen!");
                z.sendMessage(pr + "§bNimm die Einladung innerhalb der nächsten minute mit '§a/party accept' §ban!");
            }
        } else if (inparty.containsKey(p.getName())) {
            p.sendMessage(pr + " §cDu bist nicht der Partyleiter!");
        } else if ((!inparty.containsKey(p.getName())) && (!partyführer.contains(p.getName()))) {
            neueParty(p);
            invite(p, z);
        }
    }

    public static void accept(ProxiedPlayer p) {
        if ((partyführer.contains(p.getName()) | inparty.containsKey(p.getName()))) {
            p.sendMessage(pr + "§4Du bist bereits in einer Party!");
        } else if (invite.containsKey(p.getName())) {
            Long aktuell = System.currentTimeMillis();
            Long diff = aktuell / 1000L - invitetime.get(p.getName()) / 1000L;
            if (diff > 60L) {
                p.sendMessage(pr + "§bDu hast deine Einladung zu spät angenommen!");
                p.sendMessage("diff:" + diff);
                p.sendMessage("aktuell" + aktuell / 1000L);
            } else {
                ProxiedPlayer Leiter = ProxyServer.getInstance().getPlayer(invite.get(p.getName()));
                Leiter.sendMessage(pr + "§b" + p.getName() + " ist der Party beigetreten!");
                invite.remove(p.getName());
                invitetime.remove(p.getName());
                inparty.put(p.getName(), Leiter.getName());
                p.sendMessage(pr + "§bDu bist " + Leiter.getName() + "'s Party beigetreten!");
            }
        } else {
            p.sendMessage(pr + "§4Du hast keine Einladung!");
        }
    }

    public static void kick(ProxiedPlayer pl, ProxiedPlayer p) {
        if (partyführer.contains(pl.getName())) {
            if ((inparty.containsKey(p.getName())) && (inparty.get(p.getName()).equals(pl.getName()))) {
                inparty.remove(p.getName());
                p.sendMessage("§6Du wurdest von§c " + pl.getName() + " §6aus der Party gekickt!");
                pl.sendMessage(pr + " §6Du hast §b" + p.getName() + " §6aus deiner Party gekickt!");
                for (ProxiedPlayer ip : ProxyServer.getInstance().getPlayers()) {
                    if ((inparty.containsKey(ip.getName())) && (inparty.get(ip.getName()).equals(pl.getName()))) {
                        ip.sendMessage("§b" + p.getName() + " §6wurde von §c" + pl.getName() + " §6aus der Party gekickt!");
                    }
                }
            } else {
                pl.sendMessage("§cDieser Spieler ist nicht in ihrer Party!");
            }
        } else {
            pl.sendMessage("§cDu hast keine Party!");
        }
    }
}