package net.beastcube.api.bungeecord;

import com.ikeirnez.pluginmessageframework.bungeecord.BungeeGatewayProvider;
import com.ikeirnez.pluginmessageframework.gateway.ProxyGateway;
import com.ikeirnez.pluginmessageframework.gateway.ProxySide;
import lombok.Getter;
import net.beastcube.api.bungeecord.listeners.onServerSwitchEvent;
import net.beastcube.api.bungeecord.minigames.MinigamesManager;
import net.beastcube.api.bungeecord.network.IncomingPacketHandler;
import net.beastcube.api.bungeecord.players.*;
import net.beastcube.api.bungeecord.players.ban.BanCommand;
import net.beastcube.api.bungeecord.players.ban.KickCommand;
import net.beastcube.api.bungeecord.players.ban.TempBanCommand;
import net.beastcube.api.bungeecord.players.friends.Friends;
import net.beastcube.api.bungeecord.players.party.PartyCommands;
import net.beastcube.api.bungeecord.players.party.PartyEvents;
import net.beastcube.api.bungeecord.server.BroadcastCommand;
import net.beastcube.api.bungeecord.server.MotdCommand;
import net.beastcube.api.bungeecord.server.hub.HubMagic;
import net.beastcube.api.bungeecord.server.mainteance.MainteanceManager;
import net.beastcube.api.bungeecord.server.pinghandler.PingHandler;
import net.beastcube.api.bungeecord.vote.BungeeVoteListener;
import net.beastcube.api.bungeecord.vote.VoteCommands;
import net.beastcube.api.commons.database.DatabaseHandler;
import net.beastcube.api.commons.database.FriendsDatabaseHandler;
import net.beastcube.api.commons.database.PlayersDatabaseHandler;
import net.beastcube.api.commons.database.mysql.BungeeDatabase;
import net.beastcube.api.commons.database.mysql.DatabaseHelper;
import net.beastcube.api.commons.packets.UpdateScoreboardPacket;
import net.beastcube.api.commons.permissions.Role;
import net.beastcube.api.commons.players.bans.Ban;
import net.beastcube.api.commons.players.bans.Bans;
import net.beastcube.api.commons.util.Constants;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.Title;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PermissionCheckEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.PluginManager;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * @author BeastCube
 */
public class BeastCubeAPI implements Listener {
    public static Config config;
    private Friends friends;
    @Getter
    private static ProxyGateway<ProxiedPlayer, Server, ServerInfo> gateway;
    private static BeastCubeMasterBungeePlugin plugin;
    private static MainteanceManager mainteanceManager;
    @Getter
    private static HubMagic hubMagic;
    private static BeastCubeAPI instance;
    @Getter
    private static Logger logger = BeastCubeMasterBungeePlugin.getInstance().getLogger();

    public void onEnable() {
        plugin = BeastCubeMasterBungeePlugin.getInstance();
        PluginManager p = ProxyServer.getInstance().getPluginManager();
        config = new Config();
        gateway = BungeeGatewayProvider.getGateway(Constants.CHANNELNAME, ProxySide.SERVER, BeastCubeMasterBungeePlugin.getInstance());
        gateway.registerListener(new IncomingPacketHandler());

        DatabaseHelper.database = new BungeeDatabase(config.getHost(), config.getPort(), config.getDatabase(), config.getUsername(), config.getPassword(), plugin);
        DatabaseHelper.database.connect();
        if (DatabaseHandler.createTables()) {
            logger.info("Successfully connected to database and created tables!");
        } else {
            logger.severe("Failed to connect to database!");
        }

        mainteanceManager = new MainteanceManager();
        mainteanceManager.onEnable();

        p.registerListener(plugin, new PingHandler());
        PingHandler.setMotd(config.getMotd());

        hubMagic = new HubMagic();
        hubMagic.onEnable();
        friends = new Friends();
        p.registerCommand(plugin, new VoteCommands());
        p.registerListener(plugin, this);
        p.registerListener(plugin, friends);
        p.registerListener(plugin, new BungeeVoteListener());
        p.registerListener(plugin, new TeleportManager());
        p.registerListener(plugin, new onServerSwitchEvent());

        p.registerCommand(plugin, new PrivateMessageCommand());
        p.registerCommand(plugin, new TeamchatCommand());
        p.registerCommand(plugin, new BroadcastCommand());
        p.registerCommand(plugin, new PartyCommands());
        p.registerCommand(plugin, new PingCommand());
        p.registerCommand(plugin, new MotdCommand());
        p.registerCommand(plugin, new RoleCommand());
        p.registerCommand(plugin, new TempBanCommand());
        p.registerCommand(plugin, new BanCommand());
        p.registerCommand(plugin, new KickCommand());
        p.registerCommand(plugin, new ModlistCommand());
        new PartyEvents();
    }

    public void onDisable() {
        mainteanceManager.onDisable();
        hubMagic.onDisable();
        ProxyServer.getInstance().getScheduler().cancel(plugin);
    }

    @EventHandler
    public void onLogin(LoginEvent e) {
        UUID uuid = e.getConnection().getUniqueId();
        String name = e.getConnection().getName();
        if (Bans.isBanned(uuid)) {
            e.setCancelReason(ChatColor.RED + Bans.getBans(uuid).get().stream().filter(Ban::isActive).findFirst().get().getMessage());
            e.setCancelled(true);
        } else if (BeastCubeAPI.getHubMagic().getServers().size() == 0) {
            e.setCancelReason(ChatColor.RED + "Keine Lobbies verfügbar!\n Bitte versuche es später erneut");
            e.setCancelled(true);
        } else {
            PlayersDatabaseHandler.updateLastLogin(uuid, name);
        }
    }

    @EventHandler
    public void onPostLogin(final PostLoginEvent e) {
        ProxyServer.getInstance().getScheduler().schedule(plugin, () -> {
            final ProxiedPlayer p = e.getPlayer();
            Title title = ProxyServer.getInstance().createTitle();
            title.title(new ComponentBuilder("Willkommen").color(ChatColor.GREEN).create());
            title.subTitle(new ComponentBuilder("auf dem ").color(ChatColor.WHITE).append("BeastCube Netzwerk").color(ChatColor.RED).append("!").color(ChatColor.WHITE).create());
            title.stay(3 * 20);
            p.sendTitle(title);
        }, 1, TimeUnit.NANOSECONDS);
    }

    public static void sendUpdateScoreboardPacket(ProxiedPlayer proxiedPlayer) {
        ProxyServer.getInstance().getScheduler().runAsync(BeastCubeAPI.plugin, () -> {
            int count = FriendsDatabaseHandler.getOnlineFriendList(proxiedPlayer.getUniqueId()).size();
            BeastCubeAPI.gateway.sendPacket(proxiedPlayer, new UpdateScoreboardPacket(count));
        });
    }

    public static MainteanceManager getMainteanceManager() {
        return mainteanceManager;
    }

    public static BeastCubeAPI getInstance() {
        return instance != null ? instance : (instance = new BeastCubeAPI());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPermissionCheckEvent(PermissionCheckEvent e) {
        if (e.getSender().getName().equals("CONSOLE")) {
            e.setHasPermission(true);
        } else if (e.getSender() instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) e.getSender();
            Role role = PlayersDatabaseHandler.getRole(player.getUniqueId());
            e.setHasPermission(role.hasPermission(e.getPermission()));
        } else {
            e.setHasPermission(false);
        }
    }

    @EventHandler
    public void onSlaveRemoved(SlaveRemovedEvent e) {
        MinigamesManager.removeServer(e.getServer().getName());
        BeastCubeAPI.getHubMagic().removeServer(ProxyServer.getInstance().getServerInfo(e.getServer().getName()));
    }

}
