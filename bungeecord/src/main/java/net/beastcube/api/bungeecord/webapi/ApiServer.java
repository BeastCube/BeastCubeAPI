package net.beastcube.api.bungeecord.webapi;

import com.sun.jersey.api.core.DefaultResourceConfig;
import com.sun.jersey.simple.container.SimpleServerFactory;
import net.beastcube.api.bungeecord.MasterComponent;
import net.beastcube.api.commons.configuration.Configuration;

import javax.net.ssl.SSLContext;
import java.io.Closeable;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class that respresents REST API server.
 */
public class ApiServer extends MasterComponent {
    private Closeable http_server;
    private Closeable https_server;

    @SuppressWarnings("unchecked")
    @Override
    public void onEnable() {
        Logger.getLogger("com.sun.jersey.server.impl.application.WebApplicationImpl").setLevel(Level.OFF);
        try {
            String http_address = "http://0.0.0.0:"
                    + this.getMaster()
                    .getConfiguration()
                    .getSection(ApiServer.class)
                    .get(Configuration.Keys.KEY_PORT_API_HTTP, 10361)
                    .asInteger();

            String https_address = "https://0.0.0.0:"
                    + this.getMaster()
                    .getConfiguration()
                    .getSection(ApiServer.class)
                    .get(Configuration.Keys.KEY_PORT_API_HTTPS, 10362)
                    .asInteger();

            DefaultResourceConfig resourceConfig = new DefaultResourceConfig(
                    ApiResource.class, StringBodyWriter.class);

            // Apply filters.
            resourceConfig.getContainerRequestFilters().add(
                    new ApiAccessKeyRequestFilter());
            resourceConfig.getContainerRequestFilters().add(new ApiLoggingFilter());

            this.logger.info("Starting HTTP api server...");
            this.http_server = SimpleServerFactory.create(http_address, resourceConfig);

            //SelfSignedCertificate ssc = new SelfSignedCertificate();
            this.logger.info("Starting HTTPS api server...");
            SSLContext context = SSLContext.getDefault();

            this.https_server = SimpleServerFactory.create(https_address, context,
                    resourceConfig);
        } catch (IllegalArgumentException | IOException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        try {
            this.logger.info("Stopping HTTP and HTTPS api servers...");
            this.http_server.close();
            this.https_server.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
