package net.beastcube.api.bungeecord.players;

import com.google.common.base.Joiner;
import net.beastcube.api.commons.permissions.Permissions;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.stream.Collectors;

import static net.md_5.bungee.api.ChatColor.*;

/**
 * @author BeastCube
 */
public class ModlistCommand extends Command {

    public ModlistCommand() {
        super("modlist", Permissions.MODLIST);
    }


    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            if (sender instanceof ProxiedPlayer) {
                sendModlist(sender, (ProxiedPlayer) sender);
            } else {
                sender.sendMessage(new ComponentBuilder("Die Modlist kann nur für Spieler angezeigt werden!").color(RED).create());
            }
        } else if (args.length > 0) {
            ProxiedPlayer player = ProxyServer.getInstance().getPlayer(args[0]);
            if (player == null) {
                sender.sendMessage(new ComponentBuilder("Dieser Spieler ist nicht online!").color(RED).create());
            } else {
                sendModlist(sender, player);
            }
        }
    }

    public void sendModlist(CommandSender sender, ProxiedPlayer p) {
        if (p.getModList().size() > 0) {
            sender.sendMessage(new ComponentBuilder(p.getName()).color(AQUA).append("'s Modlist:").color(YELLOW).create());
            sender.sendMessage(new TextComponent(Joiner.on(GRAY + ", ").join(p.getModList().entrySet().stream().map(entry -> ChatColor.GREEN + entry.getKey() + GRAY + ":" + ChatColor.GOLD + entry.getValue()).collect(Collectors.toList()))));
        } else {
            if (p.isForgeUser()) {
                sender.sendMessage(new ComponentBuilder("Der Spieler ").color(RED).append(p.getName()).color(AQUA).append(" spielt mit Forge, hat jedoch keine Mods installiert!").create());
            } else {
                sender.sendMessage(new ComponentBuilder("Der Spieler ").color(RED).append(p.getName()).color(AQUA).append(" spielt nicht mit Forge!").create());
            }
        }
    }

}
