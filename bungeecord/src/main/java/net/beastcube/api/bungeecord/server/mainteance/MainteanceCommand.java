package net.beastcube.api.bungeecord.server.mainteance;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.beastcube.api.bungeecord.BeastCubeMasterBungeePlugin;
import net.beastcube.api.commons.permissions.Permissions;
import net.beastcube.api.commons.util.Constants;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;
import net.md_5.bungee.api.scheduler.ScheduledTask;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static net.beastcube.api.bungeecord.server.mainteance.Messages.*;

/**
 * @author BeastCube
 */
@SuppressWarnings("deprecation")
public class MainteanceCommand extends Command implements TabExecutor {

    /**
     * Store the parent plugin
     */
    private final BeastCubeMasterBungeePlugin parent;

    /**
     * Create a new instance of the class
     *
     * @param parent parent {@link BeastCubeAPI}
     */
    public MainteanceCommand(BeastCubeMasterBungeePlugin parent) {
        super("mainteance", ""); //TODO Move permission to command definition
        this.parent = parent;
    }

    /**
     * Handle the various subcommands associated with the plugin
     *
     * @param sender command sender
     * @param args   command arguments
     */
    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender.hasPermission(Permissions.MAINTENANCE)) {
            if (args != null && args.length > 0) {
                String sub = args[0];
                MainteanceManager mm = BeastCubeAPI.getMainteanceManager();
                switch (sub.toLowerCase()) { //TODO enable - cancel - not working !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    case "enable":
                        if (mm.isEnabled()) {
                            sender.sendMessage(MAINTENANCE_ENABLED_ALREADY);
                            break;
                        }

                        if (mm.getCountdown() > 0) {
                            if (mm.getTaskId() != -1) {
                                sender.sendMessage(MAINTENANCE_TASK_ALREADY_RUNNING);
                                break;
                            }

                            EnableRunnable runnable = new EnableRunnable(parent, sender);
                            ScheduledTask task = ProxyServer.getInstance().getScheduler().runAsync(parent, runnable);
                            mm.setTaskId(task.getId());
                        } else {
                            mm.kick(null);
                            mm.setEnabled(true);
                            sender.sendMessage(MAINTENANCE_ENABLED);
                        }
                        break;
                    case "cancel":
                        if (mm.getTaskId() == -1) {
                            sender.sendMessage(MAINTENANCE_TASK_NOT_RUNNING);
                            break;
                        }

                        mm.clearTask();
                        ProxyServer.getInstance().broadcast(MAINTENANCE_TASK_STOPPED);
                        break;
                    case "disable":
                        if (!mm.isEnabled()) {
                            sender.sendMessage(MAINTENANCE_DISABLED_ALREADY);
                            break;
                        }

                        mm.setEnabled(false);
                        sender.sendMessage(MAINTENANCE_DISABLED);
                        break;
                    case "add":
                    case "remove":
                        if (args.length > 1) {
                            String name = args[1];
                            switch (sub) {
                                case "add":
                                    if (mm.getWhitelist().contains(name)) {
                                        sender.sendMessage(WHITELIST_ADD_EXIST);
                                    } else {
                                        mm.getWhitelist().add(name);

                                        WhitelistUpdateEvent event = new WhitelistUpdateEvent(name, WhitelistUpdateEvent.Operation.ADD);
                                        ProxyServer.getInstance().getPluginManager().callEvent(event);

                                        sender.sendMessage(WHITELIST_ADD);
                                    }
                                    break;
                                case "remove":
                                    if (!mm.getWhitelist().contains(name)) {
                                        sender.sendMessage(WHITELIST_DEL_EXIST);
                                    } else {
                                        mm.getWhitelist().remove(name);

                                        WhitelistUpdateEvent event = new WhitelistUpdateEvent(name, WhitelistUpdateEvent.Operation.REMOVE);
                                        ProxyServer.getInstance().getPluginManager().callEvent(event);

                                        sender.sendMessage(WHITELIST_DEL);
                                    }
                                    break;
                                default:
                                    sender.sendMessage(HELP_INVALID + "maintenance <add|remove> <username>");
                                    break;
                            }
                            break;
                        } else {
                            sender.sendMessage(HELP_INVALID + "maintenance <add|remove> <username>");
                        }
                        break;
                    case "list":
                        Joiner joiner = Joiner.on(", ");
                        String joined = joiner.join(mm.getWhitelist());
                        sender.sendMessage(ChatColor.GREEN + "Whitelist: " + ChatColor.WHITE + (joined.equals("") ? "No players added to whitelist" : joined));
                        break;
                    case "reload":
                        if (mm.reload()) {
                            sender.sendMessage(CONFIG_RELOAD);
                        } else {
                            sender.sendMessage(CONFIG_RELOAD_ERR);
                        }
                        break;
                    case "help":
                        sender.sendMessage(HELP_USAGE);
                        break;
                    default:
                        sender.sendMessage(HELP_MESSAGE);
                        break;
                }
            } else {
                sender.sendMessage(HELP_MESSAGE);
            }
        } else {
            sender.sendMessage(Constants.NOPERMISSION);
        }
    }

    /**
     * Add tab completion to commands
     *
     * @param sender command sender
     * @param args   arguments
     * @return list of potential tab results
     */
    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length < 1 || args.length > 2) {
            return ImmutableSet.of();
        }

        List<String> commands = new ArrayList<>();

        if (args.length == 1) {
            String search = args[0].toLowerCase();
            if ("list".startsWith(search)) {
                commands.add("list");
            }

            if ("add".startsWith(search)) {
                commands.add("add");
            }

            if ("remove".startsWith(search)) {
                commands.add("remove");
            }

            if ("help".startsWith(search)) {
                commands.add("help");
            }

            if ("enable".startsWith(search)) {
                commands.add("enable");
            }

            if ("disable".startsWith(search)) {
                commands.add("disable");
            }

            if ("reload".startsWith(search)) {
                commands.add("reload");
            }
        } else {
            String sub = args[0].toLowerCase();
            if (sub.equals("add")) {
                String search = args[1].toLowerCase();
                commands.addAll(ProxyServer.getInstance().getPlayers().stream().filter(player -> player.getName().toLowerCase().startsWith(search)).filter(player -> !BeastCubeAPI.getMainteanceManager().getWhitelist().contains(player.getName())).map(ProxiedPlayer::getName).collect(Collectors.toList()));
            } else if (sub.equals("remove")) {
                String search = args[1].toLowerCase();
                commands.addAll(BeastCubeAPI.getMainteanceManager().getWhitelist().stream().filter(name -> name.toLowerCase().startsWith(search)).collect(Collectors.toList()));
            }
        }

        return commands;
    }
}