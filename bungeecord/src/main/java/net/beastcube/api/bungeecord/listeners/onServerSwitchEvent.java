package net.beastcube.api.bungeecord.listeners;

import net.beastcube.api.commons.database.PlayersDatabaseHandler;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * @author BeastCube
 */
public class onServerSwitchEvent implements Listener {

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onServerSwitch(ServerSwitchEvent e) {
        ProxiedPlayer p = e.getPlayer();
        Server server = p.getServer();
        if (server != null && server.getInfo() != null) {
            String serverName = server.getInfo().getName();
            PlayersDatabaseHandler.setPlayerInfos(p.getUniqueId(), serverName);
        }
        p.sendMessage("");
    }

}
