package net.beastcube.api.bungeecord.vote;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.plugin.Command;

/**
 * @author BeastCube
 */
public class VoteCommands extends Command {

    private String voteurl = "http://bit.ly/1AQpqAI";

    public VoteCommands() {
        super("vote");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        sender.sendMessage(new ComponentBuilder("Klicke ").color(ChatColor.GREEN).append("hier").color(ChatColor.AQUA).event(new ClickEvent(ClickEvent.Action.OPEN_URL, voteurl)).append(" um zu voten und 100 Coins zu erhalten!!").color(ChatColor.GREEN).create());
    }
}
