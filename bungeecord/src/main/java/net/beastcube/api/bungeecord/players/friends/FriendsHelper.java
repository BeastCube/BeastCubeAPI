package net.beastcube.api.bungeecord.players.friends;

import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.beastcube.api.bungeecord.BeastCubeMasterBungeePlugin;
import net.beastcube.api.commons.database.FriendsDatabaseHandler;
import net.beastcube.api.commons.database.PlayersDatabaseHandler;
import net.beastcube.api.commons.packets.friends.Friend;
import net.beastcube.api.commons.packets.friends.ShowFriendsPacket;
import net.beastcube.api.commons.packets.friends.ShowPlayerPacket;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.*;
import java.util.stream.Collectors;

import static net.beastcube.api.commons.database.FriendsDatabaseHandler.hasFriend;
import static net.beastcube.api.commons.database.FriendsDatabaseHandler.isRequested;
import static net.md_5.bungee.api.ChatColor.*;

/**
 * @author BeastCube
 */
public class FriendsHelper {
    public static String clickProfile = "Klicke um sein Profil zu sehen";
    public static HoverEvent showProfile = new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(clickProfile).create());

    public static HoverEvent ServerNameAndShowProfile(ProxiedPlayer player) {
        String name;
        if (player.getServer().getInfo() == null) {
            name = "Unbekannt";
        } else {
            name = player.getServer().getInfo().getName();
        }
        return new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Auf dem Server: " + name + "\n").append(clickProfile).create());
    }

    @SuppressWarnings("deprecation")
    public static void showHelp(ProxiedPlayer player) {
        player.sendMessage(colorString("$6Freunde HIlfe:"));
        player.sendMessage(colorString("$c~~~~~~~~~"));
        player.sendMessage(colorString("$a/friends ~ $bFreundesliste zeigen"));
        player.sendMessage(colorString("$a/friends $6help ~ $bHilfe ziegen"));
        player.sendMessage(colorString("$a/friends $6invite $3<name> ~ $bJemandem eine Freundschaftsanfrage schicken"));
        player.sendMessage(colorString("$a/friends $6abort $3<name> ~ $bEine Freundschaftsanfrage abbrechen"));
        player.sendMessage(colorString("$a/friends $6accept $3<name> ~ $bEine Freundschaftsanfrage annehmen"));
        player.sendMessage(colorString("$a/friends $6decline $3<name> ~ $bEine Freundschaftsanfrage ablehnen"));
        player.sendMessage(colorString("$a/friends $6requests ~ $bGeschickte Freundschaftsanfragen anzeigen")); //Von mir gesendet
        player.sendMessage(colorString("$a/friends $6invitations ~ $bErhaltene Freundschaftsanfragen anzeigen")); //An mich gesendet
        player.sendMessage(colorString("$a/friends $6remove $3<name> ~ $bFreund entfernen"));
        player.sendMessage(colorString("$a/friends $6show $3<name> ~ $bZeige einen Spieler"));
        player.sendMessage(colorString("$c~~~~~~~~~"));
    }

    public static void showInvitations(ProxiedPlayer player) {
        ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), () -> {
            List<UUID> list = FriendsDatabaseHandler.getInvitations(player.getUniqueId());

            if (list.size() == 0) {
                player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Du hast keine neuen Freundschaftsanfragen!").color(RED).create());
                return;
            }

            player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Freundschaftsanfragen:").color(GOLD).create());
            ComponentBuilder message = new ComponentBuilder("");
            Iterator<UUID> iter = list.iterator();
            while (iter.hasNext()) {
                String name = PlayersDatabaseHandler.getPlayer(iter.next()).get();
                message.append(name).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + name)).event(showProfile);
                if (iter.hasNext())
                    message.append(", ");
            }
            player.sendMessage(message.create());
        });
    }

    public static void showRequests(ProxiedPlayer player) {
        ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), () -> {
            List<UUID> list = FriendsDatabaseHandler.getRequests(player.getUniqueId());

            if (list.size() == 0) {
                player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Du hast keine gesendete Freundschaftsanfragen!").color(RED).create());
                return;
            }

            player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Gesendete Freundschaftsanfragen:").color(GOLD).create());
            ComponentBuilder message = new ComponentBuilder("");
            Iterator<UUID> iter = list.iterator();
            while (iter.hasNext()) {
                String name = PlayersDatabaseHandler.getPlayer(iter.next()).get();
                message.append(name).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + name)).event(showProfile);
                if (iter.hasNext())
                    message.append(", ");
            }
            player.sendMessage(message.create());

        });
    }

    public static void deleteFriend(ProxiedPlayer player, Optional<UUID> friend, String name) {
        ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), () -> {
            if (validatePlayer(player, friend)) {
                if (!hasFriend(player.getUniqueId(), friend.get())) {
                    player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(name).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + name)).event(showProfile).append(" ist nicht dein Freund!").color(RED).create());
                    return;
                }

                FriendsDatabaseHandler.removeFriend(player.getUniqueId(), friend.get());
                player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Du hast ").color(GREEN).append(name).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + name)).event(showProfile).append(" von deinen Freunden gelöscht!").color(GREEN).create());

                ProxiedPlayer friendPlayer = ProxyServer.getInstance().getPlayer(friend.get());
                if (friendPlayer != null) {
                    BeastCubeAPI.sendUpdateScoreboardPacket(player);
                    BeastCubeAPI.sendUpdateScoreboardPacket(friendPlayer);
                    friendPlayer.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(player.getName()).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + player.getName())).event(showProfile).append(" hat dich von seinen Freunden gelöscht!").color(GREEN).create());
                }
            }
        });
    }

    public static void abortInvitation(ProxiedPlayer player, Optional<UUID> invitated, String name) {
        ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), () -> {
            if (validatePlayer(player, invitated)) {
                if (!isRequested(player.getUniqueId(), invitated.get())) {
                    player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Du hast ").color(RED).append(name).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + name)).event(showProfile).append(" nicht eingeladen!").color(RED).create());
                    return;
                }

                FriendsDatabaseHandler.removeRequest(player.getUniqueId(), invitated.get());
                player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Du hast die Einladung an ").color(RED).append(name).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + name)).event(showProfile).append(" abgebrochen!").color(RED).create());

                ProxiedPlayer invitatedPlayer = ProxyServer.getInstance().getPlayer(invitated.get());
                if (invitatedPlayer != null) {
                    invitatedPlayer.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(player.getName()).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + player.getName())).event(showProfile).append(" hat die Freundschaftsanfrage abgebrochen!").color(RED).create());
                }
            }
        });
    }

    public static void declineInvitation(ProxiedPlayer player, Optional<UUID> sender, String name) {
        ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), () -> {
            if (validatePlayer(player, sender)) {
                if (!isRequested(sender.get(), player.getUniqueId())) {
                    player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(name).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + name)).event(showProfile).append(" hat dir keine Freundschaftsanfrage gesendet!").color(RED).create());
                    return;
                }

                FriendsDatabaseHandler.removeRequest(sender.get(), player.getUniqueId());
                player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Du hast ").color(RED).append(name).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + name)).event(showProfile).append("'s Freundschaftsanfrage abgelehnt!").color(RED).create());

                ProxiedPlayer senderPlayer = ProxyServer.getInstance().getPlayer(sender.get());
                if (senderPlayer != null) {
                    senderPlayer.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(player.getName()).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + player.getName())).event(showProfile).append(" hat deine Freundschaftsanfrage abgelehnt!").color(RED).create());
                }
            }
        });
    }

    public static void acceptInvitation(ProxiedPlayer player, Optional<UUID> friend, String name) {
        ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), () -> {
            if (validatePlayer(player, friend)) {
                if (!isRequested(friend.get(), player.getUniqueId())) {
                    player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(name).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + name)).event(showProfile).append(" hat dir keine Freundschaftsanfrage gesendet!!").color(RED).create());
                    return;
                }

                FriendsDatabaseHandler.removeRequest(friend.get(), player.getUniqueId());
                FriendsDatabaseHandler.addFriend(friend.get(), player.getUniqueId());

                player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(name).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + name)).event(showProfile).append(" ist jetzt dein Freund!").color(GREEN).create());

                ProxiedPlayer senderPlayer = ProxyServer.getInstance().getPlayer(friend.get());
                if (senderPlayer != null) {
                    BeastCubeAPI.sendUpdateScoreboardPacket(player);
                    BeastCubeAPI.sendUpdateScoreboardPacket(senderPlayer);
                    senderPlayer.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(player.getName()).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + player.getName())).event(showProfile).append(" ist jetzt dein Freund!").color(GREEN).create());
                }
            }
        });
    }

    public static void invite(ProxiedPlayer player, Optional<UUID> invited, String name) {
        ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), () -> {
            if (validatePlayer(player, invited)) {
                if (hasFriend(player.getUniqueId(), invited.get())) {
                    player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(name).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + name)).event(showProfile).append(" ist bereits dein Freund!").color(RED).create());
                    return;
                }

                if (isRequested(player.getUniqueId(), invited.get())) {
                    player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Du hast ").color(RED).append(name).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + name)).event(showProfile).append(" bereits eine Freundschaftsanfrage gesendet!").color(RED).create());
                    return;
                }

                if (isRequested(invited.get(), player.getUniqueId())) {
                    player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Du hast bereits eine Freundschaftsanfrage von ").color(RED).append(name).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + name)).event(showProfile).append(" erhalten!").color(RED).create());
                    return;
                }

                FriendsDatabaseHandler.sendRequest(player.getUniqueId(), invited.get());
                player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Du hast ").color(GREEN).append(name).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + name)).event(showProfile).append(" eine Freundschaftsanfrage gesendet!").color(GREEN).create());

                ProxiedPlayer invitedPlayer = ProxyServer.getInstance().getPlayer(invited.get());
                if (invitedPlayer != null) {
                    invitedPlayer.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(player.getName()).color(AQUA).append(" möchte dein Freund sein!").color(GOLD).create());
                    invitedPlayer.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("[Annehmen]").color(GREEN).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends accept " + player.getName())).append("  ").append("[Ablehnen]").color(RED).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends decline " + player.getName())).create());
                }
            }
        });

    }

    public static void showList(ProxiedPlayer player) {
        ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), () -> {
            List<UUID> friends = FriendsDatabaseHandler.getFriendList(player.getUniqueId());

            if (friends.size() == 0) {
                player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Du hast noch keine Freunde!").color(RED).create());
                return;
            }

            player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Freunde:").color(GOLD).create());
            ComponentBuilder message = new ComponentBuilder("");

            List<FriendPlayer> list = friends.stream().map(p -> new FriendPlayer(p, ProxyServer.getInstance().getPlayer(p))).sorted((friend1, friend2) -> Boolean.compare(friend1 == null, friend2 == null)).collect(Collectors.toList());

            Iterator<FriendPlayer> iter = list.iterator();
            while (iter.hasNext()) {
                FriendPlayer friendplayer = iter.next();
                if (friendplayer.getPlayer() != null) {
                    message.append(friendplayer.getPlayer().getName()).color(GREEN).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + friendplayer.getPlayer().getName())).event(ServerNameAndShowProfile(friendplayer.getPlayer()));
                } else {
                    String name = PlayersDatabaseHandler.getPlayer(friendplayer.getUniqueId()).get();
                    message.append(name).color(RED).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + name)).event(showProfile);
                }
                if (iter.hasNext())
                    message.append(", ");
            }
            player.sendMessage(message.create());
        });
    }

    public static void showPlayer(ProxiedPlayer player, Optional<UUID> friend, String name) {
        ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), () -> {
            if (validatePlayer(player, friend)) {
                ProxiedPlayer p = ProxyServer.getInstance().getPlayer(friend.get());
                boolean online = p != null;

                player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("~~~~~~~ ").color(GOLD).append(name).color(online ? GREEN : RED).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + name)).event(showProfile).append(" ~~~~~~~~").color(GOLD).create());
                ComponentBuilder message = new ComponentBuilder("");
                if (online) {
                    player.sendMessage(new ComponentBuilder("Online auf dem Server: " + p.getServer().getInfo().getName()).create());
                }
                //----------- Friends ----------------\\
                boolean isFriend = hasFriend(player.getUniqueId(), friend.get());
                if (isFriend) {
                    message.append("[Entfernen]").color(RED).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends remove " + name)).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Klicke um " + name + " von deinen Freunden zu entfernen").create()));
                } else {
                    if (isRequested(friend.get(), player.getUniqueId())) {
                        message.append("[Freundschaftsanfrage annehmen]").color(GREEN).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends accept " + name)).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Klicke um die Freundschaftsanfrage anzunehmen").create()));
                        message.append("  ").append("[Freundschaftsanfrage ablehnen]").color(RED).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends decline " + name)).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Klicke um die Freundschaftsanfrage abzulehnen").create()));
                    } else if (isRequested(player.getUniqueId(), friend.get())) {
                        message.append("[Freundschaftsanfrage abbrechen]").color(GRAY).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends abort " + name)).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Klicke um die Freundschaftsanfrage anzubrechen").create()));
                    } else {
                        message.append("[Freundschaftsanfrage schicken]").color(GREEN).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends invite " + name)).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Klicke um " + name + " eine Freundschaftsanfrage zu schicken").create()));
                    }
                }
                if (online && isFriend) {
                    message.append("  ").append("[Teleportieren]").color(GOLD).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends teleport " + name)).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Klicke um dich zu " + name + " zu teleportieren").create()));
                }
                //----------- Party ----------------\\
                if (online)
                    message.append("  ").append("[In Party einladen]").color(GOLD).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/party invite " + name)).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Klicke um " + name + " zu einer Party einzuladen").create()));

                //----------- Nachricht ----------------\\
                if (online && isFriend)
                    message.append("  ").append("[Nachricht]").color(GOLD).event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/msg " + name + " ")).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Klicke um " + name + " eine private Nachricht zu senden").create()));

                //----------------------------------\\
                player.sendMessage(message.create());
            }
        });
    }

    public static void showPlayerMenu(ProxiedPlayer player, UUID friend) {
        ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), () -> {

            Optional<String> name = PlayersDatabaseHandler.getPlayer(friend);
            ProxiedPlayer p = ProxyServer.getInstance().getPlayer(friend);

            if (name.isPresent()) {
                BeastCubeAPI.getGateway().sendPacket(player, new ShowPlayerPacket(friend, name.get(), hasFriend(player.getUniqueId(), friend), p != null, isRequested(friend, player.getUniqueId()), isRequested(player.getUniqueId(), friend)));
            }
        });
    }

    public static void showFriendsMenu(ProxiedPlayer player) {
        ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), () -> {
            List<UUID> friends = FriendsDatabaseHandler.getFriendList(player.getUniqueId());
            ArrayList<Friend> friendslist = new ArrayList<>();
            for (UUID frienduuid : friends) {
                String name = PlayersDatabaseHandler.getPlayer(frienduuid).get();

                ProxiedPlayer p = ProxyServer.getInstance().getPlayer(frienduuid);

                String currentServer = null;
                if (p != null) {
                    if (p.getServer().getInfo() == null) {
                        currentServer = "Unbekannt";
                    } else {
                        currentServer = p.getServer().getInfo().getName();
                    }
                }
                Friend friend = new Friend(name, frienduuid, currentServer, p != null);
                friendslist.add(friend);
            }
            BeastCubeAPI.getGateway().sendPacket(player, new ShowFriendsPacket(friendslist));
        });
    }

    public static boolean validatePlayer(ProxiedPlayer sender, Optional<UUID> player) {
        if (player.isPresent()) {
            return true;
        } else {
            sender.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Spieler nicht gefunden, falscher Name?").color(RED).create());
            return false;
        }
    }

    public static String colorString(String str) {
        if (str != null) {
            return translateAlternateColorCodes('$', str);
        }
        return null;
    }

}
