/**
 * Copyright © 2014 tuxed <write@imaginarycode.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.
 */
package net.beastcube.api.bungeecord.server.hub;

import com.google.common.collect.ImmutableList;
import lombok.Getter;
import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.beastcube.api.bungeecord.BeastCubeMasterBungeePlugin;
import net.beastcube.api.bungeecord.server.hub.ping.PingStrategy;
import net.beastcube.api.bungeecord.server.hub.ping.bungee.BungeePingStrategy;
import net.beastcube.api.bungeecord.server.hub.ping.zh32.Zh32PingStrategy;
import net.beastcube.api.bungeecord.server.hub.selectors.ServerSelector;
import net.beastcube.api.bungeecord.server.hub.selectors.ServerSelectors;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Current revision: 421daa346ce18400a9e940671515843bdb280c47
 */
public class HubMagic {
    @Getter
    private List<ServerInfo> servers = new CopyOnWriteArrayList<>();
    @Getter
    private PingManager pingManager;
    @Getter
    private ServerSelector serverSelector;
    private ServerSelector configServerSelector;
    @Getter
    private PingStrategy pingStrategy;
    @Getter
    private Configuration configuration;
    @Getter
    HubCommandConfiguration hubCommandConfiguration;
    static private BeastCubeMasterBungeePlugin plugin = BeastCubeMasterBungeePlugin.getInstance();

    public void onEnable() {

        reloadPlugin();

        if (configuration.getBoolean("kicks-lead-to-hub.enabled")) {
            String[] reason = ChatColor.translateAlternateColorCodes('&', configuration.getString("kicks-lead-to-hub.message")).split("\n");
            ServerSelector selector = ServerSelectors.parse(configuration.getString("kicks-lead-to-hub.selector", "sequential"));
            selector = selector == null ? ServerSelectors.SEQUENTIAL.get() : selector;
            ProxyServer.getInstance().getPluginManager().registerListener(plugin, new ReconnectListener(configuration.getStringList("kicks-lead-to-hub.reasons"), ImmutableList.copyOf(reason), selector));
        }

        hubCommandConfiguration = new HubCommandConfiguration(configuration);
        if (configuration.getBoolean("hub-command.enabled")) {

            for (String alias : configuration.getStringList("hub-command.aliases")) {
                ProxyServer.getInstance().getPluginManager().registerCommand(plugin, new HubCommand(alias, hubCommandConfiguration));
            }
        }

        ProxyServer.getInstance().getPluginManager().registerListener(plugin, new ConnectionListener());
        ProxyServer.getInstance().getPluginManager().registerCommand(plugin, new HubMagicCommand());

        ProxyServer.getInstance().registerChannel("HubMagic");
        // Shut Netty up when we disable ourselves.
        Logger.getLogger("io.netty.util.concurrent.DefaultPromise.rejectedExecution").setLevel(Level.OFF);
    }

    public void onDisable() {
        if (pingManager != null)
            pingManager.shutdown();
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    void reloadPlugin() {
        try {
            plugin.getDataFolder().mkdir();
            configuration = createOrLoadConfig();
        } catch (IOException e) {
            throw new RuntimeException("Could not load config", e);
        }

        configurePlugin();
    }

    private Configuration createOrLoadConfig() throws IOException {
        File file = new File(plugin.getDataFolder(), "hub.yml");
        if (!file.exists()) {
            Files.copy(plugin.getResourceAsStream("hub.yml"), file.toPath());
        }
        return ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
    }

    public void addServer(ServerInfo server) {
        this.servers.add(server);
        recalculateServerSelector();
    }

    public void removeServer(ServerInfo server) {
        if (this.servers.contains(server)) {
            this.servers.remove(server);
            recalculateServerSelector();
        }
    }

    public void recalculateServerSelector() {
        // Create our reconnect handler
        if (servers.size() > 1) {
            serverSelector = configServerSelector;
        } else {
            // Technically, since HubMagic is a "headless chicken" in single-hub mode, use the FIRST_AVAILABLE
            // selector as it is best suited to this case.
            serverSelector = ServerSelectors.FIRST_AVAILABLE;
        }
    }

    private void configurePlugin() {
        ServerSelector selector = ServerSelectors.parse(configuration.getString("type"));

        if (selector == null) {
            plugin.getLogger().info("Unrecognized selector " + configuration.getString("type") + ", using lowest.");
            serverSelector = ServerSelectors.LEAST_POPULATED;
        } else {
            serverSelector = selector;
        }
        configServerSelector = serverSelector;

        switch (configuration.getString("ping-strategy")) {
            case "bungee":
                pingStrategy = new BungeePingStrategy();
                break;
            case "fallback":
                pingStrategy = new Zh32PingStrategy();
                break;
            default:
                plugin.getLogger().info("Unrecognized ping strategy " + configuration.getString("ping-strategy") + ", using fallback.");
                pingStrategy = new Zh32PingStrategy();
                break;
        }

        if (pingManager == null)
            pingManager = new PingManager();

        switch (configuration.getString("connection-handler")) {
            case "none":
            case "reconnect":
                break;
            default:
                plugin.getLogger().info("Unrecognized connection handler " + configuration.getString("connection-handler") + ", using reconnect.");
                break;
        }
    }

    @SuppressWarnings("deprecation")
    public void connectToLobby(ProxiedPlayer player) {
        if (player != null) {
            if (BeastCubeAPI.getHubMagic().getServers().contains(player.getServer().getInfo())) {
                player.sendMessage(BeastCubeAPI.getHubMagic().getHubCommandConfiguration().getMessages().get("already_connected"));
                return;
            }

            ServerInfo selected = BeastCubeAPI.getHubMagic().getServerSelector().chooseServer(player);

            if (selected == null) {
                // We might not have a hub available.
                player.sendMessage(BeastCubeAPI.getHubMagic().getHubCommandConfiguration().getMessages().get("no_hubs_available"));
                return;
            }

            player.connect(selected);
        }
    }

}
