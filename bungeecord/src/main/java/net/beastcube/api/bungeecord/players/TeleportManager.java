package net.beastcube.api.bungeecord.players;

import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.beastcube.api.commons.packets.TeleportToLocationPacket;
import net.beastcube.api.commons.packets.TeleportToPlayerPacket;
import net.beastcube.api.commons.util.GlobalLocation;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;

/**
 * @author BeastCube
 */
public class TeleportManager implements Listener {

    public static void silentServerSwitch(ProxiedPlayer player, ServerInfo server) {
        if (!player.getServer().getInfo().equals(server)) {
            player.connect(server);
        }
    }

    public static void teleport(ProxiedPlayer p, ProxiedPlayer to) {
        silentServerSwitch(p, to.getServer().getInfo());
        BeastCubeAPI.getGateway().sendPacketServer(to.getServer().getInfo(), new TeleportToPlayerPacket(p.getUniqueId(), to.getUniqueId()), true);
    }

    public static void teleport(ProxiedPlayer p, GlobalLocation to) {
        silentServerSwitch(p, to.getBungeeServerInfo());
        BeastCubeAPI.getGateway().sendPacketServer(to.getBungeeServerInfo(), new TeleportToLocationPacket(p.getUniqueId(), to), true);
    }

}
