package net.beastcube.api.bungeecord.webapi;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import net.beastcube.api.bungeecord.BeastCubeMaster;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.Provider;

/**
 * Filter for Access-Key header.
 */
@Provider
public class ApiAccessKeyRequestFilter implements ContainerRequestFilter {
    private ApiAccessProvider provider = null;

    @Override
    public ContainerRequest filter(final ContainerRequest request) {
        // Try to obtain accessKey from headers.
        String accessKey = request.getHeaderValue("BCAPI-Access-Key");

        if (accessKey == null) {
            // Try to obtain access key from GET params.
            accessKey = request.getQueryParameters().getFirst("accessKey");

            // If is accessKey still null, filter request.
            if (accessKey == null) {
                // If can't found key, throw exception!
                ResponseBuilder builder = null;
                builder = Response.status(Response.Status.UNAUTHORIZED).entity(
                        new ApiErrorJson(ApiErrorJson.NO_ACCESSKEY_HEADER,
                                "No Access-Key header!").getJson());
                throw new WebApplicationException(builder.build());
            }
        } else {
            request.getQueryParameters().add("accessKey", accessKey);
        }

        if (this.provider == null) {
            this.provider = BeastCubeMaster.getInstance().getComponent(
                    ApiAccessProvider.class);
        }

        if (!this.provider.isValid(accessKey)) {
            ResponseBuilder builder = null;
            builder = Response.status(Response.Status.UNAUTHORIZED).entity(
                    new ApiErrorJson(ApiErrorJson.INVALID_ACCESSKEY,
                            "Invalid Access-Key!").getJson());
            throw new WebApplicationException(builder.build());
        }

        return request;
    }
}
