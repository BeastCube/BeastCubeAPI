package net.beastcube.api.bungeecord;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.beastcube.api.network.SlaveServer;
import net.md_5.bungee.api.plugin.Event;

/**
 * @author BeastCube
 */
@RequiredArgsConstructor
public class SlaveRemovedEvent extends Event {

    @Getter
    private final SlaveServer server;

}
