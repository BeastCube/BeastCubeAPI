package net.beastcube.api.bungeecord.players.friends;

import net.beastcube.api.bungeecord.BeastCubeMasterBungeePlugin;
import net.beastcube.api.bungeecord.players.TeleportManager;
import net.beastcube.api.commons.database.PlayersDatabaseHandler;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.Optional;
import java.util.UUID;

import static net.beastcube.api.bungeecord.players.friends.FriendsHelper.*;
import static net.beastcube.api.commons.database.FriendsDatabaseHandler.hasFriend;
import static net.md_5.bungee.api.ChatColor.AQUA;
import static net.md_5.bungee.api.ChatColor.RED;

public class FriendsCommands extends Command {

    public FriendsCommands() {
        super("friend", "", "friends");
    }

    @SuppressWarnings("deprecation")
    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            sender.sendMessage(colorString("$6Friends:"));
            sender.sendMessage(colorString("$c~~~~~~~~~"));
            sender.sendMessage(colorString("$4Only players may have friends"));
            sender.sendMessage(colorString("$c~~~~~~~~~"));
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) sender;

        if (args.length == 0) {
            showList(player);
            return;
        }

        ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), () -> {

            if (args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("invite")) {
                if (args.length > 1) {
                    if (args[1].equalsIgnoreCase(player.getName())) {
                        player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Du kannst dir nicht selbst eine Freundschaftsanfrage schicken!").color(ChatColor.RED).create());
                        return;
                    }
                    invite(player, PlayersDatabaseHandler.getPlayer(args[1]), args[1]);
                } else {
                    player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Usage: /friends invite <player>").event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/friends invite <player>")).color(ChatColor.RED).create());
                }
            } else if (args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("?")) {
                showHelp(player);
            } else if (args[0].equalsIgnoreCase("acc") || args[0].equalsIgnoreCase("accept")) {
                if (args.length > 1) {
                    acceptInvitation(player, PlayersDatabaseHandler.getPlayer(args[1]), args[1]);
                } else {
                    player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Usage: /friends accept <player>").event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/friends accept <player>")).color(ChatColor.RED).create());
                }
            } else if (args[0].equalsIgnoreCase("ab") || args[0].equalsIgnoreCase("abort")) {
                if (args.length > 1) {
                    abortInvitation(player, PlayersDatabaseHandler.getPlayer(args[1]), args[1]);
                } else {
                    player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Usage: /friends abort <player>").event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/friends abort <player>")).color(ChatColor.RED).create());
                }
            } else if (args[0].equalsIgnoreCase("den") || args[0].equalsIgnoreCase("decline") || args[0].equalsIgnoreCase("deny")) {
                if (args.length > 1) {
                    declineInvitation(player, PlayersDatabaseHandler.getPlayer(args[1]), args[1]);
                } else {
                    player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Usage: /friends decline <player>").event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/friends decline <player>")).color(ChatColor.RED).create());
                }
            } else if (args[0].equalsIgnoreCase("del") || args[0].equalsIgnoreCase("remove")) {
                if (args.length > 1) {
                    deleteFriend(player, PlayersDatabaseHandler.getPlayer(args[1]), args[1]);
                } else {
                    player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Usage: /friends remove <player>").event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/friends remove <player>")).color(ChatColor.RED).create());
                }
            } else if (args[0].equalsIgnoreCase("req") || args[0].equalsIgnoreCase("requests")) {
                showRequests(player);
            } else if (args[0].equalsIgnoreCase("inv") || args[0].equalsIgnoreCase("invitations")) {
                showInvitations(player);
            } else if (args[0].equalsIgnoreCase("show")) { //TODO
                if (args.length > 1) {
                    if (args[1].equalsIgnoreCase(player.getName())) {
                        player.sendMessage(colorString("$4Du kannst dich nicht selbst zeigen!"));
                        return;
                    }
                    showPlayer(player, PlayersDatabaseHandler.getPlayer(args[1]), args[1]);
                } else {
                    player.sendMessage(colorString("$bUsage: /friends show <player>"));
                }
            } else if (args[0].equalsIgnoreCase("tp") || args[0].equalsIgnoreCase("teleport")) { //TODO remove command (people can cheat themself to other players while ingame)
                if (args.length > 1) {
                    Optional<UUID> friend = PlayersDatabaseHandler.getPlayer(args[1]);
                    if (FriendsHelper.validatePlayer(player, friend)) {
                        if (!hasFriend(player.getUniqueId(), friend.get())) {
                            player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(args[1]).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + args[1])).event(showProfile).append(" ist nicht dein Freund!").color(RED).create());
                            return;
                        } else if (friend.get() == null) {
                            player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(args[1]).color(ChatColor.AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + args[1])).append(" ist Offline!").color(ChatColor.RED).create());
                            return;
                        }
                        TeleportManager.teleport(player, ProxyServer.getInstance().getPlayer(friend.get()));
                    }
                } else {
                    player.sendMessage(colorString("$bUsage: /friends teleport <player>"));
                }
            } else {
                showHelp(player);
            }
        });
    }
}
