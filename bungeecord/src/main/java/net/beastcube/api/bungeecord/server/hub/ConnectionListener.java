/**
 * Copyright © 2014 tuxed <write@imaginarycode.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.
 */
package net.beastcube.api.bungeecord.server.hub;

import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.md_5.bungee.api.AbstractReconnectHandler;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class
        ConnectionListener implements Listener {
    @EventHandler
    public void onServerConnect(ServerConnectEvent event) {
        String type = BeastCubeAPI.getHubMagic().getConfiguration().getString("connection-handler");

        switch (type) {
            case "none":
                return;
            default:
                // Send all players that join to the hub
                if (event.getPlayer().getServer() != null)
                    return;

                ServerInfo forcedHost = AbstractReconnectHandler.getForcedHost(event.getPlayer().getPendingConnection());
                if (forcedHost != null) {
                    return;
                }

                ServerInfo chosenHub = BeastCubeAPI.getHubMagic().getServerSelector().chooseServer(event.getPlayer());

                if (chosenHub == null) {
                    // No hubs are available
                    event.getPlayer().disconnect(new ComponentBuilder("Keine Hubs verfügbar!\n Bitte versuche es später erneut").color(ChatColor.RED).create());
                    return;
                }

                event.setTarget(chosenHub);
                break;
        }
    }
}
