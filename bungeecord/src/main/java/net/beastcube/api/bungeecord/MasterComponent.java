package net.beastcube.api.bungeecord;

import net.beastcube.api.commons.AbstractComponent;
import net.beastcube.api.commons.configuration.Configuration;

/**
 * Interface that represents component in MasterServer.
 */
public abstract class MasterComponent extends AbstractComponent {
    protected BeastCubeMaster master;

    /**
     * Returns current {@link BeastCubeMaster} instance.
     */
    public BeastCubeMaster getMaster() {
        return this.master;
    }

    void __initConfig(final Configuration configuration) {
        this._initConfig(configuration);
    }
}
