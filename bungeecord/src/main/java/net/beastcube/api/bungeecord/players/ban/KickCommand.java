package net.beastcube.api.bungeecord.players.ban;

import com.google.common.base.Joiner;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

import static net.beastcube.api.commons.players.bans.Bans.showHelp;
import static net.md_5.bungee.api.ChatColor.*;

/**
 * @author BeastCube
 */
public class KickCommand extends Command {

    public KickCommand() {
        super("kick");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            sender.sendMessage(new TextComponent(ChatColor.RED + "Please execute this as a player!"));
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) sender;

        if (args.length >= 2) {
            String username = args[0];

            ProxiedPlayer kickedPlayer = ProxyServer.getInstance().getPlayer(username);
            if (kickedPlayer == null) {
                sender.sendMessage(new ComponentBuilder("Dieser Spieler ist nicht online!").color(RED).create());
            } else {
                String reason = Joiner.on(" ").join(Arrays.asList(args).subList(1, args.length));
                if (!StringUtils.isWhitespace(reason)) {
                    kickedPlayer.disconnect(new ComponentBuilder("Du wurdest vom Server gekickt\n").color(RED).append("Grund: ").color(DARK_AQUA).append(reason).color(YELLOW).append("\nAchte auf dein Verhalten! Dies kann zu einem Ban führen!").color(AQUA).create());
                    player.sendMessage(new ComponentBuilder("Du hast ").color(ChatColor.GREEN).append(username).color(ChatColor.AQUA).append(" gekickt: ").color(ChatColor.GREEN).append(reason).color(ChatColor.GRAY).create());
                    //TODO Teammembers notify???
                } else {
                    player.sendMessage(new ComponentBuilder("Der Grund ist leer! Bitte gib einen gültigen an!").color(ChatColor.RED).create());
                }
            }
        } else {
            showHelp(player);
        }
    }

}
