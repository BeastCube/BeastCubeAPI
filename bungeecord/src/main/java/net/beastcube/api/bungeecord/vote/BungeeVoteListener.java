package net.beastcube.api.bungeecord.vote;

import com.vexsoftware.votifier.model.VotifierEvent;
import net.beastcube.api.bungeecord.BeastCubeMasterBungeePlugin;
import net.beastcube.api.commons.database.PlayersDatabaseHandler;
import net.beastcube.api.commons.players.Coins;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.Optional;
import java.util.UUID;

import static net.md_5.bungee.api.ChatColor.AQUA;
import static net.md_5.bungee.api.ChatColor.GREEN;

/**
 * @author BeastCube
 */
public class BungeeVoteListener implements Listener {

    int coins = 50;

    @EventHandler
    public void onVote(VotifierEvent e) {
        String username = e.getVote().getUsername();
        try {
            ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), () -> {
                Optional<UUID> uuid = PlayersDatabaseHandler.getPlayer(username);
                if (uuid.isPresent()) {
                    Coins.addCoins(uuid.get(), coins);
                    ProxiedPlayer p = ProxyServer.getInstance().getPlayer(uuid.get());
                    if (p != null) {
                        p.sendMessage(new ComponentBuilder("Danke fürs voten, ").color(GREEN).append(p.getName()).color(AQUA).append(". Du hast " + coins + " Coins erhalten!").color(GREEN).create());
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
