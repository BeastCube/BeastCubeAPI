package net.beastcube.api.bungeecord.players.ban;

import com.google.common.base.Joiner;
import net.beastcube.api.commons.database.PlayersDatabaseHandler;
import net.beastcube.api.commons.players.bans.Ban;
import net.beastcube.api.commons.players.bans.Bans;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.apache.commons.lang3.StringUtils;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import static net.beastcube.api.commons.players.bans.Bans.showHelp;

/**
 * @author BeastCube
 */
public class BanCommand extends Command {

    public BanCommand() {
        super("ban");
    }


    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            sender.sendMessage(new TextComponent(ChatColor.RED + "Please execute this as a player!"));
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) sender;

        if (args.length >= 2) {
            String username = args[0];

            Optional<UUID> uuid = PlayersDatabaseHandler.getPlayer(username);
            if (uuid.isPresent()) {
                if (!Bans.isBanned(uuid.get())) {
                    String reason = Joiner.on(" ").join(Arrays.asList(args).subList(1, args.length));
                    if (!StringUtils.isWhitespace(reason)) {
                        Ban ban = new Ban(uuid.get(), player.getUniqueId(), reason, new Timestamp(new Date().getTime()), new Timestamp(0), true);
                        Bans.addBan(ban);
                        player.sendMessage(new ComponentBuilder("Du hast ").color(ChatColor.GREEN).append(username).color(ChatColor.AQUA).append(" PERMANENT ").color(ChatColor.YELLOW).append(" gebannt: ").color(ChatColor.GREEN).append(reason).color(ChatColor.GRAY).create());
                        //TODO Teammembers notify
                    } else {
                        player.sendMessage(new ComponentBuilder("Der Grund ist leer! Bitte gib einen gültigen an!").color(ChatColor.RED).create());
                    }
                } else {
                    player.sendMessage(new ComponentBuilder("Dieser Spieler hat bereits einen aktiven Ban!").color(ChatColor.RED).create());
                }
            } else {
                sender.sendMessage(new ComponentBuilder("Spieler ").color(ChatColor.RED).append(username).color(ChatColor.AQUA).append(" nicht gefunden").color(ChatColor.RED).create());
            }
        } else {
            showHelp(player);
        }
    }

}
