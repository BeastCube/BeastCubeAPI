/**
 * Copyright © 2014 tuxed <write@imaginarycode.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.
 */
package net.beastcube.api.bungeecord.server.hub;

import lombok.Getter;
import lombok.NonNull;
import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.beastcube.api.bungeecord.BeastCubeMasterBungeePlugin;
import net.beastcube.api.bungeecord.server.hub.ping.PingResult;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.scheduler.ScheduledTask;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class PingManager {
    @Getter
    private final Map<ServerInfo, PingResult> pings = new ConcurrentHashMap<>();
    private final AtomicBoolean shutdown = new AtomicBoolean(false);
    private ScheduledTask task;

    PingManager() {
        task = ProxyServer.getInstance().getScheduler().schedule(BeastCubeMasterBungeePlugin.getInstance(), new Runnable() {
            @Override
            public void run() {
                if (shutdown.get())
                    return;

                for (final ServerInfo info : BeastCubeAPI.getHubMagic().getServers()) {
                    BeastCubeAPI.getHubMagic().getPingStrategy().ping(info, (pingResult, throwable) -> {
                        // NB: throwable can be null and we have a DOWN pingresult
                        // so always use the pingresult
                        if (pingResult.isDown()) {
                            pings.remove(info);
                        } else {
                            pings.put(info, pingResult);
                        }
                    });
                }
            }
        }, 0, BeastCubeAPI.getHubMagic().getConfiguration().getInt("ping-duration", 3), TimeUnit.SECONDS);
    }

    public void shutdown() {
        if (task != null) {
            shutdown.set(true);
            task.cancel();
            task = null;
        }
    }

    public ServerInfo firstAvailable(@NonNull ProxiedPlayer player) {
        for (Map.Entry<ServerInfo, PingResult> entry : pings.entrySet()) {
            if (entry.getValue() == null)
                continue;

            if (entry.getValue().getPlayerCount() >= entry.getValue().getPlayerMax())
                continue;

            if (player.getServer() != null && player.getServer().getInfo().equals(entry.getKey()))
                continue;

            return entry.getKey();
        }
        return null;
    }

    public ServerInfo lowestPopulation(@NonNull ProxiedPlayer player) {
        Map.Entry<ServerInfo, PingResult> lowest = null;

        for (Map.Entry<ServerInfo, PingResult> entry : pings.entrySet()) {
            if (entry.getValue() == null)
                continue;

            if (entry.getValue().getPlayerCount() >= entry.getValue().getPlayerMax())
                continue;

            if (player.getServer() != null && player.getServer().getInfo().equals(entry.getKey()))
                continue;

            if (lowest == null || lowest.getValue().getPlayerCount() > entry.getValue().getPlayerCount()) {
                lowest = entry;
            }
        }

        return lowest != null ? lowest.getKey() : null;
    }

    public boolean consideredAvailable(@NonNull ServerInfo serverInfo, @NonNull ProxiedPlayer player) {
        if (!pings.containsKey(serverInfo))
            return false;

        PingResult ping = pings.get(serverInfo);

        return (player.getServer() == null || !player.getServer().getInfo().equals(serverInfo)) &&
                ping != null && !ping.isDown() && ping.getPlayerCount() <= ping.getPlayerMax();
    }
}
