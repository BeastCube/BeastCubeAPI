package net.beastcube.api.bungeecord.server.pinghandler;

import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * @author BeastCube
 */
public class PingHandler implements Listener {

    List<String> lines = Arrays.asList(ChatColor.GOLD + "Bedwars", ChatColor.GOLD + "Free for all", ChatColor.GOLD + "Survival Games");
    String firstLine = ChatColor.GREEN + "BeastCube Network " + ChatColor.RED + " | " + ChatColor.YELLOW + "Erwecke das Beast in dir\n";
    static String motd = "&fFree for all &c| &fBedwars &c| &fSurvivalGames";

    public static String getMotd() {
        return motd;
    }

    public static void setMotd(String motd) {
        PingHandler.motd = motd;
    }

    @EventHandler
    public void ping(ProxyPingEvent e) {
        boolean mainteance = BeastCubeAPI.getMainteanceManager().isEnabled();
        if (mainteance) {
            e.getResponse().setVersion(BeastCubeAPI.getMainteanceManager().getProtocol());
        }
        e.getResponse().setDescription(firstLine + (mainteance ? BeastCubeAPI.getMainteanceManager().getMOTD() : ChatColor.translateAlternateColorCodes('&', motd)));
        ServerPing.Players players = e.getResponse().getPlayers();

        players.setMax(50);
        ServerPing.PlayerInfo[] sample = new ServerPing.PlayerInfo[lines.size()];
        for (int i = 0; i < sample.length; i++)
            sample[i] = new ServerPing.PlayerInfo(lines.get(i), UUID.fromString("0-0-0-0-0"));
        players.setSample(sample);

        e.getResponse().setPlayers(players);
    }

}
