package net.beastcube.api.bungeecord;

import net.md_5.bungee.api.plugin.Plugin;

import java.lang.reflect.Field;

public class BeastCubeMasterBungeePlugin extends Plugin {

    private static BeastCubeMasterBungeePlugin instance;

    public BeastCubeMasterBungeePlugin() {
        if (BeastCubeMasterBungeePlugin.instance == null) {
            BeastCubeMasterBungeePlugin.instance = this;
        } else {
            throw new RuntimeException("Singleton alredy initialized!");
        }
    }

    @Override
    public void onEnable() {
        super.onEnable();

        this.getLogger().info("[BOOT] Determinating BungeeCord patching status...");
        Class<?> bsm = null;
        try {
            bsm = Class.forName("net.md_5.bungee.BungeeSecurityManager");
        } catch (ClassNotFoundException e1) {
            //e1.printStackTrace();
            try {
                this.getLogger()
                        .severe("Determination of BungeeCord patching status failed! Things may not work properly!");
                throw new RuntimeException(
                        "Class BungeeSecurityManager not found! Jar is probably not Bungee or modified bungee. ");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        if (bsm != null) {
            boolean patched = false;
            for (Field f : bsm.getDeclaredFields()) {
                if (f.getName().equals("PATCHED")) {
                    try {
                        if (f.getInt(null) == 12) {
                            patched = true;
                        }
                    } catch (IllegalArgumentException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (patched) {
                this.getLogger().info(
                        "[BOOT] BungeeCord seems to be patched! Have a nice day!");
            } else {
                this.getLogger()
                        .info("[BOOT] Sorry, but it seems like your BungeeCord jar file is not patched.");
                this.getLogger()
                        .info("[BOOT] BungeeCord does have SecurityManager that denies all actions except Bungee API calls to prevent access to machine by third party code.");
                this.getLogger()
                        .info("[BOOT] This disallows using BeastCubeMaster on Bungee. Please use our patcher, to patch BungeeSecurityManager, to allow BeastCubeMaster work properly.");
                this.getLogger()
                        .info("[BOOT] BeastCubeMaster may still work, but functionality can be limited or it may crash unexceptably!");
            }
        }

        this.createMaster();
        BeastCubeAPI.getInstance().onEnable();
    }

    @Override
    public void onDisable() {
        super.onDisable();
        this.getLogger().info("[BOOT] Sending shutdown to BeastCubeMaster...");
        BeastCubeMaster.getInstance().shutdown();
        BeastCubeAPI.getInstance().onDisable();
    }

    public void createMaster() {
        // Get instance for first time - create BeastCubeMaster.
        BeastCubeMaster.init(this.getDataFolder());
        // Enable all components.
        BeastCubeMaster.getInstance().start();
    }

    public static BeastCubeMasterBungeePlugin getInstance() {
        return BeastCubeMasterBungeePlugin.instance;
    }
}
