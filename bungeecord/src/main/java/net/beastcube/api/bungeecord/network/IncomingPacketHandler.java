package net.beastcube.api.bungeecord.network;

import com.ikeirnez.pluginmessageframework.packet.PacketHandler;
import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.beastcube.api.bungeecord.minigames.MinigamesManager;
import net.beastcube.api.bungeecord.players.friends.FriendsHelper;
import net.beastcube.api.commons.minigames.MinigameInstance;
import net.beastcube.api.commons.packets.*;
import net.beastcube.api.commons.packets.friends.ShowFriendsRequestPacket;
import net.beastcube.api.commons.packets.friends.ShowPlayerRequestPacket;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author BeastCube
 */
public class IncomingPacketHandler {

    @PacketHandler
    public void onShowPlayerRequestPacket(ProxiedPlayer proxiedPlayer, ShowPlayerRequestPacket packet) {
        UUID uuid = packet.getUniqueId();
        FriendsHelper.showPlayerMenu(proxiedPlayer, uuid);
    }

    @PacketHandler
    public void onDispatchCmdPacket(ProxiedPlayer proxiedPlayer, DispatchCmdPacket packet) {
        String command = packet.getCommand();
        ProxyServer.getInstance().getPluginManager().dispatchCommand(proxiedPlayer, command);
    }

    @PacketHandler
    public void onConnectToLobby(ProxiedPlayer proxiedPlayer, ConnectToLobbyPacket packet) {
        BeastCubeAPI.getHubMagic().connectToLobby(proxiedPlayer);
    }

    @PacketHandler
    public void onShowFriendsRequest(ProxiedPlayer proxiedPlayer, ShowFriendsRequestPacket packet) {
        FriendsHelper.showFriendsMenu(proxiedPlayer);
    }

    @PacketHandler
    public void onUpdateScoreboardRequestPacket(ProxiedPlayer proxiedPlayer, UpdateScoreboardRequestPacket packet) {
        BeastCubeAPI.sendUpdateScoreboardPacket(proxiedPlayer);
    }

    @PacketHandler
    public void onMinigamesUpdateRequestPacket(ProxiedPlayer proxiedPlayer, MinigamesUpdateRequestPacket packet) {
        List<MinigameInstance> list = new ArrayList<>(MinigamesManager.getMinigameInstances().values());
        BeastCubeAPI.getGateway().sendPacket(proxiedPlayer, new MinigamesUpdatePacket(list));
    }

}