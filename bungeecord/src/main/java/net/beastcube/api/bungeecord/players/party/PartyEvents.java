package net.beastcube.api.bungeecord.players.party;

import net.beastcube.api.bungeecord.BeastCubeMasterBungeePlugin;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * @author BeastCube
 */
@SuppressWarnings("deprecation")
public class PartyEvents implements Listener {
    public PartyEvents() {
        ProxyServer.getInstance().getPluginManager().registerListener(BeastCubeMasterBungeePlugin.getInstance(), this);
    }

    @EventHandler
    public void onSwitch(ServerSwitchEvent e) {
        ProxiedPlayer p = e.getPlayer();
        if (Party.partyführer.contains(p.getName())) {
            ServerInfo si = p.getServer().getInfo();

            p.sendMessage(Party.pr + "§eDie Party hat den Server §b" + si.getName() + "§e betreten!");
            for (ProxiedPlayer x : ProxyServer.getInstance().getPlayers()) {
                if ((Party.inparty.containsKey(x.getName())) && (Party.inparty.get(x.getName()).equals(p.getName()))) {
                    x.sendMessage(Party.pr + "§eDie Party hat den Server §b" + si.getName() + "§e betreten!");
                    x.connect(si);
                }
            }
        }
    }

    @EventHandler
    public void leave(PlayerDisconnectEvent e) {
        ProxiedPlayer p = e.getPlayer();
        if (Party.inparty.containsKey(p.getName())) {
            Party.leave(p);
        }
    }
}