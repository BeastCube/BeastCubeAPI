package net.beastcube.api.bungeecord.players;

import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.beastcube.api.bungeecord.players.friends.FriendsHelper;
import net.beastcube.api.commons.database.FriendsDatabaseHandler;
import net.beastcube.api.commons.packets.PlaySoundPacket;
import net.beastcube.api.commons.permissions.Permissions;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import static net.md_5.bungee.api.ChatColor.*;

/**
 * @author BeastCube
 */
public class PrivateMessageCommand extends Command {

    public PrivateMessageCommand() {
        super("msg", "", "whisper", "pn", "pm", "tell");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) sender;

        if (args.length > 1) {
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
            if (target == null) {
                player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(args[0]).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + args[0])).event(FriendsHelper.showProfile).append(" ist Offline!").color(RED).create());
                return;
            }
            if (!FriendsDatabaseHandler.hasFriend(player.getUniqueId(), target.getUniqueId()) && player.hasPermission(Permissions.PM_EVERYBODY)) {
                player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(target.getName()).color(AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + target.getName())).event(FriendsHelper.showProfile).append(" ist nicht dein Freund!").color(RED).create());
                return;
            }
            String msg = "";
            boolean space = false;
            for (int i = 1; i < args.length; i++) {
                if (space) {
                    msg += " ";
                } else {
                    space = true;
                }
                msg += args[i];
            }

            player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Ich").color(RED).append(" » ").color(GRAY).append(target.getName()).color(RED).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + target.getName())).event(FriendsHelper.showProfile).append(": ").color(WHITE).append(msg).color(WHITE).create());
            target.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(player.getName()).color(RED).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + player.getName())).event(FriendsHelper.showProfile).append(" » ").color(GRAY).append("mir").color(RED).append(": ").color(WHITE).append(msg).color(WHITE).create());
            BeastCubeAPI.getGateway().sendPacket(target, new PlaySoundPacket("note.pling", 1, 2));
        } else {
            player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Private Nachricht: /msg <Spieler> <Nachricht>").color(RED).create());
        }
    }

}
