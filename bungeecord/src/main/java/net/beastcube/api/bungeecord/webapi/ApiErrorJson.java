package net.beastcube.api.bungeecord.webapi;

import net.beastcube.api.commons.Providers;

/**
 * Class used for generating json errors.
 */
public class ApiErrorJson {
    public static final int NO_ACCESSKEY_HEADER = 1;
    public static final int INVALID_ACCESSKEY = 2;

    public int error;
    public String message;

    public ApiErrorJson(final int error, final String message) {
        this.error = error;
        this.message = message;
    }

    public String getJson() {
        return Providers.JSON.toJson(this);
    }
}
