package net.beastcube.api.bungeecord.server.mainteance;

import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.beastcube.api.commons.permissions.Permissions;
import net.beastcube.api.commons.permissions.Roles;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Listener class to handle all events, including custom ones.
 *
 * @author BeastCube
 */
@SuppressWarnings("unused")
public class MainteanceListener implements Listener {

    @EventHandler
    public void join(PreLoginEvent e) {
        if (BeastCubeAPI.getMainteanceManager().isEnabled()) {
            String name = e.getConnection().getName();
            if (!BeastCubeAPI.getMainteanceManager().getWhitelist().contains(name) && Roles.getRole(e.getConnection().getUniqueId()).hasPermission(Permissions.MAINTENANCE_BYPASS)) {
                e.setCancelled(true);
                e.setCancelReason(BeastCubeAPI.getMainteanceManager().getKickMessage());
            }
        }
    }

    @EventHandler
    public void update(WhitelistUpdateEvent event) {
        if (BeastCubeAPI.getMainteanceManager().isEnabled()) {
            if (event.getOperation() == WhitelistUpdateEvent.Operation.REMOVE) {
                ProxiedPlayer player;

                if ((player = ProxyServer.getInstance().getPlayer(event.getName())) != null) {
                    if (!player.hasPermission(Permissions.MAINTENANCE_BYPASS)) {
                        BeastCubeAPI.getMainteanceManager().kick(player);
                    }
                }
            }
        }
    }

}