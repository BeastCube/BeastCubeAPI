package net.beastcube.api.bungeecord.server;

import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.beastcube.api.bungeecord.server.pinghandler.PingHandler;
import net.beastcube.api.commons.permissions.Permissions;
import net.beastcube.api.commons.util.Constants;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import static net.md_5.bungee.api.ChatColor.RED;

/**
 * @author BeastCube
 */
public class MotdCommand extends Command {

    public MotdCommand() {
        super("motd");
    }


    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) sender;

        if (player.hasPermission(Permissions.MOTD)) {
            if (args.length > 0) {
                String msg = "";
                boolean space = false;
                for (String arg : args) {
                    if (space) {
                        msg += " ";
                    } else {
                        space = true;
                    }
                    msg += arg;
                }
                String motd = ChatColor.translateAlternateColorCodes('&', msg);
                BeastCubeAPI.config.setMotd(motd);
                BeastCubeAPI.config.saveConfig();
                PingHandler.setMotd(motd);
                ProxyServer.getInstance().broadcast(new TextComponent(ChatColor.GREEN + "Motd geändert: " + ChatColor.RESET + motd));

            } else {
                player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Motd: /motd <Nachricht>").color(RED).create());
            }
        } else {
            player.sendMessage(ChatMessageType.CHAT, TextComponent.fromLegacyText(Constants.NOPERMISSION));
        }
    }

}
