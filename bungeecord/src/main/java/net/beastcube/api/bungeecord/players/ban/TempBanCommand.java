package net.beastcube.api.bungeecord.players.ban;

import com.google.common.base.Joiner;
import net.beastcube.api.commons.database.PlayersDatabaseHandler;
import net.beastcube.api.commons.players.bans.Ban;
import net.beastcube.api.commons.players.bans.Bans;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.apache.commons.lang3.StringUtils;

import java.sql.Timestamp;
import java.util.*;

import static net.beastcube.api.commons.players.bans.Bans.showHelp;

/**
 * @author BeastCube
 */
public class TempBanCommand extends Command {

    public TempBanCommand() {
        super("tempban");
    }

    List<String> monthStrings = Arrays.asList("Month", "Months", "Monat", "Monate");
    List<String> weekStrings = Arrays.asList("Week", "Weeks", "Woche", "Wochen");
    List<String> dayStrings = Arrays.asList("Day", "Days", "Tag", "Tage");
    List<String> hourStrings = Arrays.asList("Hour", "Hours", "Stunde", "Stunden");
    List<String> minuteStrings = Arrays.asList("Minute", "Minutes", "Minute", "Minuten");


    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            sender.sendMessage(new TextComponent(ChatColor.RED + "Please execute this as a player!"));
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) sender;

        if (args.length >= 4) {
            String username = args[0];

            Optional<UUID> uuid = PlayersDatabaseHandler.getPlayer(username);
            if (uuid.isPresent()) {
                if (!Bans.isBanned(uuid.get())) {
                    try {
                        int time = Integer.parseInt(args[1]);

                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(System.currentTimeMillis());
                        String timeFormat = args[2];
                        List<String> strings;

                        if (timeFormat.equals("M") || monthStrings.stream().filter(s -> s.equalsIgnoreCase(timeFormat)).findFirst().isPresent()) {
                            cal.add(Calendar.MONTH, time);
                            strings = monthStrings;
                        } else if (timeFormat.equalsIgnoreCase("w") || weekStrings.stream().filter(s -> s.equalsIgnoreCase(timeFormat)).findFirst().isPresent()) {
                            cal.add(Calendar.WEEK_OF_YEAR, time);
                            strings = weekStrings;
                        } else if (timeFormat.equalsIgnoreCase("d") || dayStrings.stream().filter(s -> s.equalsIgnoreCase(timeFormat)).findFirst().isPresent()) {
                            cal.add(Calendar.DAY_OF_MONTH, time);
                            strings = dayStrings;
                        } else if (timeFormat.equalsIgnoreCase("h") || hourStrings.stream().filter(s -> s.equalsIgnoreCase(timeFormat)).findFirst().isPresent()) {
                            cal.add(Calendar.HOUR, time);
                            strings = hourStrings;
                        } else if (timeFormat.equals("m") || minuteStrings.stream().filter(s -> s.equalsIgnoreCase(timeFormat)).findFirst().isPresent()) {
                            cal.add(Calendar.MINUTE, time);
                            strings = minuteStrings;
                        } else {
                            sender.sendMessage(new ComponentBuilder("Das Zeitformat ").color(ChatColor.RED).append(timeFormat).color(ChatColor.AQUA).append(" ist nicht gültig!").color(ChatColor.RED).create());
                            return;
                        }

                        String reason = Joiner.on(" ").join(Arrays.asList(args).subList(3, args.length));
                        if (!StringUtils.isWhitespace(reason)) {
                            Ban ban = new Ban(uuid.get(), player.getUniqueId(), reason, new Timestamp(new Date().getTime()), new Timestamp(cal.getTime().getTime()), false);
                            Bans.addBan(ban);
                            player.sendMessage(new ComponentBuilder("Du hast ").color(ChatColor.GREEN).append(username).color(ChatColor.AQUA).append(" für ").color(ChatColor.GREEN).append(time + " " + (time == 1 ? strings.get(2) : strings.get(3))).color(ChatColor.YELLOW).append(" gebannt: ").color(ChatColor.GREEN).append(reason).color(ChatColor.GRAY).create());
                            //TODO Teammembers notify
                        } else {
                            player.sendMessage(new ComponentBuilder("Der Grund ist leer! Bitte gib einen gültigen an!").color(ChatColor.RED).create());
                        }
                    } catch (NumberFormatException e) {
                        sender.sendMessage(new ComponentBuilder("Die Zeit ").color(ChatColor.RED).append(args[1]).color(ChatColor.AQUA).append(" ist keine gültige Zahl!").color(ChatColor.RED).create());
                    }
                } else {
                    player.sendMessage(new ComponentBuilder("Dieser Spieler hat bereits einen aktiven Ban!").color(ChatColor.RED).create());
                }
            } else {
                sender.sendMessage(new ComponentBuilder("Spieler ").color(ChatColor.RED).append(username).color(ChatColor.AQUA).append(" nicht gefunden").color(ChatColor.RED).create());
            }
        } else {
            showHelp(player);
        }
    }

}
