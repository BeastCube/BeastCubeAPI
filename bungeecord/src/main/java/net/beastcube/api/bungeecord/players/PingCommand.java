package net.beastcube.api.bungeecord.players;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import static net.md_5.bungee.api.ChatColor.*;

/**
 * @author BeastCube
 */
public class PingCommand extends Command {

    public PingCommand() {
        super("ping");
    }


    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) sender;
        if (args.length == 0) {
            player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Dein Ping: ").color(YELLOW).append(String.valueOf(player.getPing())).color(GREEN).append("ms").color(GRAY).create());
        } else if (args.length > 0) {
            ProxiedPlayer pingplayer = ProxyServer.getInstance().getPlayer(args[0]);
            if (pingplayer == null) {
                player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Dieser Spieler ist nicht online!").color(RED).create());
            } else {
                player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(args[0] + "'s Ping: ").color(YELLOW).append(String.valueOf(pingplayer.getPing())).color(GREEN).append("ms").color(GRAY).create());
            }
        }
    }

}
