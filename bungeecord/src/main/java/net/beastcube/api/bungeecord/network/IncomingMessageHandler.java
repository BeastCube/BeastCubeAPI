package net.beastcube.api.bungeecord.network;

import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.beastcube.api.bungeecord.minigames.MinigamesManager;
import net.beastcube.api.commons.minigames.MinigameInstance;
import net.beastcube.api.commons.minigames.MinigameUpdateMessage;
import net.beastcube.api.network.MessageHandler;
import net.beastcube.api.network.ServerInfo;
import net.beastcube.api.network.message.AddLobbyMessage;
import net.md_5.bungee.api.ProxyServer;

/**
 * @author BeastCube
 */
public class IncomingMessageHandler {

    @MessageHandler
    public void onMinigameUpdateMessage(MinigameUpdateMessage message) {
        if (message.isRemove()) {
            MinigamesManager.removeServer(message.getServerName());
        } else {
            MinigamesManager.put(message.getServerName(), new MinigameInstance(message.getServerName(), message.getArenaName(), message.getType(), message.getPlayers(), message.getMaxPlayers(), message.getState(), message.isJoinIngame()));
        }
    }

    @MessageHandler
    public void onAddLobbyMessage(ServerInfo server, AddLobbyMessage message) {
        BeastCubeAPI.getHubMagic().addServer(ProxyServer.getInstance().getServerInfo(server.getName())); //TODO NPE, because hubmagic isn't initialized first. combine beastcubeapi and beastubemaster
    }

}
