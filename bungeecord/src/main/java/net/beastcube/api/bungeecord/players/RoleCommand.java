package net.beastcube.api.bungeecord.players;

import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.beastcube.api.commons.database.PlayersDatabaseHandler;
import net.beastcube.api.commons.packets.RoleChangedPacket;
import net.beastcube.api.commons.permissions.Permissions;
import net.beastcube.api.commons.permissions.Role;
import net.beastcube.api.commons.permissions.Roles;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.Iterator;
import java.util.Optional;
import java.util.UUID;

import static net.md_5.bungee.api.ChatColor.WHITE;

/**
 * @author BeastCube
 */
public class RoleCommand extends Command {

    public RoleCommand() {
        super("role");
    }


    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender.hasPermission(Permissions.ROLE)) {
            if (args.length > 0) {
                if (args[0].equalsIgnoreCase("get")) {
                    if (args.length > 1) {
                        String username = args[1];
                        Optional<UUID> uuid = PlayersDatabaseHandler.getPlayer(username);
                        if (uuid.isPresent()) {
                            Role role = Roles.getRole(uuid.get());
                            sender.sendMessage(new ComponentBuilder(username).color(ChatColor.AQUA).append("'s Rolle: ").color(ChatColor.GREEN).append(role.getName()).color(ChatColor.getByChar(role.getColor().getChar())).create());
                        } else {
                            sender.sendMessage(new ComponentBuilder("Spieler ").color(ChatColor.RED).append(username).color(ChatColor.AQUA).append(" nicht gefunden").color(ChatColor.RED).create());
                        }
                    } else {
                        sender.sendMessage(new ComponentBuilder("Benutzung: /role get <player>").color(ChatColor.RED).create());
                    }
                } else if (args[0].equalsIgnoreCase("set")) {
                    if (args.length > 1) {
                        String username = args[1];
                        Optional<UUID> uuid = PlayersDatabaseHandler.getPlayer(username);
                        if (uuid.isPresent()) {
                            if (args.length > 2) {
                                Optional<Role> role = Roles.byId(args[2]);
                                if (role.isPresent()) {
                                    Roles.setRole(uuid.get(), role.get());
                                    sender.sendMessage(new ComponentBuilder("Dem Spieler ").color(ChatColor.GREEN).append(username).color(ChatColor.AQUA).append(" wurde erfolgreich die Rolle ").color(ChatColor.GREEN).append(role.get().getName()).color(ChatColor.getByChar(role.get().getColor().getChar())).append(" zugewiesen!").color(ChatColor.GREEN).create());
                                    ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid.get());
                                    if (player != null) {
                                        player.sendMessage(new ComponentBuilder("Du hast die Rolle ").color(ChatColor.GREEN).append(role.get().getName()).color(ChatColor.getByChar(role.get().getColor().getChar())).append(" erhalten!").color(ChatColor.GREEN).create());
                                        BeastCubeAPI.getGateway().sendPacket(player, new RoleChangedPacket(player.getUniqueId()));
                                    }
                                    return;
                                }
                            }
                            sender.sendMessage(new ComponentBuilder("Wähle die Rolle aus:").color(ChatColor.RED).create());
                            ComponentBuilder b = new ComponentBuilder("");
                            Iterator<Role> iter = Roles.roles.iterator();
                            while (iter.hasNext()) {
                                Role r = iter.next();
                                b.append(r.getName()).color(ChatColor.getByChar(r.getColor().getChar())).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/role set " + username + " " + r.getId()));
                                if (iter.hasNext())
                                    b.append(", ").color(WHITE);
                            }
                            sender.sendMessage(b.create());
                        } else {
                            sender.sendMessage(new ComponentBuilder("Spieler ").color(ChatColor.RED).append(username).color(ChatColor.AQUA).append(" nicht gefunden").color(ChatColor.RED).create());
                        }
                    } else {
                        sender.sendMessage(new ComponentBuilder("Benutzung: /role set <player>").color(ChatColor.RED).create());
                    }
                } else if (args[0].equalsIgnoreCase("remove")) {
                    if (args.length > 1) {
                        String username = args[1];
                        Optional<UUID> uuid = PlayersDatabaseHandler.getPlayer(username);
                        if (uuid.isPresent()) {
                            Role lastRole = Roles.getRole(uuid.get());
                            Roles.removeRole(uuid.get());
                            sender.sendMessage(new ComponentBuilder(username).color(ChatColor.AQUA).append("'s Rolle wurde erfolgreich entfernt!").color(ChatColor.GREEN).create());
                            ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid.get());
                            if (player != null) {
                                player.sendMessage(new ComponentBuilder("Dir wurde die Rolle ").color(ChatColor.GREEN).append(lastRole.getName()).color(ChatColor.getByChar(lastRole.getColor().getChar())).append(" genommen!").color(ChatColor.GREEN).create());
                                BeastCubeAPI.getGateway().sendPacket(player, new RoleChangedPacket(player.getUniqueId()));
                            }
                        } else {
                            sender.sendMessage(new ComponentBuilder("Spieler ").color(ChatColor.RED).append(username).color(ChatColor.AQUA).append(" nicht gefunden").color(ChatColor.RED).create());
                        }
                    } else {
                        sender.sendMessage(new ComponentBuilder("Benutzung: /role get <player>").color(ChatColor.RED).create());
                    }
                }
            } else {
                sender.sendMessage(new ComponentBuilder("Benutzung: /role <get:set:remove>").color(ChatColor.RED).create());
            }
            //} else {
            //No Permission
        }
    }

}
