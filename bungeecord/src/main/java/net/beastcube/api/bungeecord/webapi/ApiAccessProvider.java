package net.beastcube.api.bungeecord.webapi;

import net.beastcube.api.bungeecord.MasterComponent;

/**
 * Master component for providing API access.
 */
public class ApiAccessProvider extends MasterComponent {
    private String validAccessKey;

    public boolean isValid(final String accessKey) {
        return accessKey.equals(this.validAccessKey);
    }

    @Override
    public void onEnable() {
        this.validAccessKey = this.getConfiguration()
                .get("accessKey", "9fcf75de50a08115bf1a463a6970ab5d")
                .asString();
    }

}
