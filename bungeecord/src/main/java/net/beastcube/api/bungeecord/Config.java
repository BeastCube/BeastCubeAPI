package net.beastcube.api.bungeecord;

import net.beastcube.api.bungeecord.server.pinghandler.PingHandler;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class Config {

    private Plugin plugin = null;
    private static Configuration config = null;

    public Config() {
        this.plugin = BeastCubeMasterBungeePlugin.getInstance();
        loadConfig();
    }

    public String getURL() {
        return String.format("jdbc:mysql://%s:%s/%s", getHost(), getPort(), getDatabase());
    }

    public String getHost() {
        return config.getString("database.host", "localhost");
    }

    public int getPort() {
        return config.getInt("database.port", 3306);
    }

    public String getUsername() {
        return config.getString("database.username", "root");
    }

    public String getPassword() {
        return config.getString("database.password", "");
    }

    public String getDatabase() {
        return config.getString("database.database", "beastcube");
    }

    public boolean getAnimatedMotdEnable() {
        return config.getBoolean("animatedmotd", false);
    }

    public String getMotd() {
        return config.getString("motd", PingHandler.getMotd());
    }

    public void setMotd(String motd) {
        config.set("motd", motd);
    }

    public void saveConfig() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, new File(plugin.getDataFolder(), "config.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void loadConfig() {
        if (!plugin.getDataFolder().exists())
            plugin.getDataFolder().mkdir();

        File file = new File(plugin.getDataFolder(), "config.yml");

        boolean firststart = false;
        if (!file.exists()) {
            try {
                firststart = true;
                Files.copy(plugin.getResourceAsStream("config.yml"), file.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(plugin.getDataFolder(), "config.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (firststart) {
            config.set("database.host", "localhost");
            config.set("database.port", "3306");
            config.set("database.database", "mcserver");
            config.set("database.username", "root");
            config.set("database.password", "passwrd");
            config.set("animatedmotd", false);
            config.set("motd", "Welcome on the Server");
        }
        saveConfig();
    }

}
