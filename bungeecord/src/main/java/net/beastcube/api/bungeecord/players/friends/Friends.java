package net.beastcube.api.bungeecord.players.friends;

import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.beastcube.api.bungeecord.BeastCubeMasterBungeePlugin;
import net.beastcube.api.commons.database.FriendsDatabaseHandler;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author BeastCube
 */
public class Friends implements Listener {

    private final Plugin plugin;

    public Friends() {
        this.plugin = BeastCubeMasterBungeePlugin.getInstance();
        FriendsCommands friendsCommands = new FriendsCommands();
        ProxyServer.getInstance().getPluginManager().registerCommand(plugin, friendsCommands);
    }

    @EventHandler
    public void onPlayerLogin(final PostLoginEvent e) {
        ProxyServer.getInstance().getScheduler().schedule(plugin, () -> ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), () -> {
            for (UUID player : FriendsDatabaseHandler.getFriendList(e.getPlayer().getUniqueId())) {
                ProxiedPlayer p = ProxyServer.getInstance().getPlayer(player);
                if (p != null) {
                    p.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(e.getPlayer().getName()).color(ChatColor.AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + e.getPlayer().getName())).event(FriendsHelper.showProfile).append(" ist jetzt ").color(ChatColor.GOLD).append("online!").color(ChatColor.GREEN).create());
                    BeastCubeAPI.sendUpdateScoreboardPacket(p);
                }
            }
            FriendsHelper.showList(e.getPlayer());
            FriendsHelper.showInvitations(e.getPlayer());
        }), 1, TimeUnit.NANOSECONDS);
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerDisconnectEvent e) {
        ProxyServer.getInstance().getScheduler().schedule(plugin, () -> ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), () -> {
            for (UUID player : FriendsDatabaseHandler.getFriendList(e.getPlayer().getUniqueId())) {
                ProxiedPlayer p = ProxyServer.getInstance().getPlayer(player);
                if (p != null) {
                    p.sendMessage(ChatMessageType.CHAT, new ComponentBuilder(e.getPlayer().getName()).color(ChatColor.AQUA).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friends show " + e.getPlayer().getName())).event(FriendsHelper.showProfile).append(" ist jetzt ").color(ChatColor.GOLD).append("offline!").color(ChatColor.RED).create());
                    BeastCubeAPI.sendUpdateScoreboardPacket(p);
                }
            }
        }), 1, TimeUnit.NANOSECONDS);
    }

}
