package net.beastcube.api.bungeecord;

import net.beastcube.api.bungeecord.network.IncomingMessageHandler;
import net.beastcube.api.bungeecord.webapi.ApiAccessProvider;
import net.beastcube.api.bungeecord.webapi.ApiServer;
import net.beastcube.api.commons.Providers;
import net.beastcube.api.commons.Storage;
import net.beastcube.api.commons.StorageImpl;
import net.beastcube.api.commons.configuration.Configuration;
import net.beastcube.api.commons.configuration.ConfigurationSection;
import net.beastcube.api.network.MasterServer;
import net.beastcube.api.network.SlaveServer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * @author BeastCube
 *         BeastCube master server singleton object.
 */
public final class BeastCubeMaster {
    private static BeastCubeMaster instance = null;

    public static BeastCubeMaster getInstance() {
        return BeastCubeMaster.instance;
    }

    private final MasterServer master;
    private final Logger log = BeastCubeMasterBungeePlugin.getInstance().getLogger();
    private Configuration config;
    private final Scheduler scheduler;
    private final MasterStorageProxy storage;
    //private Database database;

    private final List<MasterComponent> components = new ArrayList<MasterComponent>();
    private boolean componentsEnabled = false;

    private BeastCubeMaster(final File dataFolder) {
        this.log.info("Booting up BeastCubeMaster...");

        Providers.LOGGER = BeastCubeMasterBungeePlugin.getInstance().getLogger();

        instance = this;

        // Load configuration.
        File f = new File(dataFolder.getAbsolutePath() + "/config.xml");
        if (!f.exists()) {
            this.log.info("Configuration file not found!");
            this.config = new Configuration(f);
        } else {
            this.log.info("Loading configuration...");
            this.config = Configuration.load(f);
        }

        // Load storage.
        File storageFolder = new File(dataFolder.getAbsolutePath() + "/storage");
        storageFolder.mkdirs();
        this.storage = new MasterStorageProxy(new StorageImpl(storageFolder,
                this.config.getSection(StorageImpl.class)));
        this.addComponent(this.storage);

        // Set up scheduler.
        this.scheduler = new Scheduler();

        // Sheduler basic tasks.
        this.scheduler.schedule(BeastCubeMaster.this::periodic, 2, TimeUnit.SECONDS);

        //TODO Initialize hubmagic inbefore
        // Set up network.
        ConfigurationSection config = this.config.getSection(MasterServer.class);
        int port = config.get(Configuration.Keys.KEY_PORT, 29631).asInteger();
        String authKey = config.get(Configuration.Keys.KEY_AUTHKEY, Configuration.Defaults.AUTH_KEY).asString();

        this.master = new MasterServer("master", new BungeeProxy(), port, authKey, this.log);
        this.master.getMessenger().registerListener(new IncomingMessageHandler());
        this.master.start();

        // Set up Database.
        //this.addComponent(new Database());

        // Set up access provider to web api.
        this.addComponent(new ApiAccessProvider());

        // Set up API server.
        this.addComponent(new ApiServer());
    }

    public void start() {
        // Enable components.
        this.log.info("Enabling all components now!");
        this.enableComponents();
    }

    /**
     * Shut's down all processes, saves files and turns off server.
     */
    public void shutdown() {
        //TODO: Wait before are all other things done.
        this.log.info("Shutting down server...");
        this.master.shutdown();

        this.log.info("Disabling scheduler...");
        this.scheduler.shutdownNow(); // TODO: Too

        this.log.info("Disabling all components...");
        this.disableComponents();

        this.log.info("Saving configuration...");
        this.config.save();

        this.log.info("Shutting down!");
        this.log.info("Thanks for using and bye!");
    }

    // Each 2 seconds sends ServerStatusRequest to all servers.
    protected void periodic() {
        this.updateSlaves();
    }

    protected void updateSlaves() {
        // Updates slaves.
        for (final SlaveServer slave : this.master.getSlaveServers()) {
            //slave.sendRequest(new ServerStatusRequest(new SlaveStatusCallbackHander(slave)));
        }
    }

    /**
     * Adds component to master server.
     *
     * @param component component to add
     */
    public void addComponent(final MasterComponent component) {
        this.components.add(component);

        if (this.componentsEnabled) {
            component.onEnable();
        }
    }

    protected void enableComponents() {
        this.componentsEnabled = true;
        this.components.forEach(this::enableComponent);
    }

    protected void disableComponents() {
        this.components.forEach(this::disableComponent);
    }

    protected void enableComponent(final MasterComponent e) {
        this.log.info("Enabling [" + e.getClass().getSimpleName() + "] ...");
        try {
            e.master = this;
            e.__initConfig(this.getConfiguration());
            e.onEnable();
        } catch (Exception ex) {
            ex.printStackTrace();
            this.log.info("Error '" + ex.getMessage() + "' while enabling component "
                    + e.getClass().getSimpleName());
        }
    }

    protected void disableComponent(final MasterComponent e) {
        this.log.info("Disabling [" + e.getClass().getSimpleName() + "] ...");
        e.onDisable();
    }

    public <T extends MasterComponent> T getComponent(final Class<T> type) {
        for (MasterComponent c : this.components) {
            if (type.isInstance(c)) {
                return type.cast(c);
            }
        }
        return null;
    }

    public Configuration getConfiguration() {
        return this.config;
    }

    public MasterServer getMasterServer() {
        return this.master;
    }

    public static void init(final File dataFolder) {
        BeastCubeMaster.instance = new BeastCubeMaster(dataFolder);
    }

    public Logger getLogger() {
        return this.log;
    }

    public Storage getStorage() {
        return this.storage;
    }

    public Scheduler getScheduler() {
        return this.scheduler;
    }

    /*public static class SlaveStatusCallbackHander extends Callback<ServerStatusResponse> {
        private final SlaveServer slave;

        public SlaveStatusCallbackHander(final SlaveServer slave) {
            this.slave = slave;
        }

        @Override
        public void onResponse(final ServerStatusResponse response) {
            this.slave.setCustom("maxMem", Long.toString(response.maxMem));
            this.slave.setCustom("usedMem", Long.toString(response.usedMem));
        }
    }*/

}
