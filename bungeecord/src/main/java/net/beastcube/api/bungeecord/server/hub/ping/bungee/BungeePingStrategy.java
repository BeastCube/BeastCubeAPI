/**
 * Copyright © 2014 tuxed <write@imaginarycode.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.
 */
package net.beastcube.api.bungeecord.server.hub.ping.bungee;

import net.beastcube.api.bungeecord.server.hub.ping.PingResult;
import net.beastcube.api.bungeecord.server.hub.ping.PingStrategy;
import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.config.ServerInfo;

public class BungeePingStrategy implements PingStrategy {
    @Override
    public void ping(ServerInfo info, final Callback<PingResult> callback) {
        try {
            info.ping((serverPing, throwable) -> {
                if (throwable != null) {
                    callback.done(PingResult.DOWN, throwable);
                    return;
                }

                callback.done(PingResult.from(false, serverPing.getPlayers().getOnline(), serverPing.getPlayers().getMax()), null);
            });
        } catch (Exception e) {
            callback.done(PingResult.DOWN, e);
        }
    }
}
