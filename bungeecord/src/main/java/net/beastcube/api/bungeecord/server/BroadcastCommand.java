package net.beastcube.api.bungeecord.server;

import net.beastcube.api.commons.permissions.Permissions;
import net.beastcube.api.commons.util.Constants;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import static net.md_5.bungee.api.ChatColor.DARK_GREEN;
import static net.md_5.bungee.api.ChatColor.RED;

/**
 * @author BeastCube
 */
public class BroadcastCommand extends Command {

    public BroadcastCommand() {
        super("broadcast");
    }


    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) sender;

        if (player.hasPermission(Permissions.BROADCAST)) {
            if (args.length > 0) {
                String msg = "";
                boolean space = false;
                for (String arg : args) {
                    if (space) {
                        msg += " ";
                    } else {
                        space = true;
                    }
                    msg += arg;
                }

                ProxyServer.getInstance().broadcast(new ComponentBuilder("-------------------------------------").color(DARK_GREEN).create());
                ProxyServer.getInstance().broadcast(new TextComponent(""));
                ProxyServer.getInstance().broadcast(new TextComponent(ChatColor.GOLD + " » " + ChatColor.RESET + ChatColor.translateAlternateColorCodes('&', msg)));
                ProxyServer.getInstance().broadcast(new TextComponent(""));
                ProxyServer.getInstance().broadcast(new ComponentBuilder("-------------------------------------").color(DARK_GREEN).create());

            } else {
                player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Broadcast: /broadcast <Nachricht>").color(RED).create());
            }
        } else {
            player.sendMessage(ChatMessageType.CHAT, TextComponent.fromLegacyText(Constants.NOPERMISSION));
        }
    }

}
