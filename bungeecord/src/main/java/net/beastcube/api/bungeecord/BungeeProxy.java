package net.beastcube.api.bungeecord;

import net.beastcube.api.network.ProxyBrand;
import net.beastcube.api.network.SlaveServer;
import net.beastcube.api.network.api.Proxy;
import net.md_5.bungee.api.ProxyServer;

import java.net.InetSocketAddress;

/**
 * @author BeastCube
 */
public class BungeeProxy implements Proxy {

    @Override
    public ProxyBrand getBrand() {
        return ProxyBrand.BUNGEECORD;
    }

    @Override
    public void addServer(SlaveServer server) {
        ProxyServer.getInstance().getServers().put(server.getName(), ProxyServer.getInstance()
                .constructServerInfo(server.getName(), new InetSocketAddress(server.getIp(), server.getPort()), server.getMotd(), false));
    }

    @Override
    public void removeServer(SlaveServer server) {
        ProxyServer.getInstance().getServers().remove(server.getName());
        ProxyServer.getInstance().getPluginManager().callEvent(new SlaveRemovedEvent(server));
    }
}
