package net.beastcube.api.bungeecord.webapi;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;

import java.util.logging.Logger;

/**
 * Logging fitler for API.
 */
public class ApiLoggingFilter implements ContainerRequestFilter {
    private final Logger log = Logger.getLogger(this.getClass().getCanonicalName());

    @Override
    public ContainerRequest filter(final ContainerRequest request) {
        String accessKey = request.getQueryParameters().getFirst("accessKey");
        String url = request.getPath();
        this.log.info(accessKey + " accesses " + url);
        return request;
    }

    public Logger getLogger() {
        return this.log;
    }
}
