package net.beastcube.api.bungeecord.minigames;

import lombok.Getter;
import lombok.NoArgsConstructor;
import net.beastcube.api.commons.minigames.MinigameInstance;

import java.util.HashMap;
import java.util.Map;

/**
 * @author BeastCube
 */
@NoArgsConstructor
public class MinigamesManager {
    @Getter
    public static Map<String, MinigameInstance> minigameInstances = new HashMap<>();

    public static void removeServer(String serverName) {
        if (minigameInstances.containsKey(serverName)) {
            minigameInstances.remove(serverName);
        }
    }

    public static void put(String serverName, MinigameInstance instance) {
        minigameInstances.put(serverName, instance);
    }

}
