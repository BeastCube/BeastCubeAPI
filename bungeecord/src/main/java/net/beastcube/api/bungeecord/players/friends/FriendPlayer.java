package net.beastcube.api.bungeecord.players.friends;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.UUID;

/**
 * @author BeastCube
 */
@AllArgsConstructor
class FriendPlayer {

    @Setter
    @Getter
    UUID uniqueId;

    @Setter
    @Getter
    ProxiedPlayer player;
}