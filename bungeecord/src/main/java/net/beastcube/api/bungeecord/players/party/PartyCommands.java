package net.beastcube.api.bungeecord.players.party;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * @author BeastCube
 */
@SuppressWarnings("deprecation")
public class PartyCommands extends Command {
    public PartyCommands() {
        super("party");
    }

    public static String prefix = "§6[PARTY] ";

    public void execute(CommandSender cs, String[] args) {
        if ((cs instanceof ProxiedPlayer)) {
            ProxiedPlayer p = (ProxiedPlayer) cs;
            if (args.length < 1) {
                p.sendMessage("§6#################§8§lParty§6#################");

                p.sendMessage("§a/party list   §f| §bZeigt die Partymitglieder an! ");
                p.sendMessage("§a/party leave  §f| §bLeavet/löscht die Party!");
                p.sendMessage("§a/party chat   §f| §bChat with your Party!");
                p.sendMessage("§a/party invite §f| §bInvite Players!");
                p.sendMessage("§a/party accept §f| §bAnfrage annehmen!");
                p.sendMessage("§a/party kick   §f| §bSpieler aus der Party kicken!");
            } else if (args[0].equalsIgnoreCase("create")) {
                Party.neueParty(p);
            } else if (args[0].equalsIgnoreCase("list")) {
                Party.List(p);
            } else if (args[0].equalsIgnoreCase("leave")) {
                Party.leave(p);
            } else {
                int i;
                if (args[0].equalsIgnoreCase("chat")) {
                    String msg = "";
                    for (i = 1; i < args.length; i++) {
                        msg = msg + args[i] + " ";
                    }
                    Party.chat(p, msg);
                } else if (args[0].equalsIgnoreCase("invite")) {
                    if (args.length > 1) {
                        for (ProxiedPlayer x : ProxyServer.getInstance().getPlayers()) {
                            if (x.getName().equalsIgnoreCase(args[1])) {
                                ProxiedPlayer z = ProxyServer.getInstance().getPlayer(args[1]);
                                Party.invite(p, z);

                                return;
                            }
                        }
                        p.sendMessage(prefix + "§cDer Spieler §b" + args[1] + " §cist nicht Online!");
                    } else {
                        p.sendMessage(prefix + "§cZu wenig Argumente");
                        p.sendMessage("§cBenutze: §a/party invite <player>");
                    }
                } else if (args[0].equalsIgnoreCase("accept")) {
                    Party.accept(p);
                } else if (args[0].equalsIgnoreCase("kick")) {
                    if (args.length > 1) {
                        for (ProxiedPlayer x : ProxyServer.getInstance().getPlayers()) {
                            if (x.getName().equalsIgnoreCase(args[1])) {
                                Party.kick(p, x);

                                return;
                            }
                        }
                        p.sendMessage("§cDieser Spieler ist nicht Online!");
                    } else {
                        p.sendMessage("§cBenutze /party kick <player>");
                    }
                } else if (args[0].equalsIgnoreCase("help")) {
                    p.sendMessage("§6#################§8§lParty§6#################");
                    p.sendMessage("§a/party list   §f| §bZeigt die Partymitglieder an! ");
                    p.sendMessage("§a/party leave  §f| §bLeavet/löscht die Party!");
                    p.sendMessage("§a/party chat   §f| §bChat with your Party!");
                    p.sendMessage("§a/party invite §f| §bInvite Players!");
                    p.sendMessage("§a/party accept §f| §bAnfrage annehmen!");
                    p.sendMessage("§a/party kick   §f| §bSpieler aus der Party kicken!");
                }
            }
        }
    }
}