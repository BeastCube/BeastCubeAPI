/**
 * Copyright © 2014 tuxed <write@imaginarycode.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.
 */
package net.beastcube.api.bungeecord.server.hub.selectors;

import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public enum ServerSelectors implements ServerSelector {
    FIRST_AVAILABLE {
        @Override
        public ServerInfo chooseServer(ProxiedPlayer player) {
            return BeastCubeAPI.getHubMagic().getPingManager().firstAvailable(player);
        }
    },
    LEAST_POPULATED {
        @Override
        public ServerInfo chooseServer(ProxiedPlayer player) {
            return BeastCubeAPI.getHubMagic().getPingManager().lowestPopulation(player);
        }
    },
    RANDOM {
        private final Random random = new Random();

        @Override
        public ServerInfo chooseServer(ProxiedPlayer player) {
            List<ServerInfo> available = BeastCubeAPI.getHubMagic().getServers().stream().filter(info -> BeastCubeAPI.getHubMagic().getPingManager().consideredAvailable(info, player)).collect(Collectors.toList());

            if (available.isEmpty()) {
                return null;
            }

            return available.get(random.nextInt(available.size()));
        }
    },
    SEQUENTIAL {
        @Override
        public ServerInfo chooseServer(ProxiedPlayer player) {
            throw new RuntimeException("Please use get() to get a proper ServerSelector.");
        }

        @Override
        public ServerSelector get() {
            return new SequentialSelector();
        }
    };

    public ServerSelector get() {
        return this;
    }

    public static ServerSelector parse(String type) {
        switch (type) {
            case "lowest":
                return LEAST_POPULATED;
            case "firstavailable":
                return FIRST_AVAILABLE;
            case "random":
                return RANDOM;
            case "sequential":
                return SEQUENTIAL.get();
            default:
                return null;
        }
    }
}
