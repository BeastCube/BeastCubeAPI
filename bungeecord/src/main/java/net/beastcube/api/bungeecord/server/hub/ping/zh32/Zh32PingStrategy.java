/**
 * Copyright © 2014 tuxed <write@imaginarycode.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.
 */
package net.beastcube.api.bungeecord.server.hub.ping.zh32;


import net.beastcube.api.bungeecord.BeastCubeMasterBungeePlugin;
import net.beastcube.api.bungeecord.server.hub.ping.PingResult;
import net.beastcube.api.bungeecord.server.hub.ping.PingStrategy;
import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;

import java.io.IOException;

public class Zh32PingStrategy implements PingStrategy {
    @Override
    public void ping(final ServerInfo info, final Callback<PingResult> callback) {
        // zh32's library is synchronous, so we emulate asynchronous pings
        Runnable runnable = () -> {
            ServerListPing ping = new ServerListPing();
            ping.setHost(info.getAddress());
            try {
                ServerListPing.StatusResponse reply = ping.fetchData();
                callback.done(reply == null ? PingResult.DOWN : PingResult.from(false, reply.getPlayers().getOnline(),
                        reply.getPlayers().getMax()), null);
            } catch (IOException e) {
                BeastCubeMasterBungeePlugin.getInstance().getLogger().warning("Unable to ping " + info.getName() + " (" + info.getAddress() + "): " + e.getMessage());
                callback.done(PingResult.DOWN, e);
            }
        };

        ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), runnable);
    }
}
