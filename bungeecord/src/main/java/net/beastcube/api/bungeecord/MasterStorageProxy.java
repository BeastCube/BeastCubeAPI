package net.beastcube.api.bungeecord;

import net.beastcube.api.commons.Storage;
import net.beastcube.api.commons.StorageImpl;
import net.beastcube.api.commons.storage.MapDescriptor;
import net.beastcube.api.commons.storage.MinigameDescriptor;

import java.util.Collection;

/**
 * Proxy object to allow usage of {@link StorageImpl} as {@link MasterComponent} by using {@link Storage} methods.
 */
public class MasterStorageProxy extends MasterComponent implements Storage {
    private final StorageImpl storage;

    public MasterStorageProxy(final StorageImpl target) {
        this.storage = target;
    }

    @Override
    public void onEnable() {
        this.storage.onEnable();
    }

    @Override
    public void onDisable() {
        this.storage.onDisable();
    }

    @Override
    public Collection<MinigameDescriptor> getAvaiablePlugins() {
        return this.storage.getAvaiablePlugins();
    }

    @Override
    public Collection<MapDescriptor> getAvaiableMaps() {
        return this.storage.getAvaiableMaps();
    }

    @Override
    public boolean hasPlugin(final String pluginName) {
        return this.storage.hasPlugin(pluginName);
    }
}
