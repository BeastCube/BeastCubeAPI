package net.beastcube.api.bungeecord.players;

import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.beastcube.api.bungeecord.BeastCubeMasterBungeePlugin;
import net.beastcube.api.commons.database.PlayersDatabaseHandler;
import net.beastcube.api.commons.packets.PlaySoundPacket;
import net.beastcube.api.commons.permissions.Permissions;
import net.beastcube.api.commons.permissions.Role;
import net.beastcube.api.commons.permissions.Roles;
import net.beastcube.api.commons.util.Constants;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

import static net.md_5.bungee.api.ChatColor.*;

/**
 * @author BeastCube
 */
public class TeamchatCommand extends Command {

    public TeamchatCommand() {
        super("teamchat", "", "tc", "staffchat");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) sender;
        Role senderRole = Roles.getRole(player.getUniqueId());
        if (!senderRole.hasPermission(Permissions.TEAMCHAT)) {
            player.sendMessage(ChatMessageType.CHAT, TextComponent.fromLegacyText(Constants.NOPERMISSION));
            return;
        }

        ProxyServer.getInstance().getScheduler().runAsync(BeastCubeMasterBungeePlugin.getInstance(), () -> {
            if (args.length > 0) {
                String msg = "";
                boolean space = false;
                for (String arg : args) {
                    if (space) {
                        msg += " ";
                    } else {
                        space = true;
                    }
                    msg += arg;
                }

                for (UUID uuid : PlayersDatabaseHandler.getPlayersWithRole()) {
                    ProxiedPlayer p = ProxyServer.getInstance().getPlayer(uuid);
                    if (p != null) {
                        if (p.hasPermission(Permissions.TEAMCHAT)) {
                            p.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("[").color(GRAY).append("Teamchat").color(RED).append("]").color(GRAY).append(" " + player.getName()).color(ChatColor.getByChar(senderRole.getColor().getChar())).append(": ").color(GRAY).append(msg).color(WHITE).create());
                            BeastCubeAPI.getGateway().sendPacket(p, new PlaySoundPacket("note.pling", 1, 2));
                        }
                    }
                }
            } else {
                player.sendMessage(ChatMessageType.CHAT, new ComponentBuilder("Teamchat: /tc <Nachricht>").color(RED).create());
            }
        });
    }

}
