package net.beastcube.api.bungeecord.server.mainteance;

import net.beastcube.api.bungeecord.BeastCubeAPI;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import org.apache.commons.lang3.time.DurationFormatUtils;

/**
 * @author BeastCube
 */
public class EnableRunnable implements Runnable {

    private final Plugin parent;
    private final CommandSender sender;

    public EnableRunnable(Plugin parent, CommandSender sender) {
        this.parent = parent;
        this.sender = sender;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void run() {
        int loops = BeastCubeAPI.getMainteanceManager().getCountdown();
        ProxyServer.getInstance().broadcast(ChatColor.translateAlternateColorCodes('&', (format(BeastCubeAPI.getMainteanceManager().getCountdownMessage(), loops))));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = loops - 1; i > 0; i--) {
            if (BeastCubeAPI.getMainteanceManager().getAlertTimes().contains(i)) {
                ProxyServer.getInstance().broadcast(ChatColor.translateAlternateColorCodes('&', format(BeastCubeAPI.getMainteanceManager().getCountdownMessage(), i)));
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (i == 1) {
                BeastCubeAPI.getMainteanceManager().kick(null);
                BeastCubeAPI.getMainteanceManager().setEnabled(true);
                sender.sendMessage(Messages.MAINTENANCE_ENABLED);
                BeastCubeAPI.getMainteanceManager().clearTask();
            }
        }
    }

    private String format(String message, int seconds) {
        return message.replace("{{ TIME }}", DurationFormatUtils.formatDurationWords(seconds * 1000, true, false)).replace("{{ SECONDS }}", String.valueOf(seconds));
    }
}
