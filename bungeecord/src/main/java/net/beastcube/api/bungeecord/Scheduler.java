package net.beastcube.api.bungeecord;

import java.util.concurrent.*;

/**
 * Scheduler
 */
public class Scheduler {
    private final ScheduledExecutorService service;

    public Scheduler() {
        this.service = Executors.newScheduledThreadPool(1);
    }

    public ScheduledFuture<?> schedule(final Runnable command, final long delay,
                                       final TimeUnit unit) {
        return this.service.schedule(command, delay, unit);
    }

    public <V> ScheduledFuture<V> schedule(final Callable<V> callable, final long delay,
                                           final TimeUnit unit) {
        return this.service.schedule(callable, delay, unit);
    }

    public ScheduledFuture<?> scheduleAtFixedRate(final Runnable command,
                                                  final long initialDelay, final long period, final TimeUnit unit) {
        return this.service.scheduleAtFixedRate(command, initialDelay, period, unit);
    }

    public ScheduledFuture<?> scheduleWithFixedDelay(final Runnable command,
                                                     final long initialDelay, final long delay, final TimeUnit unit) {
        return this.service.scheduleWithFixedDelay(command, initialDelay, delay, unit);
    }

    public void shutdown() {
        this.service.shutdown();
    }

    public void shutdownNow() {
        this.service.shutdownNow();
    }
}
