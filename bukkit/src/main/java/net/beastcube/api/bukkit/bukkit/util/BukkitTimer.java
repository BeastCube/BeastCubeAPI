package net.beastcube.api.bukkit.bukkit.util;

import net.beastcube.api.bukkit.bukkit.BeastCube;

/**
 * Timer implemented over bukkit tasks.
 */
public class BukkitTimer {
    private final int interval;
    private int taskId;
    private final Runnable onTick;

    public BukkitTimer(final int interval, final Runnable onTick) {
        this.interval = interval;
        this.onTick = onTick;
    }

    public boolean isRunning() {
        return this.taskId == 0;
    }

    public void start() {
        this.taskId = BeastCube.getScheduler().scheduleSyncRepeatingTask(this.onTick, 0L,
                this.interval);
    }

    public void stop() {
        BeastCube.getScheduler().cancelTask(this.taskId);
        this.taskId = 0;
    }
}
