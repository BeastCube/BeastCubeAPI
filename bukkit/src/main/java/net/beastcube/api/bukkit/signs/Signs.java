package net.beastcube.api.bukkit.signs;

import net.beastcube.api.commons.permissions.Permissions;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

/**
 * @author BeastCube
 */
public class Signs implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onSignChange(SignChangeEvent event) {
        if (event.getPlayer().hasPermission(Permissions.COLORED_SIGNS_PERMISSION)) {
            for (int line = 0; line <= 3; line++) {
                String text = ChatColor.translateAlternateColorCodes('&', event.getLine(line));
                event.setLine(line, text);
            }
        }
    }
}
