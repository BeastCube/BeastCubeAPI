package net.beastcube.api.bukkit.config;

import org.bukkit.configuration.ConfigurationSection;

public interface Configuration {

    void load(ConfigurationSection section);

    void save(ConfigurationSection section);

}
