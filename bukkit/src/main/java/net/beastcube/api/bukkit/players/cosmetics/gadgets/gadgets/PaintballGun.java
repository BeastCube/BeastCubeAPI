package net.beastcube.api.bukkit.players.cosmetics.gadgets.gadgets;

import lombok.Getter;
import net.beastcube.api.bukkit.BeastCubeSlaveBukkitPlugin;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.Gadget;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.GadgetManager;
import net.beastcube.api.bukkit.util.ParticleEffect;
import net.beastcube.api.commons.util.RandomHelper;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileHitEvent;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * @author BeastCube
 */
public class PaintballGun extends Gadget {
    @Getter
    private String id = "paintballgun";
    @Getter
    private String displayName = GadgetManager.DISPLAY_NAME_COLOR + "Paintball Pistole";
    @Getter
    private Material material = Material.DIAMOND_BARDING;
    @Getter
    private String description = ChatColor.GRAY + "Eine Paintball Pistole";

    public static LinkedList<Projectile> pcooldown = new LinkedList<>();

    @Override
    public boolean hasLeftClickAction() {
        return false;
    }

    @Override
    public boolean hasRightClickAction() {
        return true;
    }

    @Override
    public boolean LeftClick(Player p) {
        return false;
    }

    @Override
    public boolean RightClick(Player p) {
        final Snowball sb = p.launchProjectile(Snowball.class);
        sb.setVelocity(p.getEyeLocation().getDirection());
        pcooldown.add(sb);
        p.getWorld().playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 1.5F, 1.2F);
        return true;
    }

    @Override
    public int getCooldown() {
        return 0;
    }

    //****************************************************************************\\
    //********************************** Events **********************************\\
    //****************************************************************************\\

    public List<Material> ignore = Arrays.asList(
            Material.STONE_PLATE, //Launchpads
            Material.SKULL,
            Material.CARPET,
            Material.CACTUS,
            Material.REDSTONE_BLOCK //Launchpads
    );

    List<Material> blacklisted = Arrays.asList(
            Material.BANNER,
            Material.STANDING_BANNER,
            Material.WALL_BANNER,
            Material.SIGN,
            Material.AIR,
            Material.BEDROCK,
            Material.SOIL,
            Material.WALL_SIGN,
            Material.WOOD_DOOR,
            Material.IRON_DOOR,
            Material.IRON_DOOR_BLOCK,
            Material.WOODEN_DOOR,
            Material.WOODEN_DOOR,
            Material.ACACIA_DOOR,
            Material.BIRCH_DOOR,
            Material.DARK_OAK_DOOR,
            Material.JUNGLE_DOOR,
            Material.SPRUCE_DOOR,
            Material.SIGN_POST,
            Material.BURNING_FURNACE,
            Material.FURNACE,
            Material.CHEST,
            Material.DISPENSER,
            Material.HOPPER,
            Material.MINECART,
            Material.EXPLOSIVE_MINECART,
            Material.HOPPER_MINECART,
            Material.POWERED_MINECART,
            Material.STORAGE_MINECART,
            Material.JUKEBOX,
            Material.DROPPER,
            Material.BED,
            Material.BED_BLOCK,
            Material.POTATO,
            Material.CARROT,
            Material.PUMPKIN_STEM,
            Material.MELON_STEM,
            Material.PUMPKIN,
            Material.NETHER_WARTS,
            Material.CAKE,
            Material.SUGAR_CANE,
            Material.SUGAR_CANE_BLOCK,
            Material.WHEAT,
            Material.CROPS,
            Material.COCOA,
            Material.BOAT,
            Material.ITEM_FRAME
    );

    @EventHandler
    public void onProjectileHit(ProjectileHitEvent e) {
        Projectile en = e.getEntity();
        if (PaintballGun.pcooldown.contains(en)) {

            PaintballGun.pcooldown.remove(en);

            Location loc = e.getEntity().getLocation().add(e.getEntity().getVelocity());
            loc.getWorld().playEffect(loc, Effect.STEP_SOUND, 49);

            byte color = (byte) RandomHelper.randomInt(15);
            for (Block block : getInRadius(loc, 2D).keySet()) {
                if (!ignore.contains(block.getType()) && !blacklisted.contains(block.getType()) && !block.isLiquid()) {
                    Location bLoc = block.getLocation().add(0.0D, 1.0D, 0.0D);
                    if (block.getType() == Material.CARPET) {
                        setBlockToRestore(block, 171, color, 4L);
                    }
                    if (block.getType() != Material.WOOL) {
                        setBlockToRestore(block, 35, color, 4L);
                        for (int k = 0; k < 7; k++) {
                            ParticleEffect.FIREWORKS_SPARK.display(0, 0, 0, 0.1F, 1, bLoc);
                        }
                    }
                }
            }

        }
    }

    public static HashMap<Block, Double> getInRadius(Location loc, double dR) {
        return getInRadius(loc, dR, 999.0D);
    }

    public static HashMap<Block, Double> getInRadius(Location loc, double dR, double heightLimit) {
        HashMap<Block, Double> blockList = new HashMap<>();
        int iR = (int) dR + 1;
        for (int x = -iR; x <= iR; x++) {
            for (int z = -iR; z <= iR; z++) {
                for (int y = -iR; y <= iR; y++) {
                    if (Math.abs(y) <= heightLimit) {
                        Block curBlock = loc.getWorld().getBlockAt((int) (loc.getX() + x), (int) (loc.getY() + y), (int) (loc.getZ() + z));

                        double offset = offset(loc, curBlock.getLocation().add(0.5D, 0.5D, 0.5D));
                        if (offset <= dR) {
                            blockList.put(curBlock, 1.0D - offset / dR);
                        }
                    }
                }
            }
        }
        return blockList;
    }

    @SuppressWarnings("deprecation")
    public void setBlockToRestore(final Block block, final int id, final byte color, final long timeInSecs) {
        final int BeforeId = block.getTypeId();
        final byte BeforeData = block.getData();
        block.setTypeIdAndData(id, color, false);
        Bukkit.getScheduler().runTaskLater(BeastCubeSlaveBukkitPlugin.getInstance(), () -> block.setTypeIdAndData(BeforeId, BeforeData, false), timeInSecs * 20L);
    }

    public static double offset(Entity a, Entity b) {
        return offset(a.getLocation().toVector(), b.getLocation().toVector());
    }

    public static double offset(Location a, Location b) {
        return offset(a.toVector(), b.toVector());
    }

    public static double offset(org.bukkit.util.Vector a, org.bukkit.util.Vector b) {
        return a.subtract(b).length();
    }

}
