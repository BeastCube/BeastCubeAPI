package net.beastcube.api.bukkit.players.cosmetics.effects.effects;

import de.slikey.effectlib.effect.ShieldEffect;
import de.slikey.effectlib.util.ParticleEffect;
import lombok.Getter;
import net.beastcube.api.bukkit.players.cosmetics.effects.Effect;
import net.beastcube.api.bukkit.players.cosmetics.effects.EffectManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * @author BeastCube
 */
public class Shield extends Effect {
    @Getter
    private String id = "shield";

    @Override
    public String getDisplayName() {
        return EffectManager.DISPLAY_NAME_COLOR + "Shield";
    }

    @Override
    public Material getMaterial() {
        return Material.SLIME_BALL;
    }

    @Override
    public de.slikey.effectlib.Effect getEffect(Player p, de.slikey.effectlib.EffectManager e) {
        ShieldEffect effect = new ShieldEffect(e);
        effect.particle = ParticleEffect.PORTAL;
        effect.particles = 20;
        //effect.particle = ParticleEffect.BARRIER;
        return effect;
    }
}
