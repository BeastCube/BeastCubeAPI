package net.beastcube.api.bukkit.commands;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Command {
    /**
     * The description of this command
     */
    String description() default "";

    /**
     * The identifier describes what command definition this will bind to. Spliced by spaces, you can define as many sub commands as you want.<br><br>
     * Example: {@code @Command(identifier="root sub1 sub2")}<br>
     * The user will be able to access the command by writing:<br>
     * {@code /root sub1 sub2}<br>
     */
    String[] identifiers();

    /**
     * If this command can only be executed by players (default true).<br>
     * If you turn this to false, the first parameter in the method must be the {@link org.bukkit.command.CommandSender} to avoid {@link ClassCastException}
     */
    boolean onlyPlayers() default true;

    /**
     * The permissions to check if the user have before execution. If it is empty the command does not require any permission.<br><br>
     * If the user don't have one of the permissions, they will get an error message stating that they do not have permission to use the command.
     */
    String[] permissions() default {};
}
