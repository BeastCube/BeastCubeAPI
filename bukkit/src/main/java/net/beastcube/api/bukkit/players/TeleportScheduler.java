package net.beastcube.api.bukkit.players;

import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.BeastCubeSlaveBukkitPlugin;
import net.beastcube.api.commons.util.GlobalLocation;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author BeastCube
 */
public class TeleportScheduler implements Listener {

    private static Map<UUID, GlobalLocation> scheduledTeleports = new HashMap<>();

    public static void scheduleTeleport(UUID player, UUID to) {
        //TODO Not able to teleport if in player is in parkour mode on lobby or if traget is not on lobby
        //TODO Do it with events
        scheduleTeleport(player, new GlobalLocation(Bukkit.getPlayer(to)));
    }

    public static void scheduleTeleport(UUID uuid, GlobalLocation loc) {
        Player p = Bukkit.getPlayer(uuid);
        if (p != null && p.isOnline()) {
            p.getPlayer().teleport(loc.getBukkitLocation());
            return;
        }
        scheduledTeleports.put(uuid, loc);
    }

    @EventHandler
    public static void onPlayerJoin(final PlayerJoinEvent e) {
        final Player player = e.getPlayer();
        if (BeastCubeAPI.isFriendsTeleportEnabled()) {
            Bukkit.getScheduler().runTask(BeastCubeSlaveBukkitPlugin.getInstance(), () -> {
                if (scheduledTeleports.containsKey(player.getUniqueId())) {
                    try {
                        GlobalLocation loc = scheduledTeleports.get(player.getUniqueId());
                        player.teleport(loc.getBukkitLocation());
                    } catch (Exception ex) {
                        // Occurs if the target World is null
                    }
                    scheduledTeleports.remove(player.getUniqueId());
                }
            });
        }
    }

}
