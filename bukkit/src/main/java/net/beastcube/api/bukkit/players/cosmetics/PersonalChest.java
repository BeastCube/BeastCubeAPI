package net.beastcube.api.bukkit.players.cosmetics;

import net.beastcube.api.bukkit.interactitem.InteractItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.api.bukkit.players.cosmetics.effects.EffectManager;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.GadgetManager;
import net.beastcube.api.bukkit.players.cosmetics.heads.HeadManager;
import net.beastcube.api.bukkit.players.cosmetics.menuitems.CosmeticTypeMenuItem;
import net.beastcube.api.bukkit.players.cosmetics.morphs.MorphManager;
import net.beastcube.api.bukkit.players.cosmetics.pets.PetManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Collections;
import java.util.List;

/**
 * @author BeastCube
 */
public class PersonalChest extends InteractItem {
    public static String TITLE = ChatColor.AQUA + "Personal Chest";

    //----------Managers----------\\
    public static EffectManager effectManager = new EffectManager();
    public static MorphManager morphManager = new MorphManager();
    public static PetManager petManager = new PetManager();
    public static GadgetManager gadgetManager = new GadgetManager();
    public static HeadManager headManager = new HeadManager();

    @Override
    public void execute(Player p, JavaPlugin plugin, Object data) {
        ItemMenu menu = new ItemMenu(TITLE, ItemMenu.Size.FOUR_LINE, plugin);
        for (int i = 0; i < CosmeticType.values().length; i++) {
            CosmeticType type = CosmeticType.values()[i];
            menu.setItem(i, new CosmeticTypeMenuItem(plugin, menu, type));
        }
        menu.open(p);
    }

    @Override
    public Material getMaterial() {
        return Material.CHEST;
    }

    @Override
    public String getDisplayName() {
        return TITLE;
    }

    @Override
    public List<String> getLore() {
        return Collections.singletonList(ChatColor.YELLOW + "Effects, Gadgets, Morphs, ...");
    }

}