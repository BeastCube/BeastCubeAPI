package net.beastcube.api.bukkit.bukkit;

import net.beastcube.api.bukkit.bukkit.core.*;
import net.beastcube.api.bukkit.bukkit.util.AsyncWorker;
import net.beastcube.api.bukkit.bukkit.util.PlayerFreezer;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * Bukkit plugin class.
 */
public class BeastCubeCore {
    protected JavaPlugin plugin;

    public BeastCubeCore(final File datafodler, final JavaPlugin plugin) {
        this.datafolder = datafodler;
        this.plugin = plugin;
    }

    /**
     * BeastCube matchmaking.
     */
    //public Matchmaking    matchmaking;
    /**
     * Player freezer.
     */
    public PlayerFreezer freezer;
    /**
     * AutoMessage instance.
     */
    public AutoMessage message;
    /**
     * AsyncWorker object.
     */
    public AsyncWorker asyncWorker;
    /**
     * BeastCube scheduler object.
     */
    public Scheduler scheduler;
    /**
     * BeastCube Ban storage.
     */
    //public BanStorage     banStorage;
    public Achievements achievementsClient;
    private final File datafolder;

    @SuppressWarnings("deprecation")
    public void onDisable() {
        Log.partDisable("Core");
        //Shutdown all updated parts.
        UpdatedParts.shutdown();

        //this.banStorage.save();

        this.asyncWorker.shutdown();

        Log.partDisable("Core");
    }

    public void onEnable() {
        Log.partEnable("Core");

        BeastCube.initialize(this);
        this.createDirectoryStructure();

        this.freezer = new PlayerFreezer();

        this.message = new AutoMessage();
        this.message.updateStart();

        //this.banStorage = new BanStorage();
        //this.banStorage.load();

        //this.matchmaking = new Matchmaking();
        //this.matchmaking.updateStart();

        this.achievementsClient = new Achievements();

        try {
            //this.matchmakingSignUpdater = new MatchmakingSignUpdater();
        } catch (Exception exc) {
            exc.printStackTrace();
        }

        this.asyncWorker = new AsyncWorker(3);
        this.asyncWorker.start();
    }

    private File getDataFolder() {
        return this.datafolder;
    }

    private void createDirectoryStructure() {
        boolean created = false;
        String path = this.getDataFolder().getAbsolutePath();
        created |= new File(path + "/arenas").mkdirs();
        created |= new File(path + "/cache").mkdirs();
        created |= new File(path + "/records").mkdirs();
        created |= new File(path + "/profiles").mkdirs();
        created |= new File(path + "/clips").mkdirs();
        if (created)
            Log.info("Directory structure expanded!");
    }
}
