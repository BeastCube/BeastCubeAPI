package net.beastcube.api.bukkit.bukkit.core;

/**
 * Interface that suggests that this call is updatable.
 */
public interface Updatable {
    /**
     * Called when part should start it's update logic.
     */
    void updateStart();

    /**
     * Called when part should stop it's update logic.
     */
    void updateStop();
}
