/*
 * This file is part of AmpMenus.
 *
 * Copyright (c) 2014 <http://github.com/ampayne2/AmpMenus/>
 *
 * AmpMenus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AmpMenus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with AmpMenus.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.beastcube.api.bukkit.menus.events;

import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

/**
 * An event called when an Item in the {@link ItemMenu} is clicked.
 */
public class ItemClickEvent {
    private Player player;
    private ItemClickAction action = ItemClickAction.NONE;
    private ClickType clickType;

    public ItemClickEvent(Player player, ClickType clickType) {
        this.player = player;
        this.clickType = clickType;
    }

    /**
     * Gets the click action that was performed (e.g. shift clicked)
     */
    public ClickType getClickType() {
        return clickType;
    }

    /**
     * Gets the player who clicked.
     *
     * @return The player who clicked.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Checks if the {@link ItemMenu} will go back to the parent menu.
     *
     * @return True if the {@link ItemMenu} will go back to the parent menu, else false.
     */
    @Deprecated
    public boolean willGoBack() {
        return this.action == ItemClickAction.GOBACK;
    }

    /**
     * Sets if the {@link ItemMenu} will go back to the parent menu.
     *
     * @param goBack If the {@link ItemMenu} will go back to the parent menu.
     */
    @Deprecated
    public void setWillGoBack(boolean goBack) {
        this.action = goBack ? ItemClickAction.GOBACK : ItemClickAction.NONE;
    }

    /**
     * Checks if the {@link ItemMenu} will close.
     *
     * @return True if the {@link ItemMenu} will close, else false.
     */
    @Deprecated
    public boolean willClose() {
        return this.action == ItemClickAction.CLOSE;
    }

    /**
     * Sets if the {@link ItemMenu} will close.
     *
     * @param close If the {@link ItemMenu} will close.
     */
    @Deprecated
    public void setWillClose(boolean close) {
        this.action = close ? ItemClickAction.CLOSE : ItemClickAction.NONE;
    }

    /**
     * Checks if the {@link ItemMenu} will update.
     *
     * @return True if the {@link ItemMenu} will update, else false.
     */
    @Deprecated
    public boolean willUpdate() {
        return this.action == ItemClickAction.UPDATE;
    }

    /**
     * Sets if the {@link ItemMenu} will update.
     *
     * @param update If the {@link ItemMenu} will update.
     */
    @Deprecated
    public void setWillUpdate(boolean update) {
        this.action = update ? ItemClickAction.UPDATE : ItemClickAction.NONE;
    }


    /**
     * Sets the {@link ItemClickAction}
     *
     * @param action The {@link ItemClickAction}
     */
    public void setAction(ItemClickAction action) {
        this.action = action;
    }

    /**
     * @return The ItemClickAction that has been set
     */
    public ItemClickAction getAction() {
        return this.action;
    }

    /**
     * Possible actions for an {@link MenuItem}.
     */
    public enum ItemClickAction {
        NONE,
        GOBACK,
        UPDATE,
        CLOSE,
        NEXTPAGE,
        PREVIOUSPAGE
    }

}

