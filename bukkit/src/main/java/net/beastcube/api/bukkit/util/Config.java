package net.beastcube.api.bukkit.util;

import net.beastcube.api.bukkit.config.annotation.AnnotationConfiguration;
import net.beastcube.api.bukkit.config.annotation.Setting;
import net.beastcube.api.bukkit.config.annotation.SettingBase;
import net.beastcube.api.commons.Providers;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

/**
 * @author BeastCube
 */
public class Config extends AnnotationConfiguration {

    public final DatabaseConfig database = new DatabaseConfig();

    @SettingBase("database")
    public class DatabaseConfig extends AnnotationConfiguration {
        @Setting
        public String host = "localhost";
        @Setting
        public int port = 3306;
        @Setting
        public String database = "beastcube";
        @Setting
        public String username = "root";
        @Setting
        public String password = "passwrd";
    }

    @Setting
    public String servername = Providers.RANDOM_NAME.next();
    @Setting
    public String externalIp = "";
    @Setting
    public String motd = "";


    private Plugin plugin = null;
    private static FileConfiguration config = null;

    public Config(Plugin plugin) {
        this.plugin = plugin;
        config = plugin.getConfig();
        this.load();
        this.save();
    }

    public void load() {
        load(config);
        database.load(config);
    }

    public void save() {
        save(config);
        database.save(config);
        plugin.saveConfig();
    }

}
