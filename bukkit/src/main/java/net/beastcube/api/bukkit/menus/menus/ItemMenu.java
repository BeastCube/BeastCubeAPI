/*
 * This file is part of AmpMenus.
 *
 * Copyright (c) 2014 <http://github.com/ampayne2/AmpMenus/>
 *
 * AmpMenus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AmpMenus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with AmpMenus.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.beastcube.api.bukkit.menus.menus;

import net.beastcube.api.bukkit.menus.MenuListener;
import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.menus.items.StaticMenuItem;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * A Menu controlled by ItemStacks in an Inventory.
 */
public class ItemMenu {
    protected JavaPlugin plugin;
    protected String name;
    protected Size size;
    protected Map<Integer, MenuItem> items = new HashMap<>();
    protected ItemMenu parent;

    /**
     * The {@link StaticMenuItem} that appears in empty slots if {@link ItemMenu#fillEmptySlots()} is called.
     */
    @SuppressWarnings("deprecation")
    private static final MenuItem EMPTY_SLOT_ITEM = new StaticMenuItem(" ", new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.GRAY.getData()));

    /**
     * Creates an {@link ItemMenu}.
     *
     * @param name   The name of the inventory.
     * @param size   The {@link ItemMenu.Size} of the inventory.
     * @param plugin The {@link org.bukkit.plugin.java.JavaPlugin} instance.
     * @param parent The ItemMenu's parent.
     */
    public ItemMenu(String name, Size size, JavaPlugin plugin, ItemMenu parent) {
        this.plugin = plugin;
        this.name = name;
        this.size = size;
        this.parent = parent;
    }

    /**
     * Creates an {@link ItemMenu} with no parent.
     *
     * @param name   The name of the inventory.
     * @param size   The {@link ItemMenu.Size} of the inventory.
     * @param plugin The {@link org.bukkit.plugin.java.JavaPlugin} instance.
     */
    public ItemMenu(String name, Size size, JavaPlugin plugin) {
        this(name, size, plugin, null);
    }

    /**
     * Gets the name of the {@link ItemMenu}.
     *
     * @return The {@link ItemMenu}'s name.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the {@link ItemMenu.Size} of the {@link ItemMenu}.
     *
     * @return The {@link ItemMenu}'s {@link ItemMenu.Size}.
     */
    public Size getSize() {
        return size;
    }

    /**
     * Checks if the {@link ItemMenu} has a parent.
     *
     * @return True if the {@link ItemMenu} has a parent, else false.
     */
    public boolean hasParent() {
        return parent != null;
    }

    /**
     * Gets the parent of the {@link ItemMenu}.
     *
     * @return The {@link ItemMenu}'s parent.
     */
    public ItemMenu getParent() {
        return parent;
    }

    /**
     * Sets the parent of the {@link ItemMenu}.
     *
     * @param parent The {@link ItemMenu}'s parent.
     */
    public void setParent(ItemMenu parent) {
        this.parent = parent;
    }

    /**
     * Sets the {@link MenuItem} of a slot.
     *
     * @param position The slot position.
     * @param menuItem The {@link MenuItem}.
     * @return The {@link ItemMenu}.
     */
    public ItemMenu setItem(int position, MenuItem menuItem) {
        items.put(position, menuItem);
        return this;
    }

    /**
     * Fills all empty slots in the {@link ItemMenu} with a certain {@link MenuItem}.
     *
     * @param menuItem The {@link MenuItem}.
     * @return The {@link ItemMenu}.
     */
    public ItemMenu fillEmptySlots(MenuItem menuItem) {
        if (items.size() > 0) {
            for (int i = 0; i < size.getSize(false); i++) {
                if (items.get(i) == null) {
                    items.put(i, menuItem);
                }
            }
        }
        return this;
    }

    /**
     * Fills all empty slots in the {@link ItemMenu} with the default empty slot item.
     *
     * @return The {@link ItemMenu}.
     */
    public ItemMenu fillEmptySlots() {
        return fillEmptySlots(EMPTY_SLOT_ITEM);
    }

    /**
     * Opens the {@link ItemMenu} for a player.
     *
     * @param player The player.
     */
    public void open(Player player) {
        if (!MenuListener.getInstance().isRegistered(plugin)) {
            MenuListener.getInstance().register(plugin);
        }
        Inventory inventory = Bukkit.createInventory(new MenuHolder(this, Bukkit.createInventory(player, size.getSize(false))), size.getSize(false), name);
        apply(inventory, player);
        player.openInventory(inventory);
    }

    /**
     * Closes the {@link ItemMenu} for a player.
     *
     * @param player The player.
     */
    public void close(Player player) {
        if (player.getOpenInventory() != null) {
            Inventory inventory = player.getOpenInventory().getTopInventory();
            if (inventory.getHolder() instanceof MenuHolder && ((MenuHolder) inventory.getHolder()).getMenu().equals(this)) {
                player.closeInventory();
            }
        }
    }

    /**
     * Updates the {@link ItemMenu} for a player.
     *
     * @param player The player to update the {@link ItemMenu} for.
     */
    @SuppressWarnings("deprecation")
    public void update(Player player) {
        if (player.getOpenInventory() != null) {
            Inventory inventory = player.getOpenInventory().getTopInventory();
            if (inventory.getHolder() instanceof MenuHolder && ((MenuHolder) inventory.getHolder()).getMenu().equals(this)) {
                apply(inventory, player);
                player.updateInventory();
            }
        }
    }

    /**
     * Applies the {@link ItemMenu} for a player to an Inventory.
     *
     * @param inventory The Inventory.
     * @param player    The Player.
     */
    protected void apply(Inventory inventory, Player player) {
        for (int i = 0; i < size.getSize(false); i++) {
            if (items.get(i) != null) {
                inventory.setItem(i, items.get(i).getFinalIcon(player, this));
            } else {
                inventory.setItem(i, null);
            }
        }
    }

    /**
     * Clears the {@link ItemMenu}.
     */
    public void clear() {
        items.clear();
    }

    /**
     * Handles InventoryClickEvents for the {@link ItemMenu}.
     */
    @SuppressWarnings("deprecation")
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getClick() == ClickType.LEFT || event.getClick() == ClickType.SHIFT_LEFT) { //TODO?
            int slot = event.getRawSlot();
            if (slot >= 0 && slot < size.getSize(false) && items.get(slot) != null) {
                Player player = (Player) event.getWhoClicked();
                ItemClickEvent itemClickEvent = new ItemClickEvent(player, event.getClick());
                items.get(slot).onItemClick(itemClickEvent);
                handleClickEvent(itemClickEvent, player);
            }
        }
    }

    public void handleClickEvent(ItemClickEvent e, Player player) {
        switch (e.getAction()) {
            case UPDATE:
                update(player);
                break;
            case CLOSE:
                player.closeInventory();
                break;
            case GOBACK:
                if (hasParent())
                    parent.open(player);
                break;
            default:
                break;
        }
    }

    /**
     * Destroys the {@link ItemMenu}.
     */
    public void destroy() {
        plugin = null;
        name = null;
        size = null;
        items = null;
        parent = null;
    }

    /**
     * Possible sizes of an {@link ItemMenu}.
     */
    public enum Size {
        ONE_LINE(9),
        TWO_LINE(18),
        THREE_LINE(27),
        FOUR_LINE(36),
        FIVE_LINE(45),
        SIX_LINE(54);

        private final int size;

        Size(int size) {
            this(size, false);
        }

        Size(int size, boolean paged) {
            this.size = size;
        }

        /**
         * Gets the {@link ItemMenu.Size}'s amount of slots.
         *
         * @return The amount of slots.
         */
        public int getSize(boolean paged) {
            return paged ? size - 9 : size;
        }

        /**
         * Gets the required {@link ItemMenu.Size} for an amount of slots.
         *
         * @param slots The amount of slots.
         * @return The required {@link ItemMenu.Size}.
         */
        public static Size fit(int slots) {
            return fit(slots, false);
        }

        /**
         * Gets the required {@link ItemMenu.Size} for an amount of slots.
         *
         * @param slots The amount of slots.
         * @param paged Whether or not it is a paged menu.
         * @return The required {@link ItemMenu.Size}.
         */
        public static Size fit(int slots, boolean paged) {
            final int slot = paged ? slots - 9 : slots;

            return Arrays.asList(Size.values()).stream()
                    .filter(size1 -> slot < size1.getSize(paged) + 1)
                    .sorted((o1, o2) -> Integer.compare(o1.getSize(paged), o2.getSize(paged))).findFirst().orElse(Size.SIX_LINE); //TODO Not working correct
        }
    }
}
