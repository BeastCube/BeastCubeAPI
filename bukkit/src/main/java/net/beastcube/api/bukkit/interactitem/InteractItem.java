package net.beastcube.api.bukkit.interactitem;

import net.beastcube.api.bukkit.util.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @author BeastCube
 */
public abstract class InteractItem {

    public abstract void execute(Player p, JavaPlugin plugin, Object data);

    @SuppressWarnings("deprecation")
    public boolean isItem(ItemStack stack) {
        if (stack != null) {
            if (stack.getType() == getMaterial() && stack.hasItemMeta()) {
                ItemMeta meta = stack.getItemMeta();
                if (meta.getLore() == null) {
                    if (stack.getData() == null) {
                        return meta.getDisplayName().equals(getDisplayName());
                    } else {
                        return meta.getDisplayName().equals(getDisplayName()) && stack.getData().getData() == getData();
                    }
                } else {
                    if (stack.getData() == null) {
                        return meta.getDisplayName().equals(getDisplayName()) && Arrays.equals(getLore().toArray(), meta.getLore().toArray());
                    } else {
                        return meta.getDisplayName().equals(getDisplayName()) && stack.getData().getData() == getData() && Arrays.equals(getLore().toArray(), meta.getLore().toArray());
                    }
                }
            }
        }
        return false;
    }

    public ItemStack createItemStack() {
        ItemBuilder ib = new ItemBuilder(getMaterial()).amount(getAmount()).durability(getData()).name(getDisplayName());
        getLore().forEach(ib::lore);
        getEnchantments().entrySet().forEach(e -> ib.enchantment(e.getKey(), e.getValue()));
        ib.addAllFlags();
        return ib.build();
    }

    public abstract String getDisplayName();

    public HashMap<Enchantment, Integer> getEnchantments() {
        return new HashMap<>();
    }

    public abstract Material getMaterial();

    public List<String> getLore() {
        return new ArrayList<>();
    }

    public Byte getAmount() {
        return 1;
    }

    public byte getData() {
        return 0;
    }
}
