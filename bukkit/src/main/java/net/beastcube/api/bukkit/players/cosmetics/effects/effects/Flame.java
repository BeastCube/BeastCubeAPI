package net.beastcube.api.bukkit.players.cosmetics.effects.effects;

import de.slikey.effectlib.effect.FlameEffect;
import lombok.Getter;
import net.beastcube.api.bukkit.players.cosmetics.effects.Effect;
import net.beastcube.api.bukkit.players.cosmetics.effects.EffectManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * @author BeastCube
 */
public class Flame extends Effect {
    @Getter
    private String id = "flame";

    @Override
    public String getDescription() {
        return ChatColor.GOLD + "Burn baby, burn!";
    }

    @Override
    public String getDisplayName() {
        return EffectManager.DISPLAY_NAME_COLOR + "Flame";
    }

    @Override
    public Material getMaterial() {
        return Material.BLAZE_POWDER;
    }

    @Override
    public de.slikey.effectlib.Effect getEffect(Player p, de.slikey.effectlib.EffectManager e) {
        return new FlameEffect(e);
    }
}
