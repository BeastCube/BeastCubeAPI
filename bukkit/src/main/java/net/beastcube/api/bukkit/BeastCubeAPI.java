package net.beastcube.api.bukkit;

import com.comphenix.protocol.ProtocolManager;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.ikeirnez.pluginmessageframework.bukkit.BukkitGatewayProvider;
import com.ikeirnez.pluginmessageframework.gateway.ServerGateway;
import de.slikey.effectlib.EffectLib;
import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.entity.EntityManager;
import lombok.Getter;
import lombok.Setter;
import net.beastcube.api.bukkit.chat.listeners.ChatListener;
import net.beastcube.api.bukkit.commands.CommandHandler;
import net.beastcube.api.bukkit.fakemob.FakeMobsPlugin;
import net.beastcube.api.bukkit.fakemob.listener.ProtocolListener;
import net.beastcube.api.bukkit.interactitem.InteractItemManager;
import net.beastcube.api.bukkit.menus.MenuListener;
import net.beastcube.api.bukkit.minigames.MinigamesManager;
import net.beastcube.api.bukkit.network.IncomingPacketHandler;
import net.beastcube.api.bukkit.players.TeleportScheduler;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticManager;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.GadgetListener;
import net.beastcube.api.bukkit.players.nametag.NametagAPI;
import net.beastcube.api.bukkit.players.nametag.NametagManager;
import net.beastcube.api.bukkit.signs.Signs;
import net.beastcube.api.bukkit.util.Config;
import net.beastcube.api.bukkit.util.title.TitleAPI;
import net.beastcube.api.commons.database.DatabaseHandler;
import net.beastcube.api.commons.database.mysql.BukkitDatabase;
import net.beastcube.api.commons.database.mysql.DatabaseHelper;
import net.beastcube.api.commons.packets.ConnectToLobbyPacket;
import net.beastcube.api.commons.permissions.Role;
import net.beastcube.api.commons.permissions.Roles;
import net.beastcube.api.commons.util.Constants;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.UUID;

/**
 * @author BeastCube
 */
public class BeastCubeAPI implements Listener {

    public static String pluginPrefix = ChatColor.GRAY + "[" + ChatColor.RED + "BeastCubeAPI" + ChatColor.GRAY + "] " + ChatColor.WHITE;
    public static String consolepluginPrefix = "[BeastCubeAPI]";
    @Getter
    @Setter
    private static boolean chatNameEnabled = true;
    @Getter
    @Setter
    private static boolean nametagEnabled = true;
    @Getter
    @Setter
    private static boolean friendsTeleportEnabled = true;
    @Getter
    private static CommandHandler commandHandler;

    public static EffectManager effectManager;
    public static EntityManager entityManager;
    public static Config config;
    private static BeastCubeAPI instance;

    @Getter
    @Setter
    private ProtocolListener protocolListener;
    @Getter
    @Setter
    private ProtocolManager protocolManager;
    private FakeMobsPlugin fakeMobplugin;
    @Getter
    private static ServerGateway<Player> gateway;
    @Getter
    private static MinigamesManager minigamesManager;
    @Getter
    HashMap<UUID, PermissionAttachment> permissionAttachments = new HashMap<>();

    public void onEnable() {
        JavaPlugin plugin = BeastCubeSlaveBukkitPlugin.getInstance();
        gateway = BukkitGatewayProvider.getGateway(Constants.CHANNELNAME, plugin);
        gateway.registerListener(new IncomingPacketHandler());

        plugin.getServer().getMessenger().registerOutgoingPluginChannel(plugin, "BungeeCord");

        commandHandler = new CommandHandler(plugin);
        commandHandler.registerCommands(this);

        commandHandler.setPermissionHandler(BeastCubeAPI::handlePermission);

        DatabaseHelper.database = new BukkitDatabase(config.database.host, config.database.port, config.database.database, config.database.username, config.database.password, plugin);
        DatabaseHelper.database.connect();
        if (DatabaseHandler.createTables()) {
            plugin.getLogger().info("Successfully connected to database and created tables!");
        } else {
            plugin.getLogger().severe("Failed to connect to database!");
        }

        Bukkit.getPluginManager().registerEvents(new InteractItemManager(), plugin);
        Bukkit.getPluginManager().registerEvents(new GadgetListener(), plugin);
        Bukkit.getPluginManager().registerEvents(new ChatListener(), plugin);
        Bukkit.getPluginManager().registerEvents(new Signs(), plugin);
        Bukkit.getPluginManager().registerEvents(new TeleportScheduler(), plugin);
        Bukkit.getPluginManager().registerEvents(this, plugin);

        effectManager = new EffectManager(EffectLib.instance());
        entityManager = new EntityManager(EffectLib.instance());
        fakeMobplugin = new FakeMobsPlugin();
        fakeMobplugin.onEnable(this);
        Bukkit.getPluginManager().registerEvents(fakeMobplugin, plugin);
        Bukkit.getOnlinePlayers().forEach(BeastCubeAPI::updateNameTag);
        NametagManager.load();
        MenuListener.getInstance().register(plugin);
        minigamesManager = new MinigamesManager();
    }

    public static boolean handlePermission(CommandSender sender, String... permissions) {
        if (sender instanceof Player) {
            Role role = Roles.getRole(((Player) sender).getUniqueId());
            for (String perm : permissions) {
                if (!role.hasPermission(perm))
                    return false;
            }
            return true;
        } else if (sender instanceof ConsoleCommandSender) {
            return true;
        }
        return false;
    }

    public void onDisable() {
        effectManager.dispose();
        fakeMobplugin.onDisable();
        NametagManager.reset();
        //PetListener.onDisable();
        //nicknamer.onDisable();
    }

    public void loadConfiguration() {
        config = new Config(BeastCubeSlaveBukkitPlugin.getInstance());
    }

    /**
     * Teleports a Player to the given server
     *
     * @param p          The player who is being teleported
     * @param serverName The server name to connect
     */
    public static void connect(Player p, String serverName) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(serverName);
        p.sendPluginMessage(BeastCubeSlaveBukkitPlugin.getInstance(), "BungeeCord", out.toByteArray());
    }

    public static void connectToHub(Player p) {
        gateway.sendPacket(p, new ConnectToLobbyPacket());
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent e) {
        final Player p = e.getPlayer();
        Bukkit.getScheduler().runTask(BeastCubeSlaveBukkitPlugin.getInstance(), () -> {
            TitleAPI.sendTabTitle(p, ChatColor.GREEN + "BeastCube Network" + " | " + ChatColor.WHITE + getServerName(), ChatColor.GOLD + "Website/Forum:" + ChatColor.AQUA + " beastcube.net");
            updateNameTag(p);
            NametagManager.sendTeamsToPlayer(p);
            NametagManager.clear(p.getName());
            CosmeticManager.removeAll(p);
            /*
            Bukkit.getScheduler().runTask(plugin, new Runnable() {
                @Override
                public void run() {
                    //TODO plugin.getNTEHandler.applyTagToPlayer
                }
            });
            */

        });
        setPermissions(p);
    }

    public void setPermissions(Player p) {
        PermissionAttachment attachment = permissionAttachments.get(p.getUniqueId());
        if (attachment == null) {
            attachment = p.addAttachment(BeastCubeSlaveBukkitPlugin.getInstance());
            permissionAttachments.put(p.getUniqueId(), attachment);
        }
        final PermissionAttachment att = attachment;
        att.getPermissions().forEach((s, x) -> att.unsetPermission(s));
        Roles.getRole(p.getUniqueId()).getPermissions().forEach(x -> att.setPermission(x, true));
    }

    public static String getServerName() {
        return config.servername;
    }

    public static void updateNameTag(Player p) { //TODO Use packetlistenerapi
        if (nametagEnabled) { //TODO Maybe not working because hub plugin not installed
            Role role = Roles.getRole(p.getUniqueId());
            if (role != Roles.DEFAULT) {
                NametagAPI.setPrefix(p.getName(), role.getColor() + role.getName() + ChatColor.RESET + " ");
            } else {
                NametagAPI.clear(p.getName());
            }
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        disableLobbyFeatures(e.getPlayer());
        NametagManager.clear(e.getPlayer().getName());
    }

    public static void disableLobbyFeatures(Player p) {
        CosmeticManager.removeAll(p);
    }

    public static BeastCubeAPI getInstance() {
        return instance != null ? instance : (instance = new BeastCubeAPI());
    }

}