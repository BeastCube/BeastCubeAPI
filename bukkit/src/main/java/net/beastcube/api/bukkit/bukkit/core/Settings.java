package net.beastcube.api.bukkit.bukkit.core;

import org.bukkit.entity.Player;

/**
 * All settings.
 *
 * @author Mato Kormuth
 */
public enum Settings {
    /**
     * If the music would be played at the end of music.
     */
    ENDROUND_MUSIC,
    /**
     * Chat has sounds, when player name is in it.
     */
    CHAT_SOUNDS;

    public boolean hasEnabled(final Player player) {
        //TODO Mysql
        return true;
    }
}
