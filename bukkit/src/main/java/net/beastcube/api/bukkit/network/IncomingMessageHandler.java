package net.beastcube.api.bukkit.network;

import net.beastcube.api.bukkit.BeastCubeSlave;
import net.beastcube.api.bukkit.events.SlaveConnectedEvent;
import net.beastcube.api.network.MessageHandler;
import net.beastcube.api.network.ServerInfo;
import net.beastcube.api.network.message.SlaveConnectedMessage;
import org.bukkit.Bukkit;

/**
 * @author BeastCube
 */
public class IncomingMessageHandler {

    @MessageHandler
    public void onSlaveConnected(ServerInfo server, SlaveConnectedMessage message) {
        BeastCubeSlave.getInstance().getLogger().info("Connected to master server!");
        Bukkit.getPluginManager().callEvent(new SlaveConnectedEvent(server));
    }

}
