package net.beastcube.api.bukkit.commands;

import java.lang.reflect.Method;


public class RootCommand extends RegisteredCommand {
    private DynamicCommand root;

    RootCommand(DynamicCommand root, CommandHandler handler) {
        super(root.getLabel(), handler, null);
        this.root = root;
    }

    public DynamicCommand getBukkitCommand() {
        return root;
    }

    @Override
    void set(Object methodInstance, Method method) {
        super.set(methodInstance, method);
        root.setDescription(getDescription());
        root.setUsage(getUsage());
    }
}
