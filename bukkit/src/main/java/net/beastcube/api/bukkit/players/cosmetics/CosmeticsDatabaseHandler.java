package net.beastcube.api.bukkit.players.cosmetics;

import net.beastcube.api.commons.database.mysql.DatabaseHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * @author BeastCube
 */
public class CosmeticsDatabaseHandler {

    public static void buy(UUID player, CosmeticItem item) {
        DatabaseHelper.database.execute("INSERT INTO cosmetics (player, type, item) VALUES (?, ?, ?)", player.toString(), item.getType().getId(), item.getId());
    }

    public static Boolean hasBought(UUID player, CosmeticItem item) {
        try {
            ResultSet result = DatabaseHelper.database.query("SELECT * FROM cosmetics WHERE player=? AND type=? AND item=?", player.toString(), item.getType().getId(), item.getId());
            if (result.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}
