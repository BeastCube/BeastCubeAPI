package net.beastcube.api.bukkit.fakemob.command;

import net.beastcube.api.bukkit.fakemob.FakeMobsPlugin;
import net.beastcube.api.bukkit.fakemob.util.Cache;
import net.beastcube.api.bukkit.fakemob.util.FakeMob;
import net.beastcube.api.bukkit.fakemob.util.ItemParser;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FakeMobCommand implements CommandExecutor {
    private FakeMobsPlugin plugin;

    @SuppressWarnings("deprecation")
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "You are not a Player!");
            return true;
        }
        Player player = (Player) sender;
        if (args.length == 0) return false;
        if (args[0].equalsIgnoreCase("create")) {
            if (args.length > 1) {
                if (!player.hasPermission("fakemob.create")) {
                    player.sendMessage(ChatColor.RED + "No permission!");
                    return true;
                }
                Location loc = player.getLocation();
                if (FakeMobsPlugin.isMobOnLocation(loc)) {
                    player.sendMessage(ChatColor.RED + "Here is already a Mob!");
                    return true;
                }
                EntityType type;
                try {
                    type = EntityType.valueOf(args[1].toUpperCase());
                } catch (Exception e) {
                    player.sendMessage(ChatColor.RED + args[1] + " is not a Entity!");
                    StringBuilder entityBuilder = new StringBuilder();
                    boolean komma = false;
                    for (int i = 0; i < EntityType.values().length; i++) {
                        EntityType t = EntityType.values()[i];
                        if (t == null || !t.isAlive() || t.getName() == null) continue;
                        if (komma) entityBuilder.append(", ");
                        entityBuilder.append(t.name());
                        komma = true;
                    }
                    player.sendMessage(ChatColor.GOLD + "Available Entitys: " + ChatColor.WHITE + entityBuilder.toString());
                    return true;
                }
                if (!type.isAlive()) {
                    player.sendMessage(ChatColor.RED + "This entity is not alive!");
                    return true;
                }
                FakeMob mob = FakeMobsPlugin.spawnMob(loc, type);
                if (mob == null) {
                    player.sendMessage(ChatColor.RED + "An error occurred while creating the Mob!");
                    return true;
                }
                player.sendMessage(ChatColor.GREEN + "Created and selected Mob with ID " + ChatColor.GRAY + "#" + mob.getId());
                Cache.selectedMobs.put(player, mob);
                return true;
            } else {
                return false;
            }
        } else if (args[0].equalsIgnoreCase("interactaction")) {
            if (args.length < 2) return false;
            if (!player.hasPermission("fakemob.interactaction")) {
                player.sendMessage(ChatColor.RED + "No permission!");
                return true;
            }
            FakeMob mob = Cache.selectedMobs.get(player);
            if (mob == null) {
                player.sendMessage(ChatColor.RED + "You don't have a Selection!");
                return true;
            }
            mob.setInteractAction(args[1]);
            player.sendMessage(ChatColor.GREEN + "Mob InteractAction set to " + ChatColor.GRAY + args[1] + ChatColor.GREEN + "!");
            FakeMobsPlugin.saveMobs();
            return true;
        } else if (args[0].equalsIgnoreCase("mobdata")) {
            if (args.length < 2) return false;
            if (!player.hasPermission("fakemob.mobdata")) {
                player.sendMessage(ChatColor.RED + "No permission!");
                return true;
            }
            FakeMob mob = Cache.selectedMobs.get(player);
            if (mob == null) {
                player.sendMessage(ChatColor.RED + "You don't have a Selection!");
                return true;
            }

            String msg = "";
            boolean space = false;
            for (int i = 1; i < args.length; i++) {
                if (space) {
                    msg += " ";
                } else {
                    space = true;
                }
                msg += args[i];
            }

            mob.setMobData(msg);
            player.sendMessage(ChatColor.GREEN + "MobData set to " + ChatColor.GRAY + msg + ChatColor.GREEN + "!");
            FakeMobsPlugin.saveMobs();
            return true;
        } else if (args[0].equalsIgnoreCase("select")) {
            if (args.length != 1 && args.length != 2) return false;
            if (!player.hasPermission("fakemob.select")) {
                player.sendMessage(ChatColor.RED + "No permission!");
                return true;
            }
            if (args.length == 2) {
                int id;
                try {
                    id = Integer.valueOf(args[1]);
                } catch (Exception e) {
                    player.sendMessage(ChatColor.RED + "Please enter a valid Id!");
                    return true;
                }
                FakeMob mob = FakeMobsPlugin.getMob(id);
                if (mob == null) {
                    player.sendMessage(ChatColor.RED + "A Mob with ID " + ChatColor.GRAY + "#" + id + ChatColor.RED + " doesn't exist!");
                    return true;
                }
                Cache.selectedMobs.put(player, mob);
                player.sendMessage(ChatColor.GREEN + "Mob " + ChatColor.GRAY + "#" + id + ChatColor.GREEN + " selected!");
                return true;
            }

            if (Cache.selectedMobs.containsKey(player) && Cache.selectedMobs.get(player) == null) {
                Cache.selectedMobs.remove(player);
                player.sendMessage(ChatColor.GOLD + "Selection cancelled!");
            } else {
                Cache.selectedMobs.put(player, null);
                player.sendMessage(ChatColor.GREEN + "Click on the FakeMob!");
            }
            return true;
        } else if (args[0].equalsIgnoreCase("name")) {
            if (args.length < 2) return false;
            if (!player.hasPermission("fakemob.name")) {
                player.sendMessage(ChatColor.RED + "No permission!");
                return true;
            }
            FakeMob mob = Cache.selectedMobs.get(player);
            if (mob == null) {
                player.sendMessage(ChatColor.RED + "You don't have a Selection!");
                return true;
            }
            StringBuilder textBuilder = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                if (i != 1) textBuilder.append(" ");
                textBuilder.append(args[i]);
            }
            String text = ChatColor.translateAlternateColorCodes('&', textBuilder.toString());
            if (text.length() > 16) {
                player.sendMessage(ChatColor.RED + "Name too long!");
                return true;
            }
            if (text.equalsIgnoreCase("none")) {
                mob.setCustomName(null);
                player.sendMessage(ChatColor.GREEN + "Mob Name deleted!");
            } else {
                mob.setCustomName(text);
                player.sendMessage(ChatColor.GREEN + "Mob Name set to " + ChatColor.GRAY + text + ChatColor.GREEN + "!");
            }
            mob.updateCustomName();
            FakeMobsPlugin.saveMobs();
            return true;
        } else if (args[0].equalsIgnoreCase("sitting")) {
            if (args.length < 1) return false;
            if (!player.hasPermission("fakemob.sitting")) {
                player.sendMessage(ChatColor.RED + "No permission!");
                return true;
            }
            FakeMob mob = Cache.selectedMobs.get(player);
            if (mob == null) {
                player.sendMessage(ChatColor.RED + "You don't have a Selection!");
                return true;
            }
            if (mob.getType() != EntityType.OCELOT && mob.getType() != EntityType.WOLF && mob.getType() != EntityType.PLAYER) {
                player.sendMessage(ChatColor.RED + "Only pets and players can sit!");
                return true;
            }
            mob.setSitting(!mob.isSitting());
            mob.updateMetadata();
            FakeMobsPlugin.saveMobs();
            player.sendMessage(ChatColor.GREEN + "Sitting Status changed: " + ChatColor.GRAY + ((mob.isSitting()) ? "on" : "off"));
            return true;
        } else if (args[0].equalsIgnoreCase("invisibility")) {
            if (args.length < 1) return false;
            if (!player.hasPermission("FakeMobs.invisibility")) {
                player.sendMessage(ChatColor.RED + "No permission!");
                return true;
            }
            FakeMob mob = Cache.selectedMobs.get(player);
            if (mob == null) {
                player.sendMessage(ChatColor.RED + "You haven't a Selection!");
                return true;
            }
            mob.setInvisibility(!mob.isInvisibility());
            mob.updateMetadata();
            FakeMobsPlugin.saveMobs();
            player.sendMessage(ChatColor.GREEN + "Invisibility Status changed: " + ChatColor.GRAY + ((mob.isInvisibility()) ? "on" : "off"));
            return true;
        } else if (args[0].equalsIgnoreCase("look")) {
            if (args.length < 1) return false;
            if (!player.hasPermission("fakemob.look")) {
                player.sendMessage(ChatColor.RED + "No permission!");
                return true;
            }
            FakeMob mob = Cache.selectedMobs.get(player);
            if (mob == null) {
                player.sendMessage(ChatColor.RED + "You don't have a Selection!");
                return true;
            }
            mob.setPlayerLook(!mob.isPlayerLook());
            if (mob.isPlayerLook())
                mob.sendLookPacket(player, player.getLocation());
            FakeMobsPlugin.saveMobs();
            player.sendMessage(ChatColor.GREEN + "Player Look: " + ChatColor.GRAY + ((mob.isPlayerLook()) ? "on" : "off"));
            return true;
        } else if (args[0].equalsIgnoreCase("teleportation")) {
            if (args.length != 1) return false;
            if (!player.hasPermission("fakemob.teleportation")) {
                player.sendMessage(ChatColor.RED + "No permission!");
                return true;
            }
            FakeMob mob = Cache.selectedMobs.get(player);
            if (mob == null) {
                player.sendMessage(ChatColor.RED + "You don't have a Selection!");
                return true;
            }
            if (player.getLocation().getBlock().getType() != Material.AIR) {
                player.sendMessage(ChatColor.RED + "You standing in a Block!");
                return true;
            }
            mob.teleport(player.getLocation());
            FakeMobsPlugin.saveMobs();
            player.sendMessage(ChatColor.GREEN + "Teleported Mob " + ChatColor.GRAY + "#" + mob.getId() + ChatColor.GREEN + "!");
            return true;
        } else if (args[0].equalsIgnoreCase("skin")) {
            if (args.length != 2) return false;
            if (!player.hasPermission("FakeMobs.skin")) {
                player.sendMessage(ChatColor.RED + "No permission!");
                return true;
            }

            FakeMob mob = Cache.selectedMobs.get(player);
            if (mob == null) {
                player.sendMessage(ChatColor.RED + "You don't have a selection!");
                return true;
            }
            if (mob.getType() != EntityType.PLAYER) {
                player.sendMessage(ChatColor.RED + "Only players can have a skin!");
                return true;
            }

            FakeMobsPlugin.getSkinQueue().addToQueue(mob, args[1]);
            player.sendMessage(ChatColor.GREEN + "Skin set!");
            return true;
        } else if (args[0].equalsIgnoreCase("inv")) {
            if (args.length < 3) return false;
            if (!player.hasPermission("fakemob.inv")) {
                player.sendMessage(ChatColor.RED + "No permission!");
                return true;
            }
            FakeMob mob = Cache.selectedMobs.get(player);
            if (mob == null) {
                player.sendMessage(ChatColor.RED + "You don't have a Selection!");
                return true;
            }

            List<String> itemArgs = new ArrayList<>();
            itemArgs.addAll(Arrays.asList(args).subList(2, args.length));

            ItemStack item;
            try {
                item = ItemParser.generateItemStack(itemArgs.toArray(new String[itemArgs.size()]));
            } catch (Exception e) {
                player.sendMessage(ChatColor.RED + "Invalid item: " + e.getMessage());
                return true;
            }

            if (args[1].equalsIgnoreCase("hand")) {
                mob.getInventory().setItemInHand(item);
                player.sendMessage(ChatColor.GOLD + "Setted Item in Hand to " + item.getType().name() + "!");
            } else if (args[1].equalsIgnoreCase("boots")) {
                mob.getInventory().setBoots(item);
                player.sendMessage(ChatColor.GOLD + "Setted Boots to " + item.getType().name() + "!");
            } else if (args[1].equalsIgnoreCase("leggings")) {
                mob.getInventory().setLeggings(item);
                player.sendMessage(ChatColor.GOLD + "Setted Leggings to " + item.getType().name() + "!");
            } else if (args[1].equalsIgnoreCase("chestplate")) {
                mob.getInventory().setChestPlate(item);
                player.sendMessage(ChatColor.GOLD + "Setted ChestPlate to " + item.getType().name() + "!");
            } else if (args[1].equalsIgnoreCase("helmet")) {
                mob.getInventory().setHelmet(item);
                player.sendMessage(ChatColor.GOLD + "Setted Helmet to " + item.getType().name() + "!");
            } else
                return false;

            mob.updateInventory();
            FakeMobsPlugin.saveMobs();
            return true;
        } else if (args[0].equalsIgnoreCase("remove")) {
            if (args.length != 1) return false;
            if (!player.hasPermission("fakemob.remove")) {
                player.sendMessage(ChatColor.RED + "No permission!");
                return true;
            }
            FakeMob mob = Cache.selectedMobs.get(player);
            if (mob == null) {
                player.sendMessage(ChatColor.RED + "You don't have a Selection!");
                return true;
            }
            FakeMobsPlugin.removeMob(mob.getId());
            player.sendMessage(ChatColor.GREEN + "Mob " + ChatColor.GRAY + "#" + mob.getId() + ChatColor.GREEN + " removed!");
            return true;
        } else if (args[0].equalsIgnoreCase("help")) {
            if (args.length != 1) return false;
            if (!player.hasPermission("fakemob.help")) {
                player.sendMessage(ChatColor.RED + "No permission!");
                return true;
            }
            player.sendMessage(ChatColor.GOLD + "Help for " + ChatColor.GRAY + "/FakeMob");
            player.sendMessage(ChatColor.GRAY + "/fakeMob create <Type> " + ChatColor.RED + "-- " + ChatColor.WHITE + "Spawn a Fakemob");
            player.sendMessage(ChatColor.GRAY + "/fakeMob select [id] " + ChatColor.RED + "-- " + ChatColor.WHITE + "Select a Fakemob");
            player.sendMessage(ChatColor.GRAY + "/fakeMob name <Name/none> " + ChatColor.RED + "-- " + ChatColor.WHITE + "Give the Fakemob a name");
            player.sendMessage(ChatColor.GRAY + "/fakeMob sitting " + ChatColor.RED + "-- " + ChatColor.WHITE + "Change the Sitting state of a pet and players (Wolf/Ocelot/Player)");
            player.sendMessage(ChatColor.GRAY + "/FakeMob invisibility " + ChatColor.RED + "-- " + ChatColor.WHITE + "Make the fakemob invisibility");
            player.sendMessage(ChatColor.GRAY + "/fakeMob look " + ChatColor.RED + "-- " + ChatColor.WHITE + "Enable/Disable the Players Look");
            player.sendMessage(ChatColor.GRAY + "/fakeMob teleportation " + ChatColor.RED + "-- " + ChatColor.WHITE + "Teleport a Fakemob to you");
            player.sendMessage(ChatColor.GRAY + "/fakeMob inv <hand/boots/leggings/chestplate/helmet> <Item> " + ChatColor.RED + "-- " + ChatColor.WHITE + "Set the Inventory of a Fakemob. Use none to delete.");
            player.sendMessage(ChatColor.GRAY + "/fakeMob mobdata " + ChatColor.RED + "-- " + ChatColor.WHITE + "Set the MobData");
            player.sendMessage(ChatColor.GRAY + "/fakeMob interactaction " + ChatColor.RED + "-- " + ChatColor.WHITE + "Set the Interact Action");
            player.sendMessage(ChatColor.GRAY + "/FakeMob skin <Playername> " + ChatColor.RED + "-- " + ChatColor.WHITE + "Set the skin for a player fakemob");
            player.sendMessage(ChatColor.GRAY + "/fakeMob remove " + ChatColor.RED + "-- " + ChatColor.WHITE + "Remove a Fakemob");
            return true;
        } else
            return false;
    }

}
