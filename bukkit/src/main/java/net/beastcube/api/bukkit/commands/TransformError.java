package net.beastcube.api.bukkit.commands;

public class TransformError extends CommandError {
    private static final long serialVersionUID = 1L;

    public TransformError(String msg) {
        super(msg);
    }

}
