package net.beastcube.api.bukkit.players.cosmetics.effects;

import de.slikey.effectlib.EffectManager;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticItem;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticType;
import org.bukkit.entity.Player;

/**
 * @author BeastCube
 */
public abstract class Effect extends CosmeticItem {

    public abstract de.slikey.effectlib.Effect getEffect(Player p, EffectManager e);

    @Override
    public CosmeticType getType() {
        return CosmeticType.EFFECT;
    }
}
