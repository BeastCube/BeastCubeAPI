package net.beastcube.api.bukkit.minigames;

import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.BeastCubeSlaveBukkitPlugin;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.api.commons.minigames.MinigameInstance;
import net.beastcube.api.commons.minigames.MinigameState;
import net.beastcube.api.commons.minigames.MinigameType;
import net.beastcube.api.commons.packets.MinigamesUpdateRequestPacket;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author BeastCube
 */
public class MinigamesManager {
    public static ChatColor InventoryTitleColor = ChatColor.AQUA;
    public static HashMap<MinigameType, ItemMenu> minigameInventories = new HashMap<>();
    private boolean initialized;

    public void init() {
        this.initialized = true;
        JavaPlugin plugin = BeastCubeSlaveBukkitPlugin.getInstance();
        for (MinigameType minigame : MinigameType.values()) {
            ItemMenu menu = new ItemMenu(InventoryTitleColor + minigame.getDisplayName(), ItemMenu.Size.FOUR_LINE, plugin);
            minigameInventories.put(minigame, menu);
        }
        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> BeastCubeAPI.getGateway().sendPacket(new MinigamesUpdateRequestPacket(), false), 0L, 2 * 20L);
    }

    public void refreshMinigames(List<MinigameInstance> minigameInstances) {
        if (initialized && Bukkit.getOnlinePlayers().size() > 0) {
            for (MinigameType mgtype : MinigameType.values()) {
                ItemMenu menu = minigameInventories.get(mgtype);
                menu.clear();
                List<ServerMenuItem> list = minigameInstances.stream().filter(item -> item.getType() == mgtype)
                        .filter(item -> item.getState() != MinigameState.DEVELOPMENT)
                        .map(ServerMenuItem::new)
                        .sorted((menu1, menu2) -> Boolean.compare(menu1.getMinigameInstance().isJoinable(), menu2.getMinigameInstance().isJoinable()))
                        .collect(Collectors.toList());
                for (int i = 0; i < list.size(); i++) {
                    menu.setItem(i, list.get(i));
                }
                Bukkit.getOnlinePlayers().forEach(menu::update);
            }
        }
    }
}
