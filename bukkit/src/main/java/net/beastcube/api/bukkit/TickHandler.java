package net.beastcube.api.bukkit;

/**
 * Interaface that represents tick handler.
 */
public interface TickHandler {
    /**
     * Fired when server tick occurs.
     *
     * @param number of tick currently executed
     */
    void tick(long number);
}
