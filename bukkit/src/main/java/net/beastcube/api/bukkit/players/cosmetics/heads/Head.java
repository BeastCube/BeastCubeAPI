package net.beastcube.api.bukkit.players.cosmetics.heads;

import com.mojang.util.UUIDTypeAdapter;
import lombok.Getter;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticItem;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticType;
import net.beastcube.api.commons.util.UUIDFetcher;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.UUID;

/**
 * @author BeastCube
 */
public class Head extends CosmeticItem {

    private final UUID uuid;
    @Getter
    private String id;
    @Getter
    private String skullOwner = "";
    private Material material = Material.SKULL_ITEM;

    public Head(String id, String displayName, String uuid, String lore) {
        this(id, displayName, UUIDTypeAdapter.fromString(uuid), lore);
    }

    public Head(String id, String displayName, UUID uuid, String lore) {
        this.id = id;
        this.uuid = uuid;
        this.displayName = displayName;
        this.description = lore;
    }

    public void load() {
        UUIDFetcher.getName(uuid, s -> skullOwner = s); //TODO is this asynch?
    }

    @Override
    public ItemStack getIcon(Player p) {
        ItemStack skull = new ItemStack(material);
        SkullMeta sm = (SkullMeta) skull.getItemMeta();
        sm.setDisplayName(ChatColor.GREEN + getSkullOwner());
        sm.setLore(getLore(p));
        skull.setDurability((short) SkullType.PLAYER.ordinal());
        sm.setOwner(getSkullOwner());
        skull.setItemMeta(sm);
        return skull;
    }


    @Override
    public CosmeticType getType() {
        return CosmeticType.HEAD;
    }

    @Override
    public String getDisplayName() {
        return HeadManager.DISPLAY_NAME_COLOR + displayName;
    }

    @Override
    public Material getMaterial() {
        return material;
    }

}