package net.beastcube.api.bukkit.players.cosmetics.gadgets;

import mkremins.fanciful.FancyMessage;
import net.beastcube.api.bukkit.BeastCubeSlaveBukkitPlugin;
import net.beastcube.api.bukkit.players.cosmetics.PersonalChest;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.gadgets.TeleportBow;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Optional;

/**
 * @author BeastCube
 */
public class GadgetListener implements Listener {

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        final Player p = e.getPlayer();
        if (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK) {
            final Optional<Gadget> gadget = PersonalChest.gadgetManager.getCosmetic(p.getItemInHand());
            if (gadget.isPresent()) {
                e.setCancelled(true);
                if (gadget.get().hasLeftClickAction()) {
                    if (gadget.get().getCooldown() > 0) {
                        if (gadget.get().getCooldowns().contains(p.getUniqueId())) {
                            new FancyMessage("Du kannst dieses Gadget nur alle ").color(ChatColor.RED).then(String.valueOf(gadget.get().getCooldown() / 20)).color(ChatColor.AQUA).then(" Sekunden benutzen!").color(ChatColor.RED).send(p);
                        } else {
                            if (gadget.get().LeftClick(p)) {
                                gadget.get().getCooldowns().add(p.getUniqueId());
                                Bukkit.getScheduler().scheduleSyncDelayedTask(BeastCubeSlaveBukkitPlugin.getInstance(), () -> gadget.get().getCooldowns().remove(p.getUniqueId()), gadget.get().getCooldown());
                            }
                        }
                    } else {
                        gadget.get().LeftClick(p);
                    }
                }
            }
        } else if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            final Optional<Gadget> gadget = PersonalChest.gadgetManager.getCosmetic(p.getItemInHand());
            if (gadget.isPresent()) {
                if (!(gadget.get() instanceof TeleportBow)) {
                    e.setCancelled(true);
                }
                if (gadget.get().hasRightClickAction()) {
                    if (gadget.get().getCooldown() > 0) {
                        if (gadget.get().getCooldowns().contains(p.getUniqueId())) {
                            new FancyMessage("Du kannst dieses Gadget nur alle ").color(ChatColor.RED).then(String.valueOf(gadget.get().getCooldown() / 20)).color(ChatColor.AQUA).then(" Sekunden benutzen!").color(ChatColor.RED).send(p);
                        } else {
                            if (gadget.get().RightClick(p)) {
                                gadget.get().getCooldowns().add(p.getUniqueId());
                                Bukkit.getScheduler().scheduleSyncDelayedTask(BeastCubeSlaveBukkitPlugin.getInstance(), () -> gadget.get().getCooldowns().remove(p.getUniqueId()), gadget.get().getCooldown());
                            }
                        }
                    } else {
                        gadget.get().RightClick(p);
                    }
                }
            }
        }
    }

}
