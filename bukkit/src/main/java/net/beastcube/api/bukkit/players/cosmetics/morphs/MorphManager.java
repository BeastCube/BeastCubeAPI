package net.beastcube.api.bukkit.players.cosmetics.morphs;

import me.libraryaddict.disguise.DisguiseAPI;
import me.libraryaddict.disguise.disguisetypes.Disguise;
import me.libraryaddict.disguise.disguisetypes.DisguiseType;
import me.libraryaddict.disguise.disguisetypes.MobDisguise;
import me.libraryaddict.disguise.disguisetypes.watchers.LivingWatcher;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * @author BeastCube
 */
public class MorphManager extends CosmeticManager<Morph> {

    public static final ChatColor DISPLAY_NAME_COLOR = ChatColor.AQUA;

    public static final Morph CHICKEN = new Morph("chicken", DISPLAY_NAME_COLOR + "Huhn", Material.EGG, DisguiseType.CHICKEN);
    public static final Morph COW = new Morph("cow", DISPLAY_NAME_COLOR + "Kuh", Material.MILK_BUCKET, DisguiseType.COW);
    public static final Morph OCELOT = new Morph("ocelot", DISPLAY_NAME_COLOR + "Ozelot", Material.RAW_FISH, DisguiseType.OCELOT);
    public static final Morph SHEEP = new Morph("sheep", DISPLAY_NAME_COLOR + "Schaf", Material.WOOL, DisguiseType.SHEEP);
    public static final Morph WOLF = new Morph("wolf", DISPLAY_NAME_COLOR + "Wolf", Material.BONE, DisguiseType.WOLF);

    private static Morph[] Morphs =
            {
                    CHICKEN,
                    COW,
                    OCELOT,
                    SHEEP,
                    WOLF
            };

    @Override
    public Morph[] getCosmetics() {
        return Morphs;
    }

    public void finalApplyCosmetic(Player p, Morph morph) {
        MobDisguise mobDisguise = new MobDisguise(morph.getDisguiseType(), morph.isAdult());
        LivingWatcher watcher = mobDisguise.getWatcher();
        watcher.setCustomName(p.getDisplayName());
        watcher.setCustomNameVisible(true);
        DisguiseAPI.disguiseToAll(p, mobDisguise);
    }

    public void finalRemoveCosmetic(Player p) {
        if (hasCosmetic(p)) {
            DisguiseAPI.undisguiseToAll(p);
        }
    }

    public boolean hasCosmetic(Player p) {
        return DisguiseAPI.isDisguised(p);
    }

    @Override
    public boolean isActivated(Player p, Morph cosmetic) {
        Disguise disguise = DisguiseAPI.getDisguise(p);
        return disguise != null && disguise.getType() == cosmetic.getDisguiseType();
    }
}
