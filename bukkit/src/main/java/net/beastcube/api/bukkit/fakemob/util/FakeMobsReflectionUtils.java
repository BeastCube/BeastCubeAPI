package net.beastcube.api.bukkit.fakemob.util;

import net.beastcube.api.bukkit.util.ReflectionUtils;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class FakeMobsReflectionUtils {

    public static class PlayerInfoAction {
        public static Object UPDATE_GAME_MODE = getNMSAction("UPDATE_GAME_MODE");
        public static Object ADD_PLAYER = getNMSAction("ADD_PLAYER");
        public static Object UPDATE_DISPLAY_NAME = getNMSAction("UPDATE_DISPLAY_NAME");
        public static Object REMOVE_PLAYER = getNMSAction("REMOVE_PLAYER");
        private static Class nmsClass;

        // Return: EnumPlayerInfoAction
        private static Object getNMSAction(String name) {
            try {
                Field field = getNMSClass().getDeclaredField(name);
                field.setAccessible(true);
                return field.get(null);
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }

        public static Class getNMSClass() {
            if (nmsClass == null) {
                nmsClass = getClassByName(ReflectionUtils.PackageType.MINECRAFT_SERVER + ".PacketPlayOutPlayerInfo$EnumPlayerInfoAction"); // Spigot 1.8.3
                if (nmsClass == null) {
                    // Spigot 1.8.0
                    nmsClass = getClassByName(ReflectionUtils.PackageType.MINECRAFT_SERVER + ".EnumPlayerInfoAction");
                }
            }
            return nmsClass;
        }
    }

    // Return: EnumGamemode
    @SuppressWarnings({"deprecation", "unchecked", "null"})
    public static Object createNMSGameMode(GameMode gameMode) {
        Class c = getClassByName(ReflectionUtils.PackageType.MINECRAFT_SERVER + ".EnumGamemode");  // Spigot 1.8.0
        if (c == null) {
            // Spigot 1.8.3
            c = getClassByName(ReflectionUtils.PackageType.MINECRAFT_SERVER + ".WorldSettings$EnumGamemode");
        }

        try {
            Method method = c.getDeclaredMethod("getById", int.class);
            method.setAccessible(true);
            return method.invoke(null, gameMode.getValue());
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public static Object createPlayerInfoData(Object gameProfile, GameMode gameMode, int ping, String nickName) {
        boolean is_1_8_3 = true;
        Class playerInfoDataClass = getClassByName(ReflectionUtils.PackageType.MINECRAFT_SERVER + ".PacketPlayOutPlayerInfo$PlayerInfoData");

        if (playerInfoDataClass == null) {
            // Spigot 1.8
            playerInfoDataClass = getClassByName(ReflectionUtils.PackageType.MINECRAFT_SERVER + ".PlayerInfoData");
            is_1_8_3 = false;
        }

        Object nmsGameMode = createNMSGameMode(gameMode);

        try {
            Constructor constructor = playerInfoDataClass.getDeclaredConstructor(
                    getClassByName(ReflectionUtils.PackageType.MINECRAFT_SERVER + ".PacketPlayOutPlayerInfo"),
                    getClassByName("com.mojang.authlib.GameProfile"),
                    int.class,
                    nmsGameMode.getClass(),
                    getClassByName(ReflectionUtils.PackageType.MINECRAFT_SERVER + ".IChatBaseComponent")
            );
            constructor.setAccessible(true);
            return constructor.newInstance(null, gameProfile, ping, nmsGameMode, createNMSTextComponent(nickName));
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public static Object fillProfileProperties(Object gameProfile) {
        Class serverClass = getClassByName(ReflectionUtils.PackageType.MINECRAFT_SERVER + ".MinecraftServer");
        Class sessionServiceClass = getClassByName("com.mojang.authlib.minecraft.MinecraftSessionService");

        try {
            Object minecraftServer;
            {
                Method method = serverClass.getDeclaredMethod("getServer");
                method.setAccessible(true);
                minecraftServer = method.invoke(null);
            }

            Object sessionService;
            {
                Method method = serverClass.getDeclaredMethod("aC");
                method.setAccessible(true);
                sessionService = method.invoke(minecraftServer);
            }

            Object result;
            {
                Method method = sessionServiceClass.getDeclaredMethod("fillProfileProperties", gameProfile.getClass(), boolean.class);
                method.setAccessible(true);
                result = method.invoke(sessionService, gameProfile, true);
            }

            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Return: GameProfile
     */
    @SuppressWarnings("unchecked")
    public static Object searchUUID(String playerName) {
        Class serverClass = getClassByName(ReflectionUtils.PackageType.MINECRAFT_SERVER + ".MinecraftServer");
        Class userCacheClass = getClassByName(ReflectionUtils.PackageType.MINECRAFT_SERVER + ".UserCache");

        try {
            Object minecraftServer;
            {
                Method method = serverClass.getDeclaredMethod("getServer");
                method.setAccessible(true);
                minecraftServer = method.invoke(null);
            }

            Object userCache;
            {
                Method method = serverClass.getDeclaredMethod("getUserCache");
                method.setAccessible(true);
                userCache = method.invoke(minecraftServer);
            }

            {
                Method method = userCacheClass.getDeclaredMethod("getProfile", String.class);
                method.setAccessible(true);
                return method.invoke(userCache, playerName);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public static Object createNMSTextComponent(String text) {
        if (text == null || text.isEmpty()) {
            return null;
        }

        Class c = getClassByName(ReflectionUtils.PackageType.MINECRAFT_SERVER + ".ChatComponentText");
        try {
            Constructor constructor = c.getDeclaredConstructor(String.class);
            return constructor.newInstance(text);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public static Object toEntityHuman(Player player) {
        try {
            Class c = getClassByName(ReflectionUtils.PackageType.CRAFTBUKKIT + ".entity.CraftPlayer");
            Method m = c.getDeclaredMethod("getHandle");
            m.setAccessible(true);
            return m.invoke(player);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Class getClassByName(String name) {
        try {
            return Class.forName(name);
        } catch (Exception e) {
            // Class not found
            return null;
        }
    }

    public static Object getField(Class c, Object obj, String key) throws Exception {
        Field field = c.getDeclaredField(key);
        field.setAccessible(true);
        return field.get(obj);
    }

    public static void replaceField(Class c, Object obj, String key, Object value) throws Exception {
        Field field = c.getDeclaredField(key);
        field.setAccessible(true);
        field.set(obj, value);
    }

}
