package net.beastcube.api.bukkit;

import com.google.common.base.Preconditions;

import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Class used for synchronizing network with minecraft server instance.
 */
public class Sync {
    /**
     * Maximum amount of tasks, that can be executed on one server tick.
     */
    public static final int MAX_TASKS_PER_TICK = 128;
    private final Runnable onTick;
    private final Queue<Runnable> tasks = new LinkedBlockingQueue<Runnable>();
    private int lastTasksCount = 0;
    private final Set<TickHandler> handlers = new HashSet<TickHandler>();
    protected long ticks = 0L;

    public Sync() {
        this.onTick = () -> {
            Sync.this.onTick(Sync.this.getTicks());
            Sync.this.incrementTicks();
        };
    }

    protected void incrementTicks() {
        this.ticks++;
    }

    public Runnable getOnTick() {
        return this.onTick;
    }

    /**
     * Add task that should be executed synchronously with main server thread.
     *
     * @param runnable task to execute
     */
    public void addTask(final Runnable runnable) {
        this.tasks.add(runnable);
    }

    /**
     * Register specified {@link TickHandler} on this sync object.
     *
     * @param handler tick handler to register
     */
    public void addTickHandler(final TickHandler handler) {
        try {
            Preconditions.checkNotNull(handler, "handler can't be null");

            this.handlers.add(handler);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void onTick(final long tick) {
        // First make sync handlers happy.
        for (TickHandler th : this.handlers) {
            try {
                th.tick(tick);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        this.lastTasksCount = 0;
        Runnable task;
        while ((task = this.tasks.peek()) != null && this.lastTasksCount < Sync.MAX_TASKS_PER_TICK) {
            try {
                task.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (this.lastTasksCount > Sync.MAX_TASKS_PER_TICK - 20) {
            // TODO: Send debug requrest about that this slave got too much tasks.
        }
    }

    public long getTicks() {
        return this.ticks;
    }
}
