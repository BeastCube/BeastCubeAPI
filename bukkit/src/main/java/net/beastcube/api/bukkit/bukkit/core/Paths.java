package net.beastcube.api.bukkit.bukkit.core;

import net.beastcube.api.bukkit.BeastCubeSlaveBukkitPlugin;

/**
 * Class used for generating paths.
 *
 * @author Mato Kormuth
 */
public class Paths {

    public static String clips() {
        return BeastCubeSlaveBukkitPlugin.getInstance().getDataFolder().getAbsolutePath() + "/clips/";
    }
}
