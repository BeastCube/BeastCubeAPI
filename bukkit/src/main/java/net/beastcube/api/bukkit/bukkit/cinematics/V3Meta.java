package net.beastcube.api.bukkit.bukkit.cinematics;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Interface specifikujuci, ze dana trieda je V3Meta.
 *
 * @author Mato Kormuth
 */
public interface V3Meta {
    /**
     * Typ meta.
     */
    V3MetaType getMetaType();

    /**
     * Zapise meta do streamu.
     *
     * @param stream stream
     */
    void writeMeta(DataOutputStream stream) throws IOException;

    /**
     * Vrati ciselny typ META.
     *
     * @return type
     */
    int getType();
}
