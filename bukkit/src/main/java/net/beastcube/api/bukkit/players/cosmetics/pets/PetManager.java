package net.beastcube.api.bukkit.players.cosmetics.pets;

import com.dsh105.echopet.compat.api.entity.IPet;
import com.dsh105.echopet.compat.api.entity.PetType;
import com.dsh105.echopet.compat.api.plugin.EchoPet;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * @author BeastCube
 */
public class PetManager extends CosmeticManager<Pet> {

    public static final ChatColor DISPLAY_NAME_COLOR = ChatColor.GREEN;

    public static final Pet CHICKEN = new Pet("chicken", DISPLAY_NAME_COLOR + "Huhn", Material.EGG, PetType.CHICKEN, 5);
    public static final Pet COW = new Pet("cow", DISPLAY_NAME_COLOR + "Kuh", Material.MILK_BUCKET, PetType.COW, 5);
    public static final Pet OCELOT = new Pet("ocelot", DISPLAY_NAME_COLOR + "Ozelot", Material.RAW_FISH, PetType.OCELOT, 5);
    public static final Pet SHEEP = new Pet("sheep", DISPLAY_NAME_COLOR + "Schaf", Material.WOOL, PetType.SHEEP, 5);
    public static final Pet WOLF = new Pet("wolf", DISPLAY_NAME_COLOR + "Wolf", Material.BONE, PetType.WOLF, 5);
    public static final Pet CREEPER = new Pet("creeper", DISPLAY_NAME_COLOR + "Creeper", Material.SULPHUR, PetType.CREEPER, 5);
    public static final Pet ZOMBIE = new Pet("zombie", DISPLAY_NAME_COLOR + "Zombie", Material.FEATHER, PetType.ZOMBIE, 5);

    private static Pet[] Pets =
            {
                    CHICKEN,
                    COW,
                    OCELOT,
                    SHEEP,
                    WOLF,
                    CREEPER,
                    ZOMBIE
            };

    @Override
    public Pet[] getCosmetics() {
        return Pets;
    }

    @Override
    public void finalApplyCosmetic(Player p, Pet pet) {
        if (hasCosmetic(p)) {
            finalRemoveCosmetic(p);
        }
        EchoPet.getManager().createPet(p, pet.getPetType(), false);
    }

    @Override
    public void finalRemoveCosmetic(Player p) {
        if (hasCosmetic(p)) {
            EchoPet.getManager().removePets(p, true);
        }
    }

    @Override
    public boolean hasCosmetic(Player p) {
        return EchoPet.getManager().getPet(p) != null;
    }

    @Override
    public boolean isActivated(Player p, Pet cosmetic) {
        IPet pet = EchoPet.getManager().getPet(p);
        return pet != null && pet.getPetType() == cosmetic.getPetType();
    }

    public boolean isPet(Entity e) {
        return EchoPet.getManager().getPet(e) != null;
    }

    public ArrayList<IPet> getPets() {
        return EchoPet.getManager().getPets();
    }
}