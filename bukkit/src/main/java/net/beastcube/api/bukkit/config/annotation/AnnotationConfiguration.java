package net.beastcube.api.bukkit.config.annotation;

import net.beastcube.api.bukkit.config.Configuration;
import net.beastcube.api.bukkit.config.InvalidConfigException;
import net.beastcube.api.commons.Providers;
import org.bukkit.configuration.ConfigurationSection;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Base class for the configurations that uses annotations on attributes to save
 * the configuration.
 */
public abstract class AnnotationConfiguration implements Configuration {

    /**
     * Set all properties annotated with {@link Setting} to the value in the
     * {@link Setting}.
     */
    @Override
    public void load(ConfigurationSection section) {
        if (getClass().isAnnotationPresent(SettingBase.class)) {
            String base = getClass().getAnnotation(SettingBase.class).value();
            section = section.getConfigurationSection(base);
            if (section == null)
                return;
        }

        for (Field field : getClass().getDeclaredFields()) {
            if (field.getClass().isAssignableFrom(AnnotationConfiguration.class)) {
                try {
                    ((AnnotationConfiguration) field.get(this)).load(section);
                } catch (IllegalAccessException e) {
                    getLogger().log(Level.SEVERE, "Error loading config " + field.getName() + " in " + this.getClass().getCanonicalName(), e);
                }
                continue;
            }
            if (!field.isAnnotationPresent(Setting.class)) {
                continue;
            }

            field.setAccessible(true);

            String key = field.getAnnotation(Setting.class).value();
            if (key.equals("")) {
                key = field.getName();
            }
            Object value = section.get(key);

            try {
                if (value != null) {
                    try {
                        field.set(this, value);
                    } catch (IllegalArgumentException e) {
                        throw new InvalidConfigException("Config value of '" + section.getCurrentPath() + "." + key
                                + "' must be assignable from '" + field.getType().getName() + "', found instead '"
                                + value.getClass().getName() + "'! Using default for this key.");
                    }
                } else {
                    section.set(key, field.get(this));
                }
            } catch (IllegalAccessException | InvalidConfigException e) {
                getLogger().log(Level.SEVERE, "Error loading config:", e);
            }
        }

    }

    /**
     * Save all contents of fields annotated with {@link Setting} to the node.
     */
    @Override
    public void save(ConfigurationSection section) {
        if (getClass().isAnnotationPresent(SettingBase.class)) {
            String base = getClass().getAnnotation(SettingBase.class).value();

            if (section.getConfigurationSection(base) == null) {
                section.createSection(base);
            }
            section = section.getConfigurationSection(base);
        }

        for (Field field : getClass().getDeclaredFields()) {
            if (field.getClass().isAssignableFrom(AnnotationConfiguration.class)) {
                try {
                    ((AnnotationConfiguration) field.get(this)).save(section);
                } catch (IllegalAccessException e) {
                    getLogger().log(Level.SEVERE, "Error saving config " + field.getName() + " in " + this.getClass().getCanonicalName(), e);
                }
                continue;
            }
            if (!field.isAnnotationPresent(Setting.class)) {
                continue;
            }

            String key = field.getAnnotation(Setting.class).value();
            if (key.equals("")) {
                key = field.getName();
            }
            try {
                section.set(key, field.get(this));
            } catch (IllegalAccessException e) {
                getLogger().log(Level.SEVERE, "Error saving config. Key: " + key + " Field: " + field.getName(), e);
            }
        }
    }

    private Logger getLogger() {
        return Providers.LOGGER;
    }

}