package net.beastcube.api.bukkit.commands;

import org.bukkit.command.CommandSender;

public interface PermissionHandler {
    boolean hasPermission(CommandSender sender, String[] permissions);
}
