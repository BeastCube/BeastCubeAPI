package net.beastcube.api.bukkit.util.serialization.adapters;

import com.google.gson.*;
import net.beastcube.api.commons.CuboidRegion;
import org.bukkit.Location;
import org.bukkit.World;

import java.lang.reflect.Type;

/**
 * @author BeastCube
 */
public class CuboidRegionSerializer implements JsonDeserializer<CuboidRegion>, JsonSerializer<CuboidRegion> {

    @Override
    public CuboidRegion deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject itemObject = jsonElement.getAsJsonObject();
        World world = jsonDeserializationContext.deserialize(itemObject.get("world"), World.class);
        Location loc1 = jsonDeserializationContext.deserialize(itemObject.get("loc1"), Location.class);
        Location loc2 = jsonDeserializationContext.deserialize(itemObject.get("loc2"), Location.class);
        return new CuboidRegion(loc1.toVector(), loc2.toVector(), world);
    }

    @Override
    public JsonElement serialize(CuboidRegion cuboidRegion, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject root = new JsonObject();
        if (cuboidRegion != null) {
            root.addProperty("world", cuboidRegion.getWorld().getName());
            root.add("loc1", jsonSerializationContext.serialize(cuboidRegion.getV1Location()));
            root.add("loc2", jsonSerializationContext.serialize(cuboidRegion.getV2Location()));
        }
        return root;
    }
}
