package net.beastcube.api.bukkit;

import net.beastcube.api.commons.AbstractComponent;
import net.beastcube.api.commons.configuration.Configuration;
import org.bukkit.event.Listener;

/**
 * Interface that represents component in MasterServer.
 */
public abstract class SlaveComponent extends AbstractComponent implements Listener {
    BeastCubeSlave slave;

    /**
     * Returns current {@link BeastCubeSlave} instance.
     */
    public BeastCubeSlave getSlave() {
        return this.slave;
    }

    @Override
    public void onEnable() {
    }

    @Override
    public void onDisable() {
    }

    protected void __initConfig(final Configuration parentConfiguration) {
        this._initConfig(parentConfiguration);
    }
}
