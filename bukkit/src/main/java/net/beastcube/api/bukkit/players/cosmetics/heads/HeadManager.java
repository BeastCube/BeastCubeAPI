package net.beastcube.api.bukkit.players.cosmetics.heads;

import net.beastcube.api.bukkit.players.cosmetics.CosmeticManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class HeadManager extends CosmeticManager<Head> {

    public static final ChatColor DISPLAY_NAME_COLOR = ChatColor.GREEN;

    public static final Head JBOU = new Head("jbou", "JBou", "37a10d05293a4c76bf1c3e1c41d77ce9", ChatColor.DARK_RED + "Admin");
    //public static final Head EP98 = new Head("ep98", "ep98", "4e7a5f5896ba47d3b756cbbecb647e53", ChatColor.DARK_RED + "Besitzer");
    public static final Head ALEXTHEBEST = new Head("alexthebest", "AlexTheBest", "af46375dc8914715b9897074723ad580", ChatColor.DARK_RED + "Admin");
    //public static final Head DOSCHMID = new Head("doschmid", "DoSchmid", "1afc2bcb3a5044b38d4daed809f6fcb4", ChatColor.RED + "Mod");
    //public static final Head PLANKE12 = new Head("planke12", "Planke12", "61c31755ef514cef9e342e65a0d9d102", ChatColor.RED + "Mod"); //TODO Show all Owners, Admins, Mods ... and then Youtuber

    public static final Head UNGE = new Head("ungespielt", "ungespielt", "1588abbbe45b49e69e438b83c5d5f812", ChatColor.DARK_PURPLE + "Youtuber");
    public static final Head GOMMEHD = new Head("gommehd", "GommeHD", "e9013c2fda01425fa48b516f55e94386", ChatColor.DARK_PURPLE + "Youtuber");
    public static final Head DNER = new Head("dnertv", "DnerTV", "20adcf960cf344a3abf5ae76a4a759e2", ChatColor.DARK_PURPLE + "Youtuber");
    public static final Head GERMANLETSPLAY = new Head("germanletsplay", "GermanLetsPlay", "2d76acc546c747ea83e42ea1fd1285de", ChatColor.DARK_PURPLE + "Youtuber");
    public static final Head REWINSIDE = new Head("rewinside", "rewinside", "7bc638a8dfcd44968fc19481421f8a3b", ChatColor.DARK_PURPLE + "Youtuber");
    public static final Head MRMOREGAME = new Head("mrmoregame", "MrMoregame", "07bf070bf759405b8e899db4626704a3", ChatColor.DARK_PURPLE + "Youtuber");
    public static final Head PALUTEN = new Head("paluten", "Paluten", "6e61a2a93cb746879013f275dd7357bd", ChatColor.DARK_PURPLE + "Youtuber");
    public static final Head HERRBERGMANN = new Head("herrbergmann", "HerrBergmann", "3ce22eb841de476cb0b0c0528a2abc0c", ChatColor.DARK_PURPLE + "Youtuber");
    public static final Head CONCRAFTER = new Head("concrafter", "ConCrafter", "90ed7af46e8c4d54824de74c2519c655", ChatColor.DARK_PURPLE + "Youtuber");
    public static final Head MCEXPERTHD = new Head("mcexperthd", "MCExpertHD", "7bf7b642524a4c8ab53681f2d7496e82", ChatColor.DARK_PURPLE + "Youtuber");

    private static Head[] Heads =
            {
                    JBOU,
                    //EP98,
                    ALEXTHEBEST,
                    //DOSCHMID,
                    //PLANKE12,

                    UNGE,
                    GOMMEHD,
                    DNER,
                    GERMANLETSPLAY,
                    REWINSIDE,
                    MRMOREGAME,
                    PALUTEN,
                    HERRBERGMANN,
                    CONCRAFTER,
                    MCEXPERTHD
            };

    public HeadManager() {
        for (Head head : Heads) {
            head.load();
        }
    }

    @Override
    public Head[] getCosmetics() {
        return Heads;
    }

    @Override
    public void finalApplyCosmetic(Player p, Head head) {
        p.getInventory().setHelmet(head.getIcon());
    }

    @Override
    public void finalRemoveCosmetic(Player p) {
        p.getInventory().setHelmet(new ItemStack(Material.AIR));
    }

}