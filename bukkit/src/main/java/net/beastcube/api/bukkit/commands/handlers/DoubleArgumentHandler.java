package net.beastcube.api.bukkit.commands.handlers;

import net.beastcube.api.bukkit.commands.CommandArgument;
import net.beastcube.api.bukkit.commands.TransformError;
import org.bukkit.command.CommandSender;

public class DoubleArgumentHandler extends NumberArgumentHandler<Double> {
    public DoubleArgumentHandler() {
        setMessage("parse_error", "The parameter [%p] is not a number");
    }

    @Override
    public Double transform(CommandSender sender, CommandArgument argument, String value) throws TransformError {
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            throw new TransformError(argument.getMessage("parse_error"));
        }
    }
}
