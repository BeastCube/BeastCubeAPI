package net.beastcube.api.bukkit.fakemob.util;

import net.beastcube.api.bukkit.fakemob.FakeMobsPlugin;
import org.bukkit.entity.Player;

import java.util.List;

public class LookUpdate implements Runnable {
    private FakeMobsPlugin plugin;

    public LookUpdate(FakeMobsPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        try {
            for (FakeMob mob : FakeMobsPlugin.getMobs()) {
                if (!mob.isPlayerLook()) continue;
                List<Player> players = mob.getNearbyPlayers(5D);
                for (Player p : players)
                    mob.sendLookPacket(p, p.getLocation());
            }
        } catch (Exception e) {
            //Do Nothing
        }
    }

}
