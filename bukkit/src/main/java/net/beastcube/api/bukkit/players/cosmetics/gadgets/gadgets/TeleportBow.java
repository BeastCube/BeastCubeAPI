package net.beastcube.api.bukkit.players.cosmetics.gadgets.gadgets;

import lombok.Getter;
import net.beastcube.api.bukkit.BeastCubeSlaveBukkitPlugin;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.Gadget;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.GadgetManager;
import net.beastcube.api.bukkit.util.ParticleEffect;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashMap;

/**
 * @author BeastCube
 */
public class TeleportBow extends Gadget {
    @Getter
    private String id = "teleportbow";
    @Getter
    private String displayName = GadgetManager.DISPLAY_NAME_COLOR + "TeleportBow";
    @Getter
    private Material material = Material.BOW;
    @Getter
    private String description = ChatColor.GRAY + "Teleportiere dich überall hin";

    @Override
    public boolean hasLeftClickAction() {
        return false;
    }

    @Override
    public boolean hasRightClickAction() {
        return true;
    }

    @Override
    public boolean LeftClick(Player p) {
        return false;
    }

    @Override
    public boolean RightClick(Player p) {
        p.getInventory().setItem(9, new ItemStack(Material.ARROW));
        return true;
    }

    @Override
    public int getCooldown() {
        return 0;
    }

    @Override
    public HashMap<Enchantment, Integer> getEnchantments() {
        return new HashMap<Enchantment, Integer>() {{
            put(Enchantment.ARROW_INFINITE, 1);
        }};
    }

    //****************************************************************************\\
    //********************************** Events **********************************\\
    //****************************************************************************\\

    @EventHandler
    public void onEntityShootBow(EntityShootBowEvent event) {
        if (event.getBow().hasItemMeta()) {
            if (event.getBow().getItemMeta().hasDisplayName()) {
                if (event.getBow().getItemMeta().getDisplayName().equalsIgnoreCase(getDisplayName())) {
                    event.getProjectile().setMetadata("portarrow", new FixedMetadataValue(BeastCubeSlaveBukkitPlugin.getInstance(), true));
                    Location loc = event.getProjectile().getLocation();
                    trail(0.3F, 0.3F, 0.3F, 0.3F, 10, 1, ParticleEffect.FLAME, (Projectile) event.getProjectile());
                }
            }
        }
    }

    @EventHandler
    public void onArrowHit(ProjectileHitEvent event) {
        if (event.getEntity().getShooter() instanceof Player) {
            Player p = (Player) event.getEntity().getShooter();
            if (event.getEntity() instanceof Arrow) {
                Arrow arrow = (Arrow) event.getEntity();
                if (arrow.hasMetadata("portarrow")) {
                    if (arrow.getMetadata("portarrow").get(0).asBoolean()) {
                        if (p.getLocation().getWorld() == arrow.getLocation().getWorld()) {
                            p.teleport(arrow);
                        }
                        arrow.remove();
                        p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);
                    }
                }
            }
        }
    }

    public void trail(final float x, final float y, final float z, final float speed, final int amount, final int wait, final ParticleEffect type, final Projectile proj) {
        if ((!proj.isOnGround()) && (!proj.isDead())) {
            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(BeastCubeSlaveBukkitPlugin.getInstance(), () -> {
                type.display(x, y, z, speed, amount, proj.getLocation(), 256D);
                trail(x, y, z, speed, amount, wait, type, proj);
            }, wait);
        }
    }

}
