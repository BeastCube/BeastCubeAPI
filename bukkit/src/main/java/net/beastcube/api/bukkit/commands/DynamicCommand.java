package net.beastcube.api.bukkit.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginIdentifiableCommand;
import org.bukkit.plugin.Plugin;

/**
 * A dynamically registered {@link org.bukkit.command.Command}.
 */
public class DynamicCommand extends org.bukkit.command.Command implements PluginIdentifiableCommand {

    private final CommandHandler commandHandler;
    private final Plugin plugin;

    public DynamicCommand(String identifier, CommandHandler commandHandler, Plugin plugin) {
        super(identifier);
        this.commandHandler = commandHandler;
        this.plugin = plugin;
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        return commandHandler.onCommand(commandSender, this, s, strings);
    }

    @Override
    public Plugin getPlugin() {
        return this.plugin;
    }

}