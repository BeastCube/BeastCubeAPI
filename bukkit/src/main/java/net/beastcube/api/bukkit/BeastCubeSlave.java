package net.beastcube.api.bukkit;

import net.beastcube.api.bukkit.network.IncomingMessageHandler;
import net.beastcube.api.commons.*;
import net.beastcube.api.commons.configuration.Configuration;
import net.beastcube.api.commons.configuration.ConfigurationSection;
import net.beastcube.api.network.SlaveServer;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.net.InetSocketAddress;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * BeastCubeSlave server singleton object.
 */
public class BeastCubeSlave {
    private static BeastCubeSlave instance;

    protected SlaveServer server;
    protected Logger log = BeastCubeSlaveBukkitPlugin.getInstance().getLogger();
    protected Configuration config;
    protected Sync sync;
    protected StorageImpl storage;
    protected Scheduler scheduler;

    protected ServerMode mode;

    protected ComponentList<SlaveComponent> components = new ComponentList<>();
    protected boolean componentsEnabled = false;

    private final ClassLoader classLoader;

    public BeastCubeSlave(final File dataFolder, final ClassLoader classLoader) {

        this.classLoader = classLoader;

        Providers.LOGGER = BeastCubeSlaveBukkitPlugin.getInstance().getLogger();

        this.log.info("Booting up BeastCubeSlave...");

        BeastCubeAPI.getInstance().loadConfiguration();

        // Load configuration.
        File f = new File(dataFolder.getAbsolutePath() + "/config.xml");
        if (!f.exists()) {
            this.log.info("Configuration file not found!");
            this.config = new Configuration(f);
        } else {
            this.log.info("Loading configuration...");
            this.config = Configuration.load(f);
        }

        // To allow BeastCubeSlave static calls from components.
        BeastCubeSlave.instance = this;

        // Add local matchmaking.
        //Matchmaking matchmaking = new Matchmaking();
        //this.addComponentSystem(matchmaking);

        // Load all components.
        //TODO Load plugins downloaded from master?

        // Initialize protects.

        // Initialize arenas.

        // Initialize whole shit from BeastCubeCore.

        // Register standart event handlers.
        //this.eventBus.register(new ChatEventHandler());

        // Add standart TPS checker.
        //TPSChecker tpsChecker = new TPSChecker();
        //this.addComponentSystem(tpsChecker);

        // Create sync object.
        this.sync = new Sync();

        // Create scheduler and attach it to sync.
        this.scheduler = new Scheduler();
        this.sync.addTickHandler(this.scheduler);
        // TPS Checker has direct connection to sync.
        //this.sync.addTickHandler(tpsChecker);

        // Connect to master - other thread.
        String externalIP = BeastCubeAPI.config.externalIp;
        String motd = BeastCubeAPI.config.motd;

        ConfigurationSection config = this.config.getSection(SlaveServer.class);

        int port = config.get(Configuration.Keys.KEY_PORT, 29631).asInteger();
        String masterIp = config.get(Configuration.Keys.KEY_MASTER_IP, "127.0.0.1").asString();
        String authKey = config.get(Configuration.Keys.KEY_AUTHKEY, Configuration.Defaults.AUTH_KEY).asString();

        UUID uniqueId;

        String arg = System.getProperty("uniqueId");
        if (arg != null) {
            try {
                uniqueId = UUID.fromString(arg);
            } catch (IllegalArgumentException e) {
                uniqueId = UUID.randomUUID();
            }
        } else {
            uniqueId = UUID.randomUUID();
        }

        this.server = new SlaveServer(BeastCubeAPI.getServerName(), uniqueId, log, port, masterIp, authKey,
                new InetSocketAddress(externalIP.equals("") ? Bukkit.getIp() : externalIP, Bukkit.getPort()), motd) {

            @Override
            public void connectFailed(Throwable e, Runnable r) {
                JavaPlugin plugin = BeastCubeSlaveBukkitPlugin.getInstance();
                if (plugin.isEnabled()) {
                    log.severe("Failed to connect to master! Retrying in 5 seconds" + (e != null ? ": " + e.getMessage() : ""));
                    Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, r, 5 * 20); //TODO Fix and use getScheduler()
                }
            }

        };

        this.server.getMessenger().registerListener(new IncomingMessageHandler());

        this.server.start();
        // Enable all components before registering on master. Buisness logic (minigames) can be registered trough components.
        this.enableComponents();

        // Register myself on master.
        /*this.server.getMasterServerInfo()
                .sendRequest(
                        new InServerMetaDataMessage(matchmaking.getMinigames(),
                                new HashSet<MapDescriptor>(1),
                                this.serverSoftware.getType(),
                                this.serverSoftware.getVersion(),
                                this.serverSoftware.getSlots()));*/

        // Register all games on master matchmaking.
        //matchmaking.registerGamesOnMaster();
    }

    public void shutdown() {
        //TODO: Wait before are all other things done.
        this.log.info("Shutting down server...");
        this.server.shutdown();

        this.log.info("Disabling scheduler...");
        this.scheduler.shutdownNow();

        this.log.info("Disabling all components...");
        this.disableComponents();

        this.log.info("Saving configuration...");
        this.config.save();

        this.log.info("Shutting down!");
        this.log.info("Thanks for using and bye!");
    }

    /**
     * Adds component to master server.
     *
     * @param component component to add
     */
    public void addComponentSystem(final SlaveComponent component) {
        this.components.addSystem(component);

        if (this.componentsEnabled) {
            component.onEnable();
        }
    }

    public void addComponentUser(final SlaveComponent component) {
        this.components.addUser(component);

        if (this.componentsEnabled) {
            component.onEnable();
        }
    }

    protected void enableComponents() {
        this.components.forEach(this::enableComponent);
    }

    protected void disableComponents() {
        this.components.forEach(this::disableComponent);
    }

    protected void enableComponent(final SlaveComponent e) {
        this.log.info("Enabling [" + e.getClass().getSimpleName() + "]...");
        e.slave = this;
        e.__initConfig(this.getConfiguration());
        try {
            Bukkit.getPluginManager().registerEvents(e, BeastCubeSlaveBukkitPlugin.getInstance());
            e.onEnable();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void disableComponent(final AbstractComponent e) {
        this.log.info("Disabling [" + e.getClass().getSimpleName() + "]...");
        e.onDisable();
    }

    public <T extends SlaveComponent> T getComponent(final Class<T> type) {
        for (SlaveComponent c : this.components) {
            if (type.isInstance(c)) {
                return type.cast(c);
            }
        }
        return null;
    }

    public static void init(final File dataFolder) {
        new BeastCubeSlave(dataFolder, BeastCubeSlave.class.getClassLoader());
    }

    public static void init(final File dataFolder, final ClassLoader classLoader) {
        new BeastCubeSlave(dataFolder, classLoader);
    }

    public static BeastCubeSlave getInstance() {
        return BeastCubeSlave.instance;
    }

    public Configuration getConfiguration() {
        return this.config;
    }

    public ServerMode getMode() {
        return this.mode;
    }

    public Sync getSync() {
        return this.sync;
    }

    public Logger getLogger() {
        return this.log;
    }

    public StorageImpl getStorage() {
        return this.storage;
    }

    public Scheduler getScheduler() {
        return this.scheduler;
    }

    public SlaveServer getServer() {
        return this.server;
    }

    public ClassLoader getClassLoader() {
        return this.classLoader;
    }
}
