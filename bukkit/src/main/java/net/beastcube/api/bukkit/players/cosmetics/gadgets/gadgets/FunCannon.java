package net.beastcube.api.bukkit.players.cosmetics.gadgets.gadgets;

import lombok.Getter;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.Gadget;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.GadgetManager;
import net.beastcube.api.bukkit.util.ParticleEffect;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * @author BeastCube
 */
public class FunCannon extends Gadget {
    @Getter
    private String id = "funcannon";
    @Getter
    private String displayName = GadgetManager.DISPLAY_NAME_COLOR + "FunCannon";
    @Getter
    private Material material = Material.BLAZE_ROD;
    @Getter
    private String description = ChatColor.GRAY + "Eine lustige Kanone :D";

    public static Map<Player, ArrayList<EnderPearl>> ender = new HashMap<>();
    public static LinkedList<Projectile> pcooldown = new LinkedList<>();

    @Override
    public boolean hasLeftClickAction() {
        return false;
    }

    @Override
    public boolean hasRightClickAction() {
        return true;
    }

    @Override
    public boolean LeftClick(Player p) {
        return false;
    }

    @Override
    public boolean RightClick(Player p) {
        final Snowball sb = p.launchProjectile(Snowball.class);
        final EnderPearl ep = p.launchProjectile(EnderPearl.class);

        sb.setVelocity(p.getEyeLocation().getDirection());
        ep.setVelocity(p.getEyeLocation().getDirection());

        pcooldown.add(ep);
        pcooldown.add(sb);

        ArrayList<EnderPearl> enders = ender.get(p);
        if (enders == null) {
            ender.put(p, enders = new ArrayList<>());
        }

        enders.add(ep);
        return true;
    }

    //****************************************************************************\\
    //********************************** Events **********************************\\
    //****************************************************************************\\

    @EventHandler
    public void onProjectileHit(ProjectileHitEvent e) {
        Projectile en = e.getEntity();
        if (FunCannon.pcooldown.contains(en)) {
            FunCannon.pcooldown.remove(en);

            ParticleEffect.FLAME.display(en.getLocation(), 1, 1, 1, 0, 3);
            ParticleEffect.HEART.display(en.getLocation(), 1, 1, 1, 0, 2);
            ParticleEffect.SMOKE_NORMAL.display(en.getLocation(), 1, 2, 1, 0, 4);
            ParticleEffect.SMOKE_LARGE.display(en.getLocation(), 1, 1, 1, 0, 1);
            ParticleEffect.CLOUD.display(en.getLocation(), 1, 1, 1, 0, 3);
            ParticleEffect.LAVA.display(en.getLocation(), 1, 1, 1, 0, 15);

            en.getLocation().getWorld().playSound(en.getLocation(), Sound.CAT_MEOW, 10, 1);
            en.getLocation().getWorld().playSound(en.getLocation(), Sound.WOLF_BARK, 10, 1);

            en.remove();

        }
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent e) {
        PlayerTeleportEvent.TeleportCause cause = e.getCause();

        if (cause == PlayerTeleportEvent.TeleportCause.ENDER_PEARL) {
            ArrayList<EnderPearl> pearls = FunCannon.ender.get(e.getPlayer());
            if (pearls != null) {
                final Location to = e.getTo();
                for (EnderPearl p : pearls) {
                    if (p != null && p.getLocation().distanceSquared(to) < 2) {
                        pearls.remove(p);
                        e.setCancelled(true);
                        return;
                    }
                }
            }
        }
    }

}
