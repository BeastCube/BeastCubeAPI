package net.beastcube.api.bukkit.bukkit.core;

import net.beastcube.api.bukkit.bukkit.BeastCube;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * AutoMessage.
 */
public class AutoMessage implements Updatable {
    private static final List<String> strings = new ArrayList<>();
    private static final String prefix = "";
    private static final long interval = 20000 / 30;             //each 60 seconds.

    private static final Random random = new Random();
    private static int taskId = 0;
    private static boolean enabled = false;

    static {
        AutoMessage.strings.add("Hras " + ChatColor.BLUE + "BETA" + ChatColor.RESET
                + " verziu beastcube-u. Dakujeme!");
        AutoMessage.strings.add("Navstivte aj nasu web stranku www.beastcube.eu (v priprave)!");
        AutoMessage.strings.add("Hras " + ChatColor.BLUE + "BETA" + ChatColor.RESET
                + " verziu beastcube-u. Dakujeme!");
        AutoMessage.strings.add("Ak mas nejake pripomienky, povedz nam na "
                + ChatColor.GREEN + "ts.mertex.eu!");
    }

    /**
     * Boradcast random message to chat.
     */
    public static void pushMessage() {
        Bukkit.broadcastMessage(AutoMessage.prefix
                + AutoMessage.strings.get(AutoMessage.random.nextInt(AutoMessage.strings.size())));
    }

    /**
     * Returns if is AutoMessage enabled.
     *
     * @return true or false
     */
    public static boolean isEnabled() {
        return AutoMessage.enabled;
    }

    @Override
    public void updateStart() {
        Log.partEnable("Automessage");
        UpdatedParts.registerPart(this);
        AutoMessage.enabled = true;
        AutoMessage.taskId = BeastCube.getScheduler().scheduleSyncRepeatingTask(AutoMessage::pushMessage, 0, AutoMessage.interval);
    }

    @Override
    public void updateStop() {
        Log.partDisable("Automessage");
        AutoMessage.enabled = false;
        BeastCube.getScheduler().cancelTask(AutoMessage.taskId);
    }
}
