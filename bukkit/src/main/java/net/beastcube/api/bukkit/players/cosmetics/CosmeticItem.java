package net.beastcube.api.bukkit.players.cosmetics;

import net.beastcube.api.bukkit.util.ItemBuilder;
import net.beastcube.api.commons.players.Coins;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author BeastCube
 */
public abstract class CosmeticItem {

    public String displayName = "You can't get the display name of this base class";
    public Material material = null;
    public byte data = 0;
    public HashMap<Enchantment, Integer> enchantments = new HashMap<>();
    public int price = 0;
    public boolean hasAmmo = false;
    public String description;

    @SuppressWarnings("unchecked")
    public boolean apply(Player p) {
        if (hasBought(p)) {
            remove(p);
            getType().getManager().applyCosmetic(p, this);
            return true;
        } else {
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    public boolean isActivated(Player p) {
        return getType().getManager().hasCosmetic(p) && getType().getManager().isActivated(p, this);
    }

    public void remove(Player p) {
        getType().getManager().removeCosmetic(p);
    }

    public boolean buy(Player p) { //TODO buy ammo with right-click?
        if (Coins.getCoins(p.getUniqueId()) < getPrice()) {
            return false;
        } else {
            CosmeticsDatabaseHandler.buy(p.getUniqueId(), this);
            Coins.removeCoins(p.getUniqueId(), getPrice());
            return true;
        }
    }

    public boolean hasAmmo() {
        return this.hasAmmo;
    }

    public int getAmmo(Player p) {
        if (hasAmmo())
            return 1; //TODO get from mysql
        return -1; //unlimited
    }

    public boolean hasPrice() {
        return this.price > 0;
    }

    public int getPrice() {
        return this.price;
    }

    public boolean hasBought(Player p) {
        if (hasPrice())
            return CosmeticsDatabaseHandler.hasBought(p.getUniqueId(), this);
        return true;
    }

    public boolean isRestricted(Player p) {
        return true;
    } //TODO for example only for premium players

    public abstract String getDisplayName();

    public abstract Material getMaterial();

    public short getData() {
        return data;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getLore() {
        return getLore(null);
    }

    public List<String> getLore(Player p) {
        List<String> lore = new ArrayList<>();
        if (getDescription() != null) {
            lore.add(getDescription());
        }
        if (p != null) {
            if (isActivated(p)) {
                lore.add("");
                lore.add("   " + ChatColor.AQUA + "Aktiviert");
            } else {
                if (hasBought(p)) {
                    lore.add("");
                    lore.add("   " + ChatColor.GREEN + "In Besitz");
                } else {
                    if (hasPrice()) {
                        lore.add("");
                        lore.add("   " + ChatColor.WHITE + "Preis: " + ChatColor.GOLD + getPrice() + " Coins");
                    }
                }
            }
        }
        return lore;
    }

    private Byte getAmount(Player p) {
        if (hasAmmo())
            return (byte) getAmmo(p);
        return 1;
    }

    public HashMap<Enchantment, Integer> getEnchantments() {
        return this.enchantments;
    }

    @SuppressWarnings("deprecation")
    public boolean isCosmeticItem(ItemStack stack) {
        if (stack != null) {
            if (stack.getType() == getMaterial() && stack.hasItemMeta()) {
                ItemMeta meta = stack.getItemMeta();
                if (meta.hasDisplayName()) {
                    if (stack.getData() == null) {
                        return meta.getDisplayName().equals(getDisplayName());
                    } else {
                        return meta.getDisplayName().equals(getDisplayName()) && stack.getData().getData() == getData();
                    }
                }
            }
        }
        return false;
    }

    public ItemStack getIcon() {
        return getIcon(null);
    }

    public ItemStack getIcon(Player p) {
        ItemBuilder is = new ItemBuilder(getMaterial()).amount(p == null ? 1 : getAmount(p)).durability(getData());
        is.name(getDisplayName());
        getLore(p).forEach(is::lore);

        for (Map.Entry<Enchantment, Integer> o : getEnchantments().entrySet()) {
            is.enchantment(o.getKey(), o.getValue());
        }
        is.addFlag(ItemFlag.HIDE_ENCHANTS);
        return is.build();
    }

    public abstract CosmeticType getType();

    public abstract String getId();

}
