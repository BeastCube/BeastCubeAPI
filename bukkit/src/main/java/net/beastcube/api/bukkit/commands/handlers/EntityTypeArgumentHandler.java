package net.beastcube.api.bukkit.commands.handlers;

import net.beastcube.api.bukkit.commands.ArgumentHandler;
import net.beastcube.api.bukkit.commands.CommandArgument;
import net.beastcube.api.bukkit.commands.TransformError;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;


public class EntityTypeArgumentHandler extends ArgumentHandler<EntityType> {
    public EntityTypeArgumentHandler() {
        setMessage("parse_error", "There is no entity named %1");
        setMessage("include_error", "There is no entity named %1");
        setMessage("exclude_error", "There is no entity named %1");
    }

    @SuppressWarnings("deprecation")
    @Override
    public EntityType transform(CommandSender sender, CommandArgument argument, String value) throws TransformError {
        try {
            return EntityType.fromId(Integer.parseInt(value));
        } catch (NumberFormatException ignored) {
        }

        EntityType t = EntityType.fromName(value);

        if (t != null)
            return t;

        throw new TransformError(argument.getMessage("parse_error", value));
    }

}
