package net.beastcube.api.bukkit.bukkit.core;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that turns off all parts when needed. Idk what is this...
 *
 * @author Mato Kormuth
 */
public class UpdatedParts {
    private static List<Updatable> parts = new ArrayList<>();

    public static void shutdown() {
        UpdatedParts.parts.forEach(Updatable::updateStop);
        UpdatedParts.parts.clear();
    }

    public static void registerPart(final Updatable part) {
        Log.info("[UpdatedParts] Registering: " + part.getClass().getSimpleName());
        UpdatedParts.parts.add(part);
    }
}
