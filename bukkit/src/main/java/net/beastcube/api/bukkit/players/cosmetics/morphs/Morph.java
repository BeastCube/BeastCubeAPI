package net.beastcube.api.bukkit.players.cosmetics.morphs;

import lombok.Getter;
import me.libraryaddict.disguise.disguisetypes.DisguiseType;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticItem;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticType;
import org.bukkit.Material;

/**
 * @author BeastCube
 */
public class Morph extends CosmeticItem {

    @Getter
    private String id;
    @Getter
    private DisguiseType disguiseType = null;
    @Getter
    boolean isAdult = true;

    public Morph(String id, String displayName, Material material, DisguiseType type) {
        this(id, displayName, material, type, true);
    }

    public Morph(String id, String displayName, Material material, DisguiseType disguiseType, boolean isAdult) {
        this.id = id;
        this.displayName = displayName;
        this.material = material;
        this.disguiseType = disguiseType;
        this.isAdult = isAdult;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public Material getMaterial() {
        return material;
    }

    @Override
    public CosmeticType getType() {
        return CosmeticType.MORPH;
    }

}
