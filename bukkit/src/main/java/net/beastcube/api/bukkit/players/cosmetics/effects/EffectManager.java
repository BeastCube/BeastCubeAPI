package net.beastcube.api.bukkit.players.cosmetics.effects;

import de.slikey.effectlib.util.DynamicLocation;
import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticManager;
import net.beastcube.api.bukkit.players.cosmetics.effects.effects.*;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

/**
 * @author BeastCube
 */
public class EffectManager extends CosmeticManager<Effect> {

    public static final ChatColor DISPLAY_NAME_COLOR = ChatColor.LIGHT_PURPLE;

    private static HashMap<UUID, de.slikey.effectlib.Effect> RunningEffects = new HashMap<>();

    public static final Effect SMOKE = new Smoke();
    public static final Effect STAR = new Flame();
    public static final Effect BLEED = new Bleed();
    public static final Effect LOVE = new Love();
    public static final Effect MUSIC = new Music();
    public static final Effect SHIELD = new Shield();

    private static Effect[] Effects =
            {
                    SMOKE,
                    STAR,
                    //BLEED,
                    LOVE,
                    MUSIC,
                    SHIELD
            };

    @Override
    public Effect[] getCosmetics() {
        return Effects;
    }

    @Override
    public void finalApplyCosmetic(Player p, Effect cosmetic) {
        de.slikey.effectlib.Effect finaleffect = cosmetic.getEffect(p, BeastCubeAPI.effectManager);
        finaleffect.iterations = -1;
        finaleffect.setDynamicOrigin(new DynamicLocation(p));
        EffectManager.RunningEffects.put(p.getUniqueId(), finaleffect);
        finaleffect.start();
    }

    @Override
    public void finalRemoveCosmetic(Player p) {
        if (hasCosmetic(p)) {
            de.slikey.effectlib.Effect effectToCancel = RunningEffects.get(p.getUniqueId());
            effectToCancel.cancel();
            RunningEffects.remove(p.getUniqueId());
        }
    }

}
