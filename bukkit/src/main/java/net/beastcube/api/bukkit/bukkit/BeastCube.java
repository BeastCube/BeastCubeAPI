package net.beastcube.api.bukkit.bukkit;

import net.beastcube.api.bukkit.BeastCubeSlaveBukkitPlugin;
import net.beastcube.api.bukkit.bukkit.core.Achievements;
import net.beastcube.api.bukkit.bukkit.core.Scheduler;
import net.beastcube.api.bukkit.bukkit.util.AsyncWorker;
import net.beastcube.api.bukkit.bukkit.util.PlayerFreezer;

import java.util.Random;

/**
 * Class used for API calls.
 */
public final class BeastCube {
    //BeastCube plugin.
    private static BeastCubeCore instance;
    //Instance of random. 
    private static Random random = new Random();

    protected final static void initialize(final BeastCubeCore plugin) {
        if (BeastCube.instance == null)
            BeastCube.instance = plugin;
        else
            throw new RuntimeException("BeastCube object already initialized!");
    }

    /**
     * Returns the main plugin instance.
     *
     * @return core
     */
    public static final BeastCubeSlaveBukkitPlugin getCore() {
        return BeastCubeSlaveBukkitPlugin.getInstance();
    }

    /**
     * Returns {@link Achievements} class.
     *
     * @return achievements
     */
    public static final Achievements getAchievements() {
        return BeastCube.instance.achievementsClient;
    }

    /**
     * Returns player freezer.
     *
     * @return player freezer
     */
    public static final PlayerFreezer getPlayerFreezer() {
        return BeastCube.instance.freezer;
    }

    /**
     * Returns instance of {@link Random}.
     *
     * @return beastcube's {@link Random}.
     */
    public final static Random getRandom() {
        return BeastCube.random;
    }

    /**
     * Returns beastcube's async wokrer instance.
     *
     * @return {@link AsyncWorker} instance.
     */
    public final static AsyncWorker getAsyncWorker() {
        return BeastCube.instance.asyncWorker;
    }

    /**
     * Returns beastcube's scheduler instance.
     *
     * @return {@link Scheduler} instance.
     */
    public final static Scheduler getScheduler() {
        return BeastCube.instance.scheduler;
    }
}
