package net.beastcube.api.bukkit.commands;

public interface HelpHandler {
    String[] getHelpMessage(RegisteredCommand command);

    String getUsage(RegisteredCommand command);
}
