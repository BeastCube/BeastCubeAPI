package net.beastcube.api.bukkit.interactitem;

import net.beastcube.api.bukkit.BeastCubeSlaveBukkitPlugin;
import net.beastcube.api.bukkit.players.cosmetics.PersonalChest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * @author BeastCube
 */
public class InteractItemManager implements Listener {

    public static InteractItem PERSONALCHEST = new PersonalChest();

    private static List<InteractItem> ITEMS = new ArrayList<InteractItem>() {{
        add(PERSONALCHEST);
    }};

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            InteractItem item = getItem(e.getPlayer().getItemInHand());
            if (item != null) {
                item.execute(e.getPlayer(), BeastCubeSlaveBukkitPlugin.getInstance(), null);
                e.setCancelled(true);
            }
        }
    }

    public static void registerInteractItem(InteractItem item) {
        ITEMS.add(item);
    }

    public static InteractItem getItem(ItemStack stack) {
        for (InteractItem item : ITEMS)
            if (item.isItem(stack))
                return item;
        return null;
    }
}