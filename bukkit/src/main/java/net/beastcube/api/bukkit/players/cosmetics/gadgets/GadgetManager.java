package net.beastcube.api.bukkit.players.cosmetics.gadgets;

import net.beastcube.api.bukkit.BeastCubeSlaveBukkitPlugin;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticManager;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.gadgets.FunCannon;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.gadgets.PaintballGun;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.gadgets.TeleportBow;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.gadgets.TntThrower;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Optional;

/**
 * @author BeastCube
 */
public class GadgetManager extends CosmeticManager<Gadget> {

    private static int gadgetSlot = 6;

    public static final ChatColor DISPLAY_NAME_COLOR = ChatColor.DARK_PURPLE;

    public static final Gadget TNTTHROWER = new TntThrower();
    public static final Gadget TELEPORTBOW = new TeleportBow();
    public static final Gadget FUNCANNON = new FunCannon();
    public static final Gadget PAINTBALLGUN = new PaintballGun();

    private static Gadget[] Gadgets =
            {
                    TNTTHROWER,
                    TELEPORTBOW,
                    FUNCANNON,
                    PAINTBALLGUN
            };

    public GadgetManager() {
        for (Gadget gadget : Gadgets) {
            Bukkit.getPluginManager().registerEvents(gadget, BeastCubeSlaveBukkitPlugin.getInstance());
        }
    }

    @Override
    public Gadget[] getCosmetics() {
        return Gadgets;
    }

    @Override
    public void finalApplyCosmetic(Player p, Gadget gadget) {
        p.getInventory().setItem(gadgetSlot, gadget.getIcon(p));
    }

    @Override
    public void finalRemoveCosmetic(Player p) {
        p.getInventory().clear(gadgetSlot);
    }

    @Override
    public boolean hasCosmetic(Player p) {
        return getCosmetic(p.getInventory().getItem(gadgetSlot)).isPresent();
    }

    @Override
    public boolean isActivated(Player p, Gadget cosmetic) {
        Optional<Gadget> gadget = getCosmetic(p.getInventory().getItem(gadgetSlot));
        return gadget.isPresent() && gadget.get() == cosmetic;
    }
}
