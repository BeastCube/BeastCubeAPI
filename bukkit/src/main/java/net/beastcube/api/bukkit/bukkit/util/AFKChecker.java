package net.beastcube.api.bukkit.bukkit.util;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class that is used for checking when is player AFK.
 */
public class AFKChecker {
    private final int time;
    private final BukkitTimer timer;
    private final List<Player> players;
    private final Map<Player, AFKEntry> entries = new HashMap<>();

    public AFKChecker(final int seconds, List<Player> players) {
        this.time = seconds;
        this.players = players;
        this.timer = new BukkitTimer(20, this::check);
        this.timer.start();
    }

    protected void check() {
        long checkTime = System.currentTimeMillis();

        for (Player p : this.players) {
            AFKEntry entry = this.entries.get(p);

            // Check for kicks
            if (System.currentTimeMillis() > entry.lastMovementTime + this.time) {
                p.kickPlayer("You were kicked, because you were AFK. If it was during a competitive match, you migth be penalized.");
                this.entries.remove(p);
                continue;
            } else if (!p.isOnline()) {
                this.entries.remove(p);
            }

            // Update last movement
            if (!entry.lastLoc.equals(p.getLocation())) {
                entry.lastLoc = p.getLocation();
                entry.lastMovementTime = checkTime;
            }
        }
    }

    public void start() {
        this.timer.start();
    }

    public void reset() {
        this.entries.clear();
        this.timer.stop();
    }

    public class AFKEntry {
        public Location lastLoc;
        public long lastMovementTime;
    }
}
