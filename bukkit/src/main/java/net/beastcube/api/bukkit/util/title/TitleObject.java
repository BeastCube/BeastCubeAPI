package net.beastcube.api.bukkit.util.title;

import org.bukkit.entity.Player;

/**
 * @author BeastCube
 */
public class TitleObject {
    private String title;
    private String subtitle;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public TitleObject(String title, String subtitle) {
        this.title = title;
        this.subtitle = subtitle;
    }

    public void send(Player p) {
        TitleAPI.sendTitle(p, title, subtitle);
    }
}
