package net.beastcube.api.bukkit.minigames;

import lombok.Getter;
import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.api.bukkit.util.ItemBuilder;
import net.beastcube.api.commons.minigames.MinigameInstance;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

/**
 * @author BeastCube
 */
public class ServerMenuItem extends MenuItem {
    private static final String DISPLAY_NAME = ChatColor.BLUE + "Minigame Server";
    private static final ItemStack ICON = new ItemStack(Material.DIAMOND);
    @Getter
    private MinigameInstance minigameInstance;

    public ServerMenuItem(MinigameInstance minigameInstance) {
        super(DISPLAY_NAME, ICON);
        this.minigameInstance = minigameInstance;
    }

    // This method controls what happens when the MenuItem is clicked
    @Override
    public void onItemClick(ItemClickEvent event) {
        BeastCubeAPI.connect(event.getPlayer(), minigameInstance.getServerName());
    }

    // This method lets you modify the ItemStack before it is displayed, based on the player opening the menu
    @SuppressWarnings("deprecation")
    @Override
    public ItemStack getFinalIcon(Player player, ItemMenu menu) {
        ItemStack itemstack = new ItemBuilder(Material.STAINED_CLAY).dyeColor(DyeColor.getByData((byte) minigameInstance.getDyeColorData())).amount(minigameInstance.getPlayers()).build();
        ItemMeta meta = itemstack.getItemMeta();
        meta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Server " + minigameInstance.getServerName());

        meta.setLore(Arrays.asList("",
                ChatColor.WHITE + minigameInstance.getState().getStateMessage(),
                "",
                ChatColor.YELLOW + "Spiel: " + ChatColor.WHITE + minigameInstance.getType().getDisplayName(),
                ChatColor.YELLOW + "Map: " + ChatColor.WHITE + (minigameInstance.getArenaName().equals("") ? "Voting" : minigameInstance.getArenaName()),
                ChatColor.YELLOW + "Spieler: " + ChatColor.WHITE + minigameInstance.getPlayers() + " / " + minigameInstance.getMaxPlayers(),
                "",
                ChatColor.translateAlternateColorCodes('&', minigameInstance.getJoinMessage())));
        itemstack.setItemMeta(meta);
        return itemstack;
    }

}
