package net.beastcube.api.bukkit.players.cosmetics.gadgets;

import lombok.Getter;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticItem;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticType;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author BeastCube
 */
public abstract class Gadget extends CosmeticItem implements Listener {

    @Getter
    private List<UUID> cooldowns = new ArrayList<>();

    public abstract boolean hasLeftClickAction();

    public abstract boolean hasRightClickAction();

    public abstract boolean LeftClick(Player p);

    public abstract boolean RightClick(Player p);

    public int getCooldown() {
        return 60;
    }

    @Override
    public CosmeticType getType() {
        return CosmeticType.GADGET;
    }

}
