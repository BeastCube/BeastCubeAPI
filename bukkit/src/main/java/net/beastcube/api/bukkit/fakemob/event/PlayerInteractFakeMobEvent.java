package net.beastcube.api.bukkit.fakemob.event;

import net.beastcube.api.bukkit.fakemob.util.FakeMob;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerInteractFakeMobEvent extends Event implements Cancellable {
    private final Player player;
    private final FakeMob mob;
    private final Action action;
    private static HandlerList handlers = new HandlerList();
    private boolean cancelled;

    public PlayerInteractFakeMobEvent(Player player, FakeMob mob, Action action) {
        this.player = player;
        this.mob = mob;
        this.action = action;
    }

    public Player getPlayer() {
        return this.player;
    }

    public FakeMob getMob() {
        return this.mob;
    }

    public Action getAction() {
        return this.action;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public enum Action {
        LEFT_CLICK, RIGHT_CLICK
    }

}
