package net.beastcube.api.bukkit.util.serialization.adapters;

import com.google.gson.*;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.lang.reflect.Type;

/**
 * @author BeastCube
 */
public class WorldSerializer implements JsonDeserializer<World>, JsonSerializer<World> {

    @Override
    public World deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return Bukkit.getWorld(jsonElement.getAsString());
    }

    @Override
    public JsonElement serialize(World world, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonElement root = new JsonObject();
        if (world != null) {
            root = new JsonPrimitive(world.getName());
        }
        return root;
    }
}
