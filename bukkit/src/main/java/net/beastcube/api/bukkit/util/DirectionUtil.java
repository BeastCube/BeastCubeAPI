package net.beastcube.api.bukkit.util;

import org.bukkit.entity.Player;

/**
 * @author BeastCube
 */
public class DirectionUtil {

    public static PlayerDirection getCardinalDirection(Player p) {
        return getCardinalDirection(p, 0);
    }

    public static PlayerDirection getCardinalDirection(Player p, int yawOffset) {
        if (p.getLocation().getPitch() > 67.5) {
            return PlayerDirection.DOWN;
        }
        if (p.getLocation().getPitch() < -67.5) {
            return PlayerDirection.UP;
        }

        // From hey0's code
        double rot = (p.getLocation().getYaw() + yawOffset) % 360; //let's use real yaw now
        if (rot < 0) {
            rot += 360.0;
        }
        return getCardinalDirection(rot);
    }

    /**
     * Returns direction according to rotation. May return null.
     *
     * @param rot yaw
     * @return the direction
     */
    private static PlayerDirection getCardinalDirection(double rot) {
        if (0 <= rot && rot < 22.5) {
            return PlayerDirection.SOUTH;
        } else if (22.5 <= rot && rot < 67.5) {
            return PlayerDirection.SOUTH_WEST;
        } else if (67.5 <= rot && rot < 112.5) {
            return PlayerDirection.WEST;
        } else if (112.5 <= rot && rot < 157.5) {
            return PlayerDirection.NORTH_WEST;
        } else if (157.5 <= rot && rot < 202.5) {
            return PlayerDirection.NORTH;
        } else if (202.5 <= rot && rot < 247.5) {
            return PlayerDirection.NORTH_EAST;
        } else if (247.5 <= rot && rot < 292.5) {
            return PlayerDirection.EAST;
        } else if (292.5 <= rot && rot < 337.5) {
            return PlayerDirection.SOUTH_EAST;
        } else if (337.5 <= rot && rot < 360.0) {
            return PlayerDirection.SOUTH;
        } else {
            return null;
        }
    }

}
