package net.beastcube.api.bukkit.players.cosmetics.pets;

import com.dsh105.echopet.compat.api.entity.PetType;
import lombok.Getter;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticItem;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticType;
import org.bukkit.Material;

/**
 * @author BeastCube
 */
public class Pet extends CosmeticItem {
    @Getter
    private String id;
    @Getter
    private PetType petType = null;

    public Pet(String id, String displayName, Material material, PetType petType, int price) {
        this.id = id;
        this.displayName = displayName;
        this.material = material;
        this.petType = petType;
        this.price = price;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public Material getMaterial() {
        return material;
    }

    @Override
    public CosmeticType getType() {
        return CosmeticType.PET;
    }

}