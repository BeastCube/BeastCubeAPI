package net.beastcube.api.bukkit.commands;

import com.google.common.base.Joiner;
import net.beastcube.api.bukkit.BeastCubeSlave;
import net.beastcube.api.bukkit.BeastCubeSlaveBukkitPlugin;
import net.beastcube.api.bukkit.commands.handlers.*;
import net.beastcube.api.bukkit.util.ReflectionUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.*;
import org.bukkit.craftbukkit.v1_8_R3.help.SimpleHelpMap;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

public class CommandHandler implements CommandExecutor { //TODO Player specific commands using BeastCubeHelpMap

    private JavaPlugin plugin;
    private Map<Class<?>, ArgumentHandler<?>> argumentHandlers = new HashMap<>();
    private Map<CommandSender, Map<org.bukkit.command.Command, RootCommand>> senderSpecificRootCommands = new HashMap<>();
    private Map<org.bukkit.command.Command, RootCommand> rootCommands = new HashMap<>();
    private Map<String, DynamicCommand> bukkitCommands = new HashMap<>();

    private PermissionHandler permissionHandler = (sender, permissions) -> {
        for (String perm : permissions) {
            if (!sender.hasPermission(perm))
                return false;
        }
        return true;
    };

    private HelpHandler helpHandler = new HelpHandler() {
        private String formatArgument(CommandArgument argument) {
            String def = argument.getDefault();
            if (def.equals(" ")) {
                def = "";
            } else if (def.startsWith("?")) {
                String varName = def.substring(1);
                def = argument.getHandler().getVariableUserFriendlyName(varName);
                if (def == null)
                    throw new IllegalArgumentException("The ArgumentVariable '" + varName + "' is not registered.");
                def = ChatColor.GOLD + " | " + ChatColor.WHITE + def;
            } else {
                def = ChatColor.GOLD + " | " + ChatColor.WHITE + def;
            }

            return ChatColor.AQUA + "[" + argument.getName() + def + ChatColor.AQUA + "] " + ChatColor.DARK_AQUA + argument.getDescription();
        }

        @Override
        public String[] getHelpMessage(RegisteredCommand command) {
            ArrayList<String> message = new ArrayList<>();

            if (command.isSet()) {
                message.add(ChatColor.AQUA + command.getDescription());
            }

            message.add(getUsage(command));

            if (command.isSet()) {
                for (CommandArgument argument : command.getArguments()) {
                    message.add(formatArgument(argument));
                }
                if (command.getWildcard() != null) {
                    message.add(formatArgument(command.getWildcard()));
                }
                List<Flag> flags = command.getFlags();
                if (flags.size() > 0) {
                    message.add(ChatColor.GOLD + "Flags:");
                    for (Flag flag : flags) {
                        StringBuilder args = new StringBuilder();
                        for (FlagArgument argument : flag.getArguments()) {
                            args.append(" [").append(argument.getName()).append("]");
                        }
                        message.add("-" + flag.getIdentifier() + ChatColor.AQUA + args.toString());
                        for (FlagArgument argument : flag.getArguments()) {
                            message.add(formatArgument(argument));
                        }
                    }
                }
            }


            List<RegisteredCommand> subcommands = command.getSuffixes();
            if (subcommands.size() > 0) {
                message.add(ChatColor.GOLD + "Subcommands:");
                for (RegisteredCommand scommand : subcommands) {
                    message.add(scommand.getUsage());
                }
            }

            return message.toArray(new String[message.size()]);
        }

        @Override
        public String getUsage(RegisteredCommand command) {
            StringBuilder usage = new StringBuilder();
            usage.append(command.getLabel());

            RegisteredCommand parent = command.getParent();
            while (parent != null) {
                usage.insert(0, parent.getLabel() + " ");
                parent = parent.getParent();
            }

            usage.insert(0, "/");

            if (!command.isSet())
                return usage.toString();

            usage.append(ChatColor.AQUA);

            for (CommandArgument argument : command.getArguments()) {
                usage.append(" [").append(argument.getName()).append("]");
            }

            usage.append(ChatColor.WHITE);

            for (Flag flag : command.getFlags()) {
                usage.append(" (-").append(flag.getIdentifier()).append(ChatColor.AQUA);
                for (FlagArgument arg : flag.getArguments()) {
                    usage.append(" [").append(arg.getName()).append("]");
                }
                usage.append(ChatColor.WHITE).append(")");
            }

            if (command.getWildcard() != null) {
                usage.append(ChatColor.AQUA).append(" [").append(command.getWildcard().getName()).append("]");
            }

            return usage.toString();
        }
    };

    private String helpSuffix = "help";

    public CommandHandler(JavaPlugin plugin) {
        this.plugin = plugin;

        registerArgumentHandler(String.class, new StringArgumentHandler());
        registerArgumentHandler(int.class, new IntegerArgumentHandler());
        registerArgumentHandler(double.class, new DoubleArgumentHandler());
        registerArgumentHandler(Player.class, new PlayerArgumentHandler());
        registerArgumentHandler(World.class, new WorldArgumentHandler());
        registerArgumentHandler(EntityType.class, new EntityTypeArgumentHandler());
        registerArgumentHandler(Material.class, new MaterialArgumentHandler());
    }

    @SuppressWarnings("unchecked")
    public <T> ArgumentHandler<? extends T> getArgumentHandler(Class<T> clazz) {
        return (ArgumentHandler<? extends T>) argumentHandlers.get(clazz);
    }

    public HelpHandler getHelpHandler() {
        return helpHandler;
    }

    public PermissionHandler getPermissionHandler() {
        return permissionHandler;
    }

    public <T> void registerArgumentHandler(Class<? extends T> clazz, ArgumentHandler<T> argHandler) {
        if (argumentHandlers.get(clazz) != null)
            throw new IllegalArgumentException("The is already a ArgumentHandler bound to the class " + clazz.getName() + ".");

        argHandler.handler = this;
        argumentHandlers.put(clazz, argHandler);
    }

    public void registerCommands(Object commands, CommandSender... senders) {
        Class clazz = commands.getClass();
        while (clazz != null) {
            for (Method method : clazz.getDeclaredMethods()) {
                Command commandAnno = method.getAnnotation(Command.class);
                if (commandAnno == null)
                    continue;
                for (String identifier : commandAnno.identifiers()) {
                    registerCommand(identifier.split(" "), commands, method, senders);
                }
            }
            clazz = clazz.getSuperclass();
        }
    }

    public void unregisterCommands(Object commands, CommandSender... senders) {
        Class clazz = commands.getClass();
        while (clazz != null) {
            for (Method method : clazz.getDeclaredMethods()) {
                Command commandAnno = method.getAnnotation(Command.class);
                if (commandAnno == null)
                    continue;
                for (String identifier : commandAnno.identifiers()) {
                    unregisterCommand(identifier.split(" "), senders);
                }
            }
            clazz = clazz.getSuperclass();
        }
    }

    @SuppressWarnings("unchecked")
    private synchronized void unregisterCommandInCommandMap(SimpleCommandMap map, String command) {
        Map<String, org.bukkit.command.Command> submap;
        try {
            Field submapField = ReflectionUtils.getField(map.getClass(), true, "knownCommands");
            submap = (Map<String, org.bukkit.command.Command>) submapField.get(map);

            org.bukkit.command.Command bukkitCommand = submap.get(command.toLowerCase());

            if (bukkitCommand != null) {
                bukkitCommand.unregister(map);
                submap.remove(command.toLowerCase());
            } else {
                System.out.println("Failed trying to unregister Command that was not registered: " + command);
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void unregisterCommandIfNotNeeded(DynamicCommand command) {
        Class<?> craftServerClass;
        try {
            craftServerClass = ReflectionUtils.PackageType.CRAFTBUKKIT.getClass("CraftServer");
            SimpleCommandMap commandMap = (SimpleCommandMap) ReflectionUtils.getField(craftServerClass, true, "commandMap").get(Bukkit.getServer());


            //unregister Bukkit command if noone is registered for it
            if (!(rootCommands.containsKey(command) || senderSpecificRootCommands.entrySet().stream().anyMatch(e -> e.getValue().containsKey(command)))) {
                unregisterCommandInCommandMap(commandMap, command.getName().toLowerCase());
                bukkitCommands.remove(command.getName().toLowerCase());

                BeastCubeSlaveBukkitPlugin.getInstance().getServer().getHelpMap().clear();
                ((SimpleHelpMap) BeastCubeSlaveBukkitPlugin.getInstance().getServer().getHelpMap()).initializeCommands();
            }
        } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void unregisterCommand(String[] identifiers, CommandSender... senders) {
        if (identifiers.length == 0)
            throw new CommandException("Invalid identifiers");

        String name = identifiers[0].toLowerCase();
        DynamicCommand rootPcommand = bukkitCommands.get(name);
        if (rootPcommand == null) {
            //System.out.println("Trying to unregister command that was not registered");
            return;
        }
        if (senders.length == 0) {
            if (identifiers.length == 1) {
                rootCommands.remove(rootPcommand);
            } else {
                RegisteredCommand mainCommand = rootCommands.get(rootPcommand);
                for (int i = 1; i < identifiers.length; i++) {
                    String suffix = identifiers[i];
                    if (mainCommand.doesSuffixCommandExist(suffix)) {
                        RegisteredCommand newCommand = mainCommand.getSuffixCommand(suffix);
                        if (newCommand.getSuffixes().size() > 0) {
                            mainCommand = newCommand;
                        } else {
                            mainCommand.removeSuffixCommand(suffix);
                        }
                    } else {
                        BeastCubeSlave.getInstance().getLogger().info("Trying to unregister command that was not registered: /" + Joiner.on(" ").join(Arrays.asList(identifiers).subList(0, i)));
                    }
                }
            }
        } else {
            Map<org.bukkit.command.Command, RootCommand> rCommands;
            for (CommandSender sender : senders) {
                rCommands = senderSpecificRootCommands.get(sender);
                if (rCommands != null) {
                    if (identifiers.length == 1) {
                        rCommands.remove(rootPcommand);
                    } else {
                        RegisteredCommand mainCommand = rCommands.get(rootPcommand);
                        for (int i = 1; i < identifiers.length; i++) {
                            String suffix = identifiers[i];
                            if (mainCommand.doesSuffixCommandExist(suffix)) {
                                RegisteredCommand newCommand = mainCommand.getSuffixCommand(suffix);
                                if (newCommand.getSuffixes().size() > 0) {
                                    mainCommand = newCommand;
                                } else {
                                    mainCommand.removeSuffixCommand(suffix);
                                }
                            } else {
                                BeastCubeSlave.getInstance().getLogger().info("Trying to unregister command that was not registered: /" + Joiner.on(" ").join(Arrays.asList(identifiers).subList(0, i)));
                            }
                        }
                    }
                } else {
                    System.out.println("Trying to unregister command for " + sender.getName() + " that was not registered");
                }
            }
        }
        unregisterCommandIfNotNeeded(rootPcommand);

    }

    private void registerCommand(String[] identifiers, Object methodInstance, Method method, CommandSender... senders) {
        if (identifiers.length == 0)
            throw new RegisterCommandMethodException(method, "Invalid identifiers");

        try {

            String name = identifiers[0].toLowerCase();
            DynamicCommand rootPcommand;

            DynamicCommand cmd = bukkitCommands.get(name);
            if (cmd != null) {
                rootPcommand = cmd;
            } else {
                rootPcommand = new DynamicCommand(name, this, plugin);

                Class<?> craftServerClass = ReflectionUtils.PackageType.CRAFTBUKKIT.getClass("CraftServer");
                CommandMap commandMap = (CommandMap) ReflectionUtils.getField(craftServerClass, true, "commandMap").get(Bukkit.getServer());

                //register it on the craftserver commandmap, so we can execute something when its called
                commandMap.register(name, rootPcommand);

                bukkitCommands.put(name, rootPcommand);

                BeastCubeSlaveBukkitPlugin.getInstance().getServer().getHelpMap().clear();
                ((SimpleHelpMap) BeastCubeSlaveBukkitPlugin.getInstance().getServer().getHelpMap()).initializeCommands();
            }

            if (senders.length == 0) {
                RootCommand rootCommand = rootCommands.get(rootPcommand);
                registerCommand(rootCommand, rootPcommand, identifiers, methodInstance, method, rootCommands);
            } else {
                for (CommandSender sender : senders) {
                    Map<org.bukkit.command.Command, RootCommand> rCommands = senderSpecificRootCommands.get(sender);
                    if (rCommands == null) {
                        rCommands = new HashMap<>();
                        senderSpecificRootCommands.put(sender, rCommands);
                    }
                    RootCommand rootCommand = rCommands.get(rootPcommand);
                    registerCommand(rootCommand, rootPcommand, identifiers, methodInstance, method, rCommands);
                }
            }
        } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void registerCommand(RootCommand rootCommand, DynamicCommand rootPcommand, String[] identifiers, Object methodInstance, Method method, Map<org.bukkit.command.Command, RootCommand> rootCommands) {
        if (rootCommand == null) {
            rootCommand = new RootCommand(rootPcommand, this);
            //register it in the rootCommands map, so we know what to execute
            rootCommands.put(rootPcommand, rootCommand);
        }

        RegisteredCommand mainCommand = rootCommand;

        for (int i = 1; i < identifiers.length; i++) {
            String suffix = identifiers[i];
            if (mainCommand.doesSuffixCommandExist(suffix)) {
                mainCommand = mainCommand.getSuffixCommand(suffix);
            } else {
                RegisteredCommand newCommand = new RegisteredCommand(suffix, this, mainCommand);
                mainCommand.addSuffixCommand(suffix, newCommand);
                mainCommand = newCommand;
            }
        }
        mainCommand.set(methodInstance, method);
    }

    public void setHelpHandler(HelpHandler helpHandler) {
        this.helpHandler = helpHandler;
    }

    public void setPermissionHandler(PermissionHandler permissionHandler) {
        this.permissionHandler = permissionHandler;
    }

    public String getHelpSuffix() {
        return helpSuffix;
    }

    public void setHelpSuffix(String suffix) {
        this.helpSuffix = suffix;
    }

    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] args) {
        //TODO If rootCammands contains a command which is also in senderSpecificRootCommands, senderSpecificRootCommands are not working
        RootCommand rootCommand = rootCommands.get(command);
        if (rootCommand == null) {
            Map<org.bukkit.command.Command, RootCommand> rCommands = senderSpecificRootCommands.get(sender);
            if (rCommands == null)
                return false;
            rootCommand = rCommands.get(command);
            if (rootCommand == null)
                return false;
        }

        if (rootCommand.onlyPlayers() && !(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Sorry, but only players can execute this command.");
            return true;
        }

        rootCommand.execute(sender, args);

        return true;
    }
}