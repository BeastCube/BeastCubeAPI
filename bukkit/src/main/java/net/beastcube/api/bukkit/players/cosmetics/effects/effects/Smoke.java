package net.beastcube.api.bukkit.players.cosmetics.effects.effects;

import de.slikey.effectlib.effect.SmokeEffect;
import lombok.Getter;
import net.beastcube.api.bukkit.players.cosmetics.effects.Effect;
import net.beastcube.api.bukkit.players.cosmetics.effects.EffectManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * @author BeastCube
 */
public class Smoke extends Effect {
    @Getter
    private String id = "smoke";

    @Override
    public String getDisplayName() {
        return EffectManager.DISPLAY_NAME_COLOR + "Smoke";
    }

    @Override
    public Material getMaterial() {
        return Material.CLAY_BALL;
    }

    @Override
    public de.slikey.effectlib.Effect getEffect(Player p, de.slikey.effectlib.EffectManager e) {
        return new SmokeEffect(e);
    }
}
