/*
 * This file is part of AmpMenus.
 *
 * Copyright (c) 2014 <http://github.com/ampayne2/AmpMenus/>
 *
 * AmpMenus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AmpMenus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with AmpMenus.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.beastcube.api.bukkit.menus.items;

import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.api.bukkit.menus.menus.PagedMenu;
import net.beastcube.api.bukkit.util.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * A {@link StaticMenuItem} that goes to the {@link PagedMenu}'s next page if it exists.
 */
public class CurrentPageItem extends StaticMenuItem {

    public CurrentPageItem() {
        super(ChatColor.YELLOW + "Seite", new ItemStack(Material.PAPER));
    }

    public CurrentPageItem(String displayName, ItemStack icon) {
        super(displayName, icon);
    }

    @Override
    public ItemStack getFinalIcon(Player player, ItemMenu menu) {
        if (menu instanceof PagedMenu) {
            PagedMenu pagedMenu = (PagedMenu) menu;
            return new ItemBuilder(getIcon().clone()).name(ChatColor.YELLOW + "Seite " + pagedMenu.getPage() + " von " + pagedMenu.getPageCount()).amount(pagedMenu.getPage()).build();
        }
        return super.getFinalIcon(player, menu);
    }
}
