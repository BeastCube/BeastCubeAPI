package net.beastcube.api.bukkit.players.cosmetics.menuitems;

import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.BackItem;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticItem;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticType;
import net.beastcube.api.bukkit.util.ItemBuilder;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author BeastCube
 */
public class CosmeticTypeMenuItem extends MenuItem {
    private final CosmeticType type;
    ItemMenu menu;

    public CosmeticTypeMenuItem(JavaPlugin plugin, ItemMenu parent, CosmeticType type) {
        super("", null);
        this.type = type;
        menu = new ItemMenu(type.getPluralDisplayName(), ItemMenu.Size.FOUR_LINE, plugin, parent);
    }

    @Override
    public void onItemClick(ItemClickEvent e) {
        menu.setItem(4, new BackItem());
        menu.setItem(6, new DisableMenuItem(type));
        int startslot = 9;
        for (int i = 0; i < type.getManager().getCosmetics().length; i++) {
            CosmeticItem cosmetic = type.getManager().getCosmetics()[i];
            menu.setItem(startslot + i, new CosmeticItemMenuItem(menu, cosmetic));
        }
        menu.open(e.getPlayer());
    }

    @Override
    public ItemStack getFinalIcon(Player p, ItemMenu menu) {
        return new ItemBuilder(type.getIcon()).amount(1).durability(type.getData()).name(type.getPluralDisplayName()).build();
    }

}
