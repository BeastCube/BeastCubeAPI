package net.beastcube.api.bukkit.fakemob.util;

import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class Config {
    public static File configFile = new File("plugins/fakemob/config.yml");
    public static int SEE_RADIUS = 60;

    public static void load() {
        YamlConfiguration config = YamlConfiguration.loadConfiguration(configFile);
        SEE_RADIUS = config.getInt("Mobs.Radius");
    }

    public static void save() {
        YamlConfiguration config = new YamlConfiguration();
        config.set("Mobs.Radius", SEE_RADIUS);

        try {
            config.save(configFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
