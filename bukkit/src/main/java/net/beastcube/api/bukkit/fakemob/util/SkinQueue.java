package net.beastcube.api.bukkit.fakemob.util;

import com.comphenix.protocol.wrappers.WrappedGameProfile;
import net.beastcube.api.bukkit.fakemob.FakeMobsPlugin;

import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

public class SkinQueue extends Thread {
    private final Queue<SkinEntry> queue = new LinkedBlockingQueue<>();

    public SkinQueue() {
        super("FakeMobs skin queue");
        this.setDaemon(true);
    }

    @Override
    public void run() {
        while (!this.isInterrupted()) {
            this.blockThread();
            while (!this.queue.isEmpty()) {
                SkinEntry entry = this.queue.poll();
                if (FakeMobsPlugin.getMob(entry.getMob().getId()) != entry.getMob()) continue;

                Object nmsProfile = FakeMobsReflectionUtils.searchUUID(entry.getSkinName());
                WrappedGameProfile profile = nmsProfile == null ? new WrappedGameProfile((UUID) null, entry.getSkinName()) : WrappedGameProfile.fromHandle(nmsProfile);

                if (profile.getProperties().isEmpty() && profile.isComplete())
                    profile = WrappedGameProfile.fromHandle(FakeMobsReflectionUtils.fillProfileProperties(profile.getHandle()));
                if (profile == null) continue;

                entry.getMob().setPlayerSkin(profile.getProperties());
                entry.getMob().updateCustomName();
                FakeMobsPlugin.saveMobs();
            }
        }
    }

    public synchronized void addToQueue(FakeMob mob, String skinName) {
        this.queue.add(new SkinEntry(mob, skinName));
        this.notify();
    }

    private synchronized void blockThread() {
        try {
            while (this.queue.isEmpty()) {
                this.wait();
            }
        } catch (Exception ignored) {
        }
    }

    private static class SkinEntry {
        private final FakeMob mob;
        private final String skinName;

        public SkinEntry(FakeMob mob, String skinName) {
            this.mob = mob;
            this.skinName = skinName;
        }

        public FakeMob getMob() {
            return this.mob;
        }

        public String getSkinName() {
            return this.skinName;
        }
    }

}