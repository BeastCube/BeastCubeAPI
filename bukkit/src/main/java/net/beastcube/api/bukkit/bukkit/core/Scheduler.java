package net.beastcube.api.bukkit.bukkit.core;

import net.beastcube.api.bukkit.BeastCubeSlaveBukkitPlugin;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;

/**
 * Scheduler class for beastcube to be used when porting from Bukkit to Sponge..
 */
public class Scheduler {
    private final List<ScheduledTask> tasks = new ArrayList<>();
    private long elapsed;

    public Scheduler() {
        Log.partEnable("Scheduler");
        Bukkit.getScheduler().scheduleSyncRepeatingTask(
                BeastCubeSlaveBukkitPlugin.getInstance(), Scheduler.this::tick, 1L, 0L);
    }

    public int scheduleSyncRepeatingTask(final Runnable runnable, final long delay,
                                         final long interval) {
        return Bukkit.getScheduler().scheduleSyncRepeatingTask(
                BeastCubeSlaveBukkitPlugin.getInstance(), runnable, delay, interval);
    }

    public int scheduleSyncDelayedTask(final Runnable runnable, final long delay) {
        return Bukkit.getScheduler().scheduleSyncDelayedTask(
                BeastCubeSlaveBukkitPlugin.getInstance(), runnable, delay);
    }

    public void delay(final long delay, final Runnable runnable) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(
                BeastCubeSlaveBukkitPlugin.getInstance(), runnable, delay);
    }

    public ScheduledTask each(final long period, final Runnable runnable) {
        ScheduledTask task = new ScheduledTask(period, runnable);
        this.tasks.add(task);
        return task;
    }

    public void cancel(final ScheduledTask task) {
        this.tasks.remove(task);
    }

    public void cancelTask(final int taskId) {
        Bukkit.getScheduler().cancelTask(taskId);
    }

    public void tick() {
        ArrayList<ScheduledTask> temptasks = new ArrayList<>(this.tasks);
        temptasks.stream().filter(task -> this.elapsed % task.period == 0).forEach(task -> {
            try {
                task.runnable.run();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        this.elapsed++;
    }

    public static final class ScheduledTask {
        public ScheduledTask(final long period2, final Runnable runnable2) {
            this.period = period2;
            this.runnable = runnable2;
        }

        public final Runnable runnable;
        public final long period;
    }
}
