package net.beastcube.api.bukkit;

import com.google.common.base.Preconditions;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Bukkit (spigot compactibile) bootloader.
 */
public class BeastCubeSlaveBukkitPlugin extends JavaPlugin implements Listener {
    private static BeastCubeSlaveBukkitPlugin instance;

    public static BeastCubeSlaveBukkitPlugin getInstance() {
        return BeastCubeSlaveBukkitPlugin.instance;
    }

    public static void setInstance(final BeastCubeSlaveBukkitPlugin plugin) {
        Preconditions.checkArgument(BeastCubeSlaveBukkitPlugin.instance == null);
        BeastCubeSlaveBukkitPlugin.instance = plugin;
    }

    public BeastCubeSlaveBukkitPlugin() {
    }

    @Override
    public void onEnable() {
        this.getLogger().info(
                "[BOOT] Bootstrapping BeastCubeSlave throught BeastCubeSlaveBukkitPlugin...");
        setInstance(this);
        BeastCubeSlave.init(this.getDataFolder(), this.getClassLoader());
        // Start sync.
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, BeastCubeSlave.getInstance().getSync().getOnTick(), 0L, 1L);
        //BeastCubeSlave.getInstance().getLogger().displayTimestamps = false;
        BeastCubeAPI.getInstance().onEnable();
    }

    @Override
    public void onDisable() {
        this.getLogger().info(
                "[BOOT] Disabling BeastCubeSlave throught BeastCubeSlaveBukkitPlugin...");
        // TODO: Maybe implement some safe-shutdown, so tasks in Sync wont be lost.
        BeastCubeSlave.getInstance().shutdown();
        BeastCubeAPI.getInstance().onDisable();
    }

}
