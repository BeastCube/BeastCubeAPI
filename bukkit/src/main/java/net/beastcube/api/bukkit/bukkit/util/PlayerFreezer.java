package net.beastcube.api.bukkit.bukkit.util;

import net.beastcube.api.bukkit.BeastCubeSlaveBukkitPlugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility class that help freezing of players.
 *
 * @author Mato Kormuth
 */
public class PlayerFreezer implements Listener {
    /**
     * List of frozen textures. <br/>
     * FIXME: Remove 'dead' disconnected player objects.
     */
    private final List<Player> frozen_movement = new ArrayList<Player>();
    private final List<Player> frozen_rotation = new ArrayList<Player>();

    public PlayerFreezer() {
        Bukkit.getPluginManager().registerEvents(this,
                BeastCubeSlaveBukkitPlugin.getInstance());
    }

    /**
     * Freezes (disables movement) the player.
     *
     * @param player player to freeze
     */
    public void freeze(final Player player) {
        this.freeze(player, false);
    }

    public void freeze(final Player player, final boolean rotation) {
        this.frozen_movement.add(player);
        if (rotation)
            this.frozen_rotation.add(player);
    }

    /**
     * Unfreezes (enables movement) the player.
     *
     * @param player player to unfreeze
     */
    public void unfreeze(final Player player) {
        this.frozen_movement.remove(player);
        this.frozen_rotation.remove(player);
    }

    @EventHandler
    private void onPlayerMove(final PlayerMoveEvent event) {
        if (this.frozen_movement.contains(event.getPlayer()))
            event.setTo(event.getFrom());
    }
}
