package net.beastcube.api.bukkit.players.cosmetics.gadgets.gadgets;

import lombok.Getter;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.Gadget;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.GadgetManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.util.Vector;

/**
 * @author BeastCube
 */
public class Jetpack extends Gadget {
    @Getter
    private String id = "jetpack";
    @Getter
    private String displayName = GadgetManager.DISPLAY_NAME_COLOR + "jetpack";
    @Getter
    private Material material = Material.FEATHER;
    @Getter
    private String description = ChatColor.GRAY + "Fliege über den Wolken";

    @Override
    public boolean hasLeftClickAction() {
        return false;
    }

    @Override
    public boolean hasRightClickAction() {
        return false;
    }

    @Override
    public boolean LeftClick(Player p) {
        return false;
    }

    @Override
    public boolean RightClick(Player p) {
        return false;
    }

    //****************************************************************************\\
    //********************************** Events **********************************\\
    //****************************************************************************\\

    @EventHandler
    public void onPlayerToggleSneak(PlayerToggleSneakEvent e) {
        Vector nearBlock = new Vector(e.getPlayer().getLocation().getDirection().getX() * 3.0D, getRecommendedVelocity(e.getPlayer()), e.getPlayer().getLocation().getDirection().getZ() * 2.0D);
        e.getPlayer().setVelocity(nearBlock);
        e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLAZE_BREATH, 1.0F, 1.0F);
    }

    public double getRecommendedVelocity(Player player) {
        if (player.getLocation().getY() > 250.0D) {
            return -6.0D;
        }
        if (player.getLocation().getY() > 512.0D) {
            return 0.0D;
        }
        return 0.8D;
    }

}
