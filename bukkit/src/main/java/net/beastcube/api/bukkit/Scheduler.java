package net.beastcube.api.bukkit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class that represents scheduler.
 */
public class Scheduler implements TickHandler {
    private final List<ScheduledTask> tasks = new ArrayList<>();

    public void shutdownNow() {
        // Remove all tasks
        this.tasks.clear();
    }

    public ScheduledTask delay(final long delay, final Runnable runnable) {
        ScheduledTask task = new ScheduledTask(runnable, -1, delay);
        this.tasks.add(task);
        return task;
    }

    public ScheduledTask each(final long interval, final Runnable runnable) {
        ScheduledTask task = new ScheduledTask(runnable, interval);
        this.tasks.add(task);
        return task;
    }

    public ScheduledTask each(final long interval, final long delay,
                              final Runnable runnable) {
        ScheduledTask task = new ScheduledTask(runnable, interval, delay);
        this.tasks.add(task);
        return task;
    }

    @Override
    public void tick(final long n) {
        long time = System.currentTimeMillis();
        for (Iterator<ScheduledTask> iterator = this.tasks.iterator(); iterator.hasNext(); ) {
            ScheduledTask task = iterator.next();

            if (task.nextRun < time) {
                task.fire();
                task.nextRun = time + task.interval;
            }

            if (task.nextRun == -1 || task.interval == -1) {
                iterator.remove();
            }
        }
    }

    public static class ScheduledTask {
        private final long interval;
        private long nextRun = 0;
        private final Runnable task;

        public ScheduledTask(final Runnable runnable, final long interval) {
            this.interval = interval;
            this.task = runnable;
            this.nextRun = System.currentTimeMillis();
        }

        public ScheduledTask(final Runnable runnable, final long interval,
                             final long delay) {
            if (interval != -1) {
                this.interval = interval * 20 * 1000;
            } else {
                this.interval = -1;
            }
            this.task = runnable;
            this.nextRun = System.currentTimeMillis() + delay * 20 * 1000;
        }

        public void fire() {
            try {
                this.task.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void cancel() {
            this.nextRun = -1;
        }
    }
}
