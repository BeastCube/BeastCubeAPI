/*
 * This file is part of AmpMenus.
 *
 * Copyright (c) 2014 <http://github.com/ampayne2/AmpMenus/>
 *
 * AmpMenus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AmpMenus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with AmpMenus.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.beastcube.api.bukkit.menus.menus;

import lombok.Getter;
import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PagedMenu extends ItemMenu {

    protected Map<Integer, MenuItem> menuItems = new HashMap<>();

    @Getter
    private int page = 1;

    public PagedMenu(String name, Size size, JavaPlugin plugin, ItemMenu parent) {
        super(name, size, plugin, parent);
    }

    public PagedMenu(String name, Size size, JavaPlugin plugin) {
        super(name, size, plugin);
    }

    /**
     * Sets the {@link MenuItem} of a slot.
     *
     * @param position The slot position.
     * @param menuItem The {@link MenuItem}.
     * @return The {@link PagedMenu}.
     */
    @Override
    public PagedMenu setItem(int position, MenuItem menuItem) {
        items.put(position, menuItem);
        return this;
    }

    /**
     * Sets the {@link MenuItem} of a menu slot.
     *
     * @param position The slot position.
     * @param menuItem The {@link MenuItem}.
     * @return The {@link PagedMenu}.
     */
    public PagedMenu setMenuItem(int position, MenuItem menuItem) {
        if (position >= 0 && position < 9) {
            menuItems.put(position, menuItem);
        }
        return this;
    }

    /**
     * Fills all empty slots in the {@link PagedMenu} with a certain {@link MenuItem}.
     *
     * @param menuItem The {@link MenuItem}.
     * @return The {@link PagedMenu}.
     */
    @Override
    public PagedMenu fillEmptySlots(MenuItem menuItem) {
        for (int i = 0; i < size.getSize(true); i++) {
            if (items.get(i) == null) {
                items.put(i, menuItem);
            }
        }
        return this;
    }

    /**
     * Applies the {@link PagedMenu} for a player to an Inventory.
     *
     * @param inventory The Inventory.
     * @param player    The Player.
     */
    @Override
    protected void apply(Inventory inventory, Player player) { //TODO ONELINE AND PAGED
        for (int i = 0; i <= size.getSize(true); i++) {
            if (items.get((page - 1) * size.getSize(true) + i) != null) {
                inventory.setItem(i, items.get((page - 1) * size.getSize(true) + i).getFinalIcon(player, this));
            } else {
                inventory.setItem(i, null);
            }
        }
        for (int i = 0; i < 9; i++) {
            if (menuItems.get(i) != null) {
                inventory.setItem(size.getSize(true) + i, menuItems.get(i).getFinalIcon(player, this));
            } else {
                inventory.setItem(size.getSize(true) + i, null);
            }
        }
    }

    /**
     * Handles InventoryClickEvents for the {@link ItemMenu}.
     */
    @SuppressWarnings("deprecation")
    @Override
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getClick() == ClickType.LEFT || event.getClick() == ClickType.SHIFT_LEFT) { //TODO?
            int slot = event.getRawSlot();
            if (slot >= 0 && slot < size.getSize(true) && items.get(slot) != null) {
                handleClickEvent(event, items.get(slot));
            } else if (slot >= size.getSize(true) && slot < size.getSize(false) && menuItems.get(slot - size.getSize(true)) != null) {
                handleClickEvent(event, menuItems.get(slot - size.getSize(true)));
            }
        }
    }

    private void handleClickEvent(InventoryClickEvent event, MenuItem item) {
        Player player = (Player) event.getWhoClicked();
        ItemClickEvent itemClickEvent = new ItemClickEvent(player, event.getClick());
        item.onItemClick(itemClickEvent);
        super.handleClickEvent(itemClickEvent, player);
        handleClickEvent(itemClickEvent, player);
    }

    @Override
    public void handleClickEvent(ItemClickEvent e, Player player) {
        switch (e.getAction()) {
            case NEXTPAGE:
                nextPage(player);
                break;
            case PREVIOUSPAGE:
                previousPage(player);
                break;
            default:
                break;
        }
    }

    private void previousPage(Player player) {
        if (page > 1)
            page -= 1;
        update(player);
    }

    private void nextPage(Player player) {
        if (page < getPageCount())
            page += 1;
        update(player);
    }

    /**
     * Clears the {@link ItemMenu}.
     */
    @Override
    public void clear() {
        super.clear();
        menuItems.clear();
    }

    /**
     * Destroys the {@link ItemMenu}.
     */
    @Override
    public void destroy() {
        super.destroy();
        menuItems = null;
    }

    /**
     * Gets the {@link ItemMenu.Size} of the {@link ItemMenu}.
     *
     * @return The {@link ItemMenu}'s {@link ItemMenu.Size}.
     */
    @Override
    public Size getSize() {
        return super.getSize(); //TODO Divider
    }

    public int getPageCount() {
        return countNumberOfPages(items.keySet().isEmpty() ? 1 : Collections.max(items.keySet()) + 1, getSize().getSize(true));
    }

    private int countNumberOfPages(int numberOfObjects, int pageSize) {
        return numberOfObjects / pageSize + (numberOfObjects % pageSize == 0 ? 0 : 1);
    }

}
