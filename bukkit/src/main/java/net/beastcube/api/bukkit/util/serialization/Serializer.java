package net.beastcube.api.bukkit.util.serialization;

import com.google.gson.*;
import com.mojang.util.UUIDTypeAdapter;
import net.beastcube.api.bukkit.util.serialization.adapters.CuboidRegionSerializer;
import net.beastcube.api.bukkit.util.serialization.adapters.ItemStackSerializer;
import net.beastcube.api.bukkit.util.serialization.adapters.LocationSerializer;
import net.beastcube.api.bukkit.util.serialization.adapters.WorldSerializer;
import net.beastcube.api.commons.CuboidRegion;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author BeastCube
 */
public class Serializer {

    private static Gson gson;
    private static Map<Class<?>, Object> typeAdapters = new HashMap<>();

    static {
        registerTypeAdapter(UUID.class, new UUIDTypeAdapter());
        registerTypeAdapter(Location.class, new LocationSerializer());
        registerTypeAdapter(ItemStack.class, new ItemStackSerializer());
        registerTypeAdapter(World.class, new WorldSerializer());
        registerTypeAdapter(CuboidRegion.class, new CuboidRegionSerializer());
        initializeGson();
    }

    public static boolean registerTypeAdapter(Class<?> type, Object typeAdapter) {
        if (typeAdapter instanceof JsonSerializer || typeAdapter instanceof JsonDeserializer || typeAdapter instanceof InstanceCreator || typeAdapter instanceof TypeAdapter) {
            typeAdapters.put(type, typeAdapter);
            initializeGson();
            return true;
        } else {
            return false;
        }
    }

    private static void initializeGson() {
        GsonBuilder gsonBuilder = new GsonBuilder().setPrettyPrinting();
        typeAdapters.forEach(gsonBuilder::registerTypeHierarchyAdapter);
        gson = gsonBuilder.create();
    }

    public static <T> T deserialize(String string, Class<T> type) {
        return gson.fromJson(string, type);
    }

    public static JsonElement serialize(Object object) {
        return serialize(object, true);
    }

    public static JsonElement serialize(Object object, boolean onlyAnnotatedWithSerialize) {
        if (onlyAnnotatedWithSerialize) {
            Class clazz = object.getClass();
            JsonObject jsonObject = new JsonObject();
            while (clazz != null) {
                for (Field field : clazz.getDeclaredFields()) {
                    if (field.isAnnotationPresent(Serialize.class)) {
                        try {
                            field.setAccessible(true);
                            jsonObject.add(field.getName(), gson.toJsonTree(field.get(object)));
                        } catch (java.lang.IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
                clazz = clazz.getSuperclass();
            }
            return jsonObject;
        } else {
            return gson.toJsonTree(object);
        }
    }

    public static String formatPretty(JsonElement json) {
        return gson.toJson(json);
    }

}
