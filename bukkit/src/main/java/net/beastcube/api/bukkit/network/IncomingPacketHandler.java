package net.beastcube.api.bukkit.network;

import com.ikeirnez.pluginmessageframework.packet.PacketHandler;
import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.players.TeleportScheduler;
import net.beastcube.api.commons.packets.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * @author BeastCube
 */
public class IncomingPacketHandler {

    @SuppressWarnings("deprecation")
    @PacketHandler
    public void onPlaySoundPacket(Player player, PlaySoundPacket packet) {
        if (player.isOnline()) {
            player.playSound(player.getLocation(), packet.getSound(), packet.getVolume(), packet.getPitch());
        }
    }

    @PacketHandler
    public void onTeleportToPlayerPacket(Player player, TeleportToPlayerPacket packet) {
        TeleportScheduler.scheduleTeleport(packet.getPlayer(), packet.getTarget());
    }

    @PacketHandler
    public void onTeleportToLocatiorPacket(Player player, TeleportToLocationPacket packet) {
        TeleportScheduler.scheduleTeleport(packet.getPlayer(), packet.getLocation());
    }

    @PacketHandler
    public void onMinigamesPacket(Player player, MinigamesUpdatePacket packet) {
        BeastCubeAPI.getMinigamesManager().refreshMinigames(packet.getMinigameInstances());
    }

    @PacketHandler
    public void onRoleChangedPacket(Player player, RoleChangedPacket packet) {
        Player p = Bukkit.getPlayer(packet.getUniqueId());
        if (p != null) {
            BeastCubeAPI.updateNameTag(p);
            BeastCubeAPI.getInstance().setPermissions(p);
        }
    }

}