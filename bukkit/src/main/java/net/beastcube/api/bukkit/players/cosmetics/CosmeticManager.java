package net.beastcube.api.bukkit.players.cosmetics;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * @author BeastCube
 */
public abstract class CosmeticManager<C extends CosmeticItem> {

    public static void removeAll(Player p) {
        for (CosmeticType type : CosmeticType.values()) {
            type.getManager().removeCosmetic(p);
        }

        //TODO Save activated and activate them here
    }

    private Map<UUID, C> applied = new HashMap<>();

    public abstract C[] getCosmetics();

    public void applyCosmetic(Player p, C cosmetic) {
        finalApplyCosmetic(p, cosmetic);
        applied.put(p.getUniqueId(), cosmetic);
    }

    public void removeCosmetic(Player p) {
        finalRemoveCosmetic(p);
        applied.remove(p.getUniqueId());
    }

    protected abstract void finalApplyCosmetic(Player p, C cosmetic);

    protected abstract void finalRemoveCosmetic(Player p);

    public Optional<C> getCosmetic(Player p) {
        return Optional.ofNullable(applied.get(p.getUniqueId()));
    }

    public boolean hasCosmetic(Player p) {
        return getCosmetic(p).isPresent();
    }

    public Optional<C> getCosmetic(ItemStack stack) {
        for (C cosmetic : getCosmetics())
            if (cosmetic.isCosmeticItem(stack))
                return Optional.of(cosmetic);
        return Optional.empty();
    }

    public boolean isActivated(Player p, C cosmetic) {
        return hasCosmetic(p) && getCosmetic(p).get() == cosmetic;
    }
}
