package net.beastcube.api.bukkit.players.cosmetics.gadgets.gadgets;

import lombok.Getter;
import net.beastcube.api.bukkit.BeastCubeSlaveBukkitPlugin;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.Gadget;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.GadgetManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Bat;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

/**
 * @author BeastCube
 */
public class BatBlaster extends Gadget {
    @Getter
    private String id = "batblaster";
    @Getter
    private String displayName = GadgetManager.DISPLAY_NAME_COLOR + "Bat Blaster";
    @Getter
    private Material material = Material.IRON_BARDING;
    @Getter
    private String description = "TODO";

    @Override
    public boolean hasLeftClickAction() {
        return false;
    }

    @Override
    public boolean hasRightClickAction() {
        return false;
    }

    @Override
    public boolean LeftClick(Player p) {
        return false;
    }

    @Override
    public boolean RightClick(Player p) {
        Bat bat = (Bat) p.getWorld().spawnEntity(p.getLocation(), EntityType.BAT);
        bat.setVelocity(p.getEyeLocation().getDirection());
        Bukkit.getScheduler().runTaskLater(BeastCubeSlaveBukkitPlugin.getInstance(), bat::remove, 5 * 20L);
        //TODO Play particle
        //TODO collision and sound
        //TODO multiple bats
        return true;
    }

}
