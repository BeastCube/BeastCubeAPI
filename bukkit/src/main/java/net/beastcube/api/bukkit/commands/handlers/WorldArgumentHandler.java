package net.beastcube.api.bukkit.commands.handlers;

import net.beastcube.api.bukkit.commands.ArgumentHandler;
import net.beastcube.api.bukkit.commands.CommandArgument;
import net.beastcube.api.bukkit.commands.CommandError;
import net.beastcube.api.bukkit.commands.TransformError;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class WorldArgumentHandler extends ArgumentHandler<World> {
    public WorldArgumentHandler() {
        setMessage("world_not_found", "The world \"%1\" was not found");

        addVariable("sender", "The command executor", (sender, argument, varName) -> {
            if (!(sender instanceof Player))
                throw new CommandError(argument.getMessage("cant_as_console"));

            return ((Player) sender).getWorld();
        });
    }

    @Override
    public World transform(CommandSender sender, CommandArgument argument, String value) throws TransformError {
        World world = Bukkit.getWorld(value);
        if (world == null)
            throw new TransformError(argument.getMessage("world_not_found", value));
        return world;
    }
}
