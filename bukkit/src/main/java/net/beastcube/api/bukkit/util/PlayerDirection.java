/*
 * WorldEdit, a Minecraft world manipulation toolkit
 * Copyright (C) sk89q <http://www.sk89q.com>
 * Copyright (C) WorldEdit team and contributors
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.beastcube.api.bukkit.util;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.util.Vector;

/**
 * The player's direction.
 */
@RequiredArgsConstructor
public enum PlayerDirection {

    NORTH(new Vector(0, 0, -1), new Vector(-1, 0, 0), true, false),
    NORTH_EAST((new Vector(1, 0, -1)).normalize(), (new Vector(-1, 0, -1)).normalize(), false, false),
    EAST(new Vector(1, 0, 0), new Vector(0, 0, -1), true, false),
    SOUTH_EAST((new Vector(1, 0, 1)).normalize(), (new Vector(1, 0, -1)).normalize(), false, false),
    SOUTH(new Vector(0, 0, 1), new Vector(1, 0, 0), true, false),
    SOUTH_WEST((new Vector(-1, 0, 1)).normalize(), (new Vector(1, 0, 1)).normalize(), false, false),
    WEST(new Vector(-1, 0, 0), new Vector(0, 0, 1), true, false),
    NORTH_WEST((new Vector(-1, 0, -1)).normalize(), (new Vector(-1, 0, 1)).normalize(), false, false),
    UP(new Vector(0, 1, 0), new Vector(0, 0, 1), true, true),
    DOWN(new Vector(0, -1, 0), new Vector(0, 0, 1), true, true);

    private final Vector dir;
    private final Vector leftDir;
    @Getter
    private final boolean orthogonal;
    @Getter
    private final boolean upright;

    public Vector vector() {
        return dir;
    }

    @Deprecated
    public Vector leftVector() {
        return leftDir;
    }

}
