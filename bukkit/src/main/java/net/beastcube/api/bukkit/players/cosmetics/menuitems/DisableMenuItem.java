package net.beastcube.api.bukkit.players.cosmetics.menuitems;

import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticType;
import net.beastcube.api.bukkit.util.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * author BeastCube
 */
public class DisableMenuItem extends MenuItem {
    private final CosmeticType type;
    private final ItemStack itemstack;

    public DisableMenuItem(CosmeticType type) {
        super("", new ItemStack(Material.BARRIER));
        this.type = type;
        this.itemstack = new ItemBuilder(Material.BARRIER).name(type.getDisableTitle()).build();
    }

    @Override
    public void onItemClick(ItemClickEvent e) {
        Player p = e.getPlayer();
        type.getManager().removeCosmetic(p);
        e.setAction(ItemClickEvent.ItemClickAction.UPDATE);
    }

    @Override
    public ItemStack getFinalIcon(Player player, ItemMenu menu) {
        return itemstack;
    }
}
