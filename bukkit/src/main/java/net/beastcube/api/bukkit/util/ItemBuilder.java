package net.beastcube.api.bukkit.util;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This is a chainable builder for {@link ItemStack}s in {@link Bukkit}
 * <br>
 * Example Usage:<br>
 * {@code ItemStack is = new ItemBuilder(Material.LEATHER_HELMET).amount(2).data(4).durability(4).enchantment(Enchantment.ARROW_INFINITE).enchantment(Enchantment.LUCK, 2).name(ChatColor.RED + "the name").lore(ChatColor.GREEN + "line 1").lore(ChatColor.BLUE + "line 2").color(Color.MAROON).build();
 *
 * @author MiniDigger, BeastCube
 * @version 1.2
 */
public class ItemBuilder implements Listener {


    private final ItemStack is;

    /**
     * Inits the builder with the given {@link Material}
     *
     * @param mat the {@link Material} to start the builder from
     * @since 1.0
     */
    public ItemBuilder(final Material mat) {
        is = new ItemStack(mat);
    }

    /**
     * Inits the builder with the given {@link ItemStack}
     *
     * @param is the {@link ItemStack} to start the builder from
     * @since 1.0
     */
    public ItemBuilder(final ItemStack is) {
        this.is = is;
    }

    /**
     * Changes the amount of the {@link ItemStack}
     *
     * @param amount the new amount to set
     * @return this builder for chaining
     * @since 1.0
     */
    public ItemBuilder amount(final int amount) {
        is.setAmount(amount);
        return this;
    }

    /**
     * Changes the display name of the {@link ItemStack}
     *
     * @param name the new display name to set
     * @return this builder for chaining
     * @since 1.0
     */
    public ItemBuilder name(final String name) {
        final ItemMeta meta = is.getItemMeta();
        meta.setDisplayName(name);
        is.setItemMeta(meta);
        return this;
    }

    /**
     * Adds a new line to the lore of the {@link ItemStack}
     *
     * @param name the new line to add
     * @return this builder for chaining
     * @since 1.0
     */
    public ItemBuilder lore(final String name) {
        final ItemMeta meta = is.getItemMeta();
        List<String> lore = meta.getLore();
        if (lore == null) {
            lore = new ArrayList<>();
        }
        lore.add(name);
        meta.setLore(lore);
        is.setItemMeta(meta);
        return this;
    }

    /**
     * Changes the durability of the {@link ItemStack}
     *
     * @param durability the new durability to set
     * @return this builder for chaining
     * @since 1.0
     */
    public ItemBuilder durability(final int durability) {
        is.setDurability((short) durability);
        return this;
    }

    /**
     * Changes the durability of the {@link ItemStack} to the highest value possible
     *
     * @return this builder for chaining
     * @since 1.1
     */
    public ItemBuilder infinityDurability() {
        is.setDurability(Short.MAX_VALUE);
        return this;
    }

    /**
     * Changes the data of the {@link ItemStack}
     * <p>
     * Use {@link #durability(int durability)} instead
     *
     * @param data the new data to set
     * @return this builder for chaining
     * @since 1.0
     */
    @SuppressWarnings("deprecation")
    @Deprecated
    public ItemBuilder data(final int data) {
        is.setDurability((short) data);
        return this;
    }

    /**
     * Adds an {@link Enchantment} with the given level to the {@link ItemStack}
     *
     * @param enchantment the enchantment to add
     * @param level       the level of the enchantment
     * @return this builder for chaining
     * @since 1.0
     */
    public ItemBuilder enchantment(final Enchantment enchantment, final int level) {
        is.addUnsafeEnchantment(enchantment, level);
        return this;
    }

    /**
     * Adds an {@link Enchantment} with the level 1 to the {@link ItemStack}
     *
     * @param enchantment the enchantment to add
     * @return this builder for chaining
     * @since 1.0
     */
    public ItemBuilder enchantment(final Enchantment enchantment) {
        is.addUnsafeEnchantment(enchantment, 1);
        return this;
    }

    /**
     * Changes the {@link Material} of the {@link ItemStack}
     *
     * @param material the new material to set
     * @return this builder for chaining
     * @since 1.0
     */
    public ItemBuilder type(final Material material) {
        is.setType(material);
        return this;
    }

    /**
     * Clears the lore of the {@link ItemStack}
     *
     * @return this builder for chaining
     * @since 1.0
     */
    public ItemBuilder clearLore() {
        final ItemMeta meta = is.getItemMeta();
        meta.setLore(new ArrayList<>());
        is.setItemMeta(meta);
        return this;
    }

    /**
     * Clears the list of {@link Enchantment}s of the {@link ItemStack}
     *
     * @return this builder for chaining
     * @since 1.0
     */
    public ItemBuilder clearEnchantments() {
        is.getEnchantments().keySet().forEach(is::removeEnchantment);
        return this;
    }

    /**
     * Sets the {@link Color} of a part of leather armor
     *
     * @param color the {@link Color} to use
     * @return this builder for chaining
     * @since 1.1
     */
    public ItemBuilder color(Color color) {
        if (is.getType() == Material.LEATHER_BOOTS || is.getType() == Material.LEATHER_CHESTPLATE || is.getType() == Material.LEATHER_HELMET || is.getType() == Material.LEATHER_LEGGINGS) {
            LeatherArmorMeta meta = (LeatherArmorMeta) is.getItemMeta();
            meta.setColor(color);
            is.setItemMeta(meta);
            return this;
        } else {
            throw new IllegalArgumentException("color() only applicable for leather armor!");
        }
    }

    /**
     * Sets the {@link DyeColor} for a wool / stained clay / stained glass / stained glass pane
     *
     * @param color the {@link DyeColor} to use
     * @return this builder for chaining
     * @since 1.1
     */
    @SuppressWarnings("deprecation")
    public ItemBuilder dyeColor(DyeColor color) {
        List<Material> materials = Arrays.asList(
                Material.WOOL,
                Material.STAINED_CLAY,
                Material.STAINED_GLASS,
                Material.STAINED_GLASS_PANE);

        if (materials.contains(is.getType())) {
            is.setDurability(color.getData());
            return this;
        } else {
            throw new IllegalArgumentException("dyeColor() only applicable for wool!");
        }
    }

    /**
     * Sets the skullowner for a wool
     *
     * @param owner the owner of the skull
     * @return this builder for chaining
     * @since 1.1
     */
    public ItemBuilder skullOwner(String owner) {
        if (is.getType() == Material.SKULL || is.getType() == Material.SKULL_ITEM) {
            SkullMeta im = (SkullMeta) is.getItemMeta();
            im.setOwner(owner);
            is.setItemMeta(im);
            return this;
        } else {
            throw new IllegalArgumentException("skullOwner() only applicable for skulls!");
        }
    }

    public ItemBuilder addFlag(ItemFlag itemFlag) {
        ItemMeta meta = is.getItemMeta();
        meta.addItemFlags(itemFlag);
        is.setItemMeta(meta);
        return this;
    }

    public ItemBuilder removeFlag(ItemFlag itemFlag) {
        ItemMeta meta = is.getItemMeta();
        meta.removeItemFlags(itemFlag);
        is.setItemMeta(meta);
        return this;
    }

    public ItemBuilder addAllFlags() {
        for (ItemFlag flag : ItemFlag.values()) {
            addFlag(flag);
        }
        return this;
    }

    public ItemBuilder removeAllFlags() {
        for (ItemFlag flag : ItemFlag.values()) {
            removeFlag(flag);
        }
        return this;
    }

    /**
     * Builds the {@link ItemStack}
     *
     * @return the created {@link ItemStack}
     * @since 1.0
     */
    public ItemStack build() {
        return is;
    }

}