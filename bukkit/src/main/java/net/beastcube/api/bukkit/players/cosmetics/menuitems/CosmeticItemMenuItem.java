package net.beastcube.api.bukkit.players.cosmetics.menuitems;

import net.beastcube.api.bukkit.BeastCubeSlaveBukkitPlugin;
import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.BackItem;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.menus.items.SubMenuItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.api.bukkit.players.cosmetics.CosmeticItem;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * @author BeastCube
 */
public class CosmeticItemMenuItem extends SubMenuItem {
    private final CosmeticItem cosmetic;
    private final ItemMenu parent;

    public CosmeticItemMenuItem(ItemMenu parent, CosmeticItem cosmetic) {
        super(BeastCubeSlaveBukkitPlugin.getInstance(), "", null, parent, "");
        this.parent = parent;
        this.cosmetic = cosmetic;
    }

    @Override
    public void onItemClick(ItemClickEvent e) {
        Player p = e.getPlayer();
        if (!cosmetic.apply(p)) {
            ItemMenu menu = new ItemMenu(ChatColor.GREEN + "Wirklich kaufen?", ItemMenu.Size.TWO_LINE, BeastCubeSlaveBukkitPlugin.getInstance(), parent);
            List<String> lore = cosmetic.getLore();
            menu.setItem(4, new MenuItem(cosmetic.getDisplayName(), cosmetic.getIcon(), lore.toArray(new String[lore.size()])));
            menu.setItem(9, new BackItem(ChatColor.RED + "Abbrechen", new ItemStack(Material.HARD_CLAY)));
            menu.setItem(17, new MenuItem(ChatColor.GREEN + "Kaufen", new ItemStack(Material.EMERALD_BLOCK)) {
                @Override
                public void onItemClick(ItemClickEvent event) {
                    if (cosmetic.buy(p)) {
                        p.sendMessage(ChatColor.GREEN + "Du hast das Item " + ChatColor.RESET + cosmetic.getDisplayName() + " für " + cosmetic.getPrice() + " Coins gekauft!");
                        event.setAction(ItemClickEvent.ItemClickAction.GOBACK);
                    } else {
                        p.sendMessage(ChatColor.RED + "Du hast nicht genug Coins um dieses Item zu kaufen!");
                    }
                    //TODO Reset all items (add activated to the current item)
                    e.setAction(ItemClickEvent.ItemClickAction.UPDATE);
                }
            });

            menu.open(p);
        } else {
            parent.update(p);
        }
    }

    @Override
    public ItemStack getFinalIcon(Player p, ItemMenu menu) {
        return cosmetic.getIcon(p);
    }

}
