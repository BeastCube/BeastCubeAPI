package net.beastcube.api.bukkit.players.cosmetics;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.SkullType;

/**
 * @author BeastCube
 */
@AllArgsConstructor
public enum CosmeticType {
    EFFECT("effect", "Effekt", "Effekte", Material.BLAZE_POWDER, PersonalChest.effectManager),
    MORPH("morph", "Morph", "Morphs", Material.EGG, PersonalChest.morphManager),
    PET("pet", "Pet", "Pets", Material.MONSTER_EGG, PersonalChest.petManager),
    GADGET("gadget", "Gadget", "Gadgets", Material.TNT, PersonalChest.gadgetManager),
    HEAD("head", "Kopf", "Köpfe", Material.SKULL_ITEM, (short) SkullType.PLAYER.ordinal(), PersonalChest.headManager);

    @Getter
    private final String id;
    @Getter
    private final String name;
    @Getter
    private final String plural;
    @Getter
    private final Material icon;
    @Getter
    private final short data;
    @Getter
    private final CosmeticManager manager;

    CosmeticType(String id, String name, String plural, Material icon, CosmeticManager list) {
        this(id, name, plural, icon, (short) 0, list);
    }

    public String getDisplayName() {
        return ChatColor.YELLOW + name;
    }

    public String getPluralDisplayName() {
        return ChatColor.YELLOW + plural;
    }

    public String getDisableTitle() {
        return ChatColor.RED + name + " deaktivieren";
    }


}
