package net.beastcube.api.bukkit.bukkit.rollback;

import net.beastcube.api.bukkit.bukkit.BeastCube;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Block rollbacker.
 *
 * @author Mato Kormuth
 */
public class BlockRollbacker {
    //List of changes.
    private final List<BlockChange> changes = new ArrayList<>();
    //List of location changes.
    private final List<Location> changeLocations = new ArrayList<>();
    //Bukkit task id.
    private int taskId = 0;
    //Runnable to be run after async rollback.
    private Runnable onFinished;
    //Max block changes per one game tick.
    private int maxChangesPerTick = 128;

    /**
     * Registers a change to this rollbacker.
     *
     * @param change the BlockChange to register
     */
    public void addChange(final BlockChange change) {
        if (!this.changeLocations.contains(change.getLocation())) {
            this.changes.add(change);
            this.changeLocations.add(change.getLocation());
        }
    }

    /**
     * Reverts all registered changes right after call of this function. (Not recomended)
     */
    public void rollback() {
        for (Iterator<BlockChange> iterator = this.changes.iterator(); iterator.hasNext(); ) {
            BlockChange change = iterator.next();
            change.applyRollback();
            this.changeLocations.remove(change.getLocation());
            iterator.remove();
        }
    }

    /**
     * Starts a task for rollbacking with default (128) amount of blocks reverted in one tick.
     *
     * @param onFinished runnable, that should be called after the rollback is done.
     */
    public void rollbackAsync(final Runnable onFinished) {
        this.onFinished = onFinished;
        this.taskId = BeastCube.getScheduler().scheduleSyncRepeatingTask(BlockRollbacker.this::doRollback, 0L, 1L);
    }

    /**
     * Starts a task for rollbacking with specified amount of blocks reverted in one tick.
     *
     * @param onFinished runnable, that should be called after the rollback is done.
     */
    public void rollbackAsync(final Runnable onFinished, final int maxIterations) {
        this.maxChangesPerTick = maxIterations;
        this.rollbackAsync(onFinished);
    }

    /**
     * Performs one rollback (rollbacks this.maxChangesPerTick blocks).
     */
    private void doRollback() {
        if (this.changes.size() == 0) {
            BeastCube.getScheduler().cancelTask(this.taskId);
            if (this.onFinished != null)
                this.onFinished.run();
        } else {
            int count = 0;
            for (Iterator<BlockChange> iterator = this.changes.iterator(); iterator.hasNext(); ) {
                if (count > this.maxChangesPerTick)
                    break;
                else {
                    BlockChange bc = iterator.next();
                    this.changeLocations.remove(bc.getLocation());
                    bc.applyRollback();
                    iterator.remove();
                }
                count++;
            }
        }
    }
}
