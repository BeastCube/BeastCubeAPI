package net.beastcube.api.bukkit.players.cosmetics.effects.effects;

import de.slikey.effectlib.effect.MusicEffect;
import lombok.Getter;
import net.beastcube.api.bukkit.players.cosmetics.effects.Effect;
import net.beastcube.api.bukkit.players.cosmetics.effects.EffectManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * @author BeastCube
 */
public class Music extends Effect {
    @Getter
    private String id = "music";

    @Override
    public String getDisplayName() {
        return EffectManager.DISPLAY_NAME_COLOR + "Music";
    }

    @Override
    public Material getMaterial() {
        return Material.GREEN_RECORD;
    }

    @Override
    public de.slikey.effectlib.Effect getEffect(Player p, de.slikey.effectlib.EffectManager e) {
        return new MusicEffect(e);
    }
}
