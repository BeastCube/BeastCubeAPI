package net.beastcube.api.bukkit.menus.items;

import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.commons.packets.DispatchCmdPacket;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class BungeeCommandItem extends MenuItem {

    private final ItemStack icon;
    private final String displayName;
    private final String command;

    public BungeeCommandItem(String displayName, ItemStack icon, String command) {
        super(ChatColor.YELLOW + displayName, new ItemStack(Material.SKULL_ITEM));
        this.displayName = displayName;
        this.icon = icon;
        this.command = command;
    }

    @Override
    public void onItemClick(ItemClickEvent event) {
        BeastCubeAPI.getGateway().sendPacket(event.getPlayer(), new DispatchCmdPacket(command));
        event.setAction(ItemClickEvent.ItemClickAction.CLOSE);
    }

}
