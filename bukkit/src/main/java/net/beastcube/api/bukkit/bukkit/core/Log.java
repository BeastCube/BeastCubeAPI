package net.beastcube.api.bukkit.bukkit.core;

import net.beastcube.api.commons.Providers;
import net.beastcube.api.commons.chat.ChatChannel;
import net.beastcube.api.commons.text.ChatColor;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Logger for BeastCube.
 *
 * @author Mato Kormuth
 */
public class Log {
    /**
     * List of problems, that should admins able to view.
     */
    private final static List<String> problems = new ArrayList<>();

    /**
     * Internal logger.
     */
    private final static Logger log = Providers.LOGGER;

    /**
     * Logs 'info' message.
     *
     * @param msg message to log
     */
    public static void info(final String msg) {
        Log.log.info(msg);
        ChatChannel.CHANNEL_LOG.broadcastMessage(ChatColor.GRAY + "[INFO] " + msg);
    }

    /**
     * Logs 'warn' message.
     *
     * @param msg message to log
     */
    public static void warn(final String msg) {
        Log.log.warning(msg);
        ChatChannel.CHANNEL_LOG.broadcastMessage(ChatColor.GOLD + "[WARN] " + msg);
    }

    /**
     * Logs 'severe' message.
     *
     * @param msg message to log
     */
    public static void severe(final String msg) {
        Log.log.severe(msg);
        ChatChannel.CHANNEL_LOG.broadcastMessage(ChatColor.RED + "[ERROR] " + msg);
    }

    /**
     * Logs 'partEnable' message.
     *
     * @param partName message to log
     */
    public static void partEnable(final String partName) {
        Log.log.info("Enabling BeastCube-" + partName + "...");
        ChatChannel.CHANNEL_LOG.broadcastMessage(ChatColor.GRAY + "[PARTENABLE] "
                + partName);
    }

    /**
     * Logs 'parnDisable' message.
     *
     * @param partName message to log
     */
    public static void partDisable(final String partName) {
        Log.log.info("Disabling BeastCube-" + partName + "...");
        ChatChannel.CHANNEL_LOG.broadcastMessage(ChatColor.GRAY + "[PARTDISABLE] "
                + partName);
    }

    /**
     * Logs 'gameEnable' message.
     *
     * @param gameName message to log
     */
    public static void gameEnable(final String gameName) {
        Log.log.info("Enabling Minigame-" + gameName + "...");
    }

    /**
     * Logs 'gameDisable' message.
     *
     * @param gameName message to log
     */
    public static void gameDisable(final String gameName) {
        Log.log.info("Disabling Minigame-" + gameName + "...");
    }

    /**
     * Adds problem to list. Problem is added to log and reported to all OP on server.
     *
     * @param message message
     */
    public static void addProblem(final String message) {
        Log.problems.add(message);
    }

    /**
     * Returns list of all problems.
     *
     * @return list of problems
     */
    protected static List<String> getProblems() {
        return Log.problems;
    }

    public static void chat(final String msg) {
        Log.log.info("[CHAT] " + msg);
    }
}
