package net.beastcube.api.bukkit.fakemob;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedSignedProperty;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.BeastCubeSlaveBukkitPlugin;
import net.beastcube.api.bukkit.fakemob.adjuster.MyWorldAccess;
import net.beastcube.api.bukkit.fakemob.command.FakeMobCommand;
import net.beastcube.api.bukkit.fakemob.event.RemoveFakeMobEvent;
import net.beastcube.api.bukkit.fakemob.event.SpawnFakeMobEvent;
import net.beastcube.api.bukkit.fakemob.listener.InteractListener;
import net.beastcube.api.bukkit.fakemob.listener.MobListener;
import net.beastcube.api.bukkit.fakemob.listener.ProtocolListener;
import net.beastcube.api.bukkit.fakemob.util.*;
import net.beastcube.api.bukkit.util.ReflectionUtils;
import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class FakeMobsPlugin implements Listener {
    private static BeastCubeAPI instance;
    private static final Map<Integer, FakeMob> mobs = new HashMap<>();
    public static Logger log;
    private static SkinQueue skinQueue;
    private JavaPlugin plugin = BeastCubeSlaveBukkitPlugin.getInstance();

    @SuppressWarnings("deprecation")
    public void onEnable(BeastCubeAPI instance) {
        log = plugin.getLogger();
        FakeMobsPlugin.instance = instance;
        instance.setProtocolManager(ProtocolLibrary.getProtocolManager());

        mobs.clear();
        Bukkit.getWorlds().forEach(FakeMobsPlugin::loadMobs);

        skinQueue = new SkinQueue();
        skinQueue.start();

        if (!Config.configFile.exists()) Config.save();
        Config.load();

        Bukkit.getPluginManager().registerEvents(new InteractListener(), plugin);
        Bukkit.getPluginManager().registerEvents(new MobListener(this), plugin);
        plugin.getCommand("FakeMob").setExecutor(new FakeMobCommand());

        Bukkit.getScheduler().scheduleAsyncRepeatingTask(plugin, new LookUpdate(this), 5L, 5L);
        instance.setProtocolListener(new ProtocolListener(this));
        instance.getProtocolManager().addPacketListener(instance.getProtocolListener());

        for (World world : Bukkit.getWorlds()) {
            MyWorldAccess.registerWorldAccess(world);
        }
    }

    public void onDisable() {
        instance.getProtocolManager().removePacketListener(instance.getProtocolListener());
        Bukkit.getScheduler().cancelTasks(plugin);
        for (FakeMob mob : getMobs())
            Bukkit.getOnlinePlayers().stream().filter(player -> mob.getWorld() == player.getWorld()).forEach(mob::sendDestroyPacket);

        Bukkit.getWorlds().forEach(MyWorldAccess::unregisterWorldAccess);
    }

    public static SkinQueue getSkinQueue() {
        return skinQueue;
    }

    public static boolean existsMob(int id) {
        return mobs.containsKey(id);
    }

    public static FakeMob getMob(Location loc) {
        for (FakeMob mob : getMobs()) {
            if (mob.getLocation().getWorld() == loc.getWorld() &&
                    mob.getLocation().getBlockX() == loc.getBlockX() &&
                    mob.getLocation().getBlockY() == loc.getBlockY() &&
                    mob.getLocation().getBlockZ() == loc.getBlockZ())
                return mob;
        }
        return null;
    }

    public static boolean isMobOnLocation(Location loc) {
        return (getMob(loc) != null);
    }

    public static FakeMob getMob(int id) {
        return mobs.get(id);
    }

    public static void removeMob(int id) {
        FakeMob mob = mobs.get(id);
        if (mob == null) return;

        RemoveFakeMobEvent event = new RemoveFakeMobEvent(mob);
        Bukkit.getPluginManager().callEvent(event);

        mob.getWorld().getPlayers().forEach(mob::unloadPlayer);

        Map<Player, FakeMob> selectedMap = new HashMap<>();
        selectedMap.putAll(Cache.selectedMobs);
        selectedMap.entrySet().stream().filter(e -> e.getValue() == mob).forEach(e -> Cache.selectedMobs.remove(e.getKey()));

        mobs.remove(id);
        saveMobs();
    }

    public static FakeMob spawnMob(Location loc, EntityType type) {
        return spawnMob(loc, type, "");
    }

    public static FakeMob spawnMob(Location loc, EntityType type, String customName) {
        if (!type.isAlive()) return null;

        int id = getNewId();
        FakeMob mob = new FakeMob(id, loc, type);
        mob.setCustomName(customName);

        SpawnFakeMobEvent event = new SpawnFakeMobEvent(loc, type, mob);
        Bukkit.getPluginManager().callEvent(event);
        if (event.isCancelled()) return null;

        loc.getWorld().getPlayers().stream().filter(mob::isInRange).forEach(mob::loadPlayer);

        mobs.put(id, mob);
        saveMobs();
        return mob;
    }

    public FakeMob spawnPlayer(Location loc, String name) {
        return this.spawnPlayer(loc, name, (Multimap<String, WrappedSignedProperty>) null);
    }

    public FakeMob spawnPlayer(Location loc, String name, Player skin) {
        return this.spawnPlayer(loc, name, WrappedGameProfile.fromPlayer(skin).getProperties());
    }

    public FakeMob spawnPlayer(Location loc, String name, Multimap<String, WrappedSignedProperty> skin) {
        int id = getNewId();
        FakeMob mob = new FakeMob(id, loc, EntityType.PLAYER);
        mob.setCustomName(name);
        mob.setPlayerSkin(skin);

        SpawnFakeMobEvent event = new SpawnFakeMobEvent(loc, EntityType.PLAYER, mob);
        Bukkit.getPluginManager().callEvent(event);
        if (event.isCancelled()) return null;

        loc.getWorld().getPlayers().stream().filter(mob::isInRange).forEach(mob::loadPlayer);

        mobs.put(id, mob);
        saveMobs();
        return mob;
    }

    public static int getNewId() {
        int id = -1;
        for (FakeMob mob : getMobs())
            if (mob.getId() > id)
                id = mob.getId();
        return id + 1;
    }

    public static List<FakeMob> getMobs() {
        List<FakeMob> mobList = new ArrayList<>();
        mobList.addAll(mobs.values());
        return mobList;
    }

    public static Map<Integer, FakeMob> getMobsMap() {
        return mobs;
    }

    /**
     * Called every chunk move
     */
    public static void updatePlayerView(Player player) {
        for (FakeMob mob : getMobs()) {
            if (mob.isInRange(player)) {
                mob.loadPlayer(player);
            } else {
                mob.unloadPlayer(player);
            }
        }
    }

    public static List<FakeMob> getMobsInRadius(Location loc, int radius) {
        return getMobs().stream().filter(mob -> mob.getWorld() == loc.getWorld() && mob.getLocation().distance(loc) <= radius).collect(Collectors.toList());
    }

    public static List<FakeMob> getMobsInChunk(World world, int chunkX, int chunkZ) {
        List<FakeMob> mobList = new ArrayList<>();

        for (FakeMob mob : getMobs()) {
            Chunk chunk = mob.getLocation().getChunk();
            if (mob.getWorld() == world && chunk.getX() == chunkX && chunk.getZ() == chunkZ) {
                mobList.add(mob);
            }
        }

        return mobList;
    }

    public static void loadMobs(World world) {
        int beforeSize = mobs.size();
        YamlConfiguration config = YamlConfiguration.loadConfiguration(new File(world.getWorldFolder() + "/mobs.yml"));

        for (String key : config.getKeys(false)) {
            ConfigurationSection section = config.getConfigurationSection(key);
            int id = Integer.parseInt(key);
            Location loc = new Location(world,
                    section.getDouble("X"),
                    section.getDouble("Y"),
                    section.getDouble("Z"),
                    Float.parseFloat(section.getString("Yaw")),
                    Float.parseFloat(section.getString("Pitch")));
            EntityType type = EntityType.valueOf(section.getString("Type").toUpperCase());
            FakeMob mob = new FakeMob(id, loc, type);
            if (section.isSet("Name") && section.getString("Name").length() <= 16)
                mob.setCustomName(section.getString("Name"));
            mob.setSitting(section.getBoolean("Sitting"));
            if (section.contains("Invisibility"))
                mob.setInvisibility(section.getBoolean("Invisibility"));
            mob.setPlayerLook(section.getBoolean("PlayerLook"));
            String interactAction = section.getString("InteractAction");
            mob.setInteractAction(interactAction == null ? "" : interactAction);
            String mobData = section.getString("MobData");
            mob.setMobData(mobData == null ? "" : mobData);
            if (section.contains("Inventory")) {
                MobInventory inv = new MobInventory();
                ConfigurationSection invSection = section.getConfigurationSection("Inventory");
                if (invSection.contains("ItemInHand"))
                    inv.setItemInHand(invSection.getItemStack("ItemInHand"));
                if (invSection.contains("Boots"))
                    inv.setBoots(invSection.getItemStack("Boots"));
                if (invSection.contains("Leggings"))
                    inv.setLeggings(invSection.getItemStack("Leggings"));
                if (invSection.contains("ChestPlate"))
                    inv.setChestPlate(invSection.getItemStack("ChestPlate"));
                if (invSection.contains("Helmet"))
                    inv.setHelmet(invSection.getItemStack("Helmet"));

                mob.setInventory(inv);
            }

            if (mob.getType() == EntityType.PLAYER && section.contains("Skin")) {
                ConfigurationSection skinsSection = section.getConfigurationSection("Skin");
                Multimap<String, WrappedSignedProperty> skins = ArrayListMultimap.create();

                for (String k : skinsSection.getKeys(false)) {
                    ConfigurationSection skinSection = skinsSection.getConfigurationSection(k);
                    WrappedSignedProperty property = new WrappedSignedProperty(skinSection.getString("Name"), skinSection.getString("Value"), skinSection.getString("Signature"));
                    skins.put(property.getName(), property);
                }

                mob.setPlayerSkin(skins);
            }

            mobs.put(id, mob);
        }
        log.info("Loaded " + (mobs.size() - beforeSize) + " mobs for world \"" + world.getName() + "\"!");
        Bukkit.getOnlinePlayers().forEach(FakeMobsPlugin::updatePlayerView);
    }

    public static void saveMobs() {
        for (World world : Bukkit.getWorlds()) {
            YamlConfiguration config = new YamlConfiguration();
            for (FakeMob mob : getMobs()) {
                if (mob.getWorld() == world) {
                    ConfigurationSection section = config.createSection(String.valueOf(mob.getId()));
                    section.set("X", mob.getLocation().getX());
                    section.set("Y", mob.getLocation().getY());
                    section.set("Z", mob.getLocation().getZ());
                    section.set("Yaw", mob.getLocation().getYaw());
                    section.set("Pitch", mob.getLocation().getPitch());
                    section.set("Type", mob.getType().name());
                    if (mob.getCustomName() != null)
                        section.set("Name", mob.getCustomName());
                    section.set("Sitting", mob.isSitting());
                    section.set("Invisibility", mob.isInvisibility());
                    section.set("PlayerLook", mob.isPlayerLook());
                    section.set("InteractAction", mob.getInteractAction());
                    section.set("MobData", mob.getMobData());
                    if (!mob.getInventory().isEmpty()) {
                        ConfigurationSection invSection = section.createSection("Inventory");
                        if (mob.getInventory().getItemInHand() != null && mob.getInventory().getItemInHand().getType() != Material.AIR)
                            invSection.set("ItemInHand", mob.getInventory().getItemInHand());
                        if (mob.getInventory().getBoots() != null && mob.getInventory().getBoots().getType() != Material.AIR)
                            invSection.set("Boots", mob.getInventory().getBoots());
                        if (mob.getInventory().getLeggings() != null && mob.getInventory().getLeggings().getType() != Material.AIR)
                            invSection.set("Leggings", mob.getInventory().getLeggings());
                        if (mob.getInventory().getChestPlate() != null && mob.getInventory().getChestPlate().getType() != Material.AIR)
                            invSection.set("ChestPlate", mob.getInventory().getChestPlate());
                        if (mob.getInventory().getHelmet() != null && mob.getInventory().getHelmet().getType() != Material.AIR)
                            invSection.set("Helmet", mob.getInventory().getHelmet());
                    }

                    if (mob.getType() == EntityType.PLAYER && mob.getPlayerSkin() != null && !mob.getPlayerSkin().isEmpty()) {
                        ConfigurationSection skinsSection = section.createSection("Skin");
                        int i = 0;

                        for (WrappedSignedProperty property : mob.getPlayerSkin().values()) {
                            ConfigurationSection skinSection = skinsSection.createSection("Property-" + String.valueOf(i));
                            skinSection.set("Name", property.getName());
                            skinSection.set("Value", property.getValue());
                            skinSection.set("Signature", property.getSignature());
                            i++;
                        }
                    }

                }
            }

            try {
                config.save(new File(world.getWorldFolder() + "/mobs.yml"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @EventHandler
    public void onWorldLoad(WorldLoadEvent e) {
        loadMobs(e.getWorld());
    }


    public static void adjustEntityCount() {
        try {
            Class entityClass = Class.forName(ReflectionUtils.PackageType.MINECRAFT_SERVER + ".Entity");

            Field field = entityClass.getDeclaredField("entityCount");
            field.setAccessible(true);
            int currentCount = field.getInt(null);

            if (currentCount >= 2300) {
                while (existsMob(currentCount - 2300)) {
                    currentCount++;
                }

                field.set(null, currentCount);
            }
        } catch (Exception ex) {
            log.log(Level.WARNING, "Can't adjust entity count", ex);
        }
    }

}
