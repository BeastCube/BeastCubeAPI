package net.beastcube.api.bukkit.util;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public final class InventoryWorkaround {

    /**
     * This will abort if it couldn't store all items
     *
     * @return what it couldn't store
     */
    public static Map<Integer, ItemStack> addAllItems(final Inventory inventory, final ItemStack... items) {
        final Inventory fakeInventory = Bukkit.getServer().createInventory(null, inventory.getType());
        fakeInventory.setContents(inventory.getContents());
        Map<Integer, ItemStack> overFlow = fakeInventory.addItem(items);
        if (overFlow.isEmpty()) {
            inventory.addItem(items);
            return new HashMap<>();
        }
        return fakeInventory.addItem(items);
    }

}
