package net.beastcube.api.bukkit.util.serialization.adapters;

import com.google.gson.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.lang.reflect.Type;

/**
 * @author BeastCube
 */
public class LocationSerializer implements JsonDeserializer<Location>, JsonSerializer<Location> {

    @Override
    public Location deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject itemObject = jsonElement.getAsJsonObject();
        String world = itemObject.get("world").getAsString();
        double x = itemObject.get("x").getAsDouble();
        double y = itemObject.get("y").getAsDouble();
        double z = itemObject.get("z").getAsDouble();
        float yaw = itemObject.get("yaw").getAsFloat();
        float pitch = itemObject.get("pitch").getAsFloat();
        return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
    }

    @Override
    public JsonElement serialize(Location location, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject root = new JsonObject();
        if (location != null) {
            root.addProperty("world", location.getWorld().getName());
            root.addProperty("x", location.getX());
            root.addProperty("y", location.getY());
            root.addProperty("z", location.getZ());
            root.addProperty("yaw", location.getYaw());
            root.addProperty("pitch", location.getPitch());
        }
        return root;
    }
}
