package net.beastcube.api.bukkit.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.beastcube.api.network.ServerInfo;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * @author BeastCube
 */
@RequiredArgsConstructor
public class SlaveConnectedEvent extends Event {
    private static HandlerList handlers = new HandlerList();
    @Getter
    private final ServerInfo server;

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
