package net.beastcube.api.bukkit.bukkit.cinematics;

/**
 * V3 Meta typ.
 *
 * @author Mato Kormuth
 */
public enum V3MetaType {
    V3MetaEntityDamage,
    V3MetaEntityInventory,
    V3MetaEntityRemove,
    V3MetaEntitySpawn,
    V3MetaEntityTeleport,
    V3MetaEntityVelocity,
    V3MetaFallingSand,
    V3MetaParticleEffect,
    V3MetaSoundEffect,
    V3MetaExplosion,
    V3MetaEntityMove
}
