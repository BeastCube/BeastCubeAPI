package net.beastcube.api.bukkit.chat.listeners;

import mkremins.fanciful.FancyMessage;
import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.commons.permissions.Permissions;
import net.beastcube.api.commons.permissions.Role;
import net.beastcube.api.commons.permissions.Roles;
import net.beastcube.api.commons.util.Constants;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * @author BeastCube
 */
public class ChatListener implements Listener {

    public static String clickProfile = "Klicke um sein Profil zu sehen";

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e) {
        if (!e.isCancelled()) {
            if (BeastCubeAPI.isChatNameEnabled()) {
                Role role = Roles.getRole(e.getPlayer().getUniqueId());
                if (role.hasPermission(Permissions.CHAT)) {
                    FancyMessage message = new FancyMessage("");
                    if (role != Roles.DEFAULT) {
                        message.then(role.getName()).color(ChatColor.getByChar(role.getColor().getChar())).then(" ");
                    }
                    message.then(e.getPlayer().getName()).color(ChatColor.GRAY)
                            .tooltip(clickProfile).command("/friends show " + e.getPlayer().getName())
                            .then(" » ").color(ChatColor.DARK_GRAY).then(e.getMessage())
                            .send(Bukkit.getOnlinePlayers());
                    System.out.println(e.getPlayer().getName() + " » " + e.getMessage());
                    e.setCancelled(true);
                } else {
                    e.getPlayer().sendMessage(Constants.NOPERMISSION);
                }
            }
        }
    }
}
