package net.beastcube.api.bukkit.players.cosmetics.gadgets.gadgets;

import lombok.Getter;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.Gadget;
import net.beastcube.api.bukkit.players.cosmetics.gadgets.GadgetManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;

/**
 * @author BeastCube
 */
public class TntThrower extends Gadget {
    @Getter
    private String id = "tntthrower";
    @Getter
    private String displayName = GadgetManager.DISPLAY_NAME_COLOR + "TNT";
    @Getter
    private Material material = Material.TNT;
    @Getter
    private String description = ChatColor.GRAY + "Wirf mit TNT um dich";

    @Override
    public boolean hasLeftClickAction() {
        return false;
    }

    @Override
    public boolean hasRightClickAction() {
        return true;
    }

    @Override
    public boolean LeftClick(Player p) {
        return false;
    }

    @Override
    public boolean RightClick(Player p) {
        TNTPrimed tnt = p.getWorld().spawn(p.getEyeLocation(), TNTPrimed.class);
        tnt.setVelocity(p.getEyeLocation().getDirection().multiply(0.7D));
        tnt.setYield(15);
        return true;
    }

}
