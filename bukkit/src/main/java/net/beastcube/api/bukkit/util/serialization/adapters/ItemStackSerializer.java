package net.beastcube.api.bukkit.util.serialization.adapters;

import com.google.gson.*;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author BeastCube
 */
public class ItemStackSerializer implements JsonDeserializer<ItemStack>, JsonSerializer<ItemStack> {

    @SuppressWarnings("deprecation")
    public JsonElement encode(ItemStack item) {
        JsonObject root = new JsonObject();
        if (item != null) {
            JsonObject itemObject = new JsonObject();
            itemObject.addProperty("type", item.getType().toString());
            itemObject.addProperty("amount", item.getAmount());
            itemObject.addProperty("durability", item.getDurability());
            itemObject.addProperty("data", item.getData().getData());
            if (item.hasItemMeta()) {
                if (item.getItemMeta().hasDisplayName()) {
                    itemObject.addProperty("name", item.getItemMeta().getDisplayName());
                }
                if (item.getItemMeta().hasLore()) {
                    List<String> lore = item.getItemMeta().getLore();
                    JsonArray loreArray = new JsonArray();
                    for (String aLore : lore) {
                        JsonObject lineObject = new JsonObject();
                        lineObject.addProperty("line", aLore);
                        loreArray.add(lineObject);
                    }
                    itemObject.add("lore", loreArray);
                }
                if (item.getItemMeta().hasEnchants()) {
                    JsonArray enchArray = new JsonArray();
                    for (Enchantment ench : item.getItemMeta().getEnchants().keySet()) {
                        JsonObject enchObject = new JsonObject();
                        enchObject.addProperty("name", ench.getName());
                        enchObject.addProperty("level", item.getItemMeta().getEnchants().get(ench));
                        enchArray.add(enchObject);
                    }
                    itemObject.add("enchants", enchArray);
                }
                if (item.getType() == Material.LEATHER_BOOTS || item.getType() == Material.LEATHER_CHESTPLATE || item.getType() == Material.LEATHER_HELMET || item.getType() == Material.LEATHER_LEGGINGS) {
                    LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
                    Color color = meta.getColor();
                    JsonObject colorRoot = new JsonObject();
                    colorRoot.addProperty("red", color.getRed());
                    colorRoot.addProperty("green", color.getGreen());
                    colorRoot.addProperty("blue", color.getBlue());
                    itemObject.add("color", colorRoot);
                }
                if (item.getType() == Material.SKULL || item.getType() == Material.SKULL_ITEM) {
                    SkullMeta meta = (SkullMeta) item.getItemMeta();
                    String owner = meta.getOwner();
                    itemObject.addProperty("skullOwner", owner);
                }
            }
            root = itemObject;
        }
        return root;
    }

    @SuppressWarnings("deprecation")
    public ItemStack decode(JsonElement item) {
        JsonObject itemObject = item.getAsJsonObject();
        Material type = Material.valueOf(itemObject.get("type").getAsString());
        int amount = itemObject.get("amount").getAsInt();
        short durability = itemObject.get("durability").getAsShort();
        byte data = itemObject.get("data").getAsByte();
        ItemStack currentItem = new ItemStack(type, amount, durability, data);
        ItemMeta meta = currentItem.getItemMeta();
        if (itemObject.has("name")) {
            meta.setDisplayName(itemObject.get("name").getAsString());
        }
        if (itemObject.has("lore")) {
            JsonArray loreArray = itemObject.get("lore").getAsJsonArray();
            List<String> loreLines = new ArrayList<>();
            for (int k = 0; k < loreArray.size(); k++) {
                String line = loreArray.get(k).getAsJsonObject().get("line").getAsString();
                loreLines.add(line);
            }
            meta.setLore(loreLines);
        }
        if (itemObject.has("enchants")) {
            JsonArray enchArray = itemObject.get("enchants").getAsJsonArray();
            for (int k = 0; k < enchArray.size(); k++) {
                JsonObject enchObject = enchArray.get(k).getAsJsonObject();
                String name = enchObject.get("name").getAsString();
                int lvl = enchObject.get("level").getAsInt();
                meta.addEnchant(Enchantment.getByName(name), lvl, false);
            }
        }
        currentItem.setItemMeta(meta);
        if (itemObject.has("color")) {
            if (type == Material.LEATHER_BOOTS || type == Material.LEATHER_CHESTPLATE || type == Material.LEATHER_HELMET || type == Material.LEATHER_LEGGINGS) {
                LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) meta;
                JsonObject color = itemObject.get("color").getAsJsonObject();
                int r = 0, g = 0, b = 0;
                if (color.has("red"))
                    r = color.get("red").getAsInt();
                if (color.has("green"))
                    g = color.get("green").getAsInt();
                if (color.has("blue"))
                    b = color.get("blue").getAsInt();
                leatherArmorMeta.setColor(Color.fromRGB(r, g, b));
                currentItem.setItemMeta(leatherArmorMeta);
            }
        }
        if (itemObject.has("skullOwner")) {
            if (type == Material.SKULL || type == Material.SKULL_ITEM) {
                SkullMeta skullMeta = (SkullMeta) meta;
                String owner = itemObject.get("skullOwner").getAsString();
                skullMeta.setOwner(owner);
                currentItem.setItemMeta(skullMeta);
            }
        }
        return currentItem;
    }

    @Override
    public ItemStack deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return decode(jsonElement);
    }

    @Override
    public JsonElement serialize(ItemStack itemStack, Type type, JsonSerializationContext jsonSerializationContext) {
        return encode(itemStack);
    }
}
