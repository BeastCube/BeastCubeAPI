package net.beastcube.api.bukkit.players.cosmetics.effects.effects;

import de.slikey.effectlib.effect.BleedEffect;
import lombok.Getter;
import net.beastcube.api.bukkit.players.cosmetics.effects.Effect;
import net.beastcube.api.bukkit.players.cosmetics.effects.EffectManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * @author BeastCube
 */
public class Bleed extends Effect {
    @Getter
    private String id = "bleed";

    @Override
    public String getDisplayName() {
        return EffectManager.DISPLAY_NAME_COLOR + "Bleed";
    }

    @Override
    public Material getMaterial() {
        return Material.ROTTEN_FLESH;
    }

    @Override
    public de.slikey.effectlib.Effect getEffect(Player p, de.slikey.effectlib.EffectManager e) {
        return new BleedEffect(e);
    }
}
