package net.beastcube.wrapper.gui;

import net.beastcube.wrapper.Wrapper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * @author BeastCube
 */
public class GUI {

    public static void openWindow() {
        //Create and set up the window.
        JFrame frame = new JFrame("BeastCube Server Wrapper");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JTextArea console = new JTextArea();
        console.setEditable(false);
        JScrollPane consolePane = new JScrollPane(console);
        consolePane.setPreferredSize(new Dimension(600, 600));
        frame.getContentPane().add(consolePane, BorderLayout.NORTH);

        MessageConsole mc = new MessageConsole(console);
        mc.redirectOut();
        mc.redirectErr(Color.RED, null);

        System.out.println("out");
        System.err.println("err");
        Wrapper.getInstance().getLogger().info("logger");

        JTextField tf = new JTextField();
        TextFieldStreamer ts = new TextFieldStreamer(tf);
        //maybe this next line should be done in the TextFieldStreamer ctor
        //but that would cause a "leak a this from the ctor" warning
        tf.addActionListener(ts);
        frame.getContentPane().add(tf, BorderLayout.SOUTH);
        frame.addWindowListener(new WindowAdapter() {
            public void windowOpened(WindowEvent e) {
                tf.requestFocus();
            }
        });
        System.setIn(ts);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

}
