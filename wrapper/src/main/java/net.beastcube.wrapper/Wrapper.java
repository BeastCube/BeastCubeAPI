package net.beastcube.wrapper;

import jline.console.ConsoleReader;
import lombok.Getter;
import lombok.Setter;
import net.beastcube.api.network.WrapperServer;
import net.beastcube.wrapper.log.WrapperLogger;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class Wrapper {

    @Getter
    private static Wrapper instance;
    @Getter
    private static ConsoleReader consoleReader;
    @Getter
    @Setter
    private boolean connectedToMaster = false;

    protected WrapperServer wrapperServer;

    static String masterIp = "localhost";
    static int masterPort = 29631;
    static String authKey = "hIFgRmfHZbncbkFgT36H3m4ENlcyoSTwqoC8BHFqJTsL3XRNQNkK0feDqh2FZM1g0uer2KHBu0coOU1vxc5oh9SyhK36mVddfiv8S3zcTCrxmiSKkOYsOHRViLRvwVyC";
    @Getter
    static UUID uniqueId;
    @Getter
    Logger logger = new WrapperLogger();
    @Getter
    private Server server;

    @Getter
    private boolean autoRestart = true;

    public static void main(final String[] args) throws IOException {
        consoleReader = new ConsoleReader();
        consoleReader.setExpandEvents(false);
        new Wrapper().init();
        String line;
        while ((line = getConsoleReader().readLine(">")) != null && line.length() > 0) {
            onCommand(line);
        }

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                //TODO stop();
            }
        });

    }

    private static void onCommand(String line) {
        System.out.println(line);
        if (line.startsWith("/")) {
            if (Wrapper.getInstance().getServer() != null) {
                Wrapper.getInstance().getServer().writeToServer(line.substring(1, line.length()));
            } else {
                System.out.println("Server not running. Can't send the command to the slave server");
            }
        } else if (line.startsWith("!")) {
            String s = line.substring(1, line.length());
            String[] command = s.trim().split(" ");
            if (command.length > 0) {
                Wrapper.getInstance().parseCommand(command[0], command.length > 1 ? Arrays.asList(command).subList(1, command.length).toArray(new String[command.length - 1]) : new String[0]);
            }
        } else {
            System.out.println("If you want to execute a command on the slave server use /, else use ! to execute it on the wrapper");
        }
    }

    private void parseCommand(String command, String[] args) {
        if (command.equalsIgnoreCase("stop")) {
            if (server != null && server.isRunning()) {
                this.autoRestart = false;
                server.writeToServer("stop");
            } else {
                System.out.println("Server is not running!");
            }
        }
        if (command.equalsIgnoreCase("start")) {
            if (!server.isRunning()) {
                this.autoRestart = true;
                start();
            } else {
                System.out.println("Server is already running!");
            }
        }
    }

    public void init() throws IOException {
        instance = this;
        if (true) { //TODO check if nogui is not present
            //new GUI().openWindow();
        }

        System.out.println("BeastCubeWrapper 1.0");

        //read config

        Scanner scan = new Scanner(System.in);

        //provide masterIp if not exists in config
        if (masterIp.equals("")) {
            System.out.println("Gib bitte die IP des Master Servers ein:");
            masterIp = scan.next();
            System.out.println("Die IP lautet: " + masterIp);
        }
        //provide masterPort if not exists in config
        if (masterPort == 0) {
            System.out.println("Gib bitte den Port des Master Servers ein:");
            masterPort = scan.nextInt();
            System.out.println("Der Port lautet: " + masterIp);
        }
        //provide authKey if not exists in config
        if (authKey.equals("")) {
            System.out.println("Gib bitte den authKey des Master Servers ein:");
            authKey = scan.next();
        }
        if (uniqueId == null) {
            uniqueId = UUID.randomUUID();
        }

        //provide serverName?!? or do this dynamically on server start

        //connect to master
        this.wrapperServer = new WrapperServer(uniqueId, logger, masterPort, masterIp, authKey) {

            @Override
            public void connectFailed(Throwable e, Runnable r) {
                connectedToMaster = false;
                log.severe("Failed to connect to master! Retrying in 5 seconds" + (e != null ? ": " + e.getMessage() : ""));
                final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
                scheduler.schedule(r, 5, TimeUnit.SECONDS);
            }

        };

        this.wrapperServer.getMessenger().registerListener(new IncomingMessageHandler());

        this.wrapperServer.start();
    }

    public void start() {
        while (autoRestart) {
            if (connectedToMaster) {
                if (this.server == null || !this.server.isRunning()) {
                    this.server = new Server();
                    server.start();
                }
            } else {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
