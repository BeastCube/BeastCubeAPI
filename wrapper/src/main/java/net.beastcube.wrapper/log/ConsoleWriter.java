package net.beastcube.wrapper.log;

import jline.console.ConsoleReader;
import org.fusesource.jansi.Ansi;

import java.io.IOException;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class ConsoleWriter extends Handler {

    private final ConsoleReader console;

    public ConsoleWriter(ConsoleReader console) {
        this.console = console;
    }

    public void print(String s) {
        try {
            console.print(ConsoleReader.RESET_LINE + s + Ansi.ansi().reset().toString());
            console.drawLine();
            console.flush();
        } catch (IOException ex) {
        }
    }

    @Override
    public void publish(LogRecord record) {
        if (isLoggable(record)) {
            print(getFormatter().format(record));
        }
    }

    @Override
    public void flush() {
    }

    @Override
    public void close() throws SecurityException {
    }

}
