package net.beastcube.wrapper.log;

import net.beastcube.wrapper.Wrapper;

import java.io.File;
import java.io.IOException;
import java.util.logging.*;

public class WrapperLogger extends Logger {

    private final Formatter formatter = new ShortConsoleLogFormatter();
    private final LogDispatcher dispatcher = new LogDispatcher(this);

    public WrapperLogger() {
        super("BeastCubeWrapper", null);
        setLevel(Level.ALL);

        try {
            FileHandler fileHandler = new FileHandler("logs" + File.separator + "wrapper.log", 1 << 24, 8, true);
            fileHandler.setFormatter(formatter);
            addHandler(fileHandler);

            ConsoleWriter consoleHandler = new ConsoleWriter(Wrapper.getConsoleReader());
            consoleHandler.setLevel(Level.INFO);
            consoleHandler.setFormatter(formatter);
            addHandler(consoleHandler);
        } catch (IOException ex) {
            System.err.println("Could not register logger!");
            ex.printStackTrace();
        }
        dispatcher.start();
    }

    @Override
    public void log(LogRecord record) {
        dispatcher.queue(record);
    }

    void doLog(LogRecord record) {
        super.log(record);
    }
}
