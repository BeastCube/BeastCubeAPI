package net.beastcube.wrapper;

import net.beastcube.api.network.MessageHandler;
import net.beastcube.api.network.ServerInfo;
import net.beastcube.api.network.message.WrapperConnectedMessage;

/**
 * @author BeastCube
 */
public class IncomingMessageHandler {

    @MessageHandler
    public void onWrapperConnectedMessage(ServerInfo server, WrapperConnectedMessage message) {
        Wrapper.getInstance().setConnectedToMaster(true);
        Wrapper.getInstance().getLogger().info("Connected to master server");
        Server s = Wrapper.getInstance().getServer();
        if (s == null || !Wrapper.getInstance().getServer().isRunning()) {
            Wrapper.getInstance().start();
        }
    }

}
