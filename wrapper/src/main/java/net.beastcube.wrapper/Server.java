package net.beastcube.wrapper;

import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @author BeastCube
 */
public class Server {

    String file = "spigot.jar";
    String minRam = "512M";
    String maxRam = "1G";
    private Process process;

    public boolean start() {

        //create eula.txt
        File eula = new File("eula.txt");
        if (eula.exists()) {
            eula.delete();
        }
        writeToFile(eula, "eula=true");

        //Download spigot
        //Download server.properties
        //Download spigot.yml
        //Download bukkit.yml

        //download plugins
        //download configs

        //Download all other files

        //get free port
        //start first time with free port
        //send servername to master requested by the master Server
        //redirect output
        //redirect input, but also allow commands to be send to the wrapper
        //show ram and such things?


        int port;
        try {
            port = AvailablePortFinder.createPrivate().getNextAvailable();
        } catch (NoSuchElementException e) {
            Wrapper.getInstance().getLogger().severe("Failed to find an open port");
            return false;
        }

        try {
            ProcessBuilder processBuilder = new ProcessBuilder("java", "-Xms" + minRam, "-Xmx" + maxRam, "-DuniqueId=" + Wrapper.getUniqueId(),
                    "-jar", file, "-p", String.valueOf(port), "-W", "worlds")
                    .redirectError(ProcessBuilder.Redirect.INHERIT)
                    .redirectOutput(ProcessBuilder.Redirect.INHERIT);

            Wrapper.getInstance().getLogger().info(join(" ", processBuilder.command()));
            process = processBuilder.start();

            while (process.isAlive()) {
                Thread.sleep(50);
            }
            for (int i = 0; i < 5; i++) {
                System.out.println("");
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }


    //-------------------------------------\\
    //---------------Helpers---------------\\
    //-------------------------------------\\

    private String join(String separator, List<String> list) {
        String result = "";
        Iterator i = list.iterator();
        while (i.hasNext()) {
            result += i.next();
            if (i.hasNext())
                result += separator;
        }
        return result;
    }

    private boolean writeToFile(File file, String content) {
        try (PrintStream out = new PrintStream(new FileOutputStream(file))) {
            out.print(content);
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isRunning() {
        return process != null && process.isAlive();
    }

    public boolean writeToServer(String s) {
        if (isRunning()) {
            OutputStream stream = process.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stream));
            try {
                writer.write(s);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

}
