package net.beastcube.api.network;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import net.beastcube.api.network.protocol.NettyMessage;
import net.beastcube.api.network.protocol.NettyMessageComparator;
import net.beastcube.api.network.protocol.NettyMessageDecoder;
import net.beastcube.api.network.protocol.NettyMessageEncoder;

import javax.net.ssl.SSLException;
import java.net.ConnectException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public abstract class NettyClientCommunicator<T extends ClientServerInfo> extends MessageCommunicator {
    private Bootstrap bootstrap;
    private Channel channelToMaster;
    private final ServerInfo master;
    private final Logger log;

    // Queue.
    protected BlockingQueue<NettyMessage> payloads = new PriorityBlockingQueue<>(100, new NettyMessageComparator());

    private int port;
    private String host;
    protected T server;
    private NettyMessage registerMessage;

    public NettyClientCommunicator(final PayloadHandler handler, final int port, final String host, final T server, Logger logger) {
        super(handler);
        this.port = port;
        this.host = host;
        this.log = logger;
        this.server = server;
        this.master = server.getMasterServerInfo();
    }

    @Override
    public void start() {
        try {
            this.init(port, host);
        } catch (ConnectException e) {
            connectFailed(e, this::start);
        } catch (SSLException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void connectFailed(Throwable e, Runnable r) {

    }

    @SuppressWarnings("deprecation")
    private void init(final int port, final String host) throws InterruptedException, SSLException, ConnectException {
        // Configure SSL.
        final SslContext sslCtx = SslContext.newClientContext(InsecureTrustManagerFactory.INSTANCE);

        EventLoopGroup group = new NioEventLoopGroup();
        this.bootstrap = new Bootstrap();

        this.log.info(String.format("Connecting to master server (%s:%s)...", host, port));
        // Start the connection attempt. All in one.
        this.channelToMaster = this.bootstrap.group(group)
                .channel(NioSocketChannel.class)
                .handler(new NettyClientComunicatorInitializer(sslCtx, port, host))
                .connect(host, port)
                .sync()
                .channel();

        // Log in.
        this.log.info("Sending register message...");
        this.registerMessage = getRegisterMessage();

        group.scheduleAtFixedRate(NettyClientCommunicator.this::sendQueue, 0L, 10L, TimeUnit.MILLISECONDS);
    }

    protected abstract NettyMessage getRegisterMessage();

    protected void sendQueue() {
        if (this.registerMessage != null) {
            this.channelToMaster.writeAndFlush(this.registerMessage);
            this.registerMessage = null;
        }
        while (!this.payloads.isEmpty()) {
            //this.log.debug("<NettyMessage#" + this.payloads.peek().hashCode() + " - " + this.payloads.peek().payload.length);
            this.channelToMaster.writeAndFlush(this.payloads.poll());
        }
    }

    @Override
    public void send(final ServerInfo target, final byte[] payload, final int priority) {
        if (target != this.master) {
            throw new UnsupportedOperationException("Sorry, can only send payloads to master.");
        } else {
            this.payloads.offer(new NettyMessage(payload, priority));
        }
    }

    @Override
    public void send(final ServerInfo target, final byte[] payload) {
        this.send(target, payload, 0);
    }

    @Override
    public void stop() {
        this.bootstrap.group().shutdownGracefully();
        this.channelToMaster.close();
    }

    class NettyClientComunicatorInitializer extends ChannelInitializer<SocketChannel> {
        private final SslContext sslCtx;
        private final String host;
        private final int port;

        public NettyClientComunicatorInitializer(final SslContext sslCtx, final int port, final String host) {
            this.sslCtx = sslCtx;
            this.host = host;
            this.port = port;
        }

        @Override
        public void initChannel(final SocketChannel ch) throws Exception {
            ChannelPipeline pipeline = ch.pipeline();
            pipeline.addLast(this.sslCtx.newHandler(ch.alloc(), this.host, this.port));

            // On top of the SSL handler, add the text line codec.
            pipeline.addLast(new IntegerHeaderFrameDecoder());
            pipeline.addLast(new IntegerHeaderFrameEncoder());
            pipeline.addLast(new NettyMessageDecoder());
            pipeline.addLast(new NettyMessageEncoder());

            // and then business logic.
            pipeline.addLast(new NettyClientCommunicatorHandler());
        }

    }

    class NettyClientCommunicatorHandler extends SimpleChannelInboundHandler<NettyMessage> {
        @Override
        public void channelRead0(final ChannelHandlerContext ctx, final NettyMessage msg) throws Exception {
            //NettyClientCommunicator.this.log.debug(">NettyMessage#" + msg.hashCode() + " - " + msg.payload.length + "[" + new String(msg.payload) + "]");
            NettyClientCommunicator.this.onReceive(NettyClientCommunicator.this.master, msg.payload);
        }

        @Override
        public void exceptionCaught(final ChannelHandlerContext ctx, final Throwable cause) {
            //if (cause instanceof IOException) {
            //NettyClientCommunicator.this.log.info("exceptionCaught");
            //NettyClientCommunicator.this.connectFailed(cause, NettyClientCommunicator.this::start);
            //} else {
            cause.printStackTrace();
            //}
            ctx.close();
        }

        @Override
        public void channelInactive(ChannelHandlerContext ctx) throws Exception {
            NettyClientCommunicator.this.connectFailed(null, NettyClientCommunicator.this::start);
        }
    }

}
