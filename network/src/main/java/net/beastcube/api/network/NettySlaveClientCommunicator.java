package net.beastcube.api.network;

import net.beastcube.api.network.protocol.NettyMessage;
import net.beastcube.api.network.protocol.NettySlaveRegisterMessage;
import net.beastcube.api.network.protocol.Protocol;

import java.util.logging.Logger;

public abstract class NettySlaveClientCommunicator extends NettyClientCommunicator<SlaveServer> {
    private String authKey;

    public NettySlaveClientCommunicator(final PayloadHandler handler, final int port, final String host, final String authKey, final SlaveServer server, Logger logger) {
        super(handler, port, host, server, logger);
        this.authKey = authKey;
    }

    public NettyMessage getRegisterMessage() {
        return new NettyMessage(Protocol.writeOutgoingPacket(new NettySlaveRegisterMessage(authKey, server.getName(), server.getUniqueId(), server.getIp(), server.getPort(), server.getMotd())));
    }

}
