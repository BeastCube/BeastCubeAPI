package net.beastcube.api.network;

import lombok.Getter;
import net.beastcube.api.network.annotations.JsonType;
import net.beastcube.api.network.protocol.Protocol;

import java.net.InetSocketAddress;
import java.util.UUID;
import java.util.logging.Logger;

@JsonType
public class SlaveServer extends ClientServerInfo {

    @Getter
    private transient NetworkMessenger messenger;
    private transient MessageCommunicator communicator;
    @Getter
    private transient ServerInfo masterServerInfo;
    @Getter
    private transient Logger log;
    @Getter
    private final String ip;
    @Getter
    private final int port;
    @Getter
    private final String motd;

    public SlaveServer(final String name, final UUID uniqueId, final Logger log, final int port, final String masterIp, final String authKey, final InetSocketAddress address, final String motd) {
        super(name, uniqueId);

        this.log = log;
        this.ip = address.getHostString();
        this.port = address.getPort();
        this.motd = motd;

        this.side = ServerSide.LOCAL;
        this.log.info("Initializing NetworkMessenger...");
        this.messenger = new NetworkMessenger(this.log);

        this.masterServerInfo = new ServerInfo("master") {
            @Override
            public void sendMessage(Message message) {
                communicator.send(masterServerInfo, Protocol.writeOutgoingPacket(message));
            }
        };
        this.log.info("Initializing NettyClientCommunicator...");
        this.communicator = new NettySlaveClientCommunicator(this.messenger, port, masterIp, authKey, this, SlaveServer.this.log) {

            @Override
            public void connectFailed(Throwable e, Runnable r) {
                SlaveServer.this.connectFailed(e, r);
            }

        };

        ServerInfo.setLocalServer(this);
        this.log.info("Initialized!");
    }

    public void connectFailed(Throwable e, Runnable r) {

    }

    /**
     * Hidden constructor. Used only from {@link NettyServerCommunicator}.
     * @param name    name
     * @param uniqueId uniqueId
     * @param address address
     * @param motd    motd
     */
    protected SlaveServer(final String name, final UUID uniqueId, InetSocketAddress address, final String motd) {
        super(name, uniqueId);
        this.ip = address.getHostString();
        this.port = address.getPort();
        this.motd = motd;
        // Does not register this as local server.
        this.side = ServerSide.REMOTE;
    }

    @Override
    public void sendMessage(final Message message) {
        if (this.side == ServerSide.REMOTE) {
            // Sending from master
            ((MasterServer) ServerInfo.localServer()).send(message, this);
        } else {
            throw new RuntimeException("Can't send message to local server");
        }
    }

    public void shutdown() {
        this.log.info("Shutting down...");
        this.communicator.stop();
    }

    @Override
    public void start() {
        this.communicator.start();
    }
}
