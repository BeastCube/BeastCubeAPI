package net.beastcube.api.network;

import java.io.Serializable;

/**
 * Class that represents message over network.
 */
public abstract class Message implements Serializable {

    private static final long serialVersionUID = 8235895603790260813L;

}
