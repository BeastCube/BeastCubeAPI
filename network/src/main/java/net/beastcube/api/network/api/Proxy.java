package net.beastcube.api.network.api;

import net.beastcube.api.network.ProxyBrand;
import net.beastcube.api.network.SlaveServer;

/**
 * @author BeastCube
 */
public interface Proxy {

    ProxyBrand getBrand();

    void addServer(SlaveServer server);

    void removeServer(SlaveServer server);

}
