package net.beastcube.api.network.protocol;

import lombok.Getter;
import net.beastcube.api.network.Message;

public abstract class NettyRegisterMessage extends Message {

    @Getter
    protected String authKey;

    protected NettyRegisterMessage() {
    }

    protected NettyRegisterMessage(String authKey) {
        if (authKey.length() != 128) {
            throw new IllegalArgumentException("authkey");
        }
        this.authKey = authKey;
    }

}
