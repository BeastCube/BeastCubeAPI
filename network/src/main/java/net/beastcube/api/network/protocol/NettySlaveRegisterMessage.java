package net.beastcube.api.network.protocol;

import lombok.Getter;

import java.util.UUID;

public class NettySlaveRegisterMessage extends NettyRegisterMessage {

    @Getter
    private String name;
    @Getter
    private String ip;
    @Getter
    private int port;
    @Getter
    private String motd;
    @Getter
    private UUID uniqueId;

    protected NettySlaveRegisterMessage() {
        super();
    }

    public NettySlaveRegisterMessage(final String authKey, final String name, final UUID uniqueId, final String ip, final int port, final String motd) {
        super(authKey);
        this.name = name;
        this.ip = ip;
        this.port = port;
        this.motd = motd;
        this.uniqueId = uniqueId;
    }
}
