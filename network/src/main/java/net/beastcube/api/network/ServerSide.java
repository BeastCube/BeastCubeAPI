package net.beastcube.api.network;

/**
 * Specifies if server is local or remote.
 */
public enum ServerSide {
    LOCAL,
    REMOTE
}
