package net.beastcube.api.network;

import net.beastcube.api.network.protocol.Protocol;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.logging.Logger;

public class NetworkMessenger implements PayloadHandler {

    private final Logger log;

    private final Map<Class<? extends Message>, List<Object>> listeners = new HashMap<>();

    public NetworkMessenger(Logger logger) {
        log = logger;
    }

    @Override
    public void handleMessage(final ServerInfo sender, final byte[] payload) {
        incomingPayload(sender, payload);
    }

    @SuppressWarnings("unchecked")
    public void registerListener(Object listener) {
        for (Method method : listener.getClass().getMethods()) {
            if (method.isAnnotationPresent(MessageHandler.class)) { // todo check parameters too
                Class<?>[] parameterTypes = method.getParameterTypes();
                Optional<Class<? extends Message>> packetClazz = Optional.empty();

                for (Class<?> parameterType : parameterTypes) { // find packet class
                    if (Message.class.isAssignableFrom(parameterType)) {
                        packetClazz = Optional.of((Class<? extends Message>) parameterType);
                        break;
                    }
                }

                if (packetClazz.isPresent()) {
                    List<Object> list = listeners.get(packetClazz.get());
                    if (list == null) {
                        list = new ArrayList<>();
                        listeners.put(packetClazz.get(), list);
                    }

                    list.add(listener);
                }
            }
        }
    }

    public void unregisterListener(Object listener) {
        for (List<Object> list : listeners.values()) {
            list.remove(listener);
        }
    }

    public void incomingPayload(ServerInfo connection, byte[] data) {
        Message message = Protocol.readIncomingPacket(data, log);

        if (message != null) {
            receivePacket(connection, message);
        }
    }

    public void receivePacket(ServerInfo connection, Message message) {
        Class<? extends Message> messageClass = message.getClass();

        if (listeners.containsKey(messageClass)) {
            for (Object listener : listeners.get(messageClass)) {
                methodLoop:
                for (Method method : listener.getClass().getMethods()) {
                    if (method.isAnnotationPresent(MessageHandler.class)) {
                        Class<?>[] parameterTypes = method.getParameterTypes();
                        Object[] parameters = new Object[parameterTypes.length];

                        for (int i = 0; i < parameters.length; i++) {
                            Class<?> parameterType = parameterTypes[i];
                            Object parameter = handleListenerParameter(parameterType, message, connection);

                            if (parameter != null) {
                                parameters[i] = parameter;
                            } else {
                                continue methodLoop;
                            }
                        }

                        try {
                            method.invoke(listener, parameters);
                        } catch (IllegalAccessException e) {
                            log.severe("Error occurred whilst dispatching packet to listeners." + e);
                        } catch (InvocationTargetException e) {
                            Throwable throwable = e.getCause();
                            if (throwable == null) {
                                throwable = e;
                            }

                            sneakyThrow(throwable);
                        }
                    }
                }
            }
        }
    }

    public static void sneakyThrow(Throwable ex) {
        sneakyThrowInner(ex);
    }

    @SuppressWarnings("unchecked")
    private static <T extends Throwable> void sneakyThrowInner(Throwable ex) throws T {
        throw (T) ex;
    }

    protected Object handleListenerParameter(Class<?> clazz, Message message, ServerInfo connection) {
        if (clazz.isAssignableFrom(message.getClass())) {
            return message;
        }

        Class<?> connectionClass = connection.getClass();
        if (clazz.isAssignableFrom(connectionClass)) {
            return connectionClass.cast(connection);
        }

        return null;
    }

}
