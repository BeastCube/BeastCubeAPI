package net.beastcube.api.network.protocol;

/**
 * Class that holds message paylod.
 */
public class NettyMessage {

    public byte[] payload;
    public int priority = 0;

    public NettyMessage(final byte[] array, final int priority) {
        this.payload = array;
        this.priority = priority;
    }

    public NettyMessage(final byte[] array) {
        this.payload = array;
    }
}
