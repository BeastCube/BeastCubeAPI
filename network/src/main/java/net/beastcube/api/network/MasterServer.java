package net.beastcube.api.network;

import net.beastcube.api.network.api.Proxy;
import net.beastcube.api.network.protocol.Protocol;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * Main class of master server.
 */
public class MasterServer extends ServerInfo {
    private final Platform platform;
    private final Logger log;
    private final NetworkMessenger messenger;
    private final MessageCommunicator communicator;
    private transient Proxy proxy;

    private final Map<String, SlaveServer> slaves = new HashMap<>();
    private final Map<String, WrapperServer> wrappers = new HashMap<>();

    public MasterServer(final String name, Proxy proxy, final int port, final String authKey, final Logger logger) {
        super(name);
        this.log = logger;
        this.proxy = proxy;
        this.log.info("Starting MasterServer...");

        // Currently we support only proxy and only bungee.
        this.platform = Platform.MINECRAFT_PROXY;

        // Create message decoder.
        this.messenger = new NetworkMessenger(log);

        // Start netty communicator.
        this.communicator = new NettyServerCommunicator(this.messenger, port, authKey, this, log);

        // Update instance in ServerInfo
        ServerInfo.setLocalServer(this);
        this.side = ServerSide.LOCAL;
        this.log.info("Network part loaded successfully!");
    }

    // Adds a slave server.
    protected void addSlave(final SlaveServer server) {
        this.slaves.put(server.getName(), server);
        proxy.addServer(server);
    }

    public void removeSlave(final SlaveServer server) {
        // Close connection.
        ((NettyServerCommunicator) this.communicator).closeConnection(server);
        // Remove.
        this.slaves.remove(server.getName());
        proxy.removeServer(server);
    }

    // Adds a wrapper server.
    protected void addWrapper(final WrapperServer server) {
        this.wrappers.put(server.getName(), server);
    }

    public void removeWrapper(final WrapperServer server) {
        // Close connection.
        ((NettyServerCommunicator) this.communicator).closeConnection(server);
        // Remove.
        this.wrappers.remove(server.getName());
    }

    /**
     * Shuts down all workers and closes all connections.
     */
    public void shutdown() {
        this.log.info("Shutting down...");
        this.communicator.stop();
        this.log.info("Saving config...");
    }

    /**
     * Returns {@link Logger}.
     *
     * @return the logger
     */
    protected Logger getLogger() {
        return this.log;
    }

    /**
     * Sends Message to specified server.
     *
     * @param message message to be send
     * @param target  target server
     */
    public void send(final Message message, final ServerInfo target) {
        this.communicator.send(target, Protocol.writeOutgoingPacket(message));
    }

    /**
     * Returns collection of all connected slave servers.
     *
     * @return collection of connected slave servers
     */
    public Collection<SlaveServer> getSlaveServers() {
        return this.slaves.values();
    }

    /**
     * Returns messanger object.
     *
     * @return current messanger
     */
    public NetworkMessenger getMessenger() {
        return this.messenger;
    }

    /**
     * Returns platform the master is running on.
     *
     * @return platform of master server
     */
    public Platform getPlatform() {
        return this.platform;
    }

    /**
     * Returns {@link ServerInfo} by name or null if not found.
     *
     * @param name name of the server
     * @return slave server or null
     */
    public Optional<ServerInfo> getSlave(final String name) {
        return Optional.ofNullable(this.slaves.get(name));
    }

    public boolean hasSlave(final String serverName) {
        return this.slaves.containsKey(serverName);
    }

    public Optional<SlaveServer> getSlave(final WrapperServer wrapperServer) {
        return slaves.values().stream().filter(p -> p.getUniqueId() == wrapperServer.getUniqueId()).findFirst();
    }

    /**
     * Returns {@link ServerInfo} by name or null if not found.
     *
     * @param name name of the server
     * @return wrapper server or null
     */
    public Optional<ServerInfo> getWrapper(final String name) {
        return Optional.ofNullable(this.wrappers.get(name));
    }

    public boolean hasWrapper(final String serverName) {
        return this.wrappers.containsKey(serverName);
    }

    public Optional<WrapperServer> getWrapper(final SlaveServer slaveServer) {
        return wrappers.values().stream().filter(p -> p.getUniqueId() == slaveServer.getUniqueId()).findFirst();
    }

    @Override
    public void start() {
        this.communicator.start();
    }
}
