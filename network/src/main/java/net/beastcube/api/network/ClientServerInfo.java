package net.beastcube.api.network;

import lombok.Getter;

import java.util.UUID;
import java.util.logging.Logger;

/**
 * @author BeastCube
 */
public abstract class ClientServerInfo extends ServerInfo {

    @Getter
    public transient NetworkMessenger messenger;
    protected transient MessageCommunicator communicator;
    @Getter
    protected transient Logger log;

    public ClientServerInfo(String name, UUID uniqueId) {
        super(name);
        this.uniqueId = uniqueId;
    }

    public abstract ServerInfo getMasterServerInfo();

    @Getter
    //The wrapper and the slave use the same uniqueId
    protected final UUID uniqueId;

    @Override
    public void sendMessage(final Message message) {
        if (this.side == ServerSide.REMOTE) {
            // Sending from master
            ((MasterServer) ServerInfo.localServer()).send(message, this);
        } else {
            throw new RuntimeException("Can't send message to local server");
        }
    }

    public void shutdown() {
        this.log.info("Shutting down...");
        this.communicator.stop();
    }

    @Override
    public void start() {
        this.communicator.start();
    }

}
