package net.beastcube.api.network;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;

import java.util.List;

/**
 * IntegerHeaderFrameDecoder.
 */
public class IntegerHeaderFrameDecoder extends ReplayingDecoder<Void> {
    @Override
    protected void decode(final ChannelHandlerContext ctx, final ByteBuf buf, final List<Object> out) throws Exception {
        out.add(buf.readBytes(buf.readInt()));
    }
}
