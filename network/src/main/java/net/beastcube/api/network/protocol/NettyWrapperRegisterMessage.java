package net.beastcube.api.network.protocol;

import lombok.Getter;

import java.util.UUID;

public class NettyWrapperRegisterMessage extends NettyRegisterMessage {

    @Getter
    UUID uniqueid;

    protected NettyWrapperRegisterMessage() {
        super();
    }

    public NettyWrapperRegisterMessage(final String authKey, final UUID uniqueid) {
        super(authKey);
        this.uniqueid = uniqueid;
    }
}
