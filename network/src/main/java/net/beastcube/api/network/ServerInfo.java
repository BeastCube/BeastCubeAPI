package net.beastcube.api.network;

import net.beastcube.api.network.annotations.JsonType;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Class that represents server on network.
 */
@JsonType
public class ServerInfo {
    private transient static ServerInfo local;

    protected final String name;
    protected ServerSide side;
    protected Map<String, Object> custom;

    public ServerInfo(final String name) {
        this.name = name;
    }

    public void sendMessage(final Message message) {
        throw new OperationNotSupportedException("ServerInfo does not support sending message!");
    }

    public boolean isLocal() {
        return this.side == ServerSide.LOCAL;
    }

    public ServerSide getSide() {
        return this.side;
    }

    public String getName() {
        return this.name;
    }

    public static ServerInfo localServer() {
        return ServerInfo.local;
    }

    public void setCustom(final String key, final String value) {
        if (this.custom == null) {
            this.custom = new HashMap<>();
        }
        this.custom.put(key, value);
    }

    public void setCustom(final String key, final Object value) {
        if (this.custom == null) {
            this.custom = new HashMap<>();
        }
        this.custom.put(key, value);
    }

    public String getCustomAsString(final String key) {
        if (this.custom == null) {
            return null;
        } else {
            return (String) this.custom.get(key);
        }
    }

    public Object getCustomAsObject(final String key) {
        if (this.custom == null) {
            return null;
        } else {
            return this.custom.get(key);
        }
    }

    public Map<String, Object> getCustom() {
        return Collections.unmodifiableMap(this.custom);
    }

    protected static void setLocalServer(final ServerInfo server) {
        if (ServerInfo.local == null) {
            ServerInfo.local = server;
        } else {
            throw new RuntimeException("Field local has been initialized before!");
        }
    }

    public void start() {

    }
}
