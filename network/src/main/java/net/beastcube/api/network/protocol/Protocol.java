package net.beastcube.api.network.protocol;

import net.beastcube.api.network.Message;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Protocol {

    public static final String PACKET_HEADER = "BeastCubeProtocol";

    public static byte[] writeOutgoingPacket(Message message) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeUTF(PACKET_HEADER);
            objectOutputStream.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream.toByteArray();
    }

    public static Message readIncomingPacket(byte[] bytes, Logger log) {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bytes));

            if (objectInputStream.readUTF().equals(Protocol.PACKET_HEADER)) {
                Object object = objectInputStream.readObject();

                if (Message.class.isAssignableFrom(object.getClass())) {
                    return (Message) object;
                } else {
                    throw new IllegalArgumentException("Class does not extend Message.");
                }
            }
        } catch (IOException e) {
            log.log(Level.SEVERE, "Exception whilst reading custom payload.", e);
        } catch (ClassNotFoundException e) {
            log.log(Level.SEVERE, "Unable to find message class whilst de-serializing.\nMake sure the Message class has the same package in all instances.", e);
        }

        return null;
    }

}
