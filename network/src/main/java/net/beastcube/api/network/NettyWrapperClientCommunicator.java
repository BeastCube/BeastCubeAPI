package net.beastcube.api.network;

import net.beastcube.api.network.protocol.NettyMessage;
import net.beastcube.api.network.protocol.NettyWrapperRegisterMessage;
import net.beastcube.api.network.protocol.Protocol;

import java.util.UUID;
import java.util.logging.Logger;

public abstract class NettyWrapperClientCommunicator extends NettyClientCommunicator<WrapperServer> {
    private final String authKey;
    private final UUID uniqueId;

    public NettyWrapperClientCommunicator(final PayloadHandler handler, final int port, final String host, final String authKey, UUID uniqueId, final WrapperServer server, Logger logger) {
        super(handler, port, host, server, logger);
        this.authKey = authKey;
        this.uniqueId = uniqueId;
    }

    public NettyMessage getRegisterMessage() {
        return new NettyMessage(Protocol.writeOutgoingPacket(new NettyWrapperRegisterMessage(authKey, uniqueId)));
    }

}
