package net.beastcube.api.network;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import io.netty.util.concurrent.GlobalEventExecutor;
import net.beastcube.api.network.message.SlaveConnectedMessage;
import net.beastcube.api.network.message.WrapperConnectedMessage;
import net.beastcube.api.network.protocol.*;

import javax.net.ssl.SSLException;
import java.net.InetSocketAddress;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NettyServerCommunicator extends MessageCommunicator {
    protected final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    protected Map<String, ChannelHandlerContext> ctxByName = new HashMap<>();
    protected Map<ChannelHandlerContext, ServerInfo> serverInfoByCTX = new HashMap<>();
    protected MasterServer server;
    protected String authKey;

    protected Map<ChannelHandlerContext, PriorityBlockingQueue<NettyMessage>> queues = new HashMap<>();

    protected Logger log;
    protected ServerBootstrap b;
    protected int port;

    static final boolean SSL = System.getProperty("ssl") != null;

    public NettyServerCommunicator(final PayloadHandler handler, final int port, final String authKey, final MasterServer server, Logger logger) {
        super(handler);
        this.log = logger;
        this.authKey = authKey;
        this.server = server;
        this.port = port;
        Logger.getLogger("io.netty").setLevel(Level.OFF);
    }

    public void closeConnection(final ServerInfo server) {
        closeConnection(server, null);
    }

    public void closeConnection(final ServerInfo server, Throwable cause) {
        this.log.info(String.format("Closing connection for slave %s%s", server.getName(), cause != null ? ": " + cause.getMessage() : ""));
        ChannelHandlerContext ctx = this.ctxByName.get(server.getName());
        if (this.queues.containsKey(ctx)) {
            if (!this.queues.get(ctx).isEmpty()) {
                this.log.warning("Throwing to trash " + this.queues.get(ctx).size() + " unsend messages!");
            }
        }
        this.queues.remove(ctx);

        this.serverInfoByCTX.remove(this.ctxByName.get(server.getName()));
        ctx.close();
        this.ctxByName.remove(server.getName());
    }

    @Override
    public void start() {
        // Use separated thread for netty, to not block main thread.
        new Thread(() -> {
            try {
                NettyServerCommunicator.this.init(NettyServerCommunicator.this.port);
            } catch (SSLException | CertificateException | InterruptedException e) {
                e.printStackTrace();
            }
        }, "Netty-ListenThread").start();
    }

    @Override
    public void stop() {
        this.b.group().shutdownGracefully();
    }

    @SuppressWarnings("deprecation")
    public void init(final int port) throws SSLException, CertificateException, InterruptedException {
        this.log.info("Initializing SSL...");
        SelfSignedCertificate ssc = new SelfSignedCertificate("beastcube.net");
        SslContext sslCtx = SslContext.newServerContext(ssc.certificate(), ssc.privateKey());

        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            this.log.info("Starting up server...");
            this.b = new ServerBootstrap();
            this.b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new NettyServerComunicatorInitializer(sslCtx));

            workerGroup.scheduleAtFixedRate(this::sendQueues, 0L, 10L, TimeUnit.MILLISECONDS);

            this.b.bind(port).sync().channel().closeFuture().sync();
        } finally {
            this.log.info("Stopping server...");
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    protected void sendQueues() {
        for (ChannelHandlerContext ctx : this.queues.keySet()) {
            while (!this.queues.get(ctx).isEmpty()) {
                ctx.writeAndFlush(this.queues.get(ctx).poll());
            }
        }
    }

    @Override
    public void send(final ServerInfo target, final byte[] payload, final int priority) {
        try {
            ChannelHandlerContext ctx = this.getCTX(target);
            if (this.queues.containsKey(ctx)) {
                this.queues.get(ctx).add(new NettyMessage(payload, priority));
            } else {
                this.queues.put(ctx, new PriorityBlockingQueue<>(100, new NettyMessageComparator()));
                PriorityBlockingQueue<NettyMessage> l = this.queues.get(ctx);
                l.add(new NettyMessage(payload, priority));
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void send(final ServerInfo target, final byte[] payload) {
        this.send(target, payload, 0);
    }

    public ServerInfo getServerInfo(final ChannelHandlerContext ctx) {
        return this.serverInfoByCTX.get(ctx);
    }

    public ChannelHandlerContext getCTX(final ServerInfo serverinfo) {
        return this.ctxByName.get(serverinfo.getName());
    }

    class NettyServerComunicatorInitializer extends ChannelInitializer<SocketChannel> {
        private final SslContext sslCtx;

        public NettyServerComunicatorInitializer(final SslContext sslCtx) {
            this.sslCtx = sslCtx;
        }

        @Override
        public void initChannel(final SocketChannel ch) throws Exception {
            ChannelPipeline pipeline = ch.pipeline();

            // Add SSL handler first to encrypt and decrypt everything.
            // In this example, we use a bogus certificate in the server side
            // and accept any invalid certificates in the client side.
            // You will need something more complicated to identify both
            // and server in the real world.
            pipeline.addLast(this.sslCtx.newHandler(ch.alloc()));

            // On top of the SSL handler, add messages decoder and encoder.
            pipeline.addLast(new IntegerHeaderFrameDecoder());
            pipeline.addLast(new IntegerHeaderFrameEncoder());
            pipeline.addLast(new NettyMessageDecoder());
            pipeline.addLast(new NettyMessageEncoder());

            // and then business logic.
            pipeline.addLast(new NettyServerCommunicatorHandler(NettyServerCommunicator.this));
        }
    }

    private static class NettyServerCommunicatorHandler extends SimpleChannelInboundHandler<NettyMessage> {
        private final NettyServerCommunicator i;

        public NettyServerCommunicatorHandler(final NettyServerCommunicator nettyServerCommunicator) {
            this.i = nettyServerCommunicator;
        }

        @Override
        public void channelActive(final ChannelHandlerContext ctx) {
            // Once session is secured, send a greeting and register the channel to the global channel
            // list so the channel received the messages from others.
            ctx.pipeline()
                    .get(SslHandler.class)
                    .handshakeFuture()
                    .addListener(future -> {
                        // Client logged in.

                        // He should send log in packet.
                        //NettyServerCommunicatorHandler.this.i.log.debug("SSL handshake ok!");
                    });
        }

        @Override
        public void channelRead0(final ChannelHandlerContext ctx, final NettyMessage msg) throws Exception {
            //this.i.log.debug(">NettyMessage#" + msg.hashCode() + " - " + msg.payload.length + "[" + new String(msg.payload) + "]");
            // Invoke onReceive if registered server.
            if (this.i.channels.contains(ctx.channel())) {
                this.i.onReceive(this.i.getServerInfo(ctx), msg.payload);
            } else {
                // Try to register.
                Message message = Protocol.readIncomingPacket(msg.payload, this.i.log);

                if (message != null && message instanceof NettyRegisterMessage) {
                    if (((NettyRegisterMessage) message).getAuthKey().equals(this.i.authKey)) {
                        if (message instanceof NettyWrapperRegisterMessage) {
                            NettyWrapperRegisterMessage registerMessage = (NettyWrapperRegisterMessage) message;
                            // Add connected server to pool.
                            NettyServerCommunicatorHandler.this.i.channels.add(ctx.channel());
                            WrapperServer server = new WrapperServer(registerMessage.getUniqueid());
                            NettyServerCommunicatorHandler.this.i.ctxByName.put(server.getName(), ctx);
                            NettyServerCommunicatorHandler.this.i.server.addWrapper(server);
                            NettyServerCommunicatorHandler.this.i.serverInfoByCTX.put(ctx, server);
                            this.i.log.info(String.format("Registered new WRAPPER server: ctx:%s", ctx.hashCode()));
                            //TODO Send response to wrapper -> success
                            this.i.send(server, Protocol.writeOutgoingPacket(new WrapperConnectedMessage()));
                        } else if (message instanceof NettySlaveRegisterMessage) {
                            //TODO send the ctx of the wrapper
                            NettySlaveRegisterMessage registerMessage = (NettySlaveRegisterMessage) message;

                            // Add connected server to pool.
                            NettyServerCommunicatorHandler.this.i.channels.add(ctx.channel());
                            String name = registerMessage.getName();
                            InetSocketAddress socketAddress = new InetSocketAddress(registerMessage.getIp(), registerMessage.getPort());
                            SlaveServer server = new SlaveServer(name, registerMessage.getUniqueId(), socketAddress, registerMessage.getMotd());
                            NettyServerCommunicatorHandler.this.i.ctxByName.put(name, ctx);
                            NettyServerCommunicatorHandler.this.i.server.addSlave(server);
                            NettyServerCommunicatorHandler.this.i.serverInfoByCTX.put(ctx, server);
                            this.i.log.info(String.format("Registered new SLAVE server: %s Ip: %s Port: %s ctx:%s",
                                    name, socketAddress.getHostString(), socketAddress.getPort(), ctx.hashCode()));
                            //TODO Send response to slave -> success
                            this.i.send(server, Protocol.writeOutgoingPacket(new SlaveConnectedMessage()));
                        } else {
                            this.i.log.warning(String.format("Unknown register message: " + message.getClass().getName(), ctx.name()));
                            failed(ctx, "Unknown register message");
                        }
                    } else {
                        // Bad login. Disconnect.
                        this.i.log.warning(String.format("Bad login authKey from %s", ctx.name()));
                        failed(ctx, "Bad login");
                    }
                } else {
                    this.i.log.warning(String.format("Expected register message but got " + (message == null ? "null" : message.getClass().getName()), ctx.name()));
                    failed(ctx, "Expected register message");
                }
            }
        }

        private void failed(final ChannelHandlerContext ctx, final String cause) {
            //TODO Send response to slave -> failure
            //ctx.writeAndFlush(new NettyMessage(Protocol.writeOutgoingPacket(new ConnectionFailedMessage(cause))));
            ctx.close();
        }

        @Override
        public void exceptionCaught(final ChannelHandlerContext ctx, final Throwable cause) {
            cause.printStackTrace();
            ctx.close();
        }

        @Override
        public void channelInactive(ChannelHandlerContext ctx) throws Exception {
            ServerInfo server = this.i.serverInfoByCTX.get(ctx);
            if (server.getClass().isAssignableFrom(SlaveServer.class)) {
                this.i.server.removeSlave((SlaveServer) server);
            } else if (server.getClass().isAssignableFrom(WrapperServer.class)) {
                //TODO Remove Wrapper server
            }
        }
    }
}
