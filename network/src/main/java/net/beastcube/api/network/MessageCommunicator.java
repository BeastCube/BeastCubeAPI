package net.beastcube.api.network;

/**
 * Interface that represents BeastCubeMasterBungeePlugin.getInstance() message comunicator.
 */
public abstract class MessageCommunicator {
    private final PayloadHandler handler;

    public MessageCommunicator(final PayloadHandler handler) {
        this.handler = handler;
    }

    /**
     * Called when payload arrives, and should be redirected to {@link PayloadHandler}.
     *
     * @param sender  server that sent this payload (raw data)
     * @param payload data
     */
    public void onReceive(final ServerInfo sender, final byte[] payload) {
        this.handler.handleMessage(sender, payload);
    }

    /**
     * Sends raw data (payload) to target server.
     *
     * @param target  target server
     * @param payload data
     */
    public abstract void send(ServerInfo target, byte[] payload);

    /**
     * Sends raw data (payload) to target server.
     *
     * @param target   target server
     * @param payload  data
     * @param priority priority
     */
    public abstract void send(ServerInfo target, byte[] payload, int priority);

    /**
     * Stops all connections.
     */
    public abstract void stop();

    /**
     * Start connection.
     */
    public abstract void start();
}
