package net.beastcube.api.network.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Annotation that specifies the class could be converted to JSON, so it should not have circular references and
 * non-json serializable values that are not transient.
 */
@Target({ElementType.TYPE})
public @interface JsonType {

}
