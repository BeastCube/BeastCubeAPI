package net.beastcube.api.network;

/**
 * Enum that specifies
 */
public enum ServerType {
    /**
     * Master server.
     */
    MASTER,
    /**
     * Slave server.
     */
    SLAVE,
    /**
     * Wrapper server.
     */
    WRAPPER
}
