package net.beastcube.api.network;

import lombok.Getter;
import net.beastcube.api.network.annotations.JsonType;
import net.beastcube.api.network.protocol.Protocol;

import java.util.UUID;
import java.util.logging.Logger;

@JsonType
public class WrapperServer extends ClientServerInfo {

    @Getter
    private transient ServerInfo masterServerInfo;


    public WrapperServer(final UUID uniqueId, final Logger log, final int port, final String masterIp, final String authKey) {
        super("WRAPPER-" + uniqueId, uniqueId);

        this.log = log;

        this.side = ServerSide.LOCAL;
        this.log.info("Initializing NetworkMessenger...");
        this.messenger = new NetworkMessenger(this.log);

        this.masterServerInfo = new ServerInfo("master") {
            @Override
            public void sendMessage(Message message) {
                communicator.send(masterServerInfo, Protocol.writeOutgoingPacket(message));
            }
        };
        this.log.info("Initializing NettyWrapperClientCommunicator...");
        this.communicator = new NettyWrapperClientCommunicator(this.messenger, port, masterIp, authKey, uniqueId, this, WrapperServer.this.log) {

            @Override
            public void connectFailed(Throwable e, Runnable r) {
                WrapperServer.this.connectFailed(e, r);
            }

        };

        ServerInfo.setLocalServer(this);
        this.log.info("Initialized!");
    }

    public void connectFailed(Throwable e, Runnable r) {

    }

    /**
     * Hidden constructor. Used only from {@link NettyServerCommunicator}.
     *
     * @param name name
     */
    protected WrapperServer(UUID uniqueId) {
        super("WRAPPER-" + uniqueId, uniqueId);
        // Does not register this as local server.
        this.side = ServerSide.REMOTE;
    }

}
