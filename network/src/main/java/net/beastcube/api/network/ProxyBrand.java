package net.beastcube.api.network;

/**
 * Brands of minecraft proxy servers.
 */
public enum ProxyBrand {
    /**
     * Bungee cord proxy server (<a
     * href="http://www.spigotmc.org/wiki/bungeecord/">http://www.spigotmc.org/wiki/bungeecord/</a>).
     */
    BUNGEECORD
}
